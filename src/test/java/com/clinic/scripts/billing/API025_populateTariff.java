package com.clinic.scripts.billing;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Billing_ParamGenerator;
import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
// depends on createBill
//import com.clinic.core.billing.API_createBill;
//Should be run last
public class API025_populateTariff extends TestBase {
	public static String tariffAmount = null;
	@Test(priority=27)
	public void populateTariff_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.populateTariff;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponseForToken = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		                                                                        //String token,String userLoginName,String userId,String organisationId,String siteId,String locationId,String billNumber,String billHeaderId
		List<NameValuePair> paramlist = Billing_ParamGenerator.Param_populateTariff(token, username, orgId, clinicId,API027_searchService.serviceId1); 
        // Post request to the URL with the param data
		HttpResponse httpResponse = requestUtil.postJSON_Request(mailUrl, paramlist);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponse);
		tariffAmount = responseJson_object.getString("tariffAmount");
		Assert.assertNotNull(responseJson_object.getString("tariffAmount"));
	}
}
