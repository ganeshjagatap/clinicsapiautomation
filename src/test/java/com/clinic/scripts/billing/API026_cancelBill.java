package com.clinic.scripts.billing;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Billing_ParamGenerator;
import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;
// depends on createBill
//import com.clinic.core.billing.API_createBill;
//Should be run last
public class API026_cancelBill extends TestBase {
	
	@Test(priority=35)
	public void cancelBill_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.cancelBill;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		                                                                        //String token,String userLoginName,String userId,String organisationId,String siteId,String locationId,String billNumber,String billHeaderId
		List<NameValuePair> paramlist = Billing_ParamGenerator.Param_cancelBill(token, username, clinicId, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, API030_createBill.billNumber, API030_createBill.billHeaderId); 
        // Post request to the URL with the param data
		HttpResponse httpResponseForCancelBill = requestUtil.postJSON_Request(mailUrl, paramlist);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForCancelBill);
		Assert.assertNotNull(responseJson_object.getString("successMsg"));
	}
}