package com.clinic.scripts.billing;


import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Billing_ParamGenerator;
import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
// depends on createBill
//import com.clinic.core.billing.API_createBill;

public class API027_searchService extends TestBase {
	public static String serviceId1 = null;
	public static String serviceName1 = null;
	public static String serviceId2 = null;
	public static String serviceName2 = null;
	@Test(priority=26)
	public void searchService_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.searchService;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
   
		List<NameValuePair> paramlist = Billing_ParamGenerator.Param_searchService(token, username, orgId, searchText ); 
        // Post request to the URL with the param data
		HttpResponse httpResponseForSearchService = requestUtil.postJSON_Request(mailUrl, paramlist);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForSearchService);
		JSONArray serviceArr = responseJson_object.getJSONArray("service");
		JSONObject serviceObj1 = serviceArr.getJSONObject(0);
		serviceId1 = serviceObj1.getString("serviceId");
		serviceName1 = serviceObj1.getString("serviceName");
		System.out.println("serviceName  "+ serviceName1);
		Assert.assertNotNull(serviceName1, "Service Name is null");
		JSONObject serviceObj2 = serviceArr.getJSONObject(1);
		serviceId2 = serviceObj2.getString("serviceId");
		serviceName2 = serviceObj2.getString("serviceName");
	}
}
