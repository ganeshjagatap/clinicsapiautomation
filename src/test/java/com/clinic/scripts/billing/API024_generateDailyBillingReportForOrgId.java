package com.clinic.scripts.billing;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Billing_ParamGenerator;
import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
// depends on createBill
//import com.clinic.core.billing.API_createBill;

public class API024_generateDailyBillingReportForOrgId extends TestBase {
	static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	static Date date = new Date();
	public String date1=null;
	@Test(priority=33)
	public void generateDailyBillingReportForOrgId_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.generateDailyBillingReportForOrgId;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		date1 =dateFormat.format(date);
		System.out.println("Date formatt  "+date1);   
		List<NameValuePair> paramlist = Billing_ParamGenerator.Param_generateDailyBillingReportForOrgId(token, username, orgId, date1 ); 
        // Post request to the URL with the param data
		HttpResponse httpResponseForGenerateDailyBillingReportForOrgId = requestUtil.postJSON_Request(mailUrl, paramlist);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForGenerateDailyBillingReportForOrgId);
		Assert.assertNotNull(responseJson_object.getString("responseMsg"));
	}
}