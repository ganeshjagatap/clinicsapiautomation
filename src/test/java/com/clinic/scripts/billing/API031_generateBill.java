package com.clinic.scripts.billing;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.*;
import com.clinic.scripts.appointment.API004_PatienSearch;

public class API031_generateBill extends TestBase {
	
	@Test(priority=31)
	public void generateBill_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.generateBill;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponseForToken = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String uhid = patientId;
		List<NameValuePair> paramlist = Billing_ParamGenerator.Param_generateBill(token, username, orgId); 
		JSONObject paramjson=Billing_ParamGenerator.JSON_generateBill(API004_PatienSearch.mobileNo,uhid,API004_PatienSearch.patientName,patientId);
		// Post request to the URL with the param data
		HttpResponse httpResponse = requestUtil.postJSONArrayy_Request(mailUrl, paramlist, paramjson);
		
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponse);
		System.out.println(responseJson_object.toString());
        //System.out.println(responseJson_object.getString("billNumber") );
		Assert.assertNotNull(responseJson_object.getString("total"));		
			}	
}