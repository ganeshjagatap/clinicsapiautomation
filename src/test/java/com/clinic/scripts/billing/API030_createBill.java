package com.clinic.scripts.billing;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.*;
import com.clinic.scripts.appointment.API004_PatienSearch;
import com.clinic.scripts.appointment.API006_HomeDoctor;
import com.clinic.scripts.appointment.API007_Appointment_HomeDoctor_List;

public class API030_createBill extends TestBase {
	public static String billHeaderId;
	public static String billNumber;	
	@Test(priority=30)
	public void createBill_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.createBill;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String uhid = patientId;
		String discountAmount = API023_calculateDiscountAndNetAmount.discount;
		List<NameValuePair> paramlist = Billing_ParamGenerator.Param_createBill(token, username, clinicId, orgId,API006_HomeDoctor.siteId,API006_HomeDoctor.locationId,API028_calculateTotal.total,API023_calculateDiscountAndNetAmount.discount,API004_PatienSearch.patientName,uhid,patientId,age,gender,discountAmount,modeOfPayment,netPayable,clinicId,API007_Appointment_HomeDoctor_List.doctorName); 
		JSONObject paramjson=Billing_ParamGenerator.JSON_createBill(API027_searchService.serviceName1, API027_searchService.serviceId1,API025_populateTariff.tariffAmount);
		// Post request to the URL with the param data
		HttpResponse httpResponseForCreateBill = requestUtil.postJSONArrayy_Request(mailUrl, paramlist, paramjson);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForCreateBill);
        billHeaderId =   responseJson_object.getString("billHeaderId");
        billNumber =   responseJson_object.getString("billNumber");         
        //System.out.println(responseJson_object.getString("billNumber") );
		Assert.assertNotNull(responseJson_object.getString("successMessage"));
		
			}	
}
