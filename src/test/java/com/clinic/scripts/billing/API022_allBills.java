package com.clinic.scripts.billing;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.clinic.core.*;

public class API022_allBills extends TestBase{

	
	@Test(priority=34)
	public void getCountryList_SuccessUsecase() throws Exception{
	//String token,String userLoginName,String userId,String organisationId,String billDate,String startRow,String interval)
		
	mailUrl = Envirnonment.env + Constants.allBills;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> all_bills_param = Billing_ParamGenerator.Param_AllBills(token, username, clinicId, orgId,"",startRow,interval);

		// Post request to the URL with the param data
		HttpResponse httpResponseForAllBills = requestUtil.postJSON_Request(mailUrl, all_bills_param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForAllBills);
		 
		 JSONArray e=responseJson_object.getJSONArray("bill");

		 JSONObject y=e.getJSONObject(0);
		 String x=y.getString("billNumber");
			Assert.assertNotNull(x, "no bills present");
		
			}
}
