package com.clinic.scripts.billing;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.*;

public class API028_calculateTotal extends TestBase {
	public static String total = null;
	@Test(priority=28)
	public void calculateTotal_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.calculateTotal;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponseForToken = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		
		List<NameValuePair> paramlist = Billing_ParamGenerator.Param_calculateTotal(token, username, clinicId , orgId); 
		JSONObject paramjson=Billing_ParamGenerator.JSON_calculateTotal(API027_searchService.serviceName1,API025_populateTariff.tariffAmount,API027_searchService.serviceName2,API025_populateTariff.tariffAmount);
		// Post request to the URL with the param data
		HttpResponse httpResponse = requestUtil.postJSONArrayy_Request(mailUrl, paramlist, paramjson);
		
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponse);
		System.out.println(responseJson_object.toString());
        //System.out.println(responseJson_object.getString("billNumber") );
		total = responseJson_object.getString("total");
		Assert.assertNotNull(responseJson_object.getString("total"));		
			}	
}