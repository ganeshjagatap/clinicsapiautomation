package com.clinic.scripts.appointment;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API009_Appointment_Reason_Master extends TestBase {
	public static String reasonId = null;
	@Test(priority=15)
	public void API009_Appointment_Reason_Master_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.appointmentReasonMaster;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGenerator.getReasonMaster_Param(token, username, clinicId, orgId,reason);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=	responseJson_object.getJSONObject("reasonsList");
		JSONArray	patien=	e.getJSONArray("reason");
		JSONObject y=patien.getJSONObject(0);
		reasonId=y.getString("reasonId");
		System.out.println("reasonID:    "+reasonId);
		Assert.assertNotNull(reasonId, "reasonID is null");
	}
}
