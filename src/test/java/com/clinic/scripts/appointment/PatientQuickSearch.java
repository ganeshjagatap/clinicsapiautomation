package com.clinic.scripts.appointment;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class PatientQuickSearch extends TestBase{
	@Test(priority =1)
	public void PatientQuickSearch_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.patientQuickSearch;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> updateQual = ParamGenerator.patientQuickSearch_Param(token, username, orgId, clinicId, startRow, interval);
		JSONObject param=ParamGenerator.getJSONPatientQuickSearch("",searchName,"","");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, updateQual, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject obj = responseJson_object.getJSONObject("patientList");
		//JSONObject arrobj = arr.getJSONObject(0);
		JSONArray patientArr = obj.getJSONArray("patient");
		JSONObject patientObj = patientArr.getJSONObject(0);
		String uhid = patientObj.getString("uhid");
		Assert.assertNotNull(uhid,"uhid is not null");
	}
}
