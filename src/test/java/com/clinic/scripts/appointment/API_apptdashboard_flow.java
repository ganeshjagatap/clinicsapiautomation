package com.clinic.scripts.appointment;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.* ;



public class API_apptdashboard_flow extends TestBase{
	public static String slotId_1=null;
	public static String dateSlotId=null;
	public static String endTime_1=null;
	public static String startTime_1=null;
	public static String apptId_1=null;
	public static String slotId_2=null;
	public static String endTime_2=null;
	public static String startTime_2=null;
	public static String apptId_2=null;
	public static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	static Date date = new Date();
	//public static String date1 =dateFormat.format(date);
	public static String slotId_last=null;
	public static String endTime_last=null;
	public static String startTime_last=null;
	
   // get two slots - HomeDoctorAppointment
	@Test(priority=201)
	public void getTwoFreeSlots_SuccessUsecase() throws Exception{
		System.out.println("Date formatt  "+date2);
		mailUrl = Envirnonment.env + Constants.homeDoctor;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username , clinicId, orgId, clinicId, date2);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		siteId=responseJson_object.getString("siteId");
		locationId=responseJson_object.getString("locationId");
		JSONObject bookedSlotsJSON =	responseJson_object.getJSONObject("bookedSlots");
		JSONArray  slotsJARR =	bookedSlotsJSON.getJSONArray("bookedSlot");
		int numOfSlots = slotsJARR.length();
		for (int i = 0 ; i < numOfSlots; i++){
			JSONObject slotObj = slotsJARR.getJSONObject(i);
			JSONObject p_list = slotObj.getJSONObject("patientList");
			JSONArray  patientsJARR =	p_list.getJSONArray("patient");
			
			if (patientsJARR.length() == 0 ) {
			     if (slotId_1 == null ){
			    	 slotId_1 = slotsJARR.getJSONObject(i).getString("slotId");
			    	 dateSlotId = slotsJARR.getJSONObject(i).getString("dateSlotId");
			         startTime_1 =	slotsJARR.getJSONObject(i).getString("startTime");
			         endTime_1 = slotsJARR.getJSONObject(i).getString("endTime");
			         continue;
			     } else {
			    	 slotId_2 = slotsJARR.getJSONObject(i).getString("slotId");
			    	 dateSlotId = slotsJARR.getJSONObject(i).getString("dateSlotId");
			         startTime_2 =	slotsJARR.getJSONObject(i).getString("startTime");
			         endTime_2 = slotsJARR.getJSONObject(i).getString("endTime");
                     break;
		   		 }
				
			}
		}
		
		JSONObject slotObj = slotsJARR.getJSONObject(numOfSlots-1);
   	    slotId_last = slotObj.getString("slotId");
   	    //dateSlotId = slotsJARR.getJSONObject(i).getString("dateSlotId");
        startTime_last = slotObj.getString("startTime");
        endTime_last = slotObj.getString("endTime");
		
			System.out.println("Slot id 1 : "+ slotId_1);
			System.out.println("Slot id 2 : "+ slotId_2);
			System.out.println("Slot id last : "+ slotId_last);
			System.out.println("Slot id last : "+ startTime_last);
			System.out.println("Slot id last : "+ endTime_last);
			Assert.assertNotNull(slotId_2, "unable to find to free slots");
	}    
	
   // book appt for one patient in one slot - BookAppointment
	@Test(priority=202)
	public void bookAppointment_SuccessUsecase() throws Exception{
     	mailUrl = Envirnonment.env + Constants.bookAppointment;
    	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> bookRegs = ParamGenerator.bookAppointment_Param(token, username, clinicId, orgId); 
		JSONObject param=ParamGenerator.getJSONBookAppointment(patientId1, dateSlotId,slotId_1, date2,startTime_1, endTime_1, locationId, siteId, clinicId, reasonOfVisit, locationName , organizationName, doctorName);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, bookRegs, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		Assert.assertNotNull(responseJson_object.getString("successMessage"));		
	}
   
   // get the appt id - HomeDocterAppointment
	@Test(priority=203)
	public void getApptId_SuccessUsecase() throws Exception{
			System.out.println("Date formatt  "+date1);
			mailUrl = Envirnonment.env + Constants.homeDoctor;
			System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();
			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
			List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username , clinicId, orgId, clinicId, date2);
			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);

			JSONObject bookedSlotsJSON =	responseJson_object.getJSONObject("bookedSlots");
			JSONArray  slotsJARR =	bookedSlotsJSON.getJSONArray("bookedSlot");
			int numOfSlots = slotsJARR.length();
			for (int i = 0 ; i < numOfSlots; i++){
				JSONObject slotObj = slotsJARR.getJSONObject(i);
				String currentSlotId = slotObj.getString("slotId");
				JSONObject p_list = slotObj.getJSONObject("patientList");
				JSONArray  patientsJARR =	p_list.getJSONArray("patient");
				
				if ( currentSlotId.equals(slotId_1)){
					JSONObject p_Obj = patientsJARR.getJSONObject(0);
					apptId_1 = p_Obj.getString("appointmentId");
					break;
				}
					
				}
				System.out.println("apptId 1 : "+ apptId_1);
				
				Assert.assertNotNull(apptId_1, "unable to find to booked appt");
		}    
		 
   // reschedule to second slot - RescheduleAppointment
    @Test(priority=204)
	public void rescheduleAppointment_SuccessUsecase() throws Exception{
     	mailUrl = Envirnonment.env + Constants.rescheduleAppointment;
    	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> Param_reschedule = ParamGenerator.Param_rescheduleAppointment(token, username, clinicId, orgId,orgId,orgId,patientId1); 
		JSONObject rescheduleJSON=ParamGenerator.JSON_rescheduleAppointment(dateSlotId, slotId_2, date2, startTime_2 , endTime_2, clinicId, reasonOfVisit,locationName, organizationName, doctorName,apptId_1, startTime_1, endTime_1, "CA");
        // Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, Param_reschedule,rescheduleJSON);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		Assert.assertNotNull(responseJson_object.getString("successMessage"));		
	}
   // get the appt id of resceduled appt - HomeDocterAppointment
	@Test(priority=205)
	public void getRescheduledApptId_SuccessUsecase() throws Exception{
			System.out.println("Date formatt  "+date1);
			mailUrl = Envirnonment.env + Constants.homeDoctor;
			System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();
			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
			List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username , clinicId, orgId, clinicId, date2);
			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);

			JSONObject bookedSlotsJSON =	responseJson_object.getJSONObject("bookedSlots");
			JSONArray  slotsJARR =	bookedSlotsJSON.getJSONArray("bookedSlot");
			int numOfSlots = slotsJARR.length();
			for (int i = 0 ; i < numOfSlots; i++){
				JSONObject slotObj = slotsJARR.getJSONObject(i);
				String currentSlotId = slotObj.getString("slotId");
				JSONObject p_list = slotObj.getJSONObject("patientList");
				JSONArray  patientsJARR =	p_list.getJSONArray("patient");

				if ( currentSlotId.equals(slotId_2)){
					System.out.println("******* number of patients" + patientsJARR.length() );
					JSONObject p_Obj = patientsJARR.getJSONObject(0);
					apptId_2 = p_Obj.getString("appointmentId");
					break;
				}
					
				}
				System.out.println("apptId 2 : "+ apptId_2);
				
				Assert.assertNotNull(apptId_2, "unable to find to booked appt");
		} 	
   // cancel the appointment - cancelAppointment
	
    @Test(priority=206)
	public void cancelAppointment_SuccessUsecase() throws Exception{
     	mailUrl = Envirnonment.env + Constants.cancelAppointment;
    	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> Param_reschedule = ParamGenerator.Param_cancelAppointment(token, username, clinicId, orgId); 
		JSONObject rescheduleJSON=ParamGenerator.JSON_cancelAppointment(siteId, locationId,patientId1, apptId_2 , date2, startTime_2, endTime_2, clinicId, roleId,"CA",API004_PatienSearch.patientName1);
        // Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, Param_reschedule,rescheduleJSON);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		
		Assert.assertNotNull(responseJson_object.getString("successMessage"));		
	}

   // block the first slot - blockSlot
    @Test(priority=207)
	public void blockAppointmentSlot_SuccessUsecase() throws Exception{
     	mailUrl = Envirnonment.env + Constants.blockAppointmentSlot;
    	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponseForToken = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> Param_blockAppointmentSlot = ParamGenerator.Param_blockUnblockAppointmentSlot(token, username, orgId);
                                                                                  	
		JSONObject blockAppointmentSlotJSON=ParamGenerator.JSON_blockUnblockAppointmentSlot(clinicId,slotId_1, date2, startTime_1, date2 ,endTime_1, clinicId , "CA", siteId, locationId);
        // Post request to the URL with the param data
		HttpResponse httpResponse = requestUtil.postJSONArrayy_Request(mailUrl, Param_blockAppointmentSlot,blockAppointmentSlotJSON);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponse);
		
		Assert.assertNotNull(responseJson_object.getString("successMessage"));		
	}	
   // unblock the first slot - unblockSlot
    @Test(priority=208)
	public void unblockAppointmentSlot_SuccessUsecase() throws Exception{
     	mailUrl = Envirnonment.env + Constants.unblockAppointmentSlot;
    	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponseForToken = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> Param_unblockAppointmentSlot = ParamGenerator.Param_blockUnblockAppointmentSlot(token, username, orgId); 
		JSONObject unblockAppointmentSlotJSON=ParamGenerator.JSON_blockUnblockAppointmentSlot(clinicId,slotId_1, date2, startTime_1, date2 ,endTime_1, clinicId , "CA", siteId, locationId);
       // Post request to the URL with the param data
		HttpResponse httpResponse = requestUtil.postJSONArrayy_Request(mailUrl, Param_unblockAppointmentSlot,unblockAppointmentSlotJSON);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponse);
		
		Assert.assertNotNull(responseJson_object.getString("successMessage"));		
	}
   // block the calender - blockAppointmentCalender
    @Test(priority=209)
	public void blockAppointmentCalendert_SuccessUsecase() throws Exception{
     	mailUrl = Envirnonment.env + Constants.blockAppointmentCalender;
    	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponseForToken = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> Param_postJSON_Request = ParamGenerator.Param_blockAppointmentCalender(token, username, clinicId, orgId, date2, startTime_last, date2 ,endTime_last, clinicId , "CA", siteId, locationId ); 
		// Post request to the URL with the param data
		HttpResponse httpResponse = requestUtil.postJSON_Request(mailUrl, Param_postJSON_Request);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponse);
		Assert.assertNotNull(responseJson_object.getString("successMessage"));		
	}
    
    //Unblock Slot blocked by calender
    @Test(priority=210)
	public void unblockAppointmentSlotAgain_SuccessUsecase() throws Exception{
     	mailUrl = Envirnonment.env + Constants.unblockAppointmentSlot;
    	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponseForToken = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> Param_unblockAppointmentSlot = ParamGenerator.Param_blockUnblockAppointmentSlot(token, username, orgId); 
		JSONObject unblockAppointmentSlotJSON=ParamGenerator.JSON_blockUnblockAppointmentSlot(clinicId,slotId_last, date2, startTime_last, date2 ,endTime_last, clinicId , "CA", siteId, locationId);
       // Post request to the URL with the param data
		HttpResponse httpResponse = requestUtil.postJSONArrayy_Request(mailUrl, Param_unblockAppointmentSlot,unblockAppointmentSlotJSON);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponse);
		Assert.assertNotNull(responseJson_object.getString("successMessage"));		
	}
}
