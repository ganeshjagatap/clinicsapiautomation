package com.clinic.scripts.appointment;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class PatientPreviousVisitsDetail extends TestBase{
	@Test(priority = 0)
	public void PatientPreviousVisitDetails_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.patientPreviousVisitsDetail;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGenerator.patientPreviousVisitsDetail_Param(interval, startRow, patientId, clinicId, username, token, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONObject obj = responseJson_object.getJSONObject("patientVisitsDetail");
		JSONArray arrobj = obj.getJSONArray("patientVisitDetail");
		JSONObject obj1 = arrobj.getJSONObject(0);
		String visitId = obj1.getString("visitId");
		System.out.println("visitId  "+visitId);
		Assert.assertNotNull(visitId, "visitId  is null");
	}
}
