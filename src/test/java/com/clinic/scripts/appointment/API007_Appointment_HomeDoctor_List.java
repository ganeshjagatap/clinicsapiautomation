package com.clinic.scripts.appointment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API007_Appointment_HomeDoctor_List extends TestBase{
	public static String siteId=null;
	public static String locationId=null;
	public static String slotId=null;
	public static String dateSlotId=null;
	public static String endTime=null;
	public static String startTime=null;
	static //public static String date1 =null;	
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	static Date date = new Date();
	public static String date1 =dateFormat.format(date);
	public static String doctorName = null;
	@Test(priority=13)
	public void AppointmentHomeDoctorList_SuccessUsecase() throws Exception{
		System.out.println("Date formatt  "+date1);
		mailUrl = Envirnonment.env + Constants.appointmentHomeDoctorList;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGenerator.getHomeDoctorList_Param(token, username, clinicId, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=	responseJson_object.getJSONObject("doctorList");
		JSONArray	docArr=	e.getJSONArray("doctor");
		int i=0;
		JSONObject docObj ;
		String clinic = null;
		
		for(i=0;i<docArr.length();i++)
		{
		   docObj= docArr.getJSONObject(i);
		   clinic = docObj.getString("clinicUserId");
		   if(clinic.equals(clinicId)){
			   doctorName = docObj.getString("doctorName");
		   }
		}
		System.out.println("doctorName:    "+doctorName);
		Assert.assertNotNull(doctorName, "doctorName is null");	
	}
}
