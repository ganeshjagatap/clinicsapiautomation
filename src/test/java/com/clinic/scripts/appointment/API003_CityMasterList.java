package com.clinic.scripts.appointment;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API003_CityMasterList extends TestBase {
	
	@Test(priority=163)
	public void getCityMasterList_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.cityMasterList;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		System.out.println("countryId"+API001_CountryList.countryId);
		List<NameValuePair> userDetails = ParamGenerator.getCityMasterList(token, username, clinicId, orgId, "1");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONObject e=responseJson_object.getJSONObject("cityList");
		JSONArray locationName=e.getJSONArray("city"); 
		JSONObject locationName1=locationName.getJSONObject(0);
		String cityId=locationName1.getString("cityId");
		System.out.println("cityId  "+cityId);
		Assert.assertNotNull(cityId, "cityId  is null");	
	}	
}
