package com.clinic.scripts.appointment;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class PatientSearchAdvance extends TestBase {
	public static String patientId1 = null;
	@Test(priority=1)
	public void PatientSearchAdvance_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.patientSearchAdvance;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearchAdvance_Param(token, username, orgId, clinicId, startRow, interval); 
		JSONObject param=ParamGenerator.getJSONPatientSearchAdvance("","","test","", "Male","", "", "", "", "");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e = responseJson_object.getJSONObject("patientList");
		JSONArray	patien = e.getJSONArray("patient");
		JSONObject y = patien.getJSONObject(0);
	    patientId1 = y.getString("patientId");
		System.out.println("Patient Id  "+patientId1);
		Assert.assertNotNull(patientId1, "PatientID is null");
	}
}
