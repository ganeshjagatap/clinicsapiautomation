package com.clinic.scripts.appointment;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API001_CountryList extends TestBase {
	public static String countryId = null;
	@Test(priority=11)
	public void getCountryList_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.countryList;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGenerator.getUpdateQualification(token, username, clinicId, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=responseJson_object.getJSONObject("countryList");
		JSONArray locationName=e.getJSONArray("country");
		JSONObject locationName1=locationName.getJSONObject(0);
		String countryId=locationName1.getString("countryId");
		System.out.println("countryId  "+countryId);
		Assert.assertNotNull(countryId, "country ID is null");	
	}
}
