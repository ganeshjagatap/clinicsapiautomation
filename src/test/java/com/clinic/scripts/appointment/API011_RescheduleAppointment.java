package com.clinic.scripts.appointment;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API011_RescheduleAppointment extends TestBase {
	@Test(priority=19)
	public void bookAppointment_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.rescheduleAppointment;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> bookRegs = ParamGenerator.rescheduleAppointment_Param(token, username, clinicId, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, patientId);
		JSONObject param=ParamGenerator.getJSONBookAppointment(patientId, API006_HomeDoctor.dateSlotId,API006_HomeDoctor.slotId, API006_HomeDoctor.date1,API006_HomeDoctor.startTime, API006_HomeDoctor.endTime, API006_HomeDoctor.locationId, API006_HomeDoctor.siteId, clinicId, "Haed Ache", "Banglore", orgName, API007_Appointment_HomeDoctor_List.doctorName);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, bookRegs, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage = responseJson_object.getString("successMessage");
		Assert.assertNotNull(successMessage, "successMessage is null");	
	}
}
