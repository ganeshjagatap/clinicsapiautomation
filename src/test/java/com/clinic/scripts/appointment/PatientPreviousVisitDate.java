package com.clinic.scripts.appointment;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class PatientPreviousVisitDate extends TestBase{
	
	@Test(priority=1)
	public void API015_PatientPreviousVisitDate_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.patientPreviousVisitDate;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGenerator.patientPreviousVisitDate_Param(username, token, clinicId, clinicId, orgId, patientId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String patientLastVisitDate = responseJson_object.getString("patientLastVisitDate");
		System.out.println("patientLastVisitDate:    "+patientLastVisitDate);
		Assert.assertNotNull(patientLastVisitDate, "patientLastVisitDate is null");
	}	
}
