package com.clinic.scripts.appointment;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;


public class API004_PatienSearch extends TestBase {
	
	public static String patientName = null;
	public static String emailId = null;
	public static String mobileNo = null;
	public static String patientName1 = null;
	public static String emailId1 = null;
	public static String mobileNo1 = null;
	@Test(priority=16)
	public void PatientSearch_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.patientSearch;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,username,orgId, clinicId,startRow,interval,hmFlag); 
		JSONObject param=ParamGenerator.getJSONPatientSearch("", "", searchName, "","");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=	responseJson_object.getJSONObject("patientList");
		JSONArray	patien=	e.getJSONArray("patient");
		int len = patien.length();
		Random r = new Random();
		int ranNum = r.nextInt(len);
		JSONObject y=patien.getJSONObject(ranNum);
		emailId = y.getString("emailId");
		mobileNo = y.getString("mobileNo");
	    patientId=y.getString("patientId");
	    patientName = y.getString("patientName");
		System.out.println("Patient Id  "+patientId);
		Assert.assertNotNull(patientId, "PatientID is null");
		ranNum = r.nextInt(len);
		JSONObject y1=patien.getJSONObject(ranNum);
		emailId1 = y1.getString("emailId");
		mobileNo1 = y1.getString("mobileNo");
	    patientId1=y1.getString("patientId");
	    patientName1 = y1.getString("patientName");
	}
	
	
	
	/*@Test(priority=2)
	public void Invalid_Token_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param("invalid",username,orgId, clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
	}
	
	
	@Test(priority=3)
	public void Token_As_A_Blank_Spaces_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param("     ",username,orgId, clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
	}
	
	
	@Test(priority=4)
	public void Username_As_A_Null_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,null,orgId, clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10000);
	}
	
	
	@Test(priority=5)
	public void Username_As_A_Blank_Spaces_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,"     ",orgId, clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
	}
	
	@Test(priority=6)
	public void Username_As_A_Special_Character_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,"@#@$$$$###",orgId, clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
	}
	
	
	@Test(priority=7)
	public void OrgnisationID_As_A_Special_Character_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,username,"@#$$$##", clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
	}
	
	
	@Test(priority=8)
	public void OrgnisationID_As_Blank_Spaces_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,username,"   ", clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
			
			
	}
	
	
	@Test(priority=9)
	public void INVALID_OrgnisationID_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,username,"INVALID", clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
	}
	
	
	
	
	@Test(priority=10)
	public void OrgnisationID_AS_A_Null_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,username,null, clinicId,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10000);
	}
	
	
	@Test(priority=11)
	public void userId_AS_A_Null_FailureUsecase() throws Exception{
	mailUrl = Envirnonment.env + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getPatienSearch_Param(token,username,orgId, null,"1","10","n"); 
JSONObject param=ParamGenerator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10000);
	}
	
	*/
	
	
	
	
	
	
	
	
	

	
}
