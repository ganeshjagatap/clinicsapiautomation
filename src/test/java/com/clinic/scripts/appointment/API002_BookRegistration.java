package com.clinic.scripts.appointment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;



public class API002_BookRegistration extends TestBase {

	
	public static JSONObject responseJson_object=null;
	public static StringBuffer sb=null;
	
	String authToken=null;
	public static String mailUrl=null;
	//String token=null;

	public String date1=null;
	ArrayList  patientAllergy=new ArrayList();
	
	static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	static Date date = new Date();


	
	
	
	
	
	
	@Test(priority=25)
	public void API002_BookRegistration_SuccessUsecase() throws Exception{
	  
		//String date="15-09-2016";
		//String dateSlotId="1298";
		Random r = new Random();
		Integer num = r.nextInt(88888);
		Integer num1 = r.nextInt(99999);
		
		String addressLine1="24th cross st";
		String addressLine2="hsr layout";
		String age="1";
		String alternate_number="67454";
		String birthDayWish="Y";
		String bloodGroup="AB+";
		String casualty="N";
		String city="BAnglore";
		//String clinic_id="";
		String country="INdia";
		String countrycode_alternateNo="8904299473";
		String countrycode_mobileNo="8904298773";
		String emailId="test1fd23@gmail.com";
		String employment="Select";
		String gender="Male";
		String government_id_number="12345";
		String government_id_type="Select";
		String homeMonitoring="Y";
		String lastName="rrr";
		String martial_status="NA";
		String mlc="N";
		String mlc_description="";
		String mlc_number="";
		String mobileNo= num.toString()+num1.toString();
		
		String patientDOB="2007-08-11";
		String patientName="kala";
		String patientPhoto="";
		String religion="";
		String state="";
		String title="hi";
		String zipCode="";
	mailUrl = Envirnonment.env + Constants.BookRegistration;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenerator.getBookregistration_Param(API006_HomeDoctor.slotId, date1, API006_HomeDoctor.dateSlotId, clinicId, doctorName, API006_HomeDoctor.startTime, 
				API006_HomeDoctor.endTime, isDuplicateCheckRequired, isPatientMappingRequired,API006_HomeDoctor.locationId, 
				locationName, medicalHistoryFlag,orgId, username,
				reasonOfVisit, API006_HomeDoctor.siteId, role, token, clinicId, username); 
		JSONObject param=ParamGenerator.getJSONBookregistration(addressLine1, addressLine2, age, alternate_number, birthDayWish, 
		bloodGroup, casualty, city, clinicId, country, countrycode_alternateNo,
		countrycode_mobileNo, employment, gender,
		government_id_number, government_id_type, homeMonitoring, 
		lastName, martial_status, mlc, mlc_description, mlc_number,
		mobileNo, patientAllergy, patientDOB, patientName, patientPhoto,
		religion, state, title, zipCode);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		//String x= responseJson_object.toString();
		/* JSONArray e=responseJson_object.getJSONArray("patientList");
		JSONObject y=e.getJSONObject(0);
		String patienID=y.getString("patientId");*/
		JSONObject e=	responseJson_object.getJSONObject("patientList");
		JSONArray	patien=	e.getJSONArray("patient");
		JSONObject y=patien.getJSONObject(2);
		String patienID=y.getString("patientId");
		System.out.println("Patient Id  "+patienID);
		Assert.assertNotNull(patienID, "PatientID is null");	
	}	
}
