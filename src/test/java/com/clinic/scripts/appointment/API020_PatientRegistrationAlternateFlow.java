package com.clinic.scripts.appointment;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;


public class API020_PatientRegistrationAlternateFlow extends TestBase{
	@Test(priority =22)
	public void PatientRegistrationAlternateFlow_SuccessUsecase() throws Exception {
		Random random=new Random();
		int randomNumber=(random.nextInt(65536)-32768);
		Integer num = random.nextInt(888) + 100;
		Integer num1 = random.nextInt(888) + 100;
		Integer num2 = random.nextInt(666) + 1000;
		String randomLast=RandomStringUtils.randomAlphabetic(5);
		String randomNumber1=num.toString()+num1.toString()+num2.toString();
		Integer day = random.nextInt(30);
		Integer month = random.nextInt(12);
		Integer year = random.nextInt(2015);
		String dob = day.toString() +"-"+ month.toString() +"-"+year.toString() ;
		System.out.println("random number"+randomNumber1);
		mailUrl = Envirnonment.env + Constants.patientRegistrationAlternateFlow;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String doctorId = clinicId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGenerator.patientRegistrationAlternateFlow_Param(token, clinicId, username, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, "Y","N","");
		JSONObject param=ParamGenerator.getJSONPatientRegistrationAlternateFlow("", "Ms", randomLast+randomNumber1, "M", "Female", "A+", dob, "", "", randomNumber1, "", "", "", "", "", "", "", "", "Y", "N",
				"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
				"", "", "", "", "", "", "", "", "", "", "", "", "single", "", "", "", "1234", randomNumber1);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String patientId = responseJson_object.getString("patientId");
		System.out.println("patientId  "+patientId);
		Assert.assertNotNull(patientId, "patient Id  is null");
	}	


}
