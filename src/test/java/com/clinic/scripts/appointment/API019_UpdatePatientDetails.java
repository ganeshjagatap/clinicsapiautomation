package com.clinic.scripts.appointment;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;



public class API019_UpdatePatientDetails extends TestBase{
	@Test(priority =21)
	public void PersistPatientDiagnosisInfoHandler_SuccessUsecase() throws Exception {
		Random random=new Random();
		int randomNumber=(random.nextInt(65536)-32768);
		String randomNumber1=""+randomNumber;
		mailUrl = Envirnonment.env + Constants.updatePatientDetails;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String siteId = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		String patientUHID = "2105";
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> updateQual = ParamGenerator.updatePatientDetails_Param(token, clinicId, username, orgId, siteId, locationId);
		JSONObject param=ParamGenerator.getJSONUpdatePatientDetails("2105", patientUHID, "Mr", "Test", "last", "Male", "B+", "14-06-2001", randomNumber1, "","jk nagar", "", "India", "Karnataka", 
				"Banglore", "", "", "Y", "N", "N", "", "", "", "", "", "", "", "8837", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
				"", "", "", "", "", "", "", "", "", "", "", "Single", "", "", "");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, updateQual, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//String noteId=responseJson_object.getString("nurserNoteId");
		Assert.assertEquals(responseJson_object.getString("successMessage"), "Patient profile of Test with UHID 2105 has been updated with specified information.");
	}
}
