package com.clinic.scripts.appointment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;


public class API018_PatientRegistration extends TestBase{
	
	@Test(priority =20)
	public void UpdateUser_SuccessUsecase() throws Exception {
		Random random=new Random();
		Integer day = random.nextInt(30);
		Integer month = random.nextInt(12);
		Integer year = random.nextInt(2015);
		String dob = day.toString() +"-"+month.toString()+"-"+year.toString() ;
		String randomLast=RandomStringUtils.randomAlphabetic(5);
		Integer num = random.nextInt(555) + 100;
		Integer num1 = random.nextInt(777) + 100;
		Integer num2 = random.nextInt(666) + 1000;
		String randomNumber1=num.toString()+num1.toString()+num2.toString();
		System.out.println("random number"+randomNumber1);
		mailUrl = Envirnonment.env + Constants.patientRegistration;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String doctorId = clinicId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGenerator.patientRegistration_Param(token, clinicId, username, orgId, doctorId,API006_HomeDoctor.startTime, API006_HomeDoctor.endTime, API006_HomeDoctor.date1, API006_HomeDoctor.dateSlotId, API006_HomeDoctor.slotId, "Fever", API006_HomeDoctor.locationId, API006_HomeDoctor.siteId, "banglore", orgName, API007_Appointment_HomeDoctor_List.doctorName,isDuplicateCheckRequired,isPatientMappingRequired);
		JSONObject param=ParamGenerator.getJSONPatientRegistration("", "Mrs",randomLast+randomNumber1, "M", "Female", "O+", dob, "", "",
				randomNumber1, "", "st nagar", "", "India", "karnataka", "Banlore", "", "", "Y", "N", randomNumber1, "N", "Hindu","SelfEmployed",
				"", "", "", "", "", "single", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String patientId = responseJson_object.getString("patientId");
		System.out.println("patientId  "+patientId);
		Assert.assertNotNull(patientId, "patient Id  is null");
	}	


}
