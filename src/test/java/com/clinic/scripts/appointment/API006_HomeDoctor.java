package com.clinic.scripts.appointment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API006_HomeDoctor extends TestBase {
	public static String siteId=null;
	public static String locationId=null;
	public static String slotId=null;
	public static String dateSlotId=null;
	public static String endTime=null;
	public static String startTime=null;
	static //public static String date1 =null;	
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	static Date date = new Date();
	public static String date1 =dateFormat.format(date);
	
	@Test(priority=0)
	public void AppointmentHomeDoctor_SuccessUsecase() throws Exception{
		System.out.println("Date formatt  "+date1);
		mailUrl = Envirnonment.env + Constants.homeDoctor;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username, clinicId, orgId, clinicId, date1);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		siteId=responseJson_object.getString("siteId");
		locationId=responseJson_object.getString("locationId");
		JSONObject e=	responseJson_object.getJSONObject("bookedSlots");
		JSONArray	slots=	e.getJSONArray("bookedSlot");
		int len = slots.length();
		Random r = new Random();
		int ran = r.nextInt(len);
		JSONObject y=slots.getJSONObject(ran);
		String normalBookSize=y.getString("normalBookSize");
		slotId=y.getString("slotId");
		dateSlotId=y.getString("dateSlotId");
		startTime=y.getString("startTime");
		endTime=y.getString("endTime");
		System.out.println("starttime"+startTime+"endTime"+endTime);
		System.out.println("normalBookSize:    "+normalBookSize);
		Assert.assertNotNull(normalBookSize, "normalBookSize is null");
	}
	
	
	/*@Test(priority=32)
	public void Invalid_Token_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param("INVALID", username, clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
		
}
	
	
	@Test(priority=33)
	public void Blank_Token_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param("", username, clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10000);
			Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=34)
	public void Token_With_Blank_Spaces_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param("     ", username, clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=35)
	public void Token_With_Speacial_Characters_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param("@@@##$$$$$$####", username, clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=36)
	public void Token_With_Alpha_Numeric_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param("@@@##$$$$$$####", username, clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=37)
	public void Invalid_Username_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, "tata1", clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	@Test(priority=38)
	public void Blank_Username_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, "", clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10000);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	@Test(priority=39)
	public void Username_With_Blank_Spaces_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, "    ", clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	
	@Test(priority=40)
	public void Username_With_SpecialCharacter_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, "@@#@$$$$$", clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 8001);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	

	@Test(priority=41)
	public void Token_As_Null_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(null, username, clinicId, orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10000);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}

	@Test(priority=42)
	public void Blank_UserId_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username, "", orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10000);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}	
	
	
	
	
	@Test(priority=43)
	public void Invalid_UserId_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username, "INVALID", orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}	
	
	@Test(priority=44)
	public void UserID_With_Blank_Spaces_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username, "      ", orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}		
	
	@Test(priority=45)
	public void UserID_With_Special_Characters_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username, "@@#$@$$$$", orgId, clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}		
		
	@Test(priority=46)
	public void organisationId_With_Special_Characters_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username, clinicId, "#$#$%#%%", clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}		
	
	
	@Test(priority=47)
	public void organisationId_With_Blank_Spaces_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, "      ", clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	
	
	@Test(priority=48)
	public void Invalid_OrganisationID_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, "INVALID", clinicId, date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=49)
	public void Invalid_ClinicUserID_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, orgId, "INVALID", date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=50)
	public void ClinicUserID_With_Balnk_Spaces_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, orgId, "    ", date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	@Test(priority=51)
	public void ClinicUserID_With_Special_Characters_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, orgId, "@@#@$$$$$", date1);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=52)
	public void AppointmentDate_With_Special_Characters_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, orgId, clinicId, "@@$$$$$$");

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	
	@Test(priority=53)
	public void AppointmentDate_With_Blank_Spaces_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, orgId, clinicId, "    ");

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=54)
	public void AppointmentDate_As_YestredayDate_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, orgId, clinicId, "07-09-2016");

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
	
	
	@Test(priority=55)
	public void AppointmentDate_As_Null_FailureUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRHomeDoctor_Param(token, username,clinicId, orgId, clinicId, null);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
			int errorCode=responseJson_object.getInt("errorCode");
			
			
			Assert.assertEquals(errorCode, 10066);
			//Assert.assertEquals(responseJson_object.getString("errorMessage"), "Mandatory fields were missing in the request[Token]");
}
		*/
	
	
	
	

}
