package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API172_CheckDuplicateUser extends TestBase{
	@Test(priority =162)
	public void CheckDuplicateUser_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile, username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String successMessage = responseJson_object.getString("successMessage");
		System.out.println("successMessage  "+successMessage);
		Assert.assertNotNull(successMessage, "successMessage  is null");
	}
	/*
	@Test(priority =163)
	public void Invalid_Token_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, "invalid", API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile, username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8001);	 
	}	
	
	@Test(priority =164)
	public void Token_As_Null_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, null, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile, username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	

	@Test(priority =165)
	public void Token_As_Blank_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, "   ", API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile, username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =166)
	public void Invalid_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile, "invalid");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8001);	 
	}	
	
	@Test(priority =167)
	public void Username_As_Null_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile, null);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =168)
	public void Username_As_Blank_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile, "    ");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =169)
	public void Username_As_Special_Character_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile, "/*-+");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8001);	 
	}	
	
	@Test(priority =170)
	public void Invalid_OrgId_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param("invalid", token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8000);	 
	}	

	@Test(priority =171)
	public void OrgId_As_Null_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(null, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =172)
	public void OrgId_As_Blank_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param("   ", token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =173)
	public void OrgId_As_Special_Character_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param("/*-+", token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8000);	 
	}
	@Test(priority =174)
	public void OrgId_With_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param("1   09", token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8000);	 
	}
	
	@Test(priority =175)
	public void Invalid_UserDob_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, "invalid", API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9111);	 
	}	

	@Test(priority =176)
	public void UserDob_As_Null_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token,null, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =177)
	public void UserDob_As_Blank_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, "   ", API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =178)
	public void UserDob_As_Special_Character_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token,"/*-+", API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9111);	 
	}
	@Test(priority =179)
	public void UserDob_With_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token,"199  4-12-22", API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9111);	 
	}
	
	@Test(priority =180)
	public void UserDob_As_Future_Date_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token,"2094-12-22", API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9111);	 
	}
	
	@Test(priority =181)
	public void Invalid_MobileNo_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, "invalid",username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8000);	 
	}	

	@Test(priority =182)
	public void MobileNo_As_Null_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, null,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =183)
	public void MobileNo_As_Blank_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, "  ",username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =184)
	public void MobileNo_As_Special_Character_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, "/*-+",username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8000);	 
	}
	@Test(priority =185)
	public void MobileNo_With_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, role, API083_GetUserProfileDetail.medicalRegNo, "693 5814 789",username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8000);	 
	}
	
	@Test(priority =186)
	public void Invalid_Gender_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, "invalid", role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8000);	 
	}	

	@Test(priority =187)
	public void Gender_As_Null_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, null, role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =188)
	public void Gender_As_Blank_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob,"   ", role, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =189)
	public void Invalid_RoleType_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob, API083_GetUserProfileDetail.gender, "invalid", API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 8000);	 
	}	

	@Test(priority =191)
	public void RoleType_As_Null_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob,API083_GetUserProfileDetail.gender, null, API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	
	@Test(priority =192)
	public void RoleType_As_Blank_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.checkDuplicateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.CheckDuplicateUser_param(orgId, token, API083_GetUserProfileDetail.userDob,API083_GetUserProfileDetail.gender,"    ", API083_GetUserProfileDetail.medicalRegNo, API083_GetUserProfileDetail.userMobile,username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		int errorCode=responseJson_object.getInt("errorCode");	
		 Assert.assertEquals(errorCode, 9102);	 
	}	
	*/
}
