package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API086_ListUserPublication  extends TestBase{
	
	 public static String publicationId = null;
	@Test(priority =79)
	public void ListUserPublication_SuccessUsecase() throws Exception {

		mailUrl = Envirnonment.env + Constants.listUserPublication;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.listUserPublication_Param(token, username, clinicId, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//JSONObject e=responseJson_object.getJSONObject("publication");
		JSONArray locationName=responseJson_object.getJSONArray("publication");
		JSONObject locationName1=locationName.getJSONObject(0);
		publicationId=locationName1.getString("publicationId");

		System.out.println("PublicationId  "+publicationId);
		Assert.assertNotNull(publicationId, "Publication ID is null");
		// Verify the Value using assertion		 
	}
}
