package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class SetUserGmailRefreshToken extends TestBase{
	@Test(priority =0)
	public void SetUserGmailRefreshToken_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.setUserGmailRefreshToken;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		String randomToken = RandomStringUtils.randomAlphanumeric(10);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.setUserGmailRefreshToken(token, username, clinicId, orgId,randomToken);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage = responseJson_object.getString("successMessage");
		Assert.assertNotNull(successMessage);
	}
	
}
