package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API083_GetUserProfileDetail extends TestBase {
	public static String userMobile = null;
	public static String userDob = null;
	public static String medicalRegNo = null;
	public static String gender = null;
	@Test(priority=76)
	public void getUserProfileDetails_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.getUserProfileDetail;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGenerator.getUpdateQualification(token, username, clinicId, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=responseJson_object.getJSONObject("user");
		String userId =e.getString("userId");
		userDob = e.getString("dateOfBirth");
		medicalRegNo = e.getString("medicalRegistrationNumber");
		userMobile = e.getString("mobileNumber");
		gender = e.getString("gender");
		System.out.println("userId  "+userId);
		Assert.assertNotNull(userId, "userId is null");	
	}
}
