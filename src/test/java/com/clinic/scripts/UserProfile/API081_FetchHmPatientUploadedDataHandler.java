package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;
public class API081_FetchHmPatientUploadedDataHandler extends TestBase {
	@Test(priority =1)
	public void FetchHmPatientUploadDataHandler_SuccessUsecase() throws Exception {

		mailUrl = Envirnonment.env + Constants.fetchHmPatientUploadedDataHandler;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();
			String siteId = API006_HomeDoctor.siteId; 
			String locationId = API006_HomeDoctor.locationId;
			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
			List<NameValuePair> signinparams = ParamGeneratorUserProfile.fetchHmPatientUploadedDataHandler_Param(clinicId, username, token, orgId, siteId, locationId, patientId, "2", "Blood Glucose Monitor", "1", "1", "CLINIC");
			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
			//JSONArray arr = responseJson_object.getJSONArray("deviceInfo");
			//JSONObject obj = arr.getJSONObject(0);
			String deviceId = responseJson_object.getString("deviceId");
			System.out.println("DeviceId  "+deviceId);
			Assert.assertNotNull(deviceId, "DeviceId  is null");
			
	}



}
