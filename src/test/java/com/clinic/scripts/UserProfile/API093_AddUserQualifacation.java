package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API093_AddUserQualifacation extends TestBase {
	
	
	/*public static JSONObject responseJson_object=null;
	public static StringBuffer sb=null;
	
	String authToken=null;
	public static String mailUrl=null;
	String token=null;
	String username="tata";
	String orgId=null;
	String clinicId=null;
	

	
	
	
	
	@Test(priority=0)
	public void login_SuccessUsecase() throws Exception{
		
		
	mailUrl = Envirnonment.env + Constants.login;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param(username, "Test123@");

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 

		 token=responseJson_object.getString("token");
		 clinicId=responseJson_object.getString("clinicUserId");
		 JSONArray e=responseJson_object.getJSONArray("organisation");
		JSONObject y=e.getJSONObject(0);
		orgId=y.getString("organisationId");
	
		 System.out.println("Clinic User id "+ clinicId);
		System.out.println("token value  "+token);
		System.out.println("organistion Id  "+orgId);
		Assert.assertNotNull(token, "token is null");
		Assert.assertNotNull(clinicId, "ClinicUser ID is null");
		Assert.assertNotNull(orgId, "Organisation ID is null");
	//////
		
			}*/
	
	@Test(priority=87)
	public void addUserQualification_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.updateQualifaction;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		
		List<NameValuePair> updateQual = ParamGenerator.getUpdateQualification(token, username, clinicId, orgId);
		JSONObject param=ParamGenerator.getJSONUpdateQualification(instutionName, fromDate, toDate, board, university, course, degree, specialization);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, updateQual, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 String sucess=responseJson_object.getString("successMessage");
			
			
			Assert.assertEquals(sucess.trim(), "Successfully added the qualification");
	//////
		
			}
	
	
	
	/*@Test(priority=3)
	public void logout() throws Exception{
	mailUrl = Envirnonment.env + Constants.logout;

	System.out.println("mailUrl "+mailUrl);
	String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
	RequestUtil requestUtil = new RequestUtil();

	// Get request for the session creation first.
	HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
	System.out.println("begin");

	List<NameValuePair> signinparams = ParamGenerator.getEMRlogout_Param("TATA", token);

	// Post request to the URL with the param data
	HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
	 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
	 
	//String x= responseJson_object.toString();
	String sucess=responseJson_object.getString("successMessage");
	
	
	Assert.assertEquals(sucess.trim(), "User logged out successfully");
	
	}
	*/
	
}
