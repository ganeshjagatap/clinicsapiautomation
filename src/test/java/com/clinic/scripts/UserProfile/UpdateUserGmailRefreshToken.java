package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class UpdateUserGmailRefreshToken extends TestBase{
	@Test(priority =21)
	public void SetUserGmailRefreshToken_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.updateUserGmailRefreshToken;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.updateUserGmailRefreshToken(token, username, clinicId, orgId,gmailRefreshTokenId, active);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage = responseJson_object.getString("successMessage");
		Assert.assertNotNull(successMessage);
	}
	
}
