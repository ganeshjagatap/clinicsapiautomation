package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API078_FetchHmNurseNotesHandler extends TestBase{
	@Test(priority =1)
	public void FetchHmNurserNotesHandler_SuccessUsecase() throws Exception {
		
		mailUrl = Envirnonment.env + Constants.fetchHmNurseNotesHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String siteId = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		String nurseNoteId = API080_PersistHmNurseNotesHandler.nurseNoteId;
		String doctorId = clinicId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.fetchHmNurseNotesHandler_Param(clinicId, username, token, orgId, siteId, locationId, patientId, doctorId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//JSONObject loc = e.getJSONObject(0);
		//JSONArray loc = responseJson_object.getJSONArray("prescriptionsAndNurseNotes");
		//JSONObject arr = loc.getJSONObject(0);
		String patientUhid=responseJson_object.getString("patientUhid");
		System.out.println("patientUhid"+patientUhid);
		Assert.assertNotNull(patientUhid, "patientUhid is null");
		// Verify the Value using assertion		 
	}	
}
