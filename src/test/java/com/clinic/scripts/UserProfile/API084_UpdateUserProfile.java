package com.clinic.scripts.UserProfile;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API084_UpdateUserProfile extends TestBase {

	@Test(priority=77)
	public void updateUserProfile_SuccessUsecase() throws Exception{
		Random r = new Random();
		Integer num = r.nextInt(99999);
		Integer num1 = r.nextInt(99999);
		String mobileNo = num.toString()+num1.toString();
		mailUrl = Envirnonment.env + Constants.updateUserProfile;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String randomName = RandomStringUtils.randomAlphabetic(8);
		List<NameValuePair> userProfile = ParamGeneratorUserProfile.updateUserProfile_Param(token, username, clinicId, orgId);
		JSONObject param=ParamGeneratorUserProfile.getJSONUpdateUserProfile("Mala", "Ms.", "Female", "O+", "22-12-1994", mobileNo, randomName+"@gmail.com", "123,KR st,banglore", "56656", "RUser", "RSignature", "cardiology", digitalSignaturePath, digitalSignaturePassword, "N");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		Assert.assertEquals(responseJson_object.getString("successMessage"), "User details have been updated successfully");	
	}
}
