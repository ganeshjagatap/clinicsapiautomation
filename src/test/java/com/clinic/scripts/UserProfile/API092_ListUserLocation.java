package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;

import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API092_ListUserLocation extends TestBase{
	@Test(priority=86)
	public void ListUserLocation_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.listUserLocation;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGenerator.getUpdateQualification(token, username, clinicId, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray e=responseJson_object.getJSONArray("locationName");
		String locationName=e.getString(0);
	//	String locationName=r.getString("locationName");
		System.out.println("locationName  "+locationName);
		Assert.assertNotNull(locationName, "locationName is null");
	}
}
