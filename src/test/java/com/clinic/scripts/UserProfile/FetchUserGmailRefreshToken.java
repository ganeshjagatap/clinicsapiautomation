package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class FetchUserGmailRefreshToken extends TestBase{
	@Test(priority =12)
	public void FetchUserGmailRefreshToken_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchUserGmailRefreshToken;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.fetchUserGmailRefreshToken(token, username, clinicId, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray arr = responseJson_object.getJSONArray("gmailRefreshToken");
		JSONObject obj = arr.getJSONObject(0);
		Integer gmailRefreshToken = obj.getInt("gmailRefreshTokenId");
		gmailRefreshTokenId = gmailRefreshToken.toString();
		active = obj.getString("isActive");
		Assert.assertNotNull(gmailRefreshTokenId);
	}
	
}
