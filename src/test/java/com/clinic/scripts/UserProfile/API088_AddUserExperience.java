package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API088_AddUserExperience extends TestBase {
	
	
	@Test(priority =82)
	public void AddUserExperience_SuccessUsecase() throws Exception {
	
		mailUrl = Envirnonment.env + Constants.addUserExperience;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGeneratorUserProfile.addUserExperience_Param(token, username, clinicId, orgId);
		JSONObject param=ParamGeneratorUserProfile.getJSONAddUserExperience(organizationName, fromDate, toDate, role, remarks);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		Assert.assertEquals(responseJson_object.getString("successMessage"), "Successfully added experience details");
	}	
}
