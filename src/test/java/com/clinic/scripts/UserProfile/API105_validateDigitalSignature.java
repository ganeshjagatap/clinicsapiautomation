package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API105_validateDigitalSignature extends TestBase {
	
	@Test(priority=98)
	public void validateDigitalSignature_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.validateDigitalSignature;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> userDetails = ParamGeneratorUserProfile.validateDigitalSignature_param(token, clinicId, username, orgId, digitalSignaturePath, digitalSignaturePassword);
		
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);

		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 String e=responseJson_object.getString("successMessage");
			//JSONObject y=e.getJSONObject(0);
			//String siteId=y.getString("siteId");
			//String userId=y.getString("userId");
		

			System.out.println("successMessage  "+e);
			Assert.assertNotNull(e,"successMessage is null");
		
			}

}
