package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API101_updateDoctorTimings extends TestBase {
	
	@Test(priority=214)
	public void updateDoctorTimings_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.updateDoctorTimings;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> userDetails = ParamGeneratorUserProfile.updateDoctorTimings_param(token, clinicId, username, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, addFromDate,addToDate, API097_GetMyTimings.dateSlotId1);
		JSONObject param=ParamGeneratorUserProfile.getJSONupdateDoctorTimings(dayName, startTime, endTime, API100_ListDoctorTimings.daySlotId);
		
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails, param);

		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 String e=responseJson_object.getString("successMessage");
			//JSONObject y=e.getJSONObject(0);
			//String siteId=y.getString("siteId");
			//String userId=y.getString("userId");
		

			System.out.println("successMessage  "+e);
			Assert.assertNotNull(e,"successMessage is null");
		
			}

}
