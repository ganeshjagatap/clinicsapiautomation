package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.billing.API023_calculateDiscountAndNetAmount;

public class API095_UpdateUserQualification extends TestBase{

	@Test(priority =89)
	public void UpdateUserQualification_SuccessUsecase() throws Exception {
	
		mailUrl = Envirnonment.env + Constants.updateUserQualification;		
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		String qualificationId = API094_ListUserQualification.qualificationId;
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGeneratorUserProfile.updateUserQualification_Param(token, username, clinicId, orgId);
		JSONObject param=ParamGeneratorUserProfile.getJSONUpdateUserQualification(qualificationId,instutionName,fromDate, date1, board,university, course,degree, specialization);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		Assert.assertEquals(responseJson_object.getString("successMessage"), "Successfully updated qualification details");
	}	


}
