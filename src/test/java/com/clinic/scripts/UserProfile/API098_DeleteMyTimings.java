package com.clinic.scripts.UserProfile;

import java.util.List;

import javax.naming.ldap.ExtendedRequest;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API098_DeleteMyTimings extends TestBase{
	@Test(priority =215)
	public void DeleteMyTimings_SuccessUsecase() throws Exception {
	
		mailUrl = Envirnonment.env + Constants.deleteMyTimings;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.deleteMyTimings_Param(token, username, clinicId, orgId, API097_GetMyTimings.dateSlotId1, API097_GetMyTimings.locationId1);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage = responseJson_object.getString("successMessage");
		Assert.assertNotNull(successMessage, "successMessage is null");
	}	
}
