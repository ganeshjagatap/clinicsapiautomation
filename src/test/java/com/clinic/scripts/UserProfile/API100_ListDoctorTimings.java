package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API100_ListDoctorTimings extends TestBase{
	public static String daySlotId = null;
	@Test(priority =213)
	public void ListDoctorTimings_SuccessUsecase() throws Exception {
	
		mailUrl = Envirnonment.env + Constants.listDoctorTimings;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String locationId = API006_HomeDoctor.locationId;
		String date1 = API006_HomeDoctor.date1;//both start date and end date
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.listDoctorTimings_Param(token, username, clinicId, orgId, locationId, addFromDate, addToDate,API097_GetMyTimings.dateSlotId1);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=responseJson_object.getJSONObject("daySlotTimings");
		JSONArray locationName=e.getJSONArray("day");
		JSONObject loc = locationName.getJSONObject(0);
		JSONArray arr = loc.getJSONArray("daySlot");
		JSONObject obj = arr.getJSONObject(0);
		daySlotId = obj.getString("daySlotId");
		dayName = loc.getString("value");
		System.out.println("daySlotId  "+daySlotId);
		Assert.assertNotNull(daySlotId, "DaySlotId  ID is null");
		// Verify the Value using assertion		 
	}	
}
