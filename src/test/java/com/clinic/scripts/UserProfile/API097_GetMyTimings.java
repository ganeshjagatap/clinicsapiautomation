package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API097_GetMyTimings extends TestBase {
	public static String dateSlotId1=null;
	public static String locationId1=null;
	@Test(priority=212)
	public void getMyTimings_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.getMyTimings;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> userDetails = ParamGenerator.getUpdateQualification(token, username, clinicId, orgId);

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		 JSONArray e=responseJson_object.getJSONArray("myTimings");
		 JSONObject r=e.getJSONObject(1);
		String locationName=r.getString("locationName");
		dateSlotId1 =r.getString("dateSlotId");
		locationId1 = r.getString("locationId");
		System.out.println("dateSlotId"+dateSlotId1);
		System.out.println("locationName  "+locationName);
		Assert.assertNotNull(locationName, "locationName is null");
	}
}
