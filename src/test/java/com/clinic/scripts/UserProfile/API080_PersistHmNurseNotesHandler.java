package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.Consultation.API153_BookConsultation;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API080_PersistHmNurseNotesHandler extends TestBase{
	
	public static String nurseNoteId = null;
	@Test(priority =0)
	public void PersistHmNotesHandler_SuccessUsecase() throws Exception {
	
		mailUrl = Envirnonment.env + Constants.persistHmNurseNotesHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String siteId = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		String doctorId = clinicId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorUserProfile.persistHmNurseNotesHandler_Param(clinicId, username, token, orgId, siteId, locationId, patientId, API153_BookConsultation.visitId, "", "nurseNotes", doctorId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		nurseNoteId=responseJson_object.getString("nurserNoteId");
		System.out.println("NurserNoteId"+nurseNoteId);
		Assert.assertNotNull(nurseNoteId, "Nurse Note Id is null");
		// Verify the Value using assertion		 
	}
}
