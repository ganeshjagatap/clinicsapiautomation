package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API087_DeleteUserPublication extends TestBase{

	@Test(priority =81)
	public void DeleteUserPublication_SuccessUsecase() throws Exception {
		
		mailUrl = Envirnonment.env + Constants.deleteUserPublication;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();
			String publicationId = API086_ListUserPublication.publicationId;
			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
			List<NameValuePair> signinparams = ParamGeneratorUserProfile.deleteUserPublication_Param(token, username, clinicId, orgId, publicationId);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			Assert.assertEquals(responseJson_object.getString("successMessage"), "Successfully deleted publication details");
			}	
}
