package com.clinic.scripts.UserProfile;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API102_UpdateUserPublication extends TestBase{
	
	@Test(priority =80)
	public void updateUserPublication_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.updateUserPublication;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String publicationId = API086_ListUserPublication.publicationId;
		List<NameValuePair> userProfile = ParamGeneratorUserProfile.addUserPublication_Param(token, username, clinicId, orgId);
		JSONObject param=ParamGeneratorUserProfile.getJSONUpdateUserPublication(publicationId, API085_AddUserPublication.abstractName, API085_AddUserPublication.journalName, API085_AddUserPublication.topic, API085_AddUserPublication.university, date1);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);	 
		 Assert.assertEquals(responseJson_object.getString("successMessage"), "Successfully updated publication details");
	//////	
	}
}
