package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;
public class API157_FetchPatientTestsOrScansInfoHandler extends TestBase {
	
	public static String serviceOrderHdrId = null;
	public static String serviceOrderDtlId = null;
	@Test(priority =116)
	public void FetchPatientTestsOrScanInfoHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchPatientTestsOrScansInfoHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String siteId = API006_HomeDoctor.siteId; 
		String locationId = API006_HomeDoctor.locationId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchPatientTestsOrScansInfoHandler_Param(clinicId, username, token, orgId, siteId, locationId, patientId, visitId,scope);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray arr = responseJson_object.getJSONArray("testOrScan");
		JSONObject obj = arr.getJSONObject(0);
		serviceOrderDtlId = obj.getString("serviceOrderDtlId");
		serviceOrderHdrId= responseJson_object.getString("serviceOrderHdrId");
		System.out.println("ServiceOrderHdrId  "+serviceOrderHdrId);
		Assert.assertNotNull(serviceOrderHdrId, "ServiceOrderHdrId  is null");
	}
}
