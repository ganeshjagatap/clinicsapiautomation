package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API004_PatienSearch;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API163_generatePrescriptionPDFHandler extends TestBase {
	
	@Test(priority=148)
	public void generatePrescriptionPDFHandler_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.generatePrescriptionPDFHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String uhid = patientId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.generatePrescriptionPDFHandler_param(clinicId, username, token, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, patientId, uhid, visitId, currentVisit, API139_FetchPharmacyListHandler.pharmacyId,doctorName, API004_PatienSearch.mobileNo,digitalSignaturePath,digitalSignaturePassword);
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 String successMessage=responseJson_object.getString("successMessage");
		//String userId=y.getString("userId");
		 System.out.println("successMessage  "+successMessage);
		 Assert.assertNotNull(successMessage,"successMessage is null");
	}
}
