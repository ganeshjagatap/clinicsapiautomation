package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;


public class API135_PersistDoctorSpecifiedTestOrScanInfoHandler extends TestBase {
	
	@Test(priority =139)
	public void PersistDoctorSpecifiedTestOrScanInfoHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.persistDoctorSpecifiedTestOrScanInfoHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String site = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> updateQual = ParamGeneratorConsultation.PersistDoctorSpecifiedTestOrScanInfoHandler_Param(clinicId, username, token, orgId, API006_HomeDoctor.siteId, locationId,patientId, visitId, API157_FetchPatientTestsOrScansInfoHandler.serviceOrderHdrId);
		JSONObject param=ParamGeneratorConsultation.getJSONPersistDoctorSpecifiedTestOrScanInfoHandler(API157_FetchPatientTestsOrScansInfoHandler.serviceOrderDtlId, API136_SearchTestOrScanHandler.serviceId, API136_SearchTestOrScanHandler.serviceName, note, operation1);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, updateQual, param);			
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//String noteId=responseJson_object.getString("nurserNoteId");
		String serviceOrderHdrId=responseJson_object.getString("serviceOrderHdrId");
		System.out.println("serviceOrderHdrId  "+serviceOrderHdrId);
		Assert.assertNotNull(serviceOrderHdrId, "Service Order Hdr Id is null");
	}
}
