package com.clinic.scripts.Consultation;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;


public class API169_RegistrationBookConsultation extends TestBase{
	
	@Test(priority =147)
	public void RegistrationBookConsultation_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.registrationBookConsultation;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String doctorId = clinicId;
		Random rand = new Random();
		 int num1, num2, num3;
		 num1 = rand.nextInt (843) + 100;
		 num2 = rand.nextInt (643) + 100;
		 num3 = rand.nextInt (999) + 1000;
		 String randomName = RandomStringUtils.randomAlphabetic(7);
		String mobileNo = new Integer(num1).toString()+new Integer(num2).toString()+new Integer(num3).toString();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGeneratorConsultation.registrationBookConsultation_Param(token, clinicId, username, orgId, doctorId, API006_HomeDoctor.startTime, API006_HomeDoctor.endTime, API006_HomeDoctor.date1, API006_HomeDoctor.dateSlotId, API006_HomeDoctor.slotId, "Fever", API006_HomeDoctor.locationId, API006_HomeDoctor.siteId, locationName, orgName, doctorName);
		JSONObject param=ParamGeneratorConsultation.getJSONRegistrationBookConsultation("", "Ms.",randomName, "vani", gender,"B+", "14-12-1975", "", "", mobileNo, randomName+"@gmail.com", "kl nagar", "", "India", "Tamilnadu", "Chennai", "","", "Y", "N", "", "N",
				"Hindu", "", "", "", "", "", "", "SINGLE", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", isDuplicateCheckRequired, isPatientMappingRequired, role);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String visitId = responseJson_object.getString("visitId");
		System.out.println("visitId  "+visitId);
		Assert.assertNotNull(visitId, "visit Id  is null");
	}	
}
