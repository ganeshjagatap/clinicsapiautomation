package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API118_FetchExaminationDtlsHandler extends TestBase {
	
	public static String templateSectionId = null;
	public static String templateSectionStatus = null;
	public static String templateId = null;
	@Test(priority =112)
	public void FetchExaminationDtlsHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchExaminationDtlsHandler;	
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchExaminationDtlsHandler_Param(username, clinicId, token, orgId, patientId, visitId, doctorSpeciality);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray arr = responseJson_object.getJSONArray("templateSection");
		JSONObject obj = arr.getJSONObject(0);
		templateSectionId = obj.getString("templateSectionId");
		templateSectionStatus = obj.getString("templateSectionStatus");
		templateId = responseJson_object.getString("templateId");
		System.out.println("Template  "+templateId);
		Assert.assertNotNull(templateId, "Template Id is null");
	}
}
