package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API171_FetchConsultationSummarySectionPrefHandler extends TestBase {
	@Test(priority =160)
	public void FetchPatientPrescription_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchConsultationSummarySectionPrefHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.FetchConsultationSummarySectionPrefHandler_Param(token, clinicId, username, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String userId = responseJson_object.getString("userId");
		System.out.println("userId  "+userId);
		Assert.assertNotNull(userId, "userId  is null");
	}

}
