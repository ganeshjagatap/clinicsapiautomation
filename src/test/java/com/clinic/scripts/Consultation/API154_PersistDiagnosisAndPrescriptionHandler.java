package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;


public class API154_PersistDiagnosisAndPrescriptionHandler extends TestBase{
	
	@Test(priority =119)
	public void PersistDiagnosisAndPrescriptionHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.persistDiagnosisAndPrescriptionHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String siteId = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> updateQual = ParamGeneratorConsultation.PersistDiagnosisAndPrescriptionHandler_Param(clinicId, username, token, orgId, siteId, locationId, patientId, visitId, currentStatus);
		JSONObject param=ParamGeneratorConsultation.getJSONPersistDiagnosisAndPrescriptionHandler("", API121_SearchDiagnosisHandler.diagnosisId, "", API121_SearchDiagnosisHandler.diagnosisName,stage, date1, status, date1, operation1, API153_BookConsultation.visitId, null, "", "", API114_SearchMedicineHandler.drugCode,
				API114_SearchMedicineHandler.drugName,API122_SearchFrequencyHandler.frequency, duration, date1, "", operation1, dosage, API114_SearchMedicineHandler.drugType, "", "");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, updateQual, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//String noteId=responseJson_object.getString("nurserNoteId");
		Assert.assertEquals(responseJson_object.getString("responseMsg"), "Diagnosis and Prescription details saved successfully");
	}
}
