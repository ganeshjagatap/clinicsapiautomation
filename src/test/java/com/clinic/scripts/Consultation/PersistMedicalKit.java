package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class PersistMedicalKit extends TestBase{
	public static String medicalKitName =null;
	@Test(priority=4000)
	public void PersistMedicalKit_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.persistMedicalKit;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		medicalKitName ="DEMO KIT - "+RandomStringUtils.randomAlphanumeric(5);
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.PersistMedicalKit_Param(username, token, clinicId, clinicId,orgId, medicalKitName,"CREATE","Y");
		JSONObject param=ParamGeneratorConsultation.getJSONPersistMedicalKit(API115_SearchSymptomsHandler.symptomId, API115_SearchSymptomsHandler.symptomName,operation1,API136_SearchTestOrScanHandler.serviceId,API136_SearchTestOrScanHandler.serviceName,note,
				API121_SearchDiagnosisHandler.diagnosisId,null,API121_SearchDiagnosisHandler.diagnosisName,API114_SearchMedicineHandler.drugName,API114_SearchMedicineHandler.drugCode,API114_SearchMedicineHandler.drugType,"",API122_SearchFrequencyHandler.frequency,duration,dosageAdvice,"");
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage=responseJson_object.getString("successMessage");
		//String userId=y.getString("userId");
		System.out.println("responseMsg  "+successMessage);
		Assert.assertEquals(successMessage,"Medical kit created successfully.");
	}
	
}
