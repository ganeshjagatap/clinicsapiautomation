package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API136_SearchTestOrScanHandler extends TestBase{
	
	public static String serviceId = null;
	public static String serviceName = null;
	@Test(priority =106)
	public void SearchTestOrScanHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.searchTestOrScanHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.searchTestOrScanHandler_Param(clinicId, username, token, orgId, searchText);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray arr = responseJson_object.getJSONArray("testOrScan");
		JSONObject obj = arr.getJSONObject(0);
		serviceId = obj.getString("serviceId");
		serviceName = obj.getString("serviceName");
		System.out.println("Service  "+serviceId);
		Assert.assertNotNull(serviceId, "Service Id is null");
	}
}
