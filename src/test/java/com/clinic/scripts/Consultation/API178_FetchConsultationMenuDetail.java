package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API178_FetchConsultationMenuDetail extends TestBase{
	public static JSONArray consultMenu = new JSONArray();
	@Test(priority = 1)
	public void FetchConsultationMenu_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchConsultationMenuDetail;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchConsultationMenuDetail_Param(orgId, token, clinicId, username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		consultMenu = responseJson_object.getJSONArray("consultationMenuDetailList");
		String successMessage = responseJson_object.getString("successMessage");
		System.out.println("successMessage  "+successMessage);
		Assert.assertNotNull(successMessage, "successMessage  is null");
	}
	
	
}
