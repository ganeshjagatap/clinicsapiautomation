package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API117_FetchExaminationTemplateHandler extends TestBase {
	@Test(priority =111)
	public void FetchExaminationTemplateHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchExaminationTemplateHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchExaminationTemplateHandler_Param(username, clinicId, token, API116_SearchExaminationTemplateHandler.templateId, visitId, orgId, patientId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String temId = responseJson_object.getString("templateId");
		System.out.println("Template  "+temId);
		Assert.assertNotNull(temId, "Template Id is null");	
	}
}
