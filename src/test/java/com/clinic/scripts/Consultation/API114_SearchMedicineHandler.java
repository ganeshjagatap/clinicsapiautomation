package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API114_SearchMedicineHandler extends TestBase{
	
	public static String drugCode = null;
	public static String drugType = null;
	public static String drugName = null;
	@Test(priority =102)
	public void SearchMedicineHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.searchMedicineHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.searchMedicineHandler_Param(clinicId, username, token, orgId, searchText);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray arr = responseJson_object.getJSONArray("medicine");
		JSONObject obj = arr.getJSONObject(0);
		drugCode = obj.getString("drugCode");
		drugName = obj.getString("drugName");
		drugType = obj.getString("drugType");
		System.out.println("DrugCode  "+drugCode);
		Assert.assertNotNull(drugCode, "Drug Code is null");
	}	
}
