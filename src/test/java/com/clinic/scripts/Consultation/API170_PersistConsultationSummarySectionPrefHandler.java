package com.clinic.scripts.Consultation;

import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;


public class API170_PersistConsultationSummarySectionPrefHandler extends TestBase{
	@Test(priority =161)
	public void PersistConsultationSummarySectionPrefHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.persistConsultationSummarySectionPrefHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		HashMap<String, String> summary = new HashMap<String, String>();
		summary.put("1", "Medical History");
		summary.put("2","Vitals");
		summary.put("3", "Lab Tests");
		summary.put("4","Symptoms");
		summary.put("5", "Diagnosis");
		summary.put("6","Prescription");
		summary.put("7", "General Advice");
		summary.put("8","Follow up");
		summary.put("9", "Referral");
		
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.PersistConsultationSummarySectionPrefHandler_Param(token, clinicId, username, orgId);
		JSONObject param=ParamGeneratorConsultation.getJSONPersistConsultationSummarySectionPrefHandler(summary);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String responseMsg = responseJson_object.getString("responseMsg");
		System.out.println("responseMsg  "+responseMsg);
		Assert.assertNotNull(responseMsg, "responseMsg  is null");
	}
}
