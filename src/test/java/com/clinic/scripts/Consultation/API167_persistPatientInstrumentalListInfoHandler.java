package com.clinic.scripts.Consultation;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API167_persistPatientInstrumentalListInfoHandler extends TestBase {
	
	@Test(priority=152)
	public void persistPatientInstrumentalListInfoHandler_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.persistPatientInstrumentalListInfoHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		String barcode  = API164_persistPatientInstrumentalInfoHandler.barCode; 
		System.out.println("barcode"+barcode);
		System.out.println("begin");
		String randomName=RandomStringUtils.randomAlphabetic(4);
		List<NameValuePair> userDetails = ParamGeneratorConsultation.persistPatientInstrumentalListInfoHandler_param(orgId, token, username, clinicId);
		JSONObject param=ParamGeneratorConsultation.getJSONpersistPatientInstrumentalListInfoHandler(randomName+API136_SearchTestOrScanHandler.serviceName,barcode , date1, randomName+labName, parameter, parameterReference, parameterUnit, parameterValue, patientInstrumentalId,parameterComment);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String e=responseJson_object.getString("summaryPdfFilePath");
		//String userId=y.getString("userId");
		System.out.println("summaryPdfFilePath  "+e);
		Assert.assertNotNull(e,"summaryPdfFilePath is null");	
	}
}
