package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class fetchVitalParameterView extends TestBase{
	
	public static JSONArray vitalParameters;
	
	@Test(priority =1)
	public void updateVitalParameterView_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchVitalParameterView;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.updateVitalParameterView_Param(orgId, token, clinicId, username);
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		vitalParameters = responseJson_object.getJSONArray("vitalParameters");
		System.out.println("vitalParameters  "+vitalParameters);
		Assert.assertNotNull(vitalParameters, "successMessage  is null");
	}
	
}
