package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class updateVitalParameterView extends TestBase{
	
	@Test(priority =2)
	public void updateVitalParameterView_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.updateVitalParameterView;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.updateVitalParameterView_Param(orgId, token, clinicId, username);
		JSONObject param = ParamGeneratorConsultation.getJSONupdateVitalParameterView(fetchVitalParameterView.vitalParameters);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String responseMsg = responseJson_object.getString("responseMsg");
		System.out.println("responseMsg  "+responseMsg);
		Assert.assertNotNull(responseMsg, "responseMsg  is null");
	}
	
}
