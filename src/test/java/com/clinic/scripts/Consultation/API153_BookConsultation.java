package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API153_BookConsultation extends TestBase{
	
	@Test(priority=100)
	public void bookConsultation_SuccessUsecase() throws Exception{		
	mailUrl = Envirnonment.env + Constants.bookConsultation;
	System.out.println("mailUrl "+mailUrl);
	String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
	RequestUtil requestUtil = new RequestUtil();
	// Get request for the session creation first.
	HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
	System.out.println("begin");
	System.out.println("SlotId"+API006_HomeDoctor.slotId+"   "+"DateSlotId"+API006_HomeDoctor.dateSlotId+"  "+"StartTime"+API006_HomeDoctor.startTime+"  "+"EndTime"+API006_HomeDoctor.endTime);
	List<NameValuePair> bookRegs = ParamGeneratorConsultation.bookConsultation_Param(token, username1, clinicId, orgId, "Doctor"); 
	JSONObject param=ParamGeneratorConsultation.getJSONBookConsultation(patientId, API006_HomeDoctor.dateSlotId, API006_HomeDoctor.slotId, API006_HomeDoctor.date1, API006_HomeDoctor.startTime, API006_HomeDoctor.endTime, API006_HomeDoctor.locationId, API006_HomeDoctor.siteId, clinicId, "Fever", "Banglore", orgName, "Mala");
	// Post request to the URL with the param data
	HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, bookRegs, param);
	responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
	visitId = responseJson_object.getString("visitId");
	appointmentId = responseJson_object.getString("appointmentId");
	System.out.println("VisitId  "+visitId);
	Assert.assertNotNull(visitId, "VisitId  is null");
	}
}
