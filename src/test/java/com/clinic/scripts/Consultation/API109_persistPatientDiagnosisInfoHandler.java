package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API109_persistPatientDiagnosisInfoHandler extends TestBase{
	
	@Test(priority =115)
	public void PersistPatientDiagnosisInfoHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.persistPatientDiagnosisInfoHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String site = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> updateQual = ParamGeneratorConsultation.persistPatientDiagnosisInfoHandler_Param(clinicId, username, token, orgId, site, locationId, patientId, visitId);
		JSONObject param=ParamGeneratorConsultation.getJSONPersistPatientDiagnosisInfoHandler(API128_FetchDiagnosisHandler.patientDiagnosisId, API121_SearchDiagnosisHandler.diagnosisId, null, API121_SearchDiagnosisHandler.diagnosisName, stage, API006_HomeDoctor.date1, status, null, operation, API153_BookConsultation.visitId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, updateQual, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//String noteId=responseJson_object.getString("nurserNoteId");
		Assert.assertEquals(responseJson_object.getString("responseMsg"), "Diagnosis details saved successfully");	
		// Verify the Value using assertion		 
	}
}
