package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API119_FetchExaminationTemplateItemsHandler extends TestBase {
	
	public static String templateItemId = null;
	public static String itemName = null;
	@Test(priority =113)
	public void FetchExaminationTemplateItemsHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchExaminationTemplateItemsHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchExaminationTemplateItemsHandler_Param(username, clinicId, token,API118_FetchExaminationDtlsHandler.templateId, API118_FetchExaminationDtlsHandler.templateSectionId, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, patientId, visitId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray arr = responseJson_object.getJSONArray("templateItem");
		JSONObject obj = arr.getJSONObject(0);
		templateItemId = obj.getString("templateItemId");
		itemName = obj.getString("itemName");
		System.out.println("Template  "+templateItemId);
		Assert.assertNotNull(templateItemId, "Template Item Id is null");
	}
}
