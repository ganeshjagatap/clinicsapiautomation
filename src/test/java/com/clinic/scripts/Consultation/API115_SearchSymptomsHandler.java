package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API115_SearchSymptomsHandler extends TestBase{
	
	public static String symptomId = null;
	public static String symptomName = null;
	@Test(priority =103)
	public void SearchSymptomsHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.searchSymptomsHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.searchSymptomsHandler_Param(clinicId, username, token, orgId, role, searchText);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray arr = responseJson_object.getJSONArray("symptom");
		JSONObject obj = arr.getJSONObject(0);
		symptomId = obj.getString("symptomId");
		symptomName = obj.getString("symptomName");
		System.out.println("Symptom  "+symptomId);
		Assert.assertNotNull(symptomId, "Symptom Id is null");
	}	
}
