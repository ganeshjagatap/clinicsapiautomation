package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API161_GenerateModuleWiseSummaryPDFHandler extends TestBase{
	@Test(priority =143)
	public void GenerateModuleWiseSummaryPDFHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.generateModuleWiseSummaryPDFHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String siteId = API006_HomeDoctor.siteId; 
		String locationId = API006_HomeDoctor.locationId;
		String uhid = patientId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.generateModuleWiseSummaryPDFHandler_Param(clinicId, username, token, orgId, siteId, locationId, patientId, uhid, visitId,currentVisit, printVitals, printSymptoms, printExamination,printDiagnosis,printPrescription, printGenericAdvice, printFollowUp, printTests, refferal, printMedicalHistory);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String filePath = responseJson_object.getString("summaryPdfFilePath");
		System.out.println("SummaryPdfFilePath  "+filePath);
		Assert.assertNotNull(filePath, "SummaryPdfFilePath  is null");
	}	
}
