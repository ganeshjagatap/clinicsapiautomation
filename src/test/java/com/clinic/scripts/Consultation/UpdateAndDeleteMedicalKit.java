package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class UpdateAndDeleteMedicalKit extends TestBase{
	@Test(priority=4085)
	public void UpdatePersistMedicalKit_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.persistMedicalKit;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.Update_DeleteMedicalKit_Param(username, token, clinicId, clinicId, orgId, medicalKitId,"UPDATE","Y");
		JSONObject param=ParamGeneratorConsultation.getJSONUpdate_DeleteMedicalKit(medicalKitSymptomId, API115_SearchSymptomsHandler.symptomId, API115_SearchSymptomsHandler.symptomName,"UPDATE",medicalKitLabTestId,API136_SearchTestOrScanHandler.serviceId,API136_SearchTestOrScanHandler.serviceName,note,API121_SearchDiagnosisHandler.diagnosisId,medicalKitDiagnosisId,null,API121_SearchDiagnosisHandler.diagnosisName,medicalKitPrescriptionId,API114_SearchMedicineHandler.drugName,
				API114_SearchMedicineHandler.drugCode,API114_SearchMedicineHandler.drugType,"",API122_SearchFrequencyHandler.frequency,duration,dosageAdvice, "");
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage=responseJson_object.getString("successMessage");
		//String userId=y.getString("userId");
		System.out.println("responseMsg  "+successMessage);
		Assert.assertEquals(successMessage,"Medical kit updated successfully.");
	}
	
}
