package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API150_FetchOrLoadTestOrScanTemplatesForUser extends TestBase{
	
	@Test(priority =137)
	public void FetchPrescriptionHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchOrLoadTestOrScanTemplatesForUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchOrLoadTestOrScanTemplatesForUser_Param(clinicId, username, token, orgId,templateName);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray arr = responseJson_object.getJSONArray("testOrScanList");
		JSONObject obj = arr.getJSONObject(0);
		String serviceId = obj.getString("serviceId");
		System.out.println("ServiceId  "+serviceId);
		Assert.assertNotNull(serviceId, "ServiceId  is null");		
	}
}
