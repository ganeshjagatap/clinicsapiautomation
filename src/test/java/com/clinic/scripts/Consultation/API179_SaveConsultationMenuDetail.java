package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API179_SaveConsultationMenuDetail extends TestBase{
	
	@Test(priority =1)
	public void PersistConsultationSummarySectionPrefHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.saveConsultationMenuDetail;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		JSONArray menu = API178_FetchConsultationMenuDetail.consultMenu;
		for(int i =0;i<menu.length();i++){
			JSONObject objList = menu.getJSONObject(i);
			if(i == 2){
				String menuOrder = (String) objList.get("menuOrder");
				Integer order = Integer.parseInt(menuOrder);
				order++;
				menu.getJSONObject(i).put("menuOrder",order.toString());
			}
			else if(i == 3){
				String menuOrder = (String) objList.get("menuOrder");
				Integer order = Integer.parseInt(menuOrder);
				order--;
				menu.getJSONObject(i).put("menuOrder",order.toString());
			}
		}
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.saveConsultationMenuDetail_Param(orgId, token, clinicId, username);
		JSONObject param = ParamGeneratorConsultation.getJSONsaveConsultationMenuDetail(menu);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String successMessage = responseJson_object.getString("successMessage");
		System.out.println("successMessage  "+successMessage);
		Assert.assertNotNull(successMessage, "successMessage  is null");
	}
	
}
