package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API143_FetchPatientInstrumentalInfoHandler extends TestBase{
	@Test(priority =153)
	public void FetchPatientPrescription_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchPatientInstrumentalInfoHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String siteId = API006_HomeDoctor.siteId; 
		String locationId = API006_HomeDoctor.locationId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchPatientInstrumentalInfoHandler_Param(clinicId, locationId, orgId,patientId, visitId, siteId,token, username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		//JSONObject ob = responseJson_object.getJSONObject("manualRecords");
		//JSONObject obj = ob.getJSONObject("cartridgeRecord");
		JSONArray arr1 = responseJson_object.getJSONArray("manualRecords");
		JSONObject obj1 = arr1.getJSONObject(0);
		String reportName = obj1.getString("reportName");
		System.out.println("reportName  "+reportName);
		Assert.assertNotNull(reportName, "reportName is null");
	}
}
