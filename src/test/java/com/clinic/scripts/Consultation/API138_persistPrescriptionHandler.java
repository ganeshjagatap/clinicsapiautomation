package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API138_persistPrescriptionHandler extends TestBase {
	
	@Test(priority=124)
	public void persistPrescriptionHandler_SuccessUsecase() throws Exception{		
		mailUrl = Envirnonment.env + Constants.persistPrescriptionHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.persistPrescriptionHandler_param(null, clinicId, username, token, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, patientId, visitId, null, null);
		JSONObject param=ParamGeneratorConsultation.getJSONpersistPrescriptionHandler(null, null, API114_SearchMedicineHandler.drugCode, API114_SearchMedicineHandler.drugName, route, frequencyMor, frequencyAn,frequencyNight,dosage,durationNo,durationUnit, date1, sos, af, bf, wf, emptyStomach, beforeSleeping,  freqSpecInstruction, API114_SearchMedicineHandler.drugType, orgName);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.post	JSON_Request(mailUrl, userDetails);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 String responseMsg=responseJson_object.getString("responseMsg");
		 //String userId=y.getString("userId");
		 System.out.println("responseMsg  "+responseMsg);
		 Assert.assertNotNull(responseMsg,"responseMsg is null");	
	}
}
