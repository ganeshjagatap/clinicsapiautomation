package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;
public class API125_UploadPatientTestOrScanReportHandler extends TestBase{
	
	@Test(priority =155)
	public void UploadPatientTestOrScanReportHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.uploadPatientTestOrScanReportHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String site = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		System.out.println("location iD"+locationId);
		System.out.println("site iD"+site);
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.uploadPatientTestOrScanReportHandler_Param(clinicId, username, token, orgId, site, locationId, patientId, visitId,reportPath, testOrScanDtlId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray arr =responseJson_object.getJSONArray("patientReport");			
		JSONObject obj = arr.getJSONObject(0);
		reportId = obj.getString("reportId");
		System.out.println("ReportId  "+reportId);
		Assert.assertNotNull(reportId, "Report Id is null");
	}
}
