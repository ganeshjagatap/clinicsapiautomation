package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API162_generateConsultingPrescriptionPdfHandler extends TestBase {
	
	@Test(priority=144)
	public void generateConsultingPrescriptionPdfHandler_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.generateConsultingPrescriptionPdfHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		String uhid = patientId;
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.generateConsultingPrescriptionPdfHandler_param(patientId, API006_HomeDoctor.siteId, token,currentVisit, clinicId, API006_HomeDoctor.locationId, visitId, uhid, orgId, username);
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String summaryPdfFilePath=responseJson_object.getString("summaryPdfFilePath");
		System.out.println("summaryPdfFilePath  "+summaryPdfFilePath);
		Assert.assertNotNull(summaryPdfFilePath,"summaryPdfFilePath is null");	
	}
}
