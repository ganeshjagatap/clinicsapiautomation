package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API176_fetchPatientLabTestsTrendsData extends TestBase {
	
	@Test(priority=0)
	public void API176_fetchPatientLabTestsTrendsData_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.fetchPatientLabTestsTrendsData;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		JSONArray e = new JSONArray();
		e.put("1");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.fetchPatientLabTestsTrendsData_Param(clinicId,username,token,"209","53702");
		JSONObject param=ParamGeneratorConsultation.getJSONfetchPatientLabTestsTrendsData(e,"Urine - Test For Alkapotonuria ");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		System.out.println(responseJson_object.toString());
		
		JSONArray innerClassForTrendsData=responseJson_object.getJSONArray("innerClassForTrendsData");
		
		System.out.println("successMessage  "+innerClassForTrendsData);
			
	}
}
