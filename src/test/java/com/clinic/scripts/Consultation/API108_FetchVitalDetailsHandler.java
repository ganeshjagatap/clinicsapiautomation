package com.clinic.scripts.Consultation;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API108_FetchVitalDetailsHandler extends TestBase{
	public static String patientVisitId = null;
	@Test(priority =109)
	public void FetchVitalDetailsHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchVitalDtlsHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String siteId = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchVitalDtlsHandler_Param(clinicId, username, token, orgId, siteId, locationId, patientId, visitId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray patientVisit = responseJson_object.getJSONArray("patientVisit");
		Random r = new Random();
		int len = patientVisit.length();
		int ranNum = r.nextInt(len);
		JSONObject patientObj = patientVisit.getJSONObject(ranNum);
		patientVisitId = patientObj.getString("patientVisitId");
		//String home=responseJson_object.getString("isRegisteredForHomeMonitoring");
		System.out.println("patientVisitId  "+patientVisitId);
		Assert.assertNotNull(patientVisitId, "Patient Visit Id is null");
		// Verify the Value using assertion		 
	}
}
