package com.clinic.scripts.Consultation;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;






import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;



public class API165_PersistPatientProvisionalInfoHandler extends TestBase{
	
	@Test(priority =154)
	public void PersistPatientProvisionalInfoHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.persistPatientProvisionalInfoHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String barcode = "0"+orgId+"0"+patientId+"000"+visitId;
		FileInputStream fis = null;
		try{
		File file = new File("E:/file.pdf");
		System.out.println(file.getName());
		fis = new FileInputStream(file);
		System.out.println(fis.toString());
		byte[] fileArray = IOUtils.toByteArray(fis);
		String provisionalPdf = Base64.encodeBase64String(fileArray);
		System.out.println("PdfString:"+provisionalPdf);
		List<NameValuePair> pdf = ParamGeneratorConsultation.persistPatientProvisionalInfoHandler_Param(barcode, provisionalPdf, fileType);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, pdf);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//JSONObject loc = e.getJSONObject(0);
		Assert.assertEquals(responseJson_object.getString("successMsg"), "Provisional Data stored successfully");
		}
		catch (Exception e) {	
			e.printStackTrace();
		} finally {
			try {
				if(fis!=null) { fis.close(); }
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
