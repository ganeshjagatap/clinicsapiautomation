package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API127_FetchDiagnosisAndPrescriptionHandler extends TestBase{
	
	public static String prescriptionHdrId = null;
	@Test(priority =118)
	public void FetchDiagnosisAndPrescriptionHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchDiagnosisAndPrescriptionHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String site = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchDiagnosisAndPrescriptionHandler_Param(clinicId, username, token, orgId, site, locationId, patientId, visitId,currentVisit);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray arr = responseJson_object.getJSONArray("templateName");
		String temName = arr.getString(0);
		System.out.println("temName  "+temName);
		Assert.assertNotNull(temName, "temName Id is null");
	}	
}
