package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API004_PatienSearch;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API112_CreateOrUpdatePatientVisitHandler extends TestBase{
	
	@Test(priority =110)
	public void CreateOrUpdatePatientVisitHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.createOrUpdatePatientVisitHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String site = API006_HomeDoctor.siteId;
		String locationId = API006_HomeDoctor.locationId;
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> updateQual = ParamGeneratorConsultation.createOrUpdatePatientVisitHandler_Param(clinicId, username, token, orgId, API006_HomeDoctor.siteId, locationId, patientId,appointmentId
				, role, currentStatus,visitId,operation, API004_PatienSearch.mobileNo, API004_PatienSearch.emailId, doctorTitle, doctorName, orgName, printVitals,printSymptoms, printExamination, printDiagnosis, printPrescription,printGenericAdvice, printFollowUp, printTests, refferal);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, updateQual);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//String noteId=responseJson_object.getString("nurserNoteId");
		String visitId=responseJson_object.getString("visitId");
		System.out.println("visitId  "+visitId);
		Assert.assertNotNull(visitId, "Patient Visit Id is null");
		// Verify the Value using assertion		 
	}	
}
