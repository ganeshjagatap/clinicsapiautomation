package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API166_savePatientTestListHandler extends TestBase {

	@Test(priority=150)
	public void savePatientTestListHandler_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.savePatientTestListHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.savePatientTestListHandler_param(patientId, orgId, token, username, clinicId,visitId,doctorName, null);
		JSONObject param=ParamGeneratorConsultation.getJSONsavePatientTestListHandler("Test");
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String patientListPdfFilePath=responseJson_object.getString("patientListPdfFilePath");
		System.out.println("patientListPdfFilePath  "+patientListPdfFilePath);
		Assert.assertNotNull(patientListPdfFilePath,"patientListPdfFilePath is null");
	}
}
