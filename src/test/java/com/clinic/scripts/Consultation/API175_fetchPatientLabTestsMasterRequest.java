package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API175_fetchPatientLabTestsMasterRequest extends TestBase {
	
	@Test(priority=0)
	public void API177_fetchPatientDtlsForVCConsultation_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.fetchPatientLabTestsMasterRequest;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.fetchPatientLabTestsTrendsData_Param(clinicId,username,token,orgId,patientId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		System.out.println(responseJson_object.toString());
		//JSONObject y=e.getJSONObject(0);
		//String userName=y.getString("userName");
		//String userId=responseJson_object.getString("successMessage");
		//System.out.println("successMessage  "+userId);
		//Assert.assertNotNull(userId,"successMessage is null");	
	}
}
