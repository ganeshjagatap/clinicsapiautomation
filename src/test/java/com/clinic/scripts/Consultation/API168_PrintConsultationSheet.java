package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;
public class API168_PrintConsultationSheet extends TestBase{
	
	@Test(priority =146)
	public void PrintConsultationSheet_SuccessUsecase() throws Exception {
		
		mailUrl = Envirnonment.env + Constants.printConsultationSheet;
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();
			String siteId = API006_HomeDoctor.siteId; 
			String locationId = API006_HomeDoctor.locationId;
			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
			List<NameValuePair> signinparams = ParamGeneratorConsultation.printConsultationSheet_Param(patientId, siteId, token, clinicId, locationId, visitId, clinicId, orgId, username, currentStatus, role);
			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
			//JSONArray arr = responseJson_object.getJSONArray("deviceInfo");
			//JSONObject obj = arr.getJSONObject(0);
			String filePath = responseJson_object.getString("filePath");
			System.out.println("FilePath  "+filePath);
			Assert.assertNotNull(filePath, "FilePath  is null");
			
	}
}
