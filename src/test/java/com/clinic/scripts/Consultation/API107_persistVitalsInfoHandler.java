package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API107_persistVitalsInfoHandler extends TestBase {
	
	@Test(priority=108)
	public void persistVitalsInfoHandler_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.persistVitalsInfoHandler;	
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.persistVitalsInfoHandler_param(clinicId, username, token, roleId, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, patientId, visitId, currentStatus, API113_SearchAllergyHandler.allergyName, persistVitals, API115_SearchSymptomsHandler.symptomName);
		JSONObject param=ParamGeneratorConsultation.getJSONpersistVitalsInfoHandler(null, API113_SearchAllergyHandler.allergyId, null, API113_SearchAllergyHandler.allergyName, orgName, null, null, null, null, null, API115_SearchSymptomsHandler.symptomId, API115_SearchSymptomsHandler.symptomName, null, null);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.post	JSON_Request(mailUrl, userDetails);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 String responseMsg=responseJson_object.getString("responseMsg");
		//String userId=y.getString("userId");
		System.out.println("responseMsg  "+responseMsg);
		Assert.assertNotNull(responseMsg,"responseMsg is null");
	}
}
