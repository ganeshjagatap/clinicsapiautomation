package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API116_SearchExaminationTemplateHandler extends TestBase{
	public static String templateId = null;
	public static String templateName = null;
	@Test(priority =107)
	public void SearchExaminationTemplateHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.searchExaminationTemplateHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.searchExaminationTemplateHandler_Param(username, clinicId, token, orgId, searchText);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray loc = responseJson_object.getJSONArray("template");
		JSONObject obj = loc.getJSONObject(0);
		templateId = obj.getString("templateId");
		templateName = obj.getString("templateName");
		System.out.println("Template  "+templateId);
		Assert.assertNotNull(templateId, "Template Id is null");		
	}
}
