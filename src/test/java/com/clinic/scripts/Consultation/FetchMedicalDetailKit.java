package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class FetchMedicalDetailKit extends TestBase{
	@Test(priority =4063)
	public void FetchMedicalDetailKit_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchMedicalDetailKit;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.FetchMedicalDetailKit_Param(orgId, token, clinicId, username,medicalKitId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray symarr = responseJson_object.getJSONArray("symptomsList");
		JSONObject symobj = symarr.getJSONObject(0);
		medicalKitSymptomId = symobj.getString("medicalKitSymptomId");
		
		JSONArray diaarr = responseJson_object.getJSONArray("diagnosesList");
		JSONObject diaobj = diaarr.getJSONObject(0);
		medicalKitDiagnosisId = diaobj.getString("medicalKitDiagnosisId");
		
		JSONArray labarr = responseJson_object.getJSONArray("labTestsList");
		JSONObject labobj = labarr.getJSONObject(0);
		medicalKitLabTestId = labobj.getString("medicalKitLabTestId");
		
		JSONArray presarr = responseJson_object.getJSONArray("prescriptionList");
		JSONObject presobj = presarr.getJSONObject(0);
		medicalKitPrescriptionId = presobj.getString("medicalKitPrescriptionId");
		
		System.out.println("medicalKitSymptomId  "+medicalKitSymptomId);
		Assert.assertNotNull(medicalKitSymptomId, "medicalKitSymptomId  is null");		
	}
	
}
