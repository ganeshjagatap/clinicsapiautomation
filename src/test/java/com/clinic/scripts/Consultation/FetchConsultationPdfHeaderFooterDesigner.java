package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class FetchConsultationPdfHeaderFooterDesigner extends TestBase{
	//UserLevel
	@Test(priority=5)
	public void FetchConsultationPdfHeaderFooterDesigner_UserLevel_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchConsultationPdfHeaderFooterDesigner;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.FetchConsultationPdfHeaderFooterDesigner_Param(token, username, orgId, clinicId,isOrgLevel);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		headerFooterDesignerId = responseJson_object.getString("headerFooterDesignerId");
		System.out.println("headerFooterDesignerId  "+headerFooterDesignerId);
		if(headerFooterDesignerId == null){
			String errorCode = responseJson_object.getString("errorCode");
			Assert.assertNotNull(errorCode);
		}
		else{
			Assert.assertNotNull(headerFooterDesignerId, "headerFooterDesignerId  is null");
		}
	}
	
}
