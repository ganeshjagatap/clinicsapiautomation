package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API120_persistExaminationInfoHandler extends TestBase {
	
	@Test(priority=114)
	public void persistExaminationInfoHandler_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.persistExaminationInfoHandler;	
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.persistExaminationInfoHandler_param(clinicId, username, token, orgId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, patientId, visitId, API116_SearchExaminationTemplateHandler.templateId);
		JSONObject param=ParamGeneratorConsultation.getJSONpersistExaminationInfoHandler(API118_FetchExaminationDtlsHandler.templateSectionId, API118_FetchExaminationDtlsHandler.templateSectionStatus, API119_FetchExaminationTemplateItemsHandler.templateItemId, API119_FetchExaminationTemplateItemsHandler.itemName);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.post	JSON_Request(mailUrl, userDetails);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 String reponseMsg=responseJson_object.getString("reponseMsg");
		//String userId=y.getString("userId");
		System.out.println("reponseMsg  "+reponseMsg);
		Assert.assertNotNull(reponseMsg,"reponseMsg is null");	
	}
}
