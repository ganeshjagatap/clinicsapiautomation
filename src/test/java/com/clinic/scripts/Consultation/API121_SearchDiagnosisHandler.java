package com.clinic.scripts.Consultation;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API121_SearchDiagnosisHandler extends TestBase{
	
	public static String diagnosisId = " ";
	public static String diagnosisName = " ";
	@Test(priority =104)
	public void SearchDiagnosisHistoryHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.searchDiagnosisHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.searchDiagnosisHandler_Param(username, clinicId, token, orgId, searchText);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray arr = responseJson_object.getJSONArray("diagnosis");
		Random r = new Random();
	    int num = r.nextInt(10);
		JSONObject obj = arr.getJSONObject(num);
		diagnosisId = obj.getString("diagnosisId");
		diagnosisName = obj.getString("diagnosisName");
		System.out.println("Diagnosis  "+diagnosisId);
		Assert.assertNotNull(diagnosisId, "Diagnosis Id is null");
	}	
}
