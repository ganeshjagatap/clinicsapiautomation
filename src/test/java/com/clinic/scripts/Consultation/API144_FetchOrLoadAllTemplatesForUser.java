package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API144_FetchOrLoadAllTemplatesForUser extends TestBase {
	@Test(priority =128)
	public void FetchOrLoadAllTemplatesForUser_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchOrLoadAllTemplatesForUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchOrLoadAllTemplatesForUser_Param(clinicId, username, token, orgId, API145_savePrescriptionTemplate.templateName);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		JSONArray arr = responseJson_object.getJSONArray("prescription");
		JSONObject obj = arr.getJSONObject(0);
		String drugCode = obj.getString("drugCode");
		System.out.println("drugCode  "+drugCode);
		Assert.assertNotNull(drugCode, "drugCode  is null");
	}


}
