package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API145_savePrescriptionTemplate extends TestBase {
	
	public static String templateName = null;
	@Test(priority=125)
	public void savePrescriptionTemplate_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.savePrescriptionTemplate;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		templateName = "testTemplate";
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorConsultation.savePrescriptionTemplate_param(clinicId, username, token, orgId, API006_HomeDoctor.locationId, templateName);
		JSONObject param=ParamGeneratorConsultation.getJSONsavePrescriptionTemplate("", API114_SearchMedicineHandler.drugCode, API114_SearchMedicineHandler.drugName, route, frequencyMor, frequencyAn, frequencyNight, dosage, durationNo, durationUnit, sos, af, bf, wf, emptyStomach, beforeSleeping, freqSpecInstruction,medicineType, frequency, duration, dosageAdvice, API114_SearchMedicineHandler.drugType);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.post	JSON_Request(mailUrl, userDetails);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 String responseMsg=responseJson_object.getString("responseMsg");
		//String userId=y.getString("userId");
		System.out.println("responseMsg  "+responseMsg);
		Assert.assertNotNull(responseMsg,"responseMsg is null");	
	}
}
