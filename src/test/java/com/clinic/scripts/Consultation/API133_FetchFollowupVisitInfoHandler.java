package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API133_FetchFollowupVisitInfoHandler extends TestBase{
	@Test(priority =134)
	public void FetchFollowupVisitInfoHandler_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.fetchFollowupVisitInfoHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		String site = API006_HomeDoctor.siteId; 
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorConsultation.fetchFollowupVisitInfoHandler_Param(clinicId, username, token, orgId, site, API006_HomeDoctor.locationId, patientId, visitId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String followUp = responseJson_object.getString("followUpAfter");
		System.out.println("followUp  "+followUp);
		Assert.assertNotNull(followUp, "FollowUp  is null");
	}	
}
