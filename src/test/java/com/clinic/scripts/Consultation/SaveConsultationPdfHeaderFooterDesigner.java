package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class SaveConsultationPdfHeaderFooterDesigner extends TestBase{
	public static String templateId = "1";
	
	//UserLevel
	@Test(priority =6)
	public void SaveConsultationPdfHeaderFooterDesigner_UserLevel_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.saveConsultationPdfHeaderFooterDesigner;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = null;
		if(headerFooterDesignerId == null){
			signinparams = ParamGeneratorConsultation.SaveConsultationPdfHeaderFooterDesigner_Param(token, username, orgId, clinicId, headerType, headerTypeValue, footerType, footerTypeValue, docBannerFlag, patBannerFlag, tataBrandingFlag, disclaimerFlag, docSignatureFlag, kmcFlag, paperType,"add","", templateId, isOrgLevel);
		}
		else{
			signinparams = ParamGeneratorConsultation.SaveConsultationPdfHeaderFooterDesigner_Param(token, username, orgId, clinicId, headerType, headerTypeValue, footerType, footerTypeValue, docBannerFlag, patBannerFlag, tataBrandingFlag, disclaimerFlag, docSignatureFlag, kmcFlag, paperType,"update",headerFooterDesignerId, templateId, isOrgLevel);
		}
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		String successMessage = responseJson_object.getString("successMessage");
		System.out.println("successMessage  "+successMessage);
		Assert.assertNotNull(successMessage, "successMessage  is null");		
	}
	
}
