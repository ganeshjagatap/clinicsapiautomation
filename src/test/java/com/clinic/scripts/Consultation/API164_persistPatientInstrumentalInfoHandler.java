package com.clinic.scripts.Consultation;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API164_persistPatientInstrumentalInfoHandler extends TestBase {
	
	public static String barCode = null;
	@Test(priority=151)
	public void persistPatientInstrumentalListInfoHandler_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.persistPatientInstrumentalInfoHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		if(orgId.length() == 3){
		if(patientId.length()==3){
			if(visitId.length() == 4){
				barCode = "0"+orgId+"00"+patientId+"000"+visitId;
			}
			else{
				barCode = "0"+orgId+"00"+patientId+"00"+visitId;
			}
		}
		else if(patientId.length()==4){
			if(visitId.length() == 4){
				barCode = "0"+orgId+"0"+patientId+"000"+visitId;
			}
			else{
				barCode = "0"+orgId+"0"+patientId+"00"+visitId;
			}
		}
		else{
			if(visitId.length()== 4){
				barCode = "0"+orgId+patientId+"000"+visitId;
			}
			else{
				barCode = "0"+orgId+patientId+"00"+visitId;
			}
		}
		}
		else{
			if(patientId.length()==3){
				if(visitId.length() == 4){
					barCode = "00"+orgId+"00"+patientId+"000"+visitId;
				}
				else{
					barCode = "00"+orgId+"00"+patientId+"00"+visitId;
				}
			}
			else if(patientId.length()==4){
				if(visitId.length() == 4){
					barCode = "00"+orgId+"0"+patientId+"000"+visitId;
				}
				else{
					barCode = "00"+orgId+"0"+patientId+"00"+visitId;
				}
			}
			else{
				if(visitId.length()== 4){
					barCode = "00"+orgId+patientId+"000"+visitId;
				}
				else{
					barCode = "00"+orgId+patientId+"00"+visitId;
				}
			}
		}
		Random r = new Random();
		Integer num = r.nextInt(30);
		String randomLast=RandomStringUtils.randomAlphabetic(5);
		List<NameValuePair> userDetails = ParamGeneratorConsultation.persistPatientInstrumentalInfoHandler_param(orgId, token, username, clinicId, barCode, num.toString()+API136_SearchTestOrScanHandler.serviceName, byPassValidationCheckForJob, randomLast+labName, date1);
		JSONObject param=ParamGeneratorConsultation.getJSONpersistPatientInstrumentalInfoHandler("12345678901234568", parameter, parameterValue, parameterUnit, parameterReference, parameterComment);
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String e=responseJson_object.getString("summaryPdfFilePath");
		//String userId=y.getString("userId");
		System.out.println("summaryPdfFilePath  "+e);
		Assert.assertNotNull(e,"summaryPdfFilePath is null");	
	}
}
