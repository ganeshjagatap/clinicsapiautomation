package com.clinic.scripts.Consultation;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorConsultation;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API140_checkCimsInteractionsHandler extends TestBase {
	
	@Test(priority=117)
	public void checkCimsInteractionsHandler_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.checkCimsInteractionsHandler;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String drugNamePrescribingDrug = "CROCIN 1000 TAB 1000mg";
		String drugCodePrescribingDrug = "{BA3EFF1A-73FF-4F3B-B5FC-2C83F1DFA2D1}";
		List<NameValuePair> userDetails = ParamGeneratorConsultation.checkCimsInteractionsHandler_param( username,clinicId, token, orgId);
		JSONObject param=ParamGeneratorConsultation.getJSONcheckCimsInteractionsHandler("PRODUCT", drugNamePrescribingDrug,drugCodePrescribingDrug,"{AC340A53-6611-425A-8A75-70E82F00B79C}", "paracetamol - (Mol)");
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.post	JSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray interaction=responseJson_object.getJSONArray("interaction");
		JSONObject y=interaction.getJSONObject(0);
		String prescribingDrugName=y.getString("prescribingDrugName");
		System.out.println("prescribingDrugName  "+prescribingDrugName);
		Assert.assertNotNull(prescribingDrugName,"prescribingDrugName is null");	
	}
}
