package com.clinic.scripts.clinicAdmin;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.commons.lang.RandomStringUtils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;


public class API073_CreateUser extends TestBase{
	public static String newUser = null;
	public static String newUserId = null;
	@Test(priority =64)
	public void CreateUser_SuccessUsecase() throws Exception {
		Random random=new Random();
		int randomNumber=(random.nextInt(65536)-32768);
		String randomNumber1=""+randomNumber;
		System.out.println("random number"+randomNumber1);
		newUser=RandomStringUtils.randomAlphabetic(5);
		mailUrl = Envirnonment.env + Constants.createUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		List<String> roleId = new ArrayList<String>();
		roleId.add("1");
		List<String> locationId = new ArrayList<String>();
		locationId.add(API006_HomeDoctor.locationId);
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGeneratorClinicAdmin.createUser_Param(token, username, clinicId, API006_HomeDoctor.siteId);
		JSONObject param=ParamGeneratorClinicAdmin.getJSONCreateUser(clinicId, newUser, API068_userMaster.title, gender, API068_userMaster.bloodGroup, "1994-01-05", randomNumber1, "cardialogy", "madhu"+randomNumber1+"@gamil.com", locationName, null, null, null, "Doctor", isActive, orgId, orgName, isVirtualClinic, roleId, locationId, isDuplicateCheckRequired, isUserMappingRequired);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		newUserId = responseJson_object.getString("userId");
		System.out.println("userId  "+newUserId);
		Assert.assertNotNull(newUserId, "user Id  is null");
	}	
}
