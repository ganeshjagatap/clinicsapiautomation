package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class updateCommunicationParam extends TestBase{
	@Test(priority =52)
	public void updateCommunicationParam_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.updateCommunicationParam;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGeneratorClinicAdmin.updateCommunicationParam_Param(username, token, clinicId, orgId, "PATIENT", "PATIENT_REGISTRATION","You have been successfully registered at <organization_name>  with Organization ID <organization_id>. Your Patient UHID no. is <patient_uhid>. Please retain this number for future use.v","SMS","","","Y");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userProfile);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String message=responseJson_object.getString("message");
		//String userId=y.getString("userId");
		System.out.println("message  "+message);
		Assert.assertNotNull(message,"message is null");
	}	

}
