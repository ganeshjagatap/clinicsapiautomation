package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API047_RetrieveClinicServices extends TestBase {
	@Test(priority=53)
	public void RetrieveClinicServices_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.retrieveClinicServices;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		Integer servId = API046_CreateService.serviceId;
		String serviceId = servId.toString();
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.retrieveClinicServices_Param(token, username,serviceId,startRow,interval,searchText,orgId,API006_HomeDoctor.locationId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		// JSONObject e=	responseJson_object.JSONObject("service");
		//	JSONObject y=e.getJSONObject(0);
		String  count= responseJson_object.getString("count");	
		System.out.println("count:    "+count);
		Assert.assertNotNull(count, "count is null");
	}	
}
