package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API076_updateUserConsultationTabDetails extends TestBase {
	
	@Test(priority=75)
	public void getUserBasicDetails_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.updateUserConsultationTabDetails;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");	
	
		List<NameValuePair> userDetails = ParamGeneratorClinicAdmin.updateUserConsultationTabDetails_Param(token, username, clinicId, orgId);
		JSONObject param=ParamGeneratorClinicAdmin.getJSONupdateUserConsultationTabDetails(consultationTabDetails);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails,param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String success =responseJson_object.getString("success");
		System.out.println("success  "+success);
		Assert.assertNotNull(success,"success is null");
	}
}