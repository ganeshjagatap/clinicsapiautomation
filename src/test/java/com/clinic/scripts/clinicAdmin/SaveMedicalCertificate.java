package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API004_PatienSearch;

public class SaveMedicalCertificate extends TestBase{
	
	@Test(priority=18)
	public void SaveMedicalCertificate_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.saveMedicalCertificate;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		certificateData = "Patient"+API004_PatienSearch.patientName+ "has been suffering from fever from"+ date1+" to "+date1+".";
		List<NameValuePair> userDetails = ParamGeneratorClinicAdmin.saveMedicalCertificate_Param(username, token, clinicId, orgId,FetchMedicalCertificateTemplate.certificateId, FetchMedicalCertificateTemplate.templateId, certificateData, clinicId,patientId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage = responseJson_object.getString("successMessage");
		Assert.assertNotNull(successMessage,"successMessage is null");	
	}
	
}
