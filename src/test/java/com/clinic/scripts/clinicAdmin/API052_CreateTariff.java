package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API052_CreateTariff extends TestBase{
	public static String tarifId = null;
	@Test(priority=54)
	public void CreateTariff_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.createTariff;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> bookRegs = ParamGeneratorClinicAdmin.createTariff_Param(token, username,  clinicId); 
		JSONObject param=ParamGeneratorClinicAdmin.getJSONcreateTariff(Integer.parseInt(tariffAmount),API046_CreateService.serviceId,API046_CreateService.serviceName,  API006_HomeDoctor.siteId,API006_HomeDoctor.locationId,orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, bookRegs, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		// JSONObject e=	responseJson_object.JSONObject("service");
		//	JSONObject y=e.getJSONObject(0);
		tarifId= responseJson_object.getString("tariffId");	
		System.out.println("tariffId:    "+tarifId);
		Assert.assertNotNull(tarifId,"tariffId is null");
	}
}