package com.clinic.scripts.clinicAdmin;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;
import com.clinic.scripts.billing.API027_searchService;


//API030_CreateService is dependent on API001_HomeDoctor  Service id=36836
public class API046_CreateService extends TestBase {
	public static Integer serviceId;
	public static String tariffId = null;
	public static String serviceName = null;
	
	@Test(priority=50)
	public void API030_CreateService_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.createService;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String randomName=RandomStringUtils.randomAlphabetic(3);
		serviceName = API027_searchService.serviceName2 + randomName;
		List<NameValuePair> bookRegs = ParamGeneratorClinicAdmin.createService_Param(token, username,  clinicId); 
		JSONObject param=ParamGeneratorClinicAdmin.getJSONcreateService(serviceName, API043_ServiceCategory.category,API043_ServiceCategory.serviceType, chargable,resultApplicable, API044_ServiceClassification.classificationId, orgId,status,tariff, API006_HomeDoctor.siteId,API006_HomeDoctor.locationId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, bookRegs, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		// JSONObject e=	responseJson_object.JSONObject("service");	
		//	JSONObject y=e.getJSONObject(0);
		serviceId= responseJson_object.getInt("serviceId");
		tariffId = responseJson_object.getString("tariffId");
		serviceName = responseJson_object.getString("serviceName");
		System.out.println("serviceId:    "+serviceId);
		Assert.assertNotNull(serviceId, "serviceId is null");
	}	
}
