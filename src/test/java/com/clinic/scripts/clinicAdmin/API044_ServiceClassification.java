package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API044_ServiceClassification extends TestBase {	
	
	public static String classificationId = null;
	public static String classificationName = null;
	@Test(priority=44)
	public void ServiceClassification_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.serviceClassification;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.serviceClassification_Param(token, username,orgId,API043_ServiceCategory.category);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray e=	responseJson_object.getJSONArray("classification");	
		JSONObject y=e.getJSONObject(0);
		classificationId= y.getString("classificationId");
		classificationName = y.getString("classificationName");
		System.out.println("classificationId:    "+classificationId);
		Assert.assertNotNull(classificationId, "classificationId is null");
	}	
}
