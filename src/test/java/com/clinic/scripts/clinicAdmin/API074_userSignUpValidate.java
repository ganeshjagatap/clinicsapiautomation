package com.clinic.scripts.clinicAdmin;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API074_userSignUpValidate extends TestBase {
	@Test(priority=73)
	public void userSignUpValidate_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.userSignUpValidate;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorClinicAdmin.userSignUpValidate_Param(API073_CreateUser.newUserId, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//JSONObject y=e.getJSONObject(0);
		//String userName=y.getString("userName");
		String userId=responseJson_object.getString("otpCode");
		System.out.println("otpCode  "+userId);
		Assert.assertNotNull(userId,"otpCode is null");	
	}
}
