package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API034_UpdateSystemParam extends TestBase {
	
	@Test(priority=39)
	public void UpdateSystemParam_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.updateSystemParam;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> updateParam = ParamGeneratorClinicAdmin.updateSystemParam_Param(token,username,clinicId,orgId); 
		JSONObject param=ParamGeneratorClinicAdmin.getJSONupdateSystemParam("83", "7", "Clinic Start Time", paramValue, "o", "11", "Asclepius Clinics New Prod", API006_HomeDoctor.siteId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, updateParam, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//	JSONObject e=	responseJson_object.getJSONObject("systemParam");
		String	successMessage=	responseJson_object.getString("successMessage");
		System.out.println("successMessage:    "+successMessage);
		Assert.assertNotNull(successMessage, "successMessage is null");
	}	
}
