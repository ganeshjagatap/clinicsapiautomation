package com.clinic.scripts.clinicAdmin;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class SearchMedicalCertificates extends TestBase{
	@Test(priority =52)
	public void SearchMedicalCertificates_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.searchMedicalCertificates;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		List<String> roleId = new ArrayList<String>();
		roleId.add("1");
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		certificateIds = new JSONArray();
		certificateIds.put(FetchMedicalCertificateTemplate.certificateId);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGeneratorClinicAdmin.searchMedicalCertificates_Param(username, token, clinicId, orgId, startRow, interval);
		JSONObject param=ParamGeneratorClinicAdmin.getJSONSearchMedicalCertificates("","","","","", certificateIds);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray arr = responseJson_object.getJSONArray("certificates");
		JSONObject arrobj = arr.getJSONObject(0);
		String patientName=arrobj.getString("patientName");
		//String userId=y.getString("userId");
		System.out.println("patientName  "+patientName);
		Assert.assertNotNull(patientName,"patientName is null");
	}	

}
