package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;


public class API057_ClinicLogin {
	
	public static JSONObject responseJson_object=null;
	public static StringBuffer sb=null;
	
	String authToken=null;
	public static String mailUrl=null;
	String token=null;
	String username="tata";
	String orgId=null;
	String clinicId=null;
	

	
	
	
	
	
	
	
	@Test(priority =1)
	public void Invalid_Username_FailureUsecase() throws Exception {
		Thread.sleep(7000);
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("invalid", "Test123@");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
			Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =2)
	public void Blank_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("", "Test123@");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("errorCode"), 9102);
			Assert.assertEquals(responseJson_object.getString("errorMessage"), " Following field(s) are missing [UserLoginName]");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =3)
	public void Blank_Spaces_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("   ", "Test123@");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("errorCode"), 9102);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), " Following field(s) are missing [UserLoginName]");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =4)
	public void Special_Character_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("@@@@@@@@@", "Test123@");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
				Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =5)
	public void Max_length_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("dfdsfdsssssssssssssssssssssssssssssssssssssssssssssssssdfsfsdfdsdfsdfssdffsdffddddffdsfddsfdssdfds", "Test123@");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =6)
	public void Min_length_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("d", "Test123@");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =7)
	public void Username_Characters_With_Spaces_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("ta  ta", "Test123@");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =8)
	public void Alfha_Numeric_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("@33344$$$sfsfsd", "Test123@");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =9)
	public void Invalid_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata", "adfassa");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =10)
	public void Blank_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata", "");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
				Assert.assertEquals(responseJson_object.getInt("errorCode"), 9102);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), " Following field(s) are missing [Password]");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	
	@Test(priority =11)
	public void Special_Character_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata", "$$%%%%%###&&****");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =12)
	public void Max_length_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata", "adasadsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvsdvdsvvdsvsddvs");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =13)
	public void Min_length_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata", "a");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =14)
	public void Blank_Spaces_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata", "     ");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9102);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), " Following field(s) are missing [Password]");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =15)
	public void Password_Characters_With_Spaces_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata", "1 2 3 4 5 6");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	
	
	
	@Test(priority =16)
	public void ALfha_Numeric_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata","@######1232222sdfssf");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =17)
	public void Both_Invalid_Password_And_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("Abhishek.puppalwar","INVALID");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =18)
	public void Both_Blank_Password_And_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("","");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9102);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), " Following field(s) are missing [UserLoginName, Password]");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =19)
	public void Invalid_Password_Given_In_Uppercase_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.login;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenerator.getEMRSignIn_Param("tata","INVALID");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 Assert.assertEquals(responseJson_object.getInt("errorCode"), 9101);
				Assert.assertEquals(responseJson_object.getString("errorMessage"), "Invalid user credentials.");
			// Verify the Value using assertion
			 
	}
	
	
	
}
