package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API048_UpdateService extends TestBase {
	@Test(priority=51)
	public void UpdateService_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.updateService;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		tariff = "20.0";
		String serviceId = API046_CreateService.serviceId.toString();
		List<NameValuePair> bookRegs = ParamGeneratorClinicAdmin.updateService_Param(token, username,  clinicId); 
		JSONObject param=ParamGeneratorClinicAdmin.getJSONupdateService(serviceId,API046_CreateService.serviceName, API043_ServiceCategory.serviceType, chargable,resultApplicable, orgId,status,tariff, API046_CreateService.tariffId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, bookRegs, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		// JSONObject e=	responseJson_object.JSONObject("service");	
		//	JSONObject y=e.getJSONObject(0);
		String successMessage= responseJson_object.getString("successMessage");	
		System.out.println("successMessage:    "+successMessage);
		Assert.assertNotNull(successMessage, "successMessage is null");
	}		
}
