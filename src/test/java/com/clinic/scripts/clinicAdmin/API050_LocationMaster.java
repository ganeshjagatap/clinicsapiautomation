package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API050_LocationMaster extends TestBase {
	@Test(priority=46)
	public void API034_LocationMaster_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.locationMaster;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.locationMaster_Param(token, username,orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		// JSONObject e=	responseJson_object.JSONObject("service");
		JSONArray e=	responseJson_object.getJSONArray("location");
		JSONObject y=e.getJSONObject(0);
		String locationId= y.getString("locationId");
		System.out.println("locationId:    "+locationId);
		Assert.assertNotNull(locationId, "locationId is null");
	}	
}
