package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API055_VerifyUserLoginName extends TestBase {
	@Test(priority=57)
	public void API038_VerifyUserLoginName_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.verifyUserLoginName;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.verifyUserLoginName_Param(userLoginName);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		// JSONObject e=	responseJson_object.JSONObject("service");	
		//	JSONObject y=e.getJSONObject(0);
		String successMessage= responseJson_object.getString("successMessage");		
		System.out.println("successMessage:    "+successMessage);
		Assert.assertEquals(successMessage, "User name is available.");
	}
}
