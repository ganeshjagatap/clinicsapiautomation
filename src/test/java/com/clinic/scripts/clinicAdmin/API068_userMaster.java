package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API068_userMaster extends TestBase {
	public static String title = null;
	public static String bloodGroup = null;
	@Test(priority=47)
	public void acceptTermCondition_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.userMaster;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorClinicAdmin.userMaster_Param(username, token, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray e=responseJson_object.getJSONArray("location");
		JSONObject y=e.getJSONObject(0);
		String locationId=y.getString("locationId");
		JSONArray titleArr = responseJson_object.getJSONArray("title");
		title = titleArr.getString(2);
		JSONArray bloodArr = responseJson_object.getJSONArray("bloodGroup");
		bloodGroup = bloodArr.getString(2);
		//String userId=responseJson_object.getString("successMessage");
		System.out.println("locationId  "+locationId);
		Assert.assertNotNull(locationId,"locationId is null");	
	}
}
