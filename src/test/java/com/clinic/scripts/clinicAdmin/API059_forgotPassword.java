package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API059_forgotPassword extends TestBase {
	public static String otp = null;
	@Test(priority=60)
	public void forgotPassword_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.forgotPassword;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorClinicAdmin.forgotPassword_Param(username);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		//JSONObject e=responseJson_object.getJSONObject("user");
		String userId =responseJson_object.getString("otpCode");
		//String userId=y.getString("userId");
		otp =responseJson_object.getString("otpCode");
		System.out.println("otpCode  "+userId);
		Assert.assertNotNull(userId, "otpCode is null");	
	}
}
