package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API041_AutoSuggestionSpeciality extends TestBase{
	@Test(priority=49)
	public void AutoSuggestionSpeciality_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.autoSuggestionSpeciality;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.autoSuggestionSpeciality_Param(token, username,orgId,searchText);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray arr = responseJson_object.getJSONArray("speciality");
		String speciality = arr.getString(0);
		System.out.println("speciality:    "+speciality);
		Assert.assertNotNull(speciality, "speciality is null");
	}	
}
