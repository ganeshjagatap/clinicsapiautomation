package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API040_SearchUserGlobal extends TestBase {
	@Test(priority=42)
	public void SearchUserGlobal_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.searchUserGlobal;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.searchUserGlobal_Param(token, username,orgId,"Doctor","mala","1",null,API038_GetSpecialty.speciality,isClinicAdminUser);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray e=	responseJson_object.getJSONArray("user");
		JSONObject y=e.getJSONObject(0);
		String userLoginName= y.getString("userLoginName");	
		System.out.println("userLoginName:    "+userLoginName);
		Assert.assertNotNull(userLoginName, "userLoginName is null");
	}	
}
