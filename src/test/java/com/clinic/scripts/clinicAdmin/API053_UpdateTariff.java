package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API053_UpdateTariff extends TestBase {
	@Test(priority=55)
	public void CreateTariff_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.UpdateTariff;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		int updateTariffAmount = 50;
		System.out.println("begin");
		List<NameValuePair> bookRegs = ParamGeneratorClinicAdmin.UpdateTariff_Param(token, username,  clinicId,orgId); 
		JSONObject param=ParamGeneratorClinicAdmin.getJSONUpdateTariff(updateTariffAmount,API039_SearchClinicServiceGlobal.tariffId,API039_SearchClinicServiceGlobal.serviceName);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, bookRegs, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage= responseJson_object.getString("successMessage");	
		System.out.println("successMessage:    "+successMessage);
		Assert.assertNotNull(successMessage,"successMessage is null");
	}
}
