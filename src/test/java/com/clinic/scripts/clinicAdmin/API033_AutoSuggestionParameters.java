package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API033_AutoSuggestionParameters extends TestBase{
	public static String systemParamterId = null;
	public static String paramName = null;
	@Test(priority=37)
	public void API033_AutoSuggestionParameters_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.autoSuggestionParameters;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.autoSuggestionParameters_Param(token, username, searchText, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//JSONObject e=	responseJson_object.getJSONObject("systemParam");
		JSONArray	patien=	responseJson_object.getJSONArray("systemParam");
		JSONObject y=patien.getJSONObject(0);
		systemParamterId=y.getString("systemParamterId");
		paramName = y.getString("paramName");
		System.out.println("systemParamterId:    "+systemParamterId);
		Assert.assertNotNull(systemParamterId, "systemParamterId is null");
	}	
}
