package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API035_RetrieveSystemParams extends TestBase {
	public static String paramId = null;
	public static String levelValueId = null;
	public static String level = null;
	public static String levelValueName = null;
	@Test(priority=38)
	public void RetrieveSystemParams_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.retrieveSystemParams;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.retrieveSystemParams_Param(token, username, startRow,interval,"Clinic Start Time","7", orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//JSONObject e=	responseJson_object.getJSONObject("systemParam");
		String	count=	responseJson_object.getString("count");
		JSONArray arr = responseJson_object.getJSONArray("systemParamList");
		JSONObject obj = arr.getJSONObject(0);
		paramId = obj.getString("paramId");
		levelValueId = obj.getString("levelValueId");
		level = obj.getString("level");
		levelValueName = obj.getString("levelValueName");
		System.out.println("count:    "+count);
		Assert.assertNotNull(count, "count is null");
	}	
}
