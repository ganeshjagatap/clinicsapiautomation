package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API039_SearchClinicServiceGlobal extends TestBase {
	public static int serviceId;
	public static String tariffId=null;
	public static String serviceName=null;
	public static String serviceCategory=null;
	@Test(priority=52)
	public void SearchClinicServiceGlobal_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.searchClinicServiceGlobal;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.searchClinicServiceGlobal_Param(token, username,orgId,API043_ServiceCategory.category,API044_ServiceClassification.classificationId,API046_CreateService.serviceName);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray e=	responseJson_object.getJSONArray("service");	
		JSONObject y=e.getJSONObject(0);
		serviceId= y.getInt("serviceId");	
		tariffId= y.getString("tariffId");
		serviceName= y.getString("serviceName");
		serviceCategory= y.getString("serviceCategory");	
		System.out.println("serviceId:    "+serviceId);
		Assert.assertNotNull(serviceId, "serviceId is null");
	}	
}
