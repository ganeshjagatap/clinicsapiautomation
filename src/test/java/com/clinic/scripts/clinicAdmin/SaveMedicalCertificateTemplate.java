package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class SaveMedicalCertificateTemplate extends TestBase {
	
	@Test(priority=18)
	public void acceptTermCondition_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.saveMedicalCertificateTemplate;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorClinicAdmin.saveMedicalCertificateTemplate_Param(orgId, token, clinicId, username, templateName, templateData,FetchMedicalCertificateTemplate.certificateId,FetchMedicalCertificateTemplate.templateId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage = responseJson_object.getString("successMessage");
		System.out.println("successMessage  "+successMessage);
		Assert.assertNotNull(successMessage,"successMessage is null");	
	}
	
}
