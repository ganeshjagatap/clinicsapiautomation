package com.clinic.scripts.clinicAdmin;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.ParamGeneratorUserProfile;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;
import com.clinic.scripts.UserProfile.API089_ListUserExperience;
import com.clinic.scripts.appointment.API006_HomeDoctor;

public class API065_UpdateUser extends TestBase{
	@Test(priority =66)
	public void UpdateUser_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.updateUser;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		List<String> roleId = new ArrayList<String>();
		roleId.add("1");
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userProfile = ParamGeneratorClinicAdmin.updateUser_Param(token, username, clinicId, API006_HomeDoctor.siteId, API006_HomeDoctor.locationId, clinicId, orgId, username, "Doctor");
		JSONObject param=ParamGeneratorClinicAdmin.getJSONUpdateUser(roleId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userProfile, param);
		// Post request to the URL with the param data
		//HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		String successMessage=responseJson_object.getString("successMessage");
		//String userId=y.getString("userId");
		System.out.println("successMessage  "+successMessage);
		Assert.assertNotNull(successMessage,"successMessage is null");
	}	
}
