package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API032_AppUpdate extends TestBase {
	@Test(priority=1)
	public void appUpdate_SuccessUsecase() throws Exception{
		
	mailUrl = Envirnonment.env + Constants.appUpdate;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.fetchAppUpdateHandler_param("Clinic APP");

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		
			//JSONObject e=	responseJson_object.getJSONObject("systemParam");
		
			String apptype=responseJson_object.getString("updateType");
			

			System.out.println("apptype:    "+apptype);
			Assert.assertNotNull(apptype, "apptype is null");
}	
}
