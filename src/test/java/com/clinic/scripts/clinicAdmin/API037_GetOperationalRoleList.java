package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API037_GetOperationalRoleList extends TestBase {
	public static String roleId = null;
	public static String role = null;
	@Test(priority=40)
	public void GetOperationalRoleList_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.getOperationalRoleList;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> signinparams = ParamGeneratorClinicAdmin.getOperationalRoleList_Param(token, username,orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONArray e=	responseJson_object.getJSONArray("role");
		//JSONArray	patien=	e.getJSONArray("patient");
		JSONObject y=e.getJSONObject(0);
		roleId=y.getString("roleId");
		role = y.getString("roleName");
		System.out.println("roleId:    "+roleId);
		Assert.assertNotNull(roleId, "roleId is null");
	}		
}
