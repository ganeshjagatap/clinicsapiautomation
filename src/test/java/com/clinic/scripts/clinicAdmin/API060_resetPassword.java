package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API060_resetPassword extends TestBase {
	@Test(priority=62)
	public void resetPassword_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.resetPassword;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGeneratorClinicAdmin.resetPassword_Param(username, password11);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin); 
		 //JSONObject e=responseJson_object.getJSONObject("user");
		 String userId =responseJson_object.getString("successMessage");
		//String userId=y.getString("userId")
		System.out.println("successMessage  "+userId);
		Assert.assertNotNull(userId, "successMessage is null");
	}
}
