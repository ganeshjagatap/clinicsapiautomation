package com.clinic.scripts.clinicAdmin;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.ParamGeneratorClinicAdmin;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class API075_createUserAuthentication extends TestBase {

	@Test(priority=74)
	public void createUserAuthentication_SuccessUsecase() throws Exception{
		mailUrl = Envirnonment.env + Constants.createUserAuthentication;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		
		List<NameValuePair> userDetails = ParamGeneratorClinicAdmin.createUserAuthentication_Param(API073_CreateUser.newUser, API073_CreateUser.newUserId, orgId, password11);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		//JSONObject y=e.getJSONObject(0);
		//String userName=y.getString("userName");
		String userId=responseJson_object.getString("isAuthenticated");
		System.out.println("clinicUserId  "+userId);
		Assert.assertNotNull(userId,"clinicUserId is null");	
	}
}
