package com.clinic.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class JsonParseLogin {
	public static String host=null;
	
	public static String userLoginName=null;
	public static String password=null;
	public static String clinicUserId=null;
	public static String organisationId=null;
	public static String otpCode=null;
	public static String organisationName=null;
	public static String siteId=null;
	public static String userId=null;
	public static String userName=null;
	public static String roleType=null;
	public static String mobileNo=null;	
	public static String mobileNumber=null;
	public static String oldPassword=null;
	public static String token=null;
	public static String user=null;
	public static String roleId=null;
	public static String locationId=null;
	public static String isDuplicateCheckRequired=null;
	public static String isUserMappingRequired=null;
	public static String title=null;
	public static String gender=null;
	public static String bloodGroup=null;
	public static String speciality=null;
	public static String email=null;
	public static String address=null;
	public static String medicalRegNo=null;
	public static String signature=null;
	public static String userPhoto=null;
	public static String isActive=null;
	public static String isVirtualClinic=null;
	public static String searchText=null;
	public static String startRow=null;
	public static String interval=null;
	public static String consultationTabDetails=null;
	public static String acceptTermAndCondition=null;
	public static String adminUserLoginName=null;
	public static String adminUserId=null;
	public static String dob=null;
	
	
	
	
	
	
	

	
	
	@Test
	public void jsonParseLogin(){
		String jsonData = "";
		
		BufferedReader br = null;
		try {
			String line;
			br = new BufferedReader(new FileReader(".//json//login.json"));
			while ((line = br.readLine()) != null) {
				jsonData += line + "\n";
				}
			}
		catch (IOException e) {
			e.printStackTrace();
			}
		finally {
			try {
				if (br != null)
					br.close();
				}
			catch (IOException ex) {
				ex.printStackTrace();
				}
			}
		JSONObject obj = new JSONObject(jsonData);
		
		host=obj.getString("host");
		System.out.println(host);
		JSONObject pathsObj=	obj.getJSONObject("paths");
		
		Iterator<?> keys = pathsObj.keys();
		
		while(keys.hasNext()){
			 String t=keys.next().toString();
			 //S.add(t);
			 String t1=t.substring(47);
			 //S1.add(t1);
			 if(t1.equalsIgnoreCase("appLogin"))
				 Constants.login=t;
			 if(t1.equalsIgnoreCase("userSignUpValidate"))
				 Constants.userSignUpValidate=t;
			 if(t1.equalsIgnoreCase("verifyUserLoginName"))
				 Constants.verifyUserLoginName=t;
			 if(t1.equalsIgnoreCase("otpVerification"))
				 Constants.otpVerification=t;
			 if(t1.equalsIgnoreCase("resendSms"))
				 Constants.resendSms=t;
			 if(t1.equalsIgnoreCase("changePassword"))
				 Constants.changePassword=t;
			 if(t1.equalsIgnoreCase("forgotPassword"))
				 Constants.forgotPassword=t;
			 if(t1.equalsIgnoreCase("resetPassword"))
				 Constants.resetPassword=t;
			 if(t1.equalsIgnoreCase("createUserAuthentication"))
				 Constants.createUserAuthentication=t;
			 if(t1.equalsIgnoreCase("getUserBasicDetails"))
				 Constants.getUserBasicDetails=t;
			 if(t1.equalsIgnoreCase("createUser"))
				 Constants.createUser=t;
			 if(t1.equalsIgnoreCase("updateUser"))
				 Constants.updateUser=t;
			 if(t1.equalsIgnoreCase("logout"))
				 Constants.logout=t;
			 if(t1.equalsIgnoreCase("autoSuggestionUsers"))
				 Constants.autoSuggestionUsers=t;
			 if(t1.equalsIgnoreCase("retrieveUsers"))
				 Constants.retrieveUsers=t;
			 if(t1.equalsIgnoreCase("userStatusUpdate"))
				 Constants.userStatusUpdate=t;
			 if(t1.equalsIgnoreCase("getUserDetails"))
				 Constants.getUserDetails=t;
			 if(t1.equalsIgnoreCase("userMaster"))
				 Constants.userMaster=t;
			 if(t1.equalsIgnoreCase("userAssocToClinic"))
				 Constants.userAssocToClinic=t;
			 if(t1.equalsIgnoreCase("getUserConsultationTabDetails"))
				 Constants.getUserConsultationTabDetails=t;
			 if(t1.equalsIgnoreCase("updateUserConsultationTabDetails"))
				 Constants.updateUserConsultationTabDetails=t;
			 if(t1.equalsIgnoreCase("acceptTermCondition"))
				 Constants.acceptTermCondition=t;
		 }
		
		
		JSONObject definitionsObj=	obj.getJSONObject("definitions");
		
		//---------------------------------login---------------------------------------//
		
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject loginObj=	definitionsObj.getJSONObject("login");
		JSONArray	loginArr=	loginObj.getJSONArray("required");
		password=loginArr.getString(1);
		//Assert.assertNotNull(patienID, "PatientID is null");
		
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------verifyUserLoginName---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject verifyUserLoginNameObj=	definitionsObj.getJSONObject("verifyUserLoginName");
		JSONArray	verifyUserLoginNameArr=	verifyUserLoginNameObj.getJSONArray("required");
		userLoginName=verifyUserLoginNameArr.getString(0);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------userSignUpValidate---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject userSignUpValidateObj=	definitionsObj.getJSONObject("userSignUpValidate");
		JSONArray	userSignUpValidateArr=	userSignUpValidateObj.getJSONArray("required");
		clinicUserId=userSignUpValidateArr.getString(0);
		organisationId=userSignUpValidateArr.getString(1);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------otpVerification---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject otpVerificationObj=	definitionsObj.getJSONObject("otpVerification");
		JSONArray	otpVerificationArr=	otpVerificationObj.getJSONArray("required");
		otpCode=otpVerificationArr.getString(0);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------resendSms---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject resendSmsObj=	definitionsObj.getJSONObject("resendSms");
		JSONArray	resendSmsArr=	resendSmsObj.getJSONArray("required");
		mobileNumber=resendSmsArr.getString(0);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------changePassword---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject changePasswordObj=	definitionsObj.getJSONObject("changePassword");
		JSONArray	changePasswordArr=	changePasswordObj.getJSONArray("required");
		oldPassword=changePasswordArr.getString(2);
		token=changePasswordArr.getString(3);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------forgotPassword---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject forgotPasswordObj=	definitionsObj.getJSONObject("forgotPassword");
		JSONArray	forgotPasswordArr=	forgotPasswordObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------resetPassword---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject resetPasswordObj=	definitionsObj.getJSONObject("resetPassword");
		JSONArray	resetPasswordArr=	resetPasswordObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------createUserAuthentication---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject createUserAuthenticationObj=	definitionsObj.getJSONObject("createUserAuthentication");
		JSONArray	createUserAuthenticationArr=	createUserAuthenticationObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------getUserBasicDetails---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject getUserBasicDetailsObj=	definitionsObj.getJSONObject("getUserBasicDetails");
		JSONArray	getUserBasicDetailsArr=	getUserBasicDetailsObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------createUser---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject createUserObj=	definitionsObj.getJSONObject("createUser");
		JSONArray	createUserArr=	createUserObj.getJSONArray("required");
		adminUserId=createUserArr.getString(2);
		siteId=createUserArr.getString(3);
		user=createUserArr.getString(4);
		roleId=createUserArr.getString(5);
		locationId=createUserArr.getString(6);
		isDuplicateCheckRequired=createUserArr.getString(7);
		isUserMappingRequired=createUserArr.getString(8);
		
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------User---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject UserObj=	definitionsObj.getJSONObject("User");
		JSONArray	UserArr=	UserObj.getJSONArray("required");
		userId=UserArr.getString(0);
		userName=UserArr.getString(1);
		title=UserArr.getString(2);
		gender=UserArr.getString(3);
		bloodGroup=UserArr.getString(4);
		dob=UserArr.getString(5);
		speciality=UserArr.getString(7);
		email=UserArr.getString(8);
		address=UserArr.getString(9);
		medicalRegNo=UserArr.getString(10);
		signature=UserArr.getString(11);
		userPhoto=UserArr.getString(12);
		roleType=UserArr.getString(13);
		isActive=UserArr.getString(14);
		organisationName=UserArr.getString(16);
		isVirtualClinic=UserArr.getString(17);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------updateUser---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject updateUserObj=	definitionsObj.getJSONObject("updateUser");
		JSONArray	updateUserArr=	updateUserObj.getJSONArray("required");
		adminUserLoginName=updateUserArr.getString(1);
		//Assert.assertNotNull(patienID, "PatientID is null");
		
		//---------------------------------logout---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject logoutObj=	definitionsObj.getJSONObject("logout");
		JSONArray	logoutArr=	logoutObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------autoSuggestionUsers---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject autoSuggestionUsersObj=	definitionsObj.getJSONObject("autoSuggestionUsers");
		JSONArray	autoSuggestionUsersArr=	autoSuggestionUsersObj.getJSONArray("required");
		searchText=autoSuggestionUsersArr.getString(0);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------retrieveUsers---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject retrieveUsersObj=	definitionsObj.getJSONObject("retrieveUsers");
		JSONArray	retrieveUsersArr=	retrieveUsersObj.getJSONArray("required");
		startRow=retrieveUsersArr.getString(3);
		interval=retrieveUsersArr.getString(4);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//--------------------------------userStatusUpdate-----------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject userStatusUpdateObj=	definitionsObj.getJSONObject("userStatusUpdate");
		JSONArray	userStatusUpdateArr=	userStatusUpdateObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------getUserDetails---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject getUserDetailsObj=	definitionsObj.getJSONObject("getUserDetails");
		JSONArray	getUserDetailsArr=	getUserDetailsObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------userMaster---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject userMasterObj=	definitionsObj.getJSONObject("userMaster");
		JSONArray	userMasterArr=	userMasterObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------userAssocToClinic---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject userAssocToClinicObj=	definitionsObj.getJSONObject("userAssocToClinic");
		JSONArray	userAssocToClinicArr=	userAssocToClinicObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------getUserConsultationTabDetails---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject getUserConsultationTabDetailsObj=	definitionsObj.getJSONObject("getUserConsultationTabDetails");
		JSONArray	getUserConsultationTabDetailsArr=	getUserConsultationTabDetailsObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------updateUserConsultationTabDetails---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject updateUserConsultationTabDetailsObj=	definitionsObj.getJSONObject("updateUserConsultationTabDetails");
		JSONArray	updateUserConsultationTabDetailsArr=	updateUserConsultationTabDetailsObj.getJSONArray("required");
		consultationTabDetails=updateUserConsultationTabDetailsArr.getString(4);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------acceptTermCondition---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject acceptTermConditionObj=	definitionsObj.getJSONObject("acceptTermCondition");
		JSONArray	acceptTermConditionArr=	acceptTermConditionObj.getJSONArray("required");
		acceptTermAndCondition=acceptTermConditionArr.getString(2);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////		
		
		
		}
	
}