package com.clinic.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class JsonParseServiceMaster {
	public static String host=null;
	
	public static String token=null;
	public static String userLoginName=null;
	public static String organisationId=null;
	public static String category=null;
	public static String adminUserId=null;
	public static String service=null;
	public static String serviceName=null;
	public static String serviceCategory=null;
	public static String serviceType=null;
	public static String chargable=null;
	public static String resultApplicable=null;
	public static String classificationId=null;
	public static String status=null;
	public static String tariff=null;
	public static String siteId=null;
	public static String locationId=null;
	public static String searchText=null;
	public static String serviceId=null;
	public static String startRow=null;
	public static String interval=null;
	public static String tariffId=null;
	
	
	
	
	@Test
	public void jsonParseServiceMaster(){
		String jsonData = "";
		
		BufferedReader br = null;
		try {
			String line;
			br = new BufferedReader(new FileReader(".//json//serviceMaster.json"));
			while ((line = br.readLine()) != null) {
				jsonData += line + "\n";
				}
			}
		catch (IOException e) {
			e.printStackTrace();
			}
		finally {
			try {
				if (br != null)
					br.close();
				}
			catch (IOException ex) {
				ex.printStackTrace();
				}
			}
		JSONObject obj = new JSONObject(jsonData);
		
		host=obj.getString("host");
		System.out.println(host);
		JSONObject pathsObj=	obj.getJSONObject("paths");
		
		Iterator<?> keys = pathsObj.keys();
		
		while(keys.hasNext()){
			 String t=keys.next().toString();
			 //S.add(t);
			 String t1=t.substring(47);
			 //S1.add(t1);
			 if(t1.equalsIgnoreCase("serviceCategory"))
				 Constants.serviceCategory=t;
			 if(t1.equalsIgnoreCase("serviceClassification"))
				 Constants.serviceClassification=t;
			 if(t1.equalsIgnoreCase("createService"))
				 Constants.createService=t;
			 if(t1.equalsIgnoreCase("searchClinicService"))
				 Constants.searchClinicService=t;
			 if(t1.equalsIgnoreCase("retrieveClinicServices"))
				 Constants.retrieveClinicServices=t;
			 if(t1.equalsIgnoreCase("updateService"))
				 Constants.updateService=t;
			 if(t1.equalsIgnoreCase("serviceMaster"))
				 Constants.serviceMaster=t;
			  }
		
		
		JSONObject definitionsObj=	obj.getJSONObject("definitions");
		
		//---------------------------------serviceCategory---------------------------------------//
		
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject serviceCategoryObj=	definitionsObj.getJSONObject("serviceCategory");
		JSONArray	serviceCategoryArr=	serviceCategoryObj.getJSONArray("required");
		token=serviceCategoryArr.getString(0);
		userLoginName=serviceCategoryArr.getString(1);
		organisationId=serviceCategoryArr.getString(2);
		//Assert.assertNotNull(patienID, "PatientID is null");
		
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------serviceClassification---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject serviceClassificationObj=	definitionsObj.getJSONObject("serviceClassification");
		JSONArray	serviceClassificationArr=	serviceClassificationObj.getJSONArray("required");
		category=serviceClassificationArr.getString(3);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------createService---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject createServiceObj=	definitionsObj.getJSONObject("createService");
		JSONArray	createServiceArr=	createServiceObj.getJSONArray("required");
		adminUserId=createServiceArr.getString(2);
		service=createServiceArr.getString(3);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------service---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject serviceObj=	definitionsObj.getJSONObject("service");
		JSONArray	serviceArr=	serviceObj.getJSONArray("required");
		serviceName=serviceArr.getString(0);
		serviceCategory=serviceArr.getString(1);
		serviceType=serviceArr.getString(2);
		chargable=serviceArr.getString(3);
		resultApplicable=serviceArr.getString(4);
		classificationId=serviceArr.getString(5);
		status=serviceArr.getString(7);
		tariff=serviceArr.getString(8);
		siteId=serviceArr.getString(9);
		locationId=serviceArr.getString(10);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------searchClinicService---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject searchClinicServiceObj=	definitionsObj.getJSONObject("searchClinicService");
		JSONArray	searchClinicServiceArr=	searchClinicServiceObj.getJSONArray("required");
		searchText=searchClinicServiceArr.getString(0);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------retrieveClinicServices---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject retrieveClinicServicesObj=	definitionsObj.getJSONObject("retrieveClinicServices");
		JSONArray	retrieveClinicServicesArr=	retrieveClinicServicesObj.getJSONArray("required");
		serviceId=retrieveClinicServicesArr.getString(2);
		startRow=retrieveClinicServicesArr.getString(3);
		interval=retrieveClinicServicesArr.getString(4);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------updateService---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject updateServiceObj=	definitionsObj.getJSONObject("updateService");
		JSONArray	updateServiceArr=	updateServiceObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------services---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject servicesObj=	definitionsObj.getJSONObject("services");
		JSONArray	servicesArr=	servicesObj.getJSONArray("required");
		tariffId=servicesArr.getString(8);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------serviceMaster---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject serviceMasterObj=	definitionsObj.getJSONObject("serviceMaster");
		JSONArray	serviceMasterArr=	serviceMasterObj.getJSONArray("required");
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
				
		}
	
}