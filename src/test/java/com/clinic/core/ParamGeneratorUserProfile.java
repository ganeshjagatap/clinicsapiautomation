package com.clinic.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class ParamGeneratorUserProfile {
	
	public static List<NameValuePair> param = null;
	
	//fetchHMDeviceDataHandler
	public static List<NameValuePair> fetchHmPatientDeviceDataHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("organizationId", organizationId));
		param.add(new BasicNameValuePair("siteId", siteId));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("patientId", patientId));
		return param;
	}

	//fetchHmNurseNotesHandler
	public static List<NameValuePair> fetchHmNurseNotesHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String doctorId){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("organizationId", organizationId));
		param.add(new BasicNameValuePair("siteId", siteId));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("patientId", patientId));
		param.add(new BasicNameValuePair("doctorId", doctorId));
		return param;
	}

	//fetchHmAlertMessagesHandler
	public static List<NameValuePair> fetchHmAlertMessagesHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String startIndex,String interval){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("organizationId", organizationId));
		param.add(new BasicNameValuePair("siteId", siteId));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("startIndex", startIndex));
		param.add(new BasicNameValuePair("interval", interval));
		return param;
	}
		
	//persistHmNurseNotesHandler
	public static List<NameValuePair> persistHmNurseNotesHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId,String nurseNoteId, String nurseNoteValue,String doctorId ){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("organizationId", organizationId));
		param.add(new BasicNameValuePair("siteId", siteId));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("patientId", patientId));
		param.add(new BasicNameValuePair("visitId", visitId));
		param.add(new BasicNameValuePair("nurseNoteId", nurseNoteId));
		param.add(new BasicNameValuePair("nurseNoteValue", nurseNoteValue));
		param.add(new BasicNameValuePair("doctorId", doctorId));
		return param;
	}
		
	//fetchHmPatientUploadedDataHandler
	public static List<NameValuePair> fetchHmPatientUploadedDataHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String deviceId,String deviceName,String startIndex,String interval,String scope){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("organizationId", organizationId));
		param.add(new BasicNameValuePair("siteId", siteId));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("patientId", patientId));
		param.add(new BasicNameValuePair("deviceId", deviceId));
		param.add(new BasicNameValuePair("deviceName", deviceName));
		param.add(new BasicNameValuePair("startIndex", startIndex));
		param.add(new BasicNameValuePair("interval", interval));
		param.add(new BasicNameValuePair("scope", scope));
		return param;
	}

	//UpdateUserProfileDetails
	public static List<NameValuePair> updateUserProfile_Param(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
		
	public static JSONObject getJSONUpdateUserProfile( String  userName, String title, String gender, String bloodGroup,String dob,String mobileNumber,String email,String address,String medicalRegNo,String userPhoto,String signature,String specialty,String digitalSignaturePath,String digitalSignaturePassword,String isVirtualClinic ) {

		JSONObject updateUser = new JSONObject();
		JSONObject UserProfileArray  = new JSONObject();
		UserProfileArray .put("userName", userName);
		UserProfileArray .put("title", title);
		UserProfileArray .put("gender", gender);
		UserProfileArray .put("bloodGroup", bloodGroup);
		UserProfileArray .put("dob", dob);
		UserProfileArray .put("mobileNumber", mobileNumber);
		UserProfileArray .put("email", email);
		UserProfileArray .put("address", address);
		UserProfileArray .put("medicalRegNo", medicalRegNo);
		UserProfileArray .put("userPhoto", userPhoto);
		UserProfileArray .put("signature", signature);
		UserProfileArray .put("specialty", specialty);
		UserProfileArray .put("digitalSignaturePath", digitalSignaturePath);
		UserProfileArray.put("digitalSignaturePassword",digitalSignaturePassword);
		UserProfileArray.put("isVirtualClinic", isVirtualClinic);
		
		updateUser.put("user", UserProfileArray );
		System.out.println("req:"+updateUser.toString());
		return updateUser;
	}
		
	//addUserPublication
	public static List<NameValuePair> addUserPublication_Param(String token,String userLoginName,String userId,String organisationId) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
		
	public static JSONObject getJSONAddUserPublication( String  abstractName, String journalName, String topic, String university,String publicationDate) {

		JSONObject addUserPublication = new JSONObject();
		JSONObject AddUserPublicationArray  = new JSONObject();
		AddUserPublicationArray .put("abstractName", abstractName);
		AddUserPublicationArray .put("journalName", journalName);
		AddUserPublicationArray .put("topic", topic);
		AddUserPublicationArray .put("university", university);
		AddUserPublicationArray .put("publicationDate", publicationDate);
		
		addUserPublication.put("publication", AddUserPublicationArray );
		System.out.println("req:"+addUserPublication.toString());
		return addUserPublication;
	}

	//listUserPublication
	public static List<NameValuePair> listUserPublication_Param(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
	
	//deleteUserPublication
	public static List<NameValuePair> deleteUserPublication_Param(String token,String userLoginName,String userId,String organisationId, String publicationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("publicationId", publicationId));
		return param;
	}

	//addUserExperience
	public static List<NameValuePair> addUserExperience_Param(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
			
	public static JSONObject getJSONAddUserExperience( String organizationName, String  fromDate, String toDate, String role, String remarks) {

		JSONObject addUserExperience = new JSONObject();
		JSONObject AddUserExperienceArray  = new JSONObject();
		AddUserExperienceArray.put("organizationName", organizationName);
		AddUserExperienceArray .put("fromDate", fromDate);
		AddUserExperienceArray .put("toDate", toDate);
		AddUserExperienceArray .put("role", role);
		AddUserExperienceArray .put("remarks", remarks);
			
		addUserExperience.put("experience", AddUserExperienceArray );
		System.out.println("req:"+addUserExperience.toString());
		return addUserExperience;
	}

	//listUserExperience
	public static List<NameValuePair> listUserExperience_Param(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
		
	//updateUserExperience
	public static List<NameValuePair> updateUserExperience_Param(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
			
	public static JSONObject getJSONUpdateUserExperience( String experienceId, String organizationName, String  fromDate, String toDate, String role, String remarks) {
		
		JSONObject updateUserExperience = new JSONObject();
		JSONObject UpdateUserExperienceArray  = new JSONObject();
		UpdateUserExperienceArray.put("experienceId", experienceId);
		UpdateUserExperienceArray.put("organizationName", organizationName);
		UpdateUserExperienceArray .put("fromDate", fromDate);
		UpdateUserExperienceArray .put("toDate", toDate);
		UpdateUserExperienceArray .put("role", role);
		UpdateUserExperienceArray .put("remarks", remarks);
			
		updateUserExperience.put("experience", UpdateUserExperienceArray );
		System.out.println("req:"+updateUserExperience.toString());
		return updateUserExperience;
	}
		
	//deleteUserExperience
	public static List<NameValuePair> deleteUserExperience_Param(String token,String userLoginName,String userId,String organisationId, String experienceId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("experienceId", experienceId));
		return param;
	}
		
	//updateUserPublication
	public static List<NameValuePair> updateUserPublication_Param(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
		
	public static JSONObject getJSONUpdateUserPublication( String publicationId, String  abstractName, String journalName, String topic, String university,String publicationDate) {

		JSONObject updateUserPublication = new JSONObject();
		JSONObject UpdateUserPublicationArray  = new JSONObject();
		UpdateUserPublicationArray.put("publicationId", publicationId);
		UpdateUserPublicationArray .put("abstractName", abstractName);
		UpdateUserPublicationArray .put("journalName", journalName);
		UpdateUserPublicationArray .put("topic", topic);
		UpdateUserPublicationArray .put("university", university);
		UpdateUserPublicationArray .put("publicationDate", publicationDate);

		updateUserPublication.put("publication", UpdateUserPublicationArray );
		System.out.println("req:"+updateUserPublication.toString());
		return updateUserPublication;

	}
	
	//updateUserQualification
	public static List<NameValuePair> updateUserQualification_Param(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
		
	public static JSONObject getJSONUpdateUserQualification( String qualificationId, String instituteName, String fromDate, String  toDate, String board, String university, String course, String degree,String specialization) {

		JSONObject updateUserQualification = new JSONObject();
		JSONObject UpdateUserQualificationArray  = new JSONObject();
		UpdateUserQualificationArray.put("qualificationId", qualificationId);
		UpdateUserQualificationArray.put("instituteName", instituteName);
		UpdateUserQualificationArray .put("fromDate", fromDate);
		UpdateUserQualificationArray .put("toDate", toDate);
		UpdateUserQualificationArray .put("board", board);
		UpdateUserQualificationArray .put("university", university);
		UpdateUserQualificationArray .put("course", course);
		UpdateUserQualificationArray .put("degree", degree);
		UpdateUserQualificationArray .put("specialization", specialization);
		
		updateUserQualification.put("qualification", UpdateUserQualificationArray );
		System.out.println("req:"+updateUserQualification.toString());
		return updateUserQualification;
	}
	
	//deleteUserQualification
	public static List<NameValuePair> deleteUserQualification_Param(String token,String userLoginName,String userId,String organisationId, String qualificationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("qualificationId", qualificationId));
		return param;
	}
	
	//deleteMyTimings
	public static List<NameValuePair> deleteMyTimings_Param(String token,String userLoginName,String userId,String organisationId, String dateSlotId, String locationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("dateSlotId", dateSlotId));
		param.add(new BasicNameValuePair("locationId", locationId));
		return param;
	}
	
	//getClinicConfigParams
	public static List<NameValuePair> getClinicConfigParams_Param(String token,String userLoginName,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
	
	//listDoctorTimings
	public static List<NameValuePair> listDoctorTimings_Param(String token,String userLoginName,String userId,String organisationId,String locationId,String fromDate, String toDate, String dateSlotId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("fromDate", fromDate));
		param.add(new BasicNameValuePair("toDate", toDate));
		param.add(new BasicNameValuePair("dateSlotId", dateSlotId));
		return param;
	}
	
	//deleteDayslotTimings
	public static List<NameValuePair> deleteDayslotTimings_Param(String token,String userLoginName,String userId,String organisationId, String dayslotId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("dayslotId", dayslotId));
		return param;
	}

	//validateDigitalSignature
	public static List<NameValuePair> validateDigitalSignature_Param(String token,String userLoginName,String userId,String organisationId, String digitalSignaturePath, String digitalSignaturePassword) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("digitalSignaturePath", digitalSignaturePath));
		param.add(new BasicNameValuePair("digitalSignaturePassword", digitalSignaturePassword));
		return param;
	}
		
	//getUserProfileInfoForCSquare
	public static List<NameValuePair> getUserProfileInfoForCSquare_Param(String barcode,String key) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("barcode", barcode));
		param.add(new BasicNameValuePair("key", key));
		return param;
	}
	
	public static List<NameValuePair> addDoctorTimings_param(String token,String userId,String userLoginName,String organisationId,String locationId,String siteId,String fromDate,String toDate) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("siteId", siteId));
		param.add(new BasicNameValuePair("fromDate", fromDate));
		param.add(new BasicNameValuePair("toDate", toDate));
		return param;
	}
	
	public static JSONObject getJSONaddDoctorTimings( String  dayName, String startTime, String endTime) {
		
		JSONObject daySlotTimings = new JSONObject();
		JSONObject day = new JSONObject();
		JSONArray dayARR = new JSONArray();
		JSONObject dayObj = new JSONObject();
		JSONArray daySlotArr=new JSONArray();
		JSONObject daySlot=new JSONObject();
		JSONObject daySlotObj=new JSONObject();
		
		daySlotObj.put("startTime", startTime);
		daySlotObj.put("endTime", endTime);
		daySlotArr.put(daySlotObj);
		dayObj .put("dayName", dayName);
		dayObj .put("daySlot", daySlotArr );
		dayARR .put(dayObj);
		day .put("day", dayARR);
		daySlotTimings .put ("daySlotTimings",day);
		return daySlotTimings;
	}

	public static List<NameValuePair> savePatientTestListHandler_param(String patientId,String organisationId,String token,String userLoginName,String userId,String visitId,String doctorName,String status) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("patientId", patientId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("visitId", visitId));
		param.add(new BasicNameValuePair("doctorName", doctorName));
		param.add(new BasicNameValuePair("status", status));
		return param;
	}
	
	public static JSONObject getJSONsavePatientTestListHandler(String cartridgeName) {
		
		JSONObject patientTestList = new JSONObject();
		JSONArray patientTestListArr = new JSONArray();
		JSONObject patientTestListObj = new JSONObject();
		
		patientTestListObj.put("cartridgeName", cartridgeName);
		patientTestListArr.put(patientTestListObj);
		patientTestList.put("patientTestList", patientTestListArr);
		return patientTestListObj;
	}
	
	public static List<NameValuePair> validateDigitalSignature_param(String token,String userId,String userLoginName,String organisationId,String digitalSignaturePath,String digitalSignaturePassword) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("digitalSignaturePath", digitalSignaturePath));
		param.add(new BasicNameValuePair("digitalSignaturePassword", digitalSignaturePassword));
		return param;
	}
	
	public static List<NameValuePair> updateDoctorTimings_param(String token,String userId,String userLoginName,String organisationId,String locationId,String siteId,String fromDate,String toDate,String dateSlotId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("siteId", siteId));
		param.add(new BasicNameValuePair("fromDate", fromDate));
		param.add(new BasicNameValuePair("toDate", toDate));
		param.add(new BasicNameValuePair("dateSlotId", dateSlotId));
		return param;
	}
	
public static JSONObject getJSONupdateDoctorTimings( String  dayName, String startTime, String endTime,String daySlotId) {
		
		JSONObject daySlotTimings = new JSONObject();
		
		JSONObject day = new JSONObject();
		JSONArray dayARR = new JSONArray();
		JSONObject dayObj = new JSONObject();
		
		JSONArray daySlotArr=new JSONArray();
		JSONObject daySlotObj=new JSONObject();
		
		daySlotObj.put("startTime", startTime);
		daySlotObj.put("endTime", endTime);
		daySlotObj.put("daySlotId", daySlotId);
		
		daySlotArr.put(daySlotObj);
		
		dayObj .put("dayName", dayName);
		dayObj .put("daySlot", daySlotArr );
		
		dayARR .put(dayObj);
		
		day .put("day", dayARR);
		
		daySlotTimings .put ("daySlotTimings",day);
		
		return daySlotTimings;
	}

	//ListUserLocation
	public static List<NameValuePair> ListUserLocation_param(String token,String userId,String userLoginName,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		return param;
	}
	
	//checkDuplicateUser
	public static List<NameValuePair> CheckDuplicateUser_param(String organisationId,String token,String userDob,String gender,String roleType,String medicalRegNo,String mobileNo,String userLoginName) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userDob", userDob));
		param.add(new BasicNameValuePair("gender", gender));
		param.add(new BasicNameValuePair("roleType", roleType));
		param.add(new BasicNameValuePair("medicalRegNo", medicalRegNo));
		param.add(new BasicNameValuePair("mobileNo", mobileNo));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		return param;
	}
	
	//setUserGmailRefreshToken
	public static List<NameValuePair> setUserGmailRefreshToken(String token,String userLoginName,String userId,String organisationId,String refreshToken) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair("refreshToken", refreshToken));
		return param;
	}
	
	//fetchUserGmailRefreshToken
	public static List<NameValuePair> fetchUserGmailRefreshToken(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		return param;
	}
	
	//updateUserGmailRefreshToken
	public static List<NameValuePair> updateUserGmailRefreshToken(String token,String userLoginName,String userId,String organisationId,String refreshTokenId,String isActive) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair("refreshTokenId", refreshTokenId));
		param.add(new BasicNameValuePair(JsonParseLogin.isActive, isActive));
		return param;
	}

}