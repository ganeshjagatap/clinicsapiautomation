package com.clinic.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class ParamGeneratorConsultation {
	
	public static List<NameValuePair> param = null;
	
	//fetchVitalDtlsHandler
	public static List<NameValuePair> fetchVitalDtlsHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String visitId){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//createOrUpdatePatientVisitHandler
	public static List<NameValuePair> createOrUpdatePatientVisitHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId,
	String appointmentId,String role, String currentStatus, String patientVisitId, String operation, String patientMobileNo, String patientEmailId, String doctorTitle, String doctorName, String organizationName,
	String printVitals, String printSymptoms, String printExamination, String printDiagnosis,
	String printPrescription, String printGenericAdvice, String printFollowUp, String printTests,String refferal){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.appointmentId, appointmentId));
		param.add(new BasicNameValuePair(JsonParseConsultation.role, role));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentStatus, currentStatus));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientVisitId, patientVisitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.operation, operation));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientMobileNo, patientMobileNo));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientEmailId, patientEmailId));
		param.add(new BasicNameValuePair(JsonParseConsultation.doctorTitle, doctorTitle));
		param.add(new BasicNameValuePair(JsonParseConsultation.doctorName, doctorName));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationName, organizationName));
		param.add(new BasicNameValuePair(JsonParseConsultation.printVitals, printVitals));
		param.add(new BasicNameValuePair(JsonParseConsultation.printSymptoms, printSymptoms));
		param.add(new BasicNameValuePair(JsonParseConsultation.printExamination, printExamination));
		param.add(new BasicNameValuePair(JsonParseConsultation.printDiagnosis, printDiagnosis));
		param.add(new BasicNameValuePair(JsonParseConsultation.printPrescription, printPrescription));
		param.add(new BasicNameValuePair(JsonParseConsultation.printGenericAdvice, printGenericAdvice));
		param.add(new BasicNameValuePair(JsonParseConsultation.printFollowUp, printFollowUp));
		param.add(new BasicNameValuePair(JsonParseConsultation.printTests, printTests));
		param.add(new BasicNameValuePair(JsonParseConsultation.refferal, refferal));
	
	return param;
	}
	
	//searchAllergyHandler
	public static List<NameValuePair> searchAllergyHandler_Param(String userId,String userLoginName,String token, String organizationId ,String role, String searchString){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.role, role));
		param.add(new BasicNameValuePair(JsonParseConsultation.searchString, searchString));
		System.out.println("org "+JsonParseConsultation.organisationId);
		return param;
	}
	
	//searchMedicineHandler
	public static List<NameValuePair> searchMedicineHandler_Param(String userId,String userLoginName,String token,String organizationId,String searchString){
	
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.searchString, searchString));
		return param;
	}

	//searchExaminationTemplateHandler
	public static List<NameValuePair> searchExaminationTemplateHandler_Param(String userLoginName,String userId,String token,String organizationId,String searchString){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.searchString, searchString));
		return param;
	}
	
	//fetchExaminationTemplateHandler
	public static List<NameValuePair> fetchExaminationTemplateHandler_Param(String userLoginName,String userId,String token, String templateId ,String visitId,String organizationId,String patientId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.templateId, templateId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		return param;
	}
	
	//fetchExaminationDtlsHandler
	public static List<NameValuePair> fetchExaminationDtlsHandler_Param(String userLoginName,String userId,String token,String organizationId,String patientId, String visitId,String doctorSpecialty){
	
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.doctorSpecialty, doctorSpecialty));
		return param;
	}

	//fetchExaminationTemplateItemsHandler
	public static List<NameValuePair> fetchExaminationTemplateItemsHandler_Param(String userLoginName,String userId,String token,String templateId,String templateSectionId,String organizationId,String siteId,String locationId,String patientId, String visitId){
	
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.templateId, templateId));
		param.add(new BasicNameValuePair(JsonParseConsultation.templateSectionId, templateSectionId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//searchDiagnosisHandler
	public static List<NameValuePair> searchDiagnosisHandler_Param(String userLoginName,String userId,String token,String organizationId,String searchString){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.searchString, searchString));
		return param;
	}

	//searchFrequencyHandler
	public static List<NameValuePair> searchFrequencyHandler_Param(String userLoginName,String token,String organizationId,String searchString,String userId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.searchString, searchString));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		return param;
	}
	
	//fetchPatientReportsHandler
	public static List<NameValuePair> fetchPatientReportsHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String testOrScanDtlId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.testOrScanDtlId, testOrScanDtlId));
		return param;
	}
	
	//deletePatientTestOrScanReportHandler
	public static List<NameValuePair> deletePatientTestOrScanReportHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String testOrScanDtlId, String reportId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.testOrScanDtlId, testOrScanDtlId));
		param.add(new BasicNameValuePair(JsonParseConsultation.reportId, reportId));
		return param;
	}
	
	//uploadPatientTestOrScanReportHandler
	public static List<NameValuePair> uploadPatientTestOrScanReportHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String visitId, String reportPath,String testOrScanDtlId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.reportPath, reportPath));
		param.add(new BasicNameValuePair(JsonParseConsultation.testOrScanDtlId, testOrScanDtlId));
		return param;
	}

	//generateSummaryHandler
	public static List<NameValuePair> generateSummaryHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String visitId, String currentVisit){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentVisit, currentVisit));
		return param;
	}
	
	//fetchDiagnosisAndPrescriptionHandler
	public static List<NameValuePair> fetchDiagnosisAndPrescriptionHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId,String currentVisit){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentVisit, currentVisit));
		return param;
	}

	//fetchDiagnosisHandler
	public static List<NameValuePair> fetchDiagnosisHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId, String visitId,String currentVisit){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentVisit, currentVisit));
		return param;
	}

	//fetchPatientPrescriptionDtlsHandler
	public static List<NameValuePair> fetchPatientPrescriptionDtlsHandler_Param(String token,String userLoginName,String userId,String organizationId, String patientId, String visitId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//loadPrescriptionTemplateHandler
	public static List<NameValuePair> loadPrescriptionTemplateHandler_Param(String token,String userLoginName,String userId,String organizationId, String templateName){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.templateName, templateName));
		return param;
	}

	//fetchReferralInfoHandler
	public static List<NameValuePair> fetchReferralInfoHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String visitId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//persistReferralInfoHandler
	public static List<NameValuePair> persistReferralInfoHandler_Param(String token,String userLoginName,String userId,String organizationId, String toUserId, String locationId, String siteId, String patientId ,String visitId, String commentRemark, String doctorUserId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.toUserId, toUserId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.commentRemark, commentRemark));
		param.add(new BasicNameValuePair("doctorUserId", doctorUserId));		
		return param;
	}
	
	//fetchFollowupVisitInfoHandler
	public static List<NameValuePair> fetchFollowupVisitInfoHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String visitId){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//persistFollowupVisitInfoHandler
	public static List<NameValuePair> persistFollowupVisitInfoHandler_Param(String userId,String userLoginName,String token,String followUpAfter, String followUpUnit, String sendSms, String sendEmail, String organizationId ,String siteId, String locationId,String patientId, String visitId){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.followUpAfter, followUpAfter));
		param.add(new BasicNameValuePair(JsonParseConsultation.followUpUnit, followUpUnit));
		param.add(new BasicNameValuePair(JsonParseConsultation.sendSms, sendSms));
		param.add(new BasicNameValuePair(JsonParseConsultation.sendEmail, sendEmail));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//searchTestOrScanHandler
	public static List<NameValuePair> searchTestOrScanHandler_Param(String userId,String userLoginName,String token,String organizationId,String searchString){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.searchString, searchString));
		return param;
	}
	
	//fetchHomeMonitoringInfoHandler
	public static List<NameValuePair> fetchHomeMonitoringInfoHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String visitId){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//fetchPharmacyListHandler
	public static List<NameValuePair> fetchPharmacyListHandler_Param(String userId,String userLoginName,String token,String organizationId,String patientId,String visitId){
	
		param = new ArrayList<NameValuePair>();		
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//deletePrescriptionAsTemplateHandler
	public static List<NameValuePair> deletePrescriptionAsTemplateHandler_Param(String organizationId,String prescriptionTemplateName,String token,String userId,String userLoginName){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionTemplateName, prescriptionTemplateName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));	
		return param;
	}
	
	//fetchPatientInstrumentalInfoHandler
	public static List<NameValuePair> fetchPatientInstrumentalInfoHandler_Param(String createdUserId,String locationId,String organizationId,String patientId,String patientVisitId,String siteId,String token,String userLoginName){
	
		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.createdUserId, createdUserId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientVisitId, patientVisitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));	
		return param;
	}

	//fetchOrLoadAllTemplatesForUser
	public static List<NameValuePair> fetchOrLoadAllTemplatesForUser_Param(String userId,String userLoginName,String token,String organizationId,String templateName){
	
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.templateName, templateName));
		return param;
	}

	//fetchPatientPrescription
	public static List<NameValuePair> fetchPatientPrescription_Param(String userId,String userLoginName,String token,String organizationId,String patientId,String visitId){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	//fetchOrLoadTestOrScanTemplatesForUser
	public static List<NameValuePair> fetchOrLoadTestOrScanTemplatesForUser_Param(String userId,String userLoginName,String token,String organizationId,String templateName){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.templateName, templateName));
		return param;
	}

	//fetchVitalsForPreviousVisits
	public static List<NameValuePair> fetchVitalsForPreviousVisits_Param(String userId,String userLoginName,String token,String organizationId,String patientId,String vitalParameterName,String limit,String patientVisitId){
	
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.vitalParameterName, vitalParameterName));
		param.add(new BasicNameValuePair(JsonParseConsultation.limit, limit));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientVisitId, patientVisitId));
		return param;
	}

	//openConsultationDetails
	public static List<NameValuePair> openConsultationDetails_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId){
	
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		return param;
	}
	
	//bookConsultation
	public static List<NameValuePair> bookConsultation_Param(String token,String userLoginName,String userId,String organisationId,String role){
	
		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.role, role));
		return param;
	}

	//fetchPrescriptionHandler
	public static List<NameValuePair> fetchPrescriptionHandler_Param(String userId,String userLoginName,String token,String organizationId,String prescriptionDetailId,String templatePrescriptionId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionDetailId, prescriptionDetailId));
		param.add(new BasicNameValuePair(JsonParseConsultation.templatePrescriptionId, templatePrescriptionId));
		return param;
	}

	//fetchPatientTestsOrScansInfoHandler
	public static List<NameValuePair> fetchPatientTestsOrScansInfoHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String visitId,String scope){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.scope, scope));
		return param;
	}
	
	//generateSummaryPDFHandler
	public static List<NameValuePair> generateSummaryPDFHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId, String visitId, String uhid,String currentVisit,String clinicUserId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.uhid, uhid));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentVisit, currentVisit));
                param.add(new BasicNameValuePair(JsonParseConsultation.clinicUserId, clinicUserId));
		return param;
	}

	//generateModuleWiseSummaryPDFHandler
	public static List<NameValuePair> generateModuleWiseSummaryPDFHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,
	String patientId,String uhid,String visitId,String currentVisit,String printVitals,String printSymptoms,String printExamination,String printDiagnosis,String printPrescription,
	String printGenericAdvice,String printFollowUp,String printTests,String printReferral,String printMedicalHistory){


		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));		
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.uhid, uhid));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentVisit, currentVisit));
		param.add(new BasicNameValuePair(JsonParseConsultation.printVitals, printVitals));
		param.add(new BasicNameValuePair(JsonParseConsultation.printSymptoms, printSymptoms));
		param.add(new BasicNameValuePair(JsonParseConsultation.printExamination, printExamination));
		param.add(new BasicNameValuePair(JsonParseConsultation.printDiagnosis, printDiagnosis));
		param.add(new BasicNameValuePair(JsonParseConsultation.printPrescription, printPrescription));
		param.add(new BasicNameValuePair(JsonParseConsultation.printGenericAdvice, printGenericAdvice));
		param.add(new BasicNameValuePair(JsonParseConsultation.printFollowUp, printFollowUp));
		param.add(new BasicNameValuePair(JsonParseConsultation.printTests, printTests));
		param.add(new BasicNameValuePair(JsonParseConsultation.printReferral, printReferral));
		param.add(new BasicNameValuePair(JsonParseConsultation.printMedicalHistory, printMedicalHistory));
		return param;
	}
	
	//printConsultationSheet
	public static List<NameValuePair> printConsultationSheet_Param(String patientId,String siteId,String token,String userId,String locationId,String visitId,String doctorId,String organizationId,String userLoginName,String currentSatus,String role){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.doctorId, doctorId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentSatus, currentSatus));
		param.add(new BasicNameValuePair(JsonParseConsultation.role, role));
		return param;
	}
	
	//persistPatientDiagnosisInfoHandler
	public static List<NameValuePair> persistPatientDiagnosisInfoHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId){
	
		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
	
	public static JSONObject getJSONPersistPatientDiagnosisInfoHandler( String  patientDiagnosisId, String diagnosisId, String drSpecDiagnosisId, String diagnosisName,String stage,String onsetDate,String status,String closeDate,String operation,String onsetVisitId ) {

		JSONObject patientDiagnosis = new JSONObject();
		JSONArray patientDiagnosisArray = new JSONArray();
		JSONObject patientDiagnosisObject  = new JSONObject();
		patientDiagnosisObject .put(JsonParseConsultation.patientDiagnosisId, patientDiagnosisId);
		patientDiagnosisObject .put(JsonParseConsultation.diagnosisId, diagnosisId);
		patientDiagnosisObject .put(JsonParseConsultation.drSpecDiagnosisId, drSpecDiagnosisId);
		patientDiagnosisObject .put(JsonParseConsultation.diagnosisName, diagnosisName);
		patientDiagnosisObject .put(JsonParseConsultation.stage, stage);
		patientDiagnosisObject .put(JsonParseConsultation.onsetDate, onsetDate);
		patientDiagnosisObject .put(JsonParseConsultation.status, status);
		patientDiagnosisObject .put(JsonParseConsultation.closeDate, closeDate);
		patientDiagnosisObject .put(JsonParseConsultation.operation, operation);
		patientDiagnosisObject .put(JsonParseConsultation.onsetVisitId, onsetVisitId);
		patientDiagnosisArray.put(patientDiagnosisObject);
		
		patientDiagnosis.put("patientDiagnosis", patientDiagnosisArray );
		System.out.println("req:"+patientDiagnosis.toString());
		return patientDiagnosis;	
	}
	
	//persistExaminationInfoHandler
	public static List<NameValuePair> PersistExaminationInfoHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId,String templateId){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.templateId, templateId));
		return param;
	}
	
	public static JSONObject getJSONPersistExaminationInfoHandler( String  templateSectionId, String templateSectionStatus, String templateItemId, String itemValue) {

		JSONObject specialtyExam = new JSONObject();
		JSONArray specialtyExamArray = new JSONArray();
		JSONObject specialtyExamObject  = new JSONObject();
		specialtyExamObject .put(JsonParseConsultation.templateSectionId, templateSectionId);
		specialtyExamObject .put(JsonParseConsultation.templateSectionStatus, templateSectionStatus);
		specialtyExamObject .put(JsonParseConsultation.templateItemId, templateItemId);
		specialtyExamObject .put(JsonParseConsultation.itemValue, itemValue);
		specialtyExamArray.put(specialtyExamObject);
		
		specialtyExam.put(JsonParseConsultation.specialtyExam, specialtyExamArray );
		System.out.println("req:"+specialtyExam.toString());
		return specialtyExam;
	}
	
	//persistPrescriptionHandler
	public static List<NameValuePair> PersistPrescriptionHandler_Param(String prescriptionHdrId,String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId,String genericAdvice,String genericAdviceId){
	
		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionHdrId, prescriptionHdrId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.genericAdvice, genericAdvice));
		param.add(new BasicNameValuePair(JsonParseConsultation.genericAdviceId, genericAdviceId));
		return param;
	}
	
	public static JSONObject getJSONPersistPrescriptionHandler( String  prescriptionId, String genericDrugCode, String drugCode, String drugName,String route,String frequencyMor,String frequencyAn,String frequencyNight,String dosage,String durationNo,String durationUnit,String startDate,String sos,
	String af,String bf,String wf,String emptyStomach,String beforeSleeping,String freqSpecInstruction,String medicineType,String operation) {
		JSONObject prescription = new JSONObject();
		JSONArray prescriptionArray = new JSONArray();
		JSONObject prescriptionObject  = new JSONObject();
		prescriptionObject .put(JsonParseConsultation.prescriptionId, prescriptionId);
		prescriptionObject .put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		prescriptionObject .put(JsonParseConsultation.drugCode, drugCode);
		prescriptionObject .put(JsonParseConsultation.drugName, drugName);
		prescriptionObject .put(JsonParseConsultation.route, route);
		prescriptionObject .put(JsonParseConsultation.frequencyMor, frequencyMor);
		prescriptionObject .put(JsonParseConsultation.frequencyAn, frequencyAn);
		prescriptionObject .put(JsonParseConsultation.frequencyNight, frequencyNight);
		prescriptionObject .put(JsonParseConsultation.dosage, dosage);
		prescriptionObject .put(JsonParseConsultation.durationNo, durationNo);
		prescriptionObject .put(JsonParseConsultation.durationUnit, durationUnit);
		prescriptionObject .put(JsonParseConsultation.startDate, startDate);
		prescriptionObject .put(JsonParseConsultation.sos, sos);
		prescriptionObject .put(JsonParseConsultation.af, af);
		prescriptionObject .put(JsonParseConsultation.bf, bf);
		prescriptionObject .put(JsonParseConsultation.wf, wf);
		prescriptionObject .put(JsonParseConsultation.emptyStomach, emptyStomach);
		prescriptionObject .put(JsonParseConsultation.beforeSleeping, beforeSleeping);
		prescriptionObject .put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		prescriptionObject .put(JsonParseConsultation.medicineType, medicineType);
		prescriptionObject .put(JsonParseConsultation.operation, operation);
		prescriptionArray.put(prescriptionObject);
				
		prescription.put(JsonParseConsultation.prescription, prescriptionArray );
		System.out.println("req:"+prescription.toString());
		return prescription;
	}
	
	//updatePrescriptionAsTemplateHandler
	public static List<NameValuePair> UpdatePrescriptionAsTemplateHandler_Param(String userId,String userLoginName,String token,String organizationId,String prescriptionTemplateName){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionTemplateName, prescriptionTemplateName));
		return param;
	}
	
	public static JSONObject getJSONUpdatePrescriptionAsTemplateHandler( String genericDrugCode, String drugCode, String drugName,String route,String frequencyMor,String frequencyAn,String frequencyNight,String dosage,String durationNo,String durationUnit,String sos,
	String af,String bf,String wf,String emptyStomach,String beforeSleeping,String freqSpecInstruction,String medicineType,String frequency,String duration,String dosageAdvice,String drugType) {

		JSONObject prescription = new JSONObject();
		JSONArray prescriptionArray = new JSONArray();
		JSONObject prescriptionObject  = new JSONObject();
		prescriptionObject .put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		prescriptionObject .put(JsonParseConsultation.drugCode, drugCode);
		prescriptionObject .put(JsonParseConsultation.drugName, drugName);
		prescriptionObject .put(JsonParseConsultation.route, route);
		prescriptionObject .put(JsonParseConsultation.frequencyMor, frequencyMor);
		prescriptionObject .put(JsonParseConsultation.frequencyAn, frequencyAn);
		prescriptionObject .put(JsonParseConsultation.frequencyNight, frequencyNight);
		prescriptionObject .put(JsonParseConsultation.dosage, dosage);
		prescriptionObject .put(JsonParseConsultation.durationNo, durationNo);
		prescriptionObject .put(JsonParseConsultation.durationUnit, durationUnit);
		prescriptionObject .put(JsonParseConsultation.sos, sos);
		prescriptionObject .put(JsonParseConsultation.af, af);
		prescriptionObject .put(JsonParseConsultation.bf, bf);
		prescriptionObject .put(JsonParseConsultation.wf, wf);
		prescriptionObject .put(JsonParseConsultation.emptyStomach, emptyStomach);
		prescriptionObject .put(JsonParseConsultation.beforeSleeping, beforeSleeping);
		prescriptionObject .put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		prescriptionObject .put(JsonParseConsultation.medicineType, medicineType);
		prescriptionObject .put(JsonParseConsultation.frequency, frequency);
		prescriptionObject .put(JsonParseConsultation.duration, duration);
		prescriptionObject .put(JsonParseConsultation.dosageAdvice, dosageAdvice);
		prescriptionObject .put(JsonParseConsultation.drugType, drugType);
		prescriptionArray.put(prescriptionObject);
		
		prescription.put(JsonParseConsultation.prescription, prescriptionArray );
		System.out.println("req:"+prescription.toString());
		return prescription;
	}
	
	//searchSymptomsHandler
	public static List<NameValuePair> searchSymptomsHandler_Param(String userId,String userLoginName,String token, String organizationId ,String role, String searchString){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.role, role));
		param.add(new BasicNameValuePair(JsonParseConsultation.searchString, searchString));
		return param;
	}
	
	//fetchPatientMedicalHistoryHandler
	public static List<NameValuePair> fetchPatientMedicalHistoryHandler_Param(String userId,String userLoginName,String token, String organizationId ,String siteId, String locationId,String patientId,String visitId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}

	//persistPatientMedicalHistoryHandler
	public static List<NameValuePair> persistPatientMedicalHistoryHandler_Param(String userId,String userLoginName,String token,
			String immunization,String bcg,String dpt,String tetanus,String immunizationDesc,String familyMedicalHistory,
			String patientMedicalHistory, String orgId,String patientId,String visitId,String siteId,String locationId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.immunization, immunization));
		param.add(new BasicNameValuePair(JsonParseConsultation.bcg, bcg));
		param.add(new BasicNameValuePair(JsonParseConsultation.dpt, dpt));
		param.add(new BasicNameValuePair(JsonParseConsultation.tetanus, tetanus));
		param.add(new BasicNameValuePair(JsonParseConsultation.immunizationDesc, immunizationDesc));
		param.add(new BasicNameValuePair(JsonParseConsultation.familyMedicalHistory, familyMedicalHistory));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientMedicalHistory, patientMedicalHistory));
		param.add(new BasicNameValuePair(JsonParseConsultation.orgId, orgId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		return param;
	}

	//fetchPatientDtlsForVCConsultation
	public static List<NameValuePair> fetchPatientDtlsForVCConsultation_Param(String userId,String userLoginName,String token,String organizationId,String patientId,String appointmentId){

		param = new ArrayList<NameValuePair>();		
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.appointmentId, appointmentId));
		return param;
	}

	
	public static JSONObject getJSONBookConsultation( String  patientId, String dateSlotId, String slotId, String appointmentDate,String startTime,String endTime,String locationId,String siteId,String clinicUserId,String reasonOfVisit,String locationName,String organizationName,String doctorName ) {

		JSONObject bookConsultation = new JSONObject();
		//JSONArray patientDetailsArray = new JSONArray();
		JSONObject bookConsultationArray  = new JSONObject();
		bookConsultationArray .put(JsonParseConsultation.patientId, patientId);
		bookConsultationArray .put(JsonParseConsultation.dateSlotId, dateSlotId);
		bookConsultationArray .put(JsonParseConsultation.slotId, slotId);
		bookConsultationArray .put(JsonParseConsultation.appointmentDate, appointmentDate);
		bookConsultationArray .put(JsonParseConsultation.startTime, startTime);
		bookConsultationArray .put(JsonParseConsultation.endTime, endTime);
		bookConsultationArray .put(JsonParseConsultation.locationId, locationId);
		bookConsultationArray .put(JsonParseConsultation.siteId, siteId);
		bookConsultationArray .put(JsonParseConsultation.clinicUserId, clinicUserId);
		bookConsultationArray .put(JsonParseConsultation.reasonOfVisit, reasonOfVisit);
		bookConsultationArray .put(JsonParseConsultation.locationName, locationName);
		bookConsultationArray .put(JsonParseConsultation.organizationName, organizationName);
		bookConsultationArray .put(JsonParseConsultation.doctorName, doctorName);
		
		//accountDetailsArray.put(patientDetailsArray );

		bookConsultation.put(JsonParseConsultation.appointment, bookConsultationArray );
		System.out.println("req:"+bookConsultation.toString());
		return bookConsultation;

	}
	
	//persistVitalsInfoHandler
	public static List<NameValuePair> persistVitalsInfoHandler_param(String userId,String userLoginName,String token,String role,String organizationId,String siteId,String locationId,String patientId,String visitId,String currentStatus,String persistAllergies,String persistVitals,String persistSymptoms) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.role, role));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentStatus, currentStatus));
		param.add(new BasicNameValuePair(JsonParseConsultation.persistAllergies, persistAllergies));
		param.add(new BasicNameValuePair(JsonParseConsultation.persistVitals, persistVitals));
		param.add(new BasicNameValuePair(JsonParseConsultation.persistSymptoms, persistSymptoms));
		return param;
	}
	
	public static JSONObject getJSONpersistVitalsInfoHandler(String patientAllergyId,String allergyId,String allergyCimsCode,String allergyName,String operation,String patientVitalId,String parameter,String parameterValue,String parameterComment,String patientSymptomId,String symptomId,String symptomName,String duration,String otherDetails) {
		
		JSONObject patient = new JSONObject();
		
		JSONArray patientAllergyArr = new JSONArray();
		JSONObject patientAllergyObj = new JSONObject();
		patientAllergyObj.put(JsonParseConsultation.patientAllergyId, patientAllergyId);
		patientAllergyObj.put(JsonParseConsultation.allergyId, allergyId);
		patientAllergyObj.put(JsonParseConsultation.allergyCimsCode, allergyCimsCode);
		patientAllergyObj.put(JsonParseConsultation.allergyName, allergyName);
		patientAllergyObj.put(JsonParseConsultation.operation, operation);
		patientAllergyArr.put(patientAllergyObj);
		patient.put(JsonParseConsultation.patientAllergy, patientAllergyArr);
		
		JSONArray patientVitalArr = new JSONArray();
		JSONObject patientVitalObj = new JSONObject();
		patientVitalObj.put(JsonParseConsultation.patientVitalId, patientVitalId);
		patientVitalObj.put(JsonParseConsultation.parameter, parameter);
		patientVitalObj.put(JsonParseConsultation.parameterValue, parameterValue);
		patientVitalObj.put(JsonParseConsultation.parameterComment, parameterComment);
		patientVitalObj.put(JsonParseConsultation.operation, operation);
		patientVitalArr.put(patientVitalObj);
		patient.put(JsonParseConsultation.patientVital, patientVitalArr);
		
		JSONArray patientSymptomArr = new JSONArray();
		JSONObject patientSymptomObj = new JSONObject();
		patientSymptomObj.put(JsonParseConsultation.patientSymptomId, patientSymptomId);
		patientSymptomObj.put(JsonParseConsultation.symptomId, symptomId);
		patientSymptomObj.put(JsonParseConsultation.symptomName, symptomName);
		patientSymptomObj.put(JsonParseConsultation.duration, duration);
		patientSymptomObj.put(JsonParseConsultation.otherDetails, otherDetails);
		patientSymptomObj.put(JsonParseConsultation.operation, operation);
		patientSymptomArr.put(patientSymptomObj);
		patient.put(JsonParseConsultation.patientSymptom, patientSymptomArr);
		return patient;
	}
	
	//persistExaminationInfoHandler
	public static List<NameValuePair> persistExaminationInfoHandler_param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId,String templateId) {
		
		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.templateId, templateId));
		return param;
	}
	
	public static JSONObject getJSONpersistExaminationInfoHandler(String templateSectionId,String templateSectionStatus,String templateItemId,String itemValue) {
		
		JSONObject specialtyExam = new JSONObject();
		JSONArray specialtyExamArray = new JSONArray();
		JSONObject specialtyExamObject  = new JSONObject();
		specialtyExamObject .put(JsonParseConsultation.templateSectionId, templateSectionId);
		specialtyExamObject .put(JsonParseConsultation.templateSectionStatus, templateSectionStatus);
		specialtyExamObject .put(JsonParseConsultation.templateItemId, templateItemId);
		specialtyExamObject .put(JsonParseConsultation.itemValue, itemValue);
		specialtyExamArray.put(specialtyExamObject);
		
		specialtyExam.put(JsonParseConsultation.specialtyExam, specialtyExamArray );
		System.out.println("req:"+specialtyExam.toString());
		return specialtyExam;
	}
	
	//persistPrescriptionHandler
	public static List<NameValuePair> persistPrescriptionHandler_param(String prescriptionHdrId,String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId,String genericAdvice,String genericAdviceId) {
		
		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionHdrId, prescriptionHdrId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.genericAdvice, genericAdvice));
		param.add(new BasicNameValuePair(JsonParseConsultation.genericAdviceId, genericAdviceId));
		return param;
	}
	
	public static JSONObject getJSONpersistPrescriptionHandler(String prescriptionId,String genericDrugCode,String drugCode,String drugName,String route,String frequencyMor,String frequencyAn,String frequencyNight,String dosage,String durationNo,String durationUnit,String startDate,String sos,String af,String bf,
		String wf,String emptyStomach,String beforeSleeping,String freqSpecInstruction,String medicineType,String operation){
		
		JSONObject prescription = new JSONObject();
		JSONArray prescriptionArray = new JSONArray();
		JSONObject prescriptionObject  = new JSONObject();
		prescriptionObject .put(JsonParseConsultation.prescriptionId, prescriptionId);
		prescriptionObject .put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		prescriptionObject .put(JsonParseConsultation.drugCode, drugCode);
		prescriptionObject .put(JsonParseConsultation.drugName, drugName);
		prescriptionObject .put(JsonParseConsultation.route, route);
		prescriptionObject .put(JsonParseConsultation.frequencyMor, frequencyMor);
		prescriptionObject .put(JsonParseConsultation.frequencyAn, frequencyAn);
		prescriptionObject .put(JsonParseConsultation.frequencyNight, frequencyNight);
		prescriptionObject .put(JsonParseConsultation.dosage, dosage);
		prescriptionObject .put(JsonParseConsultation.durationNo, durationNo);
		prescriptionObject .put(JsonParseConsultation.durationUnit, durationUnit);
		prescriptionObject .put(JsonParseConsultation.startDate, startDate);
		prescriptionObject .put(JsonParseConsultation.sos, sos);
		prescriptionObject .put(JsonParseConsultation.af, af);
		prescriptionObject .put(JsonParseConsultation.bf, bf);
		prescriptionObject .put(JsonParseConsultation.wf, wf);
		prescriptionObject .put(JsonParseConsultation.emptyStomach, emptyStomach);
		prescriptionObject .put(JsonParseConsultation.beforeSleeping, beforeSleeping);
		prescriptionObject .put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		prescriptionObject .put(JsonParseConsultation.medicineType, medicineType);
		prescriptionObject .put(JsonParseConsultation.operation, operation);
		prescriptionArray.put(prescriptionObject);
				
		prescription.put(JsonParseConsultation.prescription, prescriptionArray );
		System.out.println("req:"+prescription.toString());
		return prescription;
	}

	//checkCimsInteractionsHandler
	public static List<NameValuePair> checkCimsInteractionsHandler_param(String userLoginName,String userId,String token,String organizationId) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		return param;
	}
	
	public static JSONObject getJSONcheckCimsInteractionsHandler(String drugType,String drugNameprescribingDrug,String drugCodeprescribingDrug,String allergyCimsCode,String allergyName) {
		
		JSONObject request = new JSONObject();
		JSONArray prescribingDrugArr = new JSONArray();
		JSONObject prescribingDrugObj = new JSONObject();
		prescribingDrugObj.put(JsonParseConsultation.drugCode, drugCodeprescribingDrug);
		prescribingDrugObj.put(JsonParseConsultation.drugName, drugNameprescribingDrug);
		prescribingDrugObj.put(JsonParseConsultation.drugType, drugType);
		prescribingDrugArr.put(prescribingDrugObj);
		request.put(JsonParseConsultation.prescribingDrug, prescribingDrugArr);
				
		JSONArray prescribedDrugsArr = new JSONArray();
		request.put(JsonParseConsultation.prescribedDrugs, prescribedDrugsArr);
		
		JSONArray patientAllergiesArr = new JSONArray();
		JSONObject patientAllergiesObj = new JSONObject();
		patientAllergiesObj.put(JsonParseConsultation.allergyCimsCode, allergyCimsCode);
		patientAllergiesObj.put(JsonParseConsultation.allergyName, allergyName);
		patientAllergiesArr.put(patientAllergiesObj);
		request.put(JsonParseConsultation.patientAllergies, patientAllergiesArr);
		return request;
	}
	
	//generatePrescriptionPDFHandler
	public static List<NameValuePair> generatePrescriptionPDFHandler_param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String uhid,String visitId,String currentVisit,String pharmacyId,String doctorName,String patientContactNumber,String digitalSignaturePath,String digitalSignaturePassword) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.uhid, uhid));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentVisit, currentVisit));
		param.add(new BasicNameValuePair(JsonParseConsultation.pharmacyId, pharmacyId));
		param.add(new BasicNameValuePair(JsonParseConsultation.doctorName, doctorName));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientContactNumber, patientContactNumber));
		param.add(new BasicNameValuePair(JsonParseConsultation.digitalSignaturePath, digitalSignaturePath));
		param.add(new BasicNameValuePair(JsonParseConsultation.digitalSignaturePassword, digitalSignaturePassword));
		return param;
	}
	
	//generateConsultingPrescriptionPdfHandler
	public static List<NameValuePair> generateConsultingPrescriptionPdfHandler_param(String patientId,String siteId,String token,String currentVisit,String userId,String locationId,String visitId,String uhid,String organizationId,String userLoginName) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.currentVisit, currentVisit));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.uhid, uhid));	
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		return param;
	}
	
	//updateTestOrScanTemplate
	public static List<NameValuePair> updateTestOrScanTemplate_param(String userId,String userLoginName,String token,String organizationId,String testOrscanTemplateName) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.testOrscanTemplateName, testOrscanTemplateName));	
		return param;
	}
	
	public static JSONObject getJSONupdateTestOrScanTemplate(String serviceId,String serviceName,String notes) {
		
		JSONObject testOrscan = new JSONObject();
		JSONArray testOrscanArr = new JSONArray();
		JSONObject testOrscanObj = new JSONObject();
		
		testOrscanObj.put(JsonParseConsultation.serviceId, serviceId);
		testOrscanObj.put(JsonParseConsultation.serviceName, serviceName);
		testOrscanObj.put(JsonParseConsultation.notes, notes);
		testOrscanArr.put(testOrscanObj);
		testOrscan.put(JsonParseConsultation.testOrscan, testOrscanArr);
		return testOrscan;
		}
	
	//persistTestOrScanTemplateMaster
	public static List<NameValuePair> persistTestOrScanTemplateMaster_param(String userId,String userLoginName,String token,String organizationId,String testOrscanTemplateName) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.testOrscanTemplateName, testOrscanTemplateName));
		return param;
	}
	
	public static JSONObject getJSONpersistTestOrScanTemplateMaster(String serviceId,String serviceName,String notes) {
		
		JSONObject testOrscan = new JSONObject();
		JSONArray testOrscanArr = new JSONArray();
		JSONObject testOrscanObj = new JSONObject();
		
		testOrscanObj.put(JsonParseConsultation.serviceId, serviceId);
		testOrscanObj.put(JsonParseConsultation.serviceName, serviceName);
		testOrscanObj.put(JsonParseConsultation.notes, notes);
		testOrscanArr.put(testOrscanObj);
		testOrscan.put(JsonParseConsultation.testOrscan, testOrscanArr);
		return testOrscan;
	}
	
	//savePrescriptionTemplate
	public static List<NameValuePair> savePrescriptionTemplate_param(String userId,String userLoginName,String token,String organizationId,String locationId,String prescriptionTemplateName) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionTemplateName, prescriptionTemplateName));
		return param;
	}
	
	public static JSONObject getJSONsavePrescriptionTemplate (String genericDrugCode,String drugCode,String drugName,String route,String frequencyMor,String frequencyAn,String frequencyNight,String dosage,String durationNo,String durationUnit,String sos,String af,String bf,
		String wf,String emptyStomach,String beforeSleeping,String freqSpecInstruction,String medicineType,String frequency,String duration,String dosageAdvice,String drugType) {
		
		JSONObject prescription = new JSONObject();
		JSONArray prescriptionArr = new JSONArray();
		JSONObject prescriptionObj = new JSONObject();
		
		prescriptionObj.put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		prescriptionObj.put(JsonParseConsultation.drugCode, drugCode);
		prescriptionObj.put(JsonParseConsultation.drugName, drugName);
		prescriptionObj.put(JsonParseConsultation.route, route);
		prescriptionObj.put(JsonParseConsultation.frequencyMor, frequencyMor);
		prescriptionObj.put(JsonParseConsultation.frequencyAn, frequencyAn);
		prescriptionObj.put(JsonParseConsultation.frequencyNight, frequencyNight);
		prescriptionObj.put(JsonParseConsultation.dosage, dosage);
		prescriptionObj.put(JsonParseConsultation.durationNo, durationNo);
		prescriptionObj.put(JsonParseConsultation.durationUnit, durationUnit);
		prescriptionObj.put(JsonParseConsultation.sos, sos);
		prescriptionObj.put(JsonParseConsultation.af, af);
		prescriptionObj.put(JsonParseConsultation.bf, bf);
		prescriptionObj.put(JsonParseConsultation.wf, wf);
		prescriptionObj.put(JsonParseConsultation.emptyStomach, emptyStomach);
		prescriptionObj.put(JsonParseConsultation.beforeSleeping, beforeSleeping);
		prescriptionObj.put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		prescriptionObj.put(JsonParseConsultation.medicineType, medicineType);
		prescriptionObj.put(JsonParseConsultation.frequency, frequency);
		prescriptionObj.put(JsonParseConsultation.duration, duration);
		prescriptionObj.put(JsonParseConsultation.dosageAdvice, dosageAdvice);
		prescriptionObj.put(JsonParseConsultation.drugType, drugType);
		prescriptionArr.put(prescriptionObj);
		prescription.put(JsonParseConsultation.prescription, prescriptionArr);
		return prescription;
	}
	
	//updatePrescriptionTemplate
	public static List<NameValuePair> updatePrescriptionTemplate_param(String userId,String userLoginName,String token,String organizationId,String locationId,String prescriptionTemplateName) {
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionTemplateName, prescriptionTemplateName));
		return param;
	}
	
	public static JSONObject getJSONupdatePrescriptionTemplate(String genericDrugCode,String drugCode,String drugName,String route,String frequencyMor,String frequencyAn,String frequencyNight,String dosage,String durationNo,String durationUnit,String sos,String af,String bf,
		String wf,String emptyStomach,String beforeSleeping,String freqSpecInstruction,String medicineType,String frequency,String duration,String dosageAdvice,String drugType) {
		
		JSONObject prescription = new JSONObject();
		JSONArray prescriptionArr = new JSONArray();
		JSONObject prescriptionObj = new JSONObject();
		
		prescriptionObj.put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		prescriptionObj.put(JsonParseConsultation.drugCode, drugCode);
		prescriptionObj.put(JsonParseConsultation.drugName, drugName);
		prescriptionObj.put(JsonParseConsultation.route, route);
		prescriptionObj.put(JsonParseConsultation.frequencyMor, frequencyMor);
		prescriptionObj.put(JsonParseConsultation.frequencyAn, frequencyAn);
		prescriptionObj.put(JsonParseConsultation.frequencyNight, frequencyNight);
		prescriptionObj.put(JsonParseConsultation.dosage, dosage);
		prescriptionObj.put(JsonParseConsultation.durationNo, durationNo);
		prescriptionObj.put(JsonParseConsultation.durationUnit, durationUnit);
		prescriptionObj.put(JsonParseConsultation.sos, sos);
		prescriptionObj.put(JsonParseConsultation.af, af);
		prescriptionObj.put(JsonParseConsultation.bf, bf);
		prescriptionObj.put(JsonParseConsultation.wf, wf);
		prescriptionObj.put(JsonParseConsultation.emptyStomach, emptyStomach);
		prescriptionObj.put(JsonParseConsultation.beforeSleeping, beforeSleeping);
		prescriptionObj.put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		prescriptionObj.put(JsonParseConsultation.medicineType, medicineType);
		prescriptionObj.put(JsonParseConsultation.frequency, frequency);
		prescriptionObj.put(JsonParseConsultation.duration, duration);
		prescriptionObj.put(JsonParseConsultation.dosageAdvice, dosageAdvice);
		prescriptionObj.put(JsonParseConsultation.drugType, drugType);
		prescriptionArr.put(prescriptionObj);
		prescription.put(JsonParseConsultation.prescription, prescriptionArr);
		return prescription;
	}

	//updatePrescriptionAsTemplateHandler
	public static List<NameValuePair> updatePrescriptionAsTemplateHandler_param(String userId,String userLoginName,String token,String organizationId,String prescriptionTemplateName) {
		
		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionTemplateName, prescriptionTemplateName));
		return param;
	}
	
	public static JSONObject getJSONupdatePrescriptionAsTemplateHandler(String genericDrugCode,String drugCode,String drugName,String route,String frequencyMor,String frequencyAn,String frequencyNight,String dosage,String durationNo,String durationUnit,String sos,String af,String bf,
		String wf,String emptyStomach,String beforeSleeping,String freqSpecInstruction,String medicineType,String frequency,String duration,String dosageAdvice,String drugType) {
		
		JSONObject prescription = new JSONObject();
		JSONArray prescriptionArray = new JSONArray();
		JSONObject prescriptionObject  = new JSONObject();
		prescriptionObject .put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		prescriptionObject .put(JsonParseConsultation.drugCode, drugCode);
		prescriptionObject .put(JsonParseConsultation.drugName, drugName);
		prescriptionObject .put(JsonParseConsultation.route, route);
		prescriptionObject .put(JsonParseConsultation.frequencyMor, frequencyMor);
		prescriptionObject .put(JsonParseConsultation.frequencyAn, frequencyAn);
		prescriptionObject .put(JsonParseConsultation.frequencyNight, frequencyNight);
		prescriptionObject .put(JsonParseConsultation.dosage, dosage);
		prescriptionObject .put(JsonParseConsultation.durationNo, durationNo);
		prescriptionObject .put(JsonParseConsultation.durationUnit, durationUnit);
		prescriptionObject .put(JsonParseConsultation.sos, sos);
		prescriptionObject .put(JsonParseConsultation.af, af);
		prescriptionObject .put(JsonParseConsultation.bf, bf);
		prescriptionObject .put(JsonParseConsultation.wf, wf);
		prescriptionObject .put(JsonParseConsultation.emptyStomach, emptyStomach);
		prescriptionObject .put(JsonParseConsultation.beforeSleeping, beforeSleeping);
		prescriptionObject .put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		prescriptionObject .put(JsonParseConsultation.medicineType, medicineType);
		prescriptionObject .put(JsonParseConsultation.frequency, frequency);
		prescriptionObject .put(JsonParseConsultation.duration, duration);
		prescriptionObject .put(JsonParseConsultation.dosageAdvice, dosageAdvice);
		prescriptionObject .put(JsonParseConsultation.drugType, drugType);
		prescriptionArray.put(prescriptionObject);
		
		prescription.put(JsonParseConsultation.prescription, prescriptionArray );
		System.out.println("req:"+prescription.toString());
		return prescription;
	}
	
	//persistPatientInstrumentalListInfoHandler
	public static List<NameValuePair> persistPatientInstrumentalListInfoHandler_param(String organizationId,String token,String userLoginName,String userId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		return param;
	}
	
	public static JSONObject getJSONpersistPatientInstrumentalListInfoHandler(String cartridgeName, String patientId, String testDate, String labName, String parameter, String parameterreference, String parameterunit, String parametervalue, String patientInstrumentalId, String parameterComment) {
		
		JSONObject patientInstrumentaldatalist = new JSONObject();
		JSONArray patientInstrumentaldatalistArr = new JSONArray();
		JSONObject patientInstrumentaldatalistObj = new JSONObject();
		
		patientInstrumentaldatalistObj.put(JsonParseConsultation.cartridgeName, cartridgeName);
		patientInstrumentaldatalistObj.put(JsonParseConsultation.patientId, patientId);
		patientInstrumentaldatalistObj.put(JsonParseConsultation.testDate, testDate);
		patientInstrumentaldatalistObj.put(JsonParseConsultation.labName, labName);
		
		JSONArray patientInstrumentalArr = new JSONArray();
		JSONObject patientInstrumentalObj = new JSONObject();
		
		patientInstrumentalObj.put(JsonParseConsultation.parameter, parameter);
		patientInstrumentalObj.put(JsonParseConsultation.parameterreference, parameterreference);
		patientInstrumentalObj.put(JsonParseConsultation.parameterunit, parameterunit);
		patientInstrumentalObj.put(JsonParseConsultation.parametervalue, parametervalue);
		patientInstrumentalObj.put(JsonParseConsultation.patientInstrumentalId, patientInstrumentalId);
		patientInstrumentalObj.put(JsonParseConsultation.parameterComment, parameterComment);
		patientInstrumentalArr.put(patientInstrumentalObj);
		patientInstrumentaldatalistObj.put(JsonParseConsultation.patientInstrumental, patientInstrumentalArr);
		
		patientInstrumentaldatalistArr.put(patientInstrumentaldatalistObj);
		patientInstrumentaldatalist.put(JsonParseConsultation.patientInstrumentaldatalist, patientInstrumentaldatalistArr);
		return patientInstrumentaldatalist;
	}
	
	//persistPatientInstrumentalInfoHandler
	public static List<NameValuePair> persistPatientInstrumentalInfoHandler_param(String organizationId,String token,String userLoginName,String userId,String patientId,String cartridgeName,String byPassValidationCheckForJob,String labName,String testDate) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.cartridgeName, cartridgeName));
		param.add(new BasicNameValuePair(JsonParseConsultation.byPassValidationCheckForJob, byPassValidationCheckForJob));
		param.add(new BasicNameValuePair(JsonParseConsultation.labName, labName));
		param.add(new BasicNameValuePair(JsonParseConsultation.testDate, testDate));
		return param;
	}
	
	public static JSONObject getJSONpersistPatientInstrumentalInfoHandler(String patientInstrumentalId, String parameter, String parametervalue, String parameterunit, String parameterreference, String parametercomment) {
		
		JSONObject patientInstrumental = new JSONObject();
		JSONArray patientInstrumentalArr = new JSONArray();
		JSONObject patientInstrumentalObj = new JSONObject();
		
		patientInstrumentalObj.put(JsonParseConsultation.patientInstrumentalId, patientInstrumentalId);
		patientInstrumentalObj.put(JsonParseConsultation.parameter, parameter);
		patientInstrumentalObj.put(JsonParseConsultation.parametervalue, parametervalue);
		patientInstrumentalObj.put(JsonParseConsultation.parameterunit, parameterunit);
		patientInstrumentalObj.put(JsonParseConsultation.parameterreference, parameterreference);
		patientInstrumentalObj.put(JsonParseConsultation.parametercomment, parametercomment);
		
		patientInstrumentalArr.put(patientInstrumentalObj);
		patientInstrumental.put(JsonParseConsultation.patientInstrumental, patientInstrumentalArr);
		return patientInstrumental;
	}
	
	//savePatientTestListHandler
	public static List<NameValuePair> savePatientTestListHandler_param(String patientId,String organizationId,String token,String userLoginName,String userId,String visitId,String doctorName,String status) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.doctorName, doctorName));
		param.add(new BasicNameValuePair(JsonParseConsultation.status, status));
		return param;
	}
	
	public static JSONObject getJSONsavePatientTestListHandler(String cartridgeName) {
		
		JSONObject patientTestList = new JSONObject();
		JSONArray patientTestListArr = new JSONArray();
		JSONObject patientTestListObj = new JSONObject();
		
		patientTestListObj.put(JsonParseConsultation.cartridgeName, cartridgeName);
		patientTestListArr.put(patientTestListObj);
		patientTestList.put(JsonParseConsultation.patientTestList, patientTestListArr);
		return patientTestList;
	}
	
	//PersistDiagnosisAndPrescriptionHandler
	public static List<NameValuePair> PersistDiagnosisAndPrescriptionHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId,String status){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.status, status));
		return param;
	}
		
	public static JSONObject getJSONPersistDiagnosisAndPrescriptionHandler( String patientDiagnosisId, String diagnosisId, String drSpecDiagnosisId,String diagnosisName,String stage,String onsetDate,String status,String closeDate,String operation,String onsetVisitId,String prescriptionHdrId,
		String prescriptionId,String genericDrugCode,String drugCode,String drugName,String frequency,String duration,String startDate,String freqSpecInstruction,String operation1,String dosageAdvice,String drugType,String genericAdvice,String genericAdviceId) {
		
		JSONObject diagnosis = new JSONObject();
		JSONArray diagnosisArray = new JSONArray();
		JSONObject patientDiagnosis = new JSONObject();
		
		patientDiagnosis .put(JsonParseConsultation.patientDiagnosisId, patientDiagnosisId);
		patientDiagnosis .put(JsonParseConsultation.diagnosisId, diagnosisId);
		patientDiagnosis .put(JsonParseConsultation.drSpecDiagnosisId, drSpecDiagnosisId);
		patientDiagnosis .put(JsonParseConsultation.diagnosisName, diagnosisName);
		patientDiagnosis .put(JsonParseConsultation.stage, stage);
		patientDiagnosis .put(JsonParseConsultation.onsetDate, onsetDate);
		patientDiagnosis .put(JsonParseConsultation.status, status);
		patientDiagnosis .put(JsonParseConsultation.closeDate, closeDate);
		patientDiagnosis .put(JsonParseConsultation.operation, operation);
		patientDiagnosis .put(JsonParseConsultation.onsetVisitId, onsetVisitId);
		
		diagnosisArray.put(patientDiagnosis);
		diagnosis .put(JsonParseConsultation.patientDiagnosis, diagnosisArray);
		
		diagnosis .put(JsonParseConsultation.prescriptionHdrId, prescriptionHdrId);
		
	
		JSONArray prescriptionArray = new JSONArray();
		JSONObject prescriptionObject  = new JSONObject();
		prescriptionObject .put(JsonParseConsultation.prescriptionId, prescriptionId);
		prescriptionObject .put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		prescriptionObject .put(JsonParseConsultation.drugCode, drugCode);
		prescriptionObject .put(JsonParseConsultation.drugName, drugName);
		prescriptionObject .put(JsonParseConsultation.frequency, frequency);
		prescriptionObject .put(JsonParseConsultation.duration, duration);
		prescriptionObject .put(JsonParseConsultation.startDate, startDate);
		prescriptionObject .put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		prescriptionObject .put(JsonParseConsultation.operation, operation1);
		prescriptionObject .put(JsonParseConsultation.dosageAdvice, dosageAdvice);
		prescriptionObject .put(JsonParseConsultation.drugType, drugType);
		prescriptionArray.put(prescriptionObject);
		
		diagnosis.put(JsonParseConsultation.prescription, prescriptionArray );
		diagnosis.put(JsonParseConsultation.genericAdvice, genericAdvice );
		diagnosis.put(JsonParseConsultation.genericAdviceId, genericAdviceId );
		System.out.println("req:"+diagnosis.toString());
		return diagnosis;
	}
	
	//PersistPrescription
	public static List<NameValuePair> PersistPrescription_Param(String prescriptionHdrId,String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.prescriptionHdrId, prescriptionHdrId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		return param;
	}
		
	public static JSONObject getJSONPersistPrescription(String prescriptionId,String genericDrugCode,String drugCode,String drugName,String frequency,String duration,String startDate,String freqSpecInstruction,String operation,String dosageAdvice,String drugType,String genericAdvice,String genericAdviceId) {
		
		JSONObject prescription = new JSONObject();
		JSONArray prescriptionArray = new JSONArray();
		JSONObject prescriptionObject  = new JSONObject();
		prescriptionObject .put(JsonParseConsultation.prescriptionId, prescriptionId);
		prescriptionObject .put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		prescriptionObject .put(JsonParseConsultation.drugCode, drugCode);
		prescriptionObject .put(JsonParseConsultation.drugName, drugName);
		prescriptionObject .put(JsonParseConsultation.frequency, frequency);
		prescriptionObject .put(JsonParseConsultation.duration, duration);
		prescriptionObject .put(JsonParseConsultation.startDate, startDate);
		prescriptionObject .put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		prescriptionObject .put(JsonParseConsultation.operation, operation);
		prescriptionObject .put(JsonParseConsultation.dosageAdvice, dosageAdvice);
		prescriptionObject .put(JsonParseConsultation.drugType, drugType);
		prescriptionArray.put(prescriptionObject);
		
		prescription.put(JsonParseConsultation.prescription, prescriptionArray );
		prescription.put(JsonParseConsultation.genericAdvice, genericAdvice );
		prescription.put(JsonParseConsultation.genericAdviceId, genericAdviceId );
		System.out.println("req:"+prescription.toString());
		return prescription;
	}
	
	//persistPatientProvisionalInfoHandler
	public static List<NameValuePair> persistPatientProvisionalInfoHandler_Param(String barcode,String provisionalPdf,String fileType){

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.barcode, barcode));
		param.add(new BasicNameValuePair(JsonParseConsultation.provisionalPdf, provisionalPdf));
		param.add(new BasicNameValuePair(JsonParseConsultation.fileType, fileType));
		return param;
	}
				
	//registrationBookConsultation
	public static List<NameValuePair> registrationBookConsultation_Param(String token,String userId,String userLoginName,String organisationId,String doctorId,String startTime,String endTime,String date,String dateSlotId,String bookedSlotId,String reasonForVisit,String locationId,String siteId,String locationName,String organizationName,String doctorName){
		
		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userId", userId));
		param.add(new BasicNameValuePair("userLoginName", userLoginName));
		param.add(new BasicNameValuePair("organisationId", organisationId));
		param.add(new BasicNameValuePair("doctorId", doctorId));
		param.add(new BasicNameValuePair("startTime", startTime));
		param.add(new BasicNameValuePair("endTime", endTime));
		param.add(new BasicNameValuePair("date", date));
		param.add(new BasicNameValuePair("dateSlotId", dateSlotId));
		param.add(new BasicNameValuePair("bookedSlotId", bookedSlotId));
		param.add(new BasicNameValuePair("reasonForVisit", reasonForVisit));
		param.add(new BasicNameValuePair("locationId", locationId));
		param.add(new BasicNameValuePair("siteId", siteId));
		param.add(new BasicNameValuePair("locationName", locationName));
		param.add(new BasicNameValuePair("organizationName", organizationName));
		param.add(new BasicNameValuePair("doctorName", doctorName));
		return param;
	}
		
	public static JSONObject getJSONRegistrationBookConsultation(String patientId,String title,String patientName,String lastName,String gender,String bloodGroup,String patientDOB,String age,String cityId,String mobileNo,String emailId,String addressLine1,String addressLine2,String country,
			String state,String city,String zipCode,String patientPhoto,String birthDayWish,String homeMonitoring,String alternate_number,String casualty,String religion,String employment,String mlc,String mlc_number,String mlc_description,String government_id_type,String government_id_number,
			String martial_status,String clinic_id,String countrycode_mobileNo,String countrycode_alternateNo,String patientAllergyId,String allergyId,String allergyCimsCode,String allergyName,String operation,String medicalHistoryFlag,String immunization,String bcg,String dpt,String tetnus,String immOtherDesc,
			String familyMedicalHistory,String patientMedicalHistory,String pneumonia,String hepatitis_a,String hepatitis_b,String typhoid,String influenza,String polio,String socialHabitsSmoking,String socialHabitsDrinking,String socialHabitsOthers,String patientMedicalConditionsDiabetes,String patientMedicalConditionsHypertension,
			String patientMedicalConditionsAsthma,String patientMedicalConditionsHeartDisease,String patientMedicalConditionsOthers,String familyMedicalConditionsDiabetes,String familyMedicalConditionsHypertension,
		    String familyMedicalConditionsAsthma,String familyMedicalConditionsHeartDisease,String familyMedicalConditionsOthers,String notes,String medicine,String comments,String operation1,
			String isDuplicateCheckRequired,String isPatientMappingRequired,String role) {
		

		JSONObject patient = new JSONObject();
		
		JSONObject patientRegistrationArray = new JSONObject();
		patientRegistrationArray .put(JsonParseConsultation.patientId, patientId);
		patientRegistrationArray .put(JsonParseConsultation.title, title);
		patientRegistrationArray .put(JsonParseConsultation.patientName,patientName);
		patientRegistrationArray .put(JsonParseConsultation.lastName,lastName);
		patientRegistrationArray .put(JsonParseConsultation.gender, gender);
		patientRegistrationArray .put(JsonParseConsultation.bloodGroup, bloodGroup);
		patientRegistrationArray .put(JsonParseConsultation.patientDOB, patientDOB);
		patientRegistrationArray .put(JsonParseConsultation.age, age);
		patientRegistrationArray .put(JsonParseConsultation.cityId, cityId);
		patientRegistrationArray .put(JsonParseConsultation.mobileNo, mobileNo);
		patientRegistrationArray .put(JsonParseConsultation.emailId, emailId);
		patientRegistrationArray .put(JsonParseConsultation.addressLine1, addressLine1);
		patientRegistrationArray .put(JsonParseConsultation.addressLine2, addressLine2);
		patientRegistrationArray .put(JsonParseConsultation.country, country);
		patientRegistrationArray .put(JsonParseConsultation.state, state);
		patientRegistrationArray .put(JsonParseConsultation.city, city);
		patientRegistrationArray .put(JsonParseConsultation.zipCode, zipCode);
		patientRegistrationArray .put(JsonParseConsultation.patientPhoto, patientPhoto);
		patientRegistrationArray .put(JsonParseConsultation.birthDayWish, birthDayWish);
		patientRegistrationArray .put(JsonParseConsultation.homeMonitoring, homeMonitoring);
		patientRegistrationArray .put(JsonParseConsultation.alternate_number, alternate_number);
		patientRegistrationArray .put(JsonParseConsultation.casualty, casualty);
		patientRegistrationArray .put(JsonParseConsultation.religion, religion);
		patientRegistrationArray .put(JsonParseConsultation.employment, employment);
		patientRegistrationArray .put(JsonParseConsultation.mlc, mlc);
		patientRegistrationArray .put(JsonParseConsultation.mlc_number, mlc_number);
		patientRegistrationArray .put(JsonParseConsultation.mlc_description, mlc_description);
		patientRegistrationArray .put(JsonParseConsultation.government_id_type, government_id_type);
		patientRegistrationArray .put(JsonParseConsultation.government_id_number, government_id_number);
		patientRegistrationArray .put(JsonParseConsultation.martial_status, martial_status);
		patientRegistrationArray .put(JsonParseConsultation.clinic_id, clinic_id);
		patientRegistrationArray .put(JsonParseConsultation.countrycode_mobileNo, countrycode_mobileNo);
		patientRegistrationArray .put(JsonParseConsultation.countrycode_alternateNo, countrycode_alternateNo);
		
		JSONArray allergyArray = new JSONArray();
		JSONObject allergy = new JSONObject();
		allergy .put(JsonParseConsultation.patientAllergyId, patientAllergyId);
		allergy .put(JsonParseConsultation.allergyId, allergyId);
		allergy .put(JsonParseConsultation.allergyCimsCode, allergyCimsCode);
		allergy .put(JsonParseConsultation.allergyName, allergyName);
		allergy .put(JsonParseConsultation.operation, operation);
		allergyArray.put(allergy);
		patientRegistrationArray .put(JsonParseConsultation.patientAllergy, allergyArray);
		
		patient .put(JsonParseConsultation.medicalHistoryFlag, medicalHistoryFlag);
		
		JSONObject medicalHistoryArray = new JSONObject();
		medicalHistoryArray .put(JsonParseConsultation.immunization, immunization);
		medicalHistoryArray .put(JsonParseConsultation.bcg, bcg);
		medicalHistoryArray .put(JsonParseConsultation.dpt, dpt);
		medicalHistoryArray .put(JsonParseConsultation.tetnus, tetnus);
		medicalHistoryArray .put(JsonParseConsultation.immOtherDesc, immOtherDesc);
		medicalHistoryArray .put(JsonParseConsultation.familyMedicalHistory, familyMedicalHistory);
		medicalHistoryArray .put(JsonParseConsultation.patientMedicalHistory, patientMedicalHistory);
		medicalHistoryArray .put(JsonParseConsultation.pneumonia, pneumonia);
		medicalHistoryArray .put(JsonParseConsultation.hepatitis_a, hepatitis_a);
		medicalHistoryArray .put(JsonParseConsultation.hepatitis_b, hepatitis_b);
		medicalHistoryArray .put(JsonParseConsultation.typhoid, typhoid);
		medicalHistoryArray .put(JsonParseConsultation.influenza, influenza);
		medicalHistoryArray .put(JsonParseConsultation.polio, polio);
		medicalHistoryArray .put(JsonParseConsultation.socialHabitsSmoking, socialHabitsSmoking);
		medicalHistoryArray .put(JsonParseConsultation.socialHabitsDrinking, socialHabitsDrinking);
		medicalHistoryArray .put(JsonParseConsultation.socialHabitsOthers, socialHabitsOthers);
		medicalHistoryArray .put(JsonParseConsultation.patientMedicalConditionsDiabetes, patientMedicalConditionsDiabetes);
		medicalHistoryArray .put(JsonParseConsultation.patientMedicalConditionsHypertension, patientMedicalConditionsHypertension);
		medicalHistoryArray .put(JsonParseConsultation.patientMedicalConditionsAsthma, patientMedicalConditionsAsthma);
		medicalHistoryArray .put(JsonParseConsultation.patientMedicalConditionsHeartDisease, patientMedicalConditionsHeartDisease);
		medicalHistoryArray .put(JsonParseConsultation.patientMedicalConditionsOthers, patientMedicalConditionsOthers);
		medicalHistoryArray .put(JsonParseConsultation.familyMedicalConditionsDiabetes, familyMedicalConditionsDiabetes);
		medicalHistoryArray .put(JsonParseConsultation.familyMedicalConditionsAsthma, familyMedicalConditionsAsthma);
		medicalHistoryArray .put(JsonParseConsultation.familyMedicalConditionsHeartDisease, familyMedicalConditionsHeartDisease);
		medicalHistoryArray .put(JsonParseConsultation.familyMedicalConditionsHypertension, familyMedicalConditionsHypertension);
		medicalHistoryArray .put(JsonParseConsultation.familyMedicalConditionsOthers, familyMedicalConditionsOthers);
		medicalHistoryArray .put(JsonParseConsultation.notes, notes);
		
		JSONArray medication = new JSONArray();
		JSONObject medic = new JSONObject();
		medic .put(JsonParseConsultation.medicine, medicine);
		medic .put(JsonParseConsultation.comments, comments);
		medic .put(JsonParseConsultation.operation, operation1);
		medication.put(medic);
		medicalHistoryArray .put(JsonParseConsultation.currentMedication, medication);
		
		patient .put(JsonParseConsultation.medicalHistory, medicalHistoryArray);
		patient .put(JsonParseConsultation.patient, patientRegistrationArray);
		patient .put(JsonParseConsultation.isDuplicateCheckRequired, isDuplicateCheckRequired);
		patient .put(JsonParseConsultation.isPatientMappingRequired, isPatientMappingRequired);
		patient .put(JsonParseConsultation.role, role);

		return patient;
	}
	
	//persistDoctorSpecifiedTestOrScanInfoHandler
	public static List<NameValuePair> PersistDoctorSpecifiedTestOrScanInfoHandler_Param(String userId,String userLoginName,String token,String organizationId,String siteId,String locationId,String patientId,String visitId,String serviceOrderHdrId){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseConsultation.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientId));
		param.add(new BasicNameValuePair(JsonParseConsultation.visitId, visitId));
		param.add(new BasicNameValuePair(JsonParseConsultation.serviceOrderHdrId, serviceOrderHdrId));
		return param;
	}
		
	public static JSONObject getJSONPersistDoctorSpecifiedTestOrScanInfoHandler( String  serviceOrderDtlId, String serviceId, String serviceName, String note,String operation) {
		
		JSONObject serviceOrderDtl = new JSONObject();
		JSONArray serviceOrderDtlArray = new JSONArray();
		JSONObject serviceOrderDtlObject  = new JSONObject();
		serviceOrderDtlObject .put(JsonParseConsultation.serviceOrderDtlId, serviceOrderDtlId);
		serviceOrderDtlObject .put(JsonParseConsultation.serviceId, serviceId);
		serviceOrderDtlObject .put(JsonParseConsultation.serviceName, serviceName);
		serviceOrderDtlObject .put(JsonParseConsultation.note, note);
		serviceOrderDtlObject .put(JsonParseConsultation.operation, operation);
		serviceOrderDtlArray.put(serviceOrderDtlObject);
		
		serviceOrderDtl.put(JsonParseConsultation.serviceOrderDtl, serviceOrderDtlArray );
		System.out.println("req:"+serviceOrderDtl.toString());
		return serviceOrderDtl;
	}
	
	//persistConsultationSummarySectionPrefHandler
public static List<NameValuePair> PersistConsultationSummarySectionPrefHandler_Param(String token,String userId,String userLoginName,String organizationId){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		return param;
	}
		
	public static JSONObject getJSONPersistConsultationSummarySectionPrefHandler( HashMap<String, String> summary) {
		
		JSONObject userSummaryPDFPref = new JSONObject();
		JSONArray userSummaryPDFPrefArr = new JSONArray();
		Set set = summary.entrySet();
		Iterator i = set.iterator();
		while(i.hasNext()){
			Map.Entry me = (Map.Entry)i.next();
	        String key = me.getKey().toString();
	        String value = me.getValue().toString();
			JSONObject userSummaryPDFPrefObj  = new JSONObject();
			userSummaryPDFPrefObj .put("pref", key);
			userSummaryPDFPrefObj.put("summarySectionName", value);
			userSummaryPDFPrefArr.put(userSummaryPDFPrefObj);
		}
		userSummaryPDFPref.put("userSummaryPDFPref", userSummaryPDFPrefArr );
		System.out.println("req:"+userSummaryPDFPref.toString());
		return userSummaryPDFPref;
	}
	
	//fetchConsultationSummarySectionPrefHandler
	public static List<NameValuePair> FetchConsultationSummarySectionPrefHandler_Param(String token,String userId,String userLoginName,String organizationId){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		return param;
	}
	
	public static List<NameValuePair> fetchPatientLabTestsTrendsData_Param(String userId,String userLoginName,String token,String organizationId,String patientid){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.patientId, patientid));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		
		return param;
	}
	
	//fetchConsultationMenuDetail
    public static List<NameValuePair> fetchConsultationMenuDetail_Param(String organisationId,String token,String clinicUserId,String userLoginName) {

        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
        param.add(new BasicNameValuePair(JsonParseLogin.token, token));
        param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
        param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
        return param;
    }
    
    //saveConsultationMenuDetail
    public static List<NameValuePair> saveConsultationMenuDetail_Param(String organisationId,String token,String clinicUserId,String userLoginName) {

        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
        param.add(new BasicNameValuePair(JsonParseLogin.token, token));
        param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
        param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
        return param;
    }
    
    //saveConsultationMenuDetail
    public static JSONObject getJSONsaveConsultationMenuDetail(JSONArray consultMenu) {

    	JSONObject consultationMenuDetail = new JSONObject();
        consultationMenuDetail.put("consultationMenuDetailList", consultMenu );
        return consultationMenuDetail;
    }

	public static JSONObject getJSONfetchPatientLabTestsTrendsData(JSONArray parameterName,String testName) {
		
		JSONObject trendsTestandParam = new JSONObject();
		JSONArray trendsTestandParamArr = new JSONArray();
		JSONObject trendsTestandParamObj = new JSONObject();
		
		trendsTestandParamObj.put("parameterName", parameterName);
		trendsTestandParamObj.put("testName", testName);
		
		trendsTestandParamArr.put(trendsTestandParamObj);
		trendsTestandParam.put("trendsTestandParam", trendsTestandParamArr);
		return trendsTestandParam;
	}
	
	public static List<NameValuePair> searchManualLabTestHandler_Param(String userId,String userLoginName,String token, String organizationId , String searchString){

		param = new ArrayList<NameValuePair>();	
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organizationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.searchString, searchString));
		return param;
	}
	
	public static List<NameValuePair> updateVitalParameterView_Param(String organisationId,String token,String userId,String userLoginName) {

        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organisationId));
        param.add(new BasicNameValuePair(JsonParseLogin.token, token));
        param.add(new BasicNameValuePair(JsonParseLogin.userId, userId));
        param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
        return param;
    }
    
    //saveConsultationMenuDetail
    public static JSONObject getJSONupdateVitalParameterView(JSONArray consultMenu) {

    	JSONObject consultationMenuDetail = new JSONObject();
        consultationMenuDetail.put("vitalParameters", consultMenu );
        return consultationMenuDetail;
    }
	
    public static JSONObject getJSONupdateGlobalPrintSelection(JSONArray consultMenu) {

    	JSONObject consultationMenuDetail = new JSONObject();
        consultationMenuDetail.put("globalPrintSections", consultMenu );
        return consultationMenuDetail;
    }
    
    public static List<NameValuePair> fetchTestParametersHandler_Param(String organisationId,String token,String userId,String userLoginName,String testId) {

        param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair(JsonParseConsultation.organizationId, organisationId));
        param.add(new BasicNameValuePair(JsonParseLogin.token, token));
        param.add(new BasicNameValuePair(JsonParseLogin.userId, userId));
        param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
        param.add(new BasicNameValuePair("testId", testId));
        return param;
    }
    
	//persistMedicalKit - CREATE
	public static List<NameValuePair> PersistMedicalKit_Param(String userLoginName,String token,String userId,String clinicUserId,String orgId,String medicalKitName,String operation,String fetchChanges){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
		param.add(new BasicNameValuePair(JsonParseConsultation.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseConsultation.orgId, orgId));
		param.add(new BasicNameValuePair("medicalKitName", medicalKitName));
		param.add(new BasicNameValuePair(JsonParseConsultation.operation, operation));
		param.add(new BasicNameValuePair("fetchChanges", fetchChanges));
		
		return param;
	}
		
	public static JSONObject getJSONPersistMedicalKit(String symptomId,String symptomName,String operation,String serviceId,String serviceName,String note,String diagnosisId,
			String drSpecDiagnosisId,String diagnosisName,String drugName,String drugCode,String drugType,String genericDrugCode,String frequency,String duration,String dosageAdvice,String freqSpecInstruction) {
		
		JSONObject medicalKit = new JSONObject();
		
		JSONArray symArr = new JSONArray();
		JSONObject symObj = new JSONObject();
		symObj.put(JsonParseConsultation.symptomId, symptomId);
		symObj.put(JsonParseConsultation.symptomName, symptomName);
		symObj.put(JsonParseConsultation.operation, operation);
		symArr.put(symObj);
		medicalKit.put("symptoms", symArr );
		
		JSONArray labArr = new JSONArray();
		JSONObject labObj = new JSONObject();
		labObj.put(JsonParseConsultation.serviceId, serviceId);
		labObj.put(JsonParseConsultation.serviceName, serviceName);
		labObj.put(JsonParseConsultation.note, note);
		labObj.put(JsonParseConsultation.operation, operation);
		labArr.put(labObj);
		medicalKit.put("labTests", labArr );
		
		JSONArray diaArr = new JSONArray();
		JSONObject diaObj = new JSONObject();
		diaObj.put(JsonParseConsultation.diagnosisId, diagnosisId);
		diaObj.put(JsonParseConsultation.drSpecDiagnosisId, drSpecDiagnosisId);
		diaObj.put(JsonParseConsultation.diagnosisName, diagnosisName);
		diaObj.put(JsonParseConsultation.operation, operation);
		diaArr.put(diaObj);
		medicalKit.put("diagnoses", diaArr );
		
		JSONArray presArr = new JSONArray();
		JSONObject presObj = new JSONObject();
		presObj.put(JsonParseConsultation.drugName, drugName);
		presObj.put(JsonParseConsultation.drugCode, drugCode);
		presObj.put(JsonParseConsultation.drugType, drugType);
		presObj.put(JsonParseConsultation.genericDrugCode, genericDrugCode);
		presObj.put(JsonParseConsultation.frequency, frequency);
		presObj.put(JsonParseConsultation.duration, duration);
		presObj.put(JsonParseConsultation.dosageAdvice, dosageAdvice);
		presObj.put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
		presObj.put(JsonParseConsultation.operation, operation);
		presArr.put(presObj);
		medicalKit.put("prescription", presArr );
		System.out.println("req:"+medicalKit.toString());
		return medicalKit;
	}
	
	//FetchMedicalKit
	public static List<NameValuePair> FetchMedicalKit_Param(String organisationId,String token,String clinicUserId,String userLoginName){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));	
		return param;
	}
	
	//FetchMedicalDetailKit
	public static List<NameValuePair> FetchMedicalDetailKit_Param(String organisationId,String token,String clinicUserId,String userLoginName,String medicalKitId){
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseConsultation.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
		param.add(new BasicNameValuePair(JsonParseConsultation.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));	
		param.add(new BasicNameValuePair("medicalKitId", medicalKitId));
		return param;
	}
	
	//persistMedicalKit - UPDATE & DELETE
	public static List<NameValuePair> Update_DeleteMedicalKit_Param(String userLoginName,String token,String userId,String clinicUserId,String orgId,String medicalKitId,String operation,String fetchChanges){
			
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
			param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
			param.add(new BasicNameValuePair(JsonParseConsultation.clinicUserId, clinicUserId));
			param.add(new BasicNameValuePair(JsonParseConsultation.orgId, orgId));
			param.add(new BasicNameValuePair("medicalKitId", medicalKitId));
			param.add(new BasicNameValuePair(JsonParseConsultation.operation, operation));
			param.add(new BasicNameValuePair("fetchChanges", fetchChanges));
			
			return param;
		}
			
		public static JSONObject getJSONUpdate_DeleteMedicalKit(String medicalKitSymptomId,String symptomId,String symptomName,String operation,String medicalKitLabTestId,String serviceId,String serviceName,String note,String diagnosisId,
				String medicalKitDiagnosisId,String drSpecDiagnosisId,String diagnosisName,String medicalKitPrescriptionId,String drugName,String drugCode,String drugType,String genericDrugCode,String frequency,String duration,String dosageAdvice,String freqSpecInstruction) {
			
			JSONObject medicalKit = new JSONObject();
			
			JSONArray symArr = new JSONArray();
			JSONObject symObj = new JSONObject();
			symObj.put("medicalKitSymptomId", medicalKitSymptomId);
			symObj.put(JsonParseConsultation.symptomId, symptomId);
			symObj.put(JsonParseConsultation.symptomName, symptomName);
			symObj.put(JsonParseConsultation.operation, operation);
			symArr.put(symObj);
			medicalKit.put("symptoms", symArr );
			
			JSONArray labArr = new JSONArray();
			JSONObject labObj = new JSONObject();
			labObj.put("medicalKitLabTestId", medicalKitLabTestId);
			labObj.put(JsonParseConsultation.serviceId, serviceId);
			labObj.put(JsonParseConsultation.serviceName, serviceName);
			labObj.put(JsonParseConsultation.note, note);
			labObj.put(JsonParseConsultation.operation, operation);
			labArr.put(labObj);
			medicalKit.put("labTests", labArr );
			
			JSONArray diaArr = new JSONArray();
			JSONObject diaObj = new JSONObject();
			diaObj.put("medicalKitDiagnosisId", medicalKitDiagnosisId);
			diaObj.put(JsonParseConsultation.diagnosisId, diagnosisId);
			diaObj.put(JsonParseConsultation.drSpecDiagnosisId, drSpecDiagnosisId);
			diaObj.put(JsonParseConsultation.diagnosisName, diagnosisName);
			diaObj.put(JsonParseConsultation.operation, operation);
			diaArr.put(diaObj);
			medicalKit.put("diagnoses", diaArr );
			
			JSONArray presArr = new JSONArray();
			JSONObject presObj = new JSONObject();
			presObj.put("medicalKitPrescriptionId", medicalKitPrescriptionId);
			presObj.put(JsonParseConsultation.drugName, drugName);
			presObj.put(JsonParseConsultation.drugCode, drugCode);
			presObj.put(JsonParseConsultation.drugType, drugType);
			presObj.put(JsonParseConsultation.genericDrugCode, genericDrugCode);
			presObj.put(JsonParseConsultation.frequency, frequency);
			presObj.put(JsonParseConsultation.duration, duration);
			presObj.put(JsonParseConsultation.dosageAdvice, dosageAdvice);
			presObj.put(JsonParseConsultation.freqSpecInstruction, freqSpecInstruction);
			presObj.put(JsonParseConsultation.operation, operation);
			presArr.put(presObj);
			medicalKit.put("prescription", presArr );
			System.out.println("req:"+medicalKit.toString());
			return medicalKit;
		}
		
		//fetchConsultationPdfHeaderFooterDesigner
		public static List<NameValuePair> FetchConsultationPdfHeaderFooterDesigner_Param(String token,String userLoginName,String organisationId,String userId,String isOrgLevel){
			
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
			param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseConsultation.organisationId, organisationId));
			param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
			param.add(new BasicNameValuePair("isOrgLevel", isOrgLevel));
			return param;
		}
		
		//saveConsultationPdfHeaderFooterDesigner
		public static List<NameValuePair> SaveConsultationPdfHeaderFooterDesigner_Param(String token,String userLoginName,String organisationId,String userId,String headerType,String headerTypeValue,String footerType,
				String footerTypeValue,String docBannerFlag,String patBannerFlag,String tataBrandingFlag,String disclaimerFlag,String docSignatureFlag,String kmcFlag,String paperType,String operation,String headerFooterDesignerId,String templateId,String isOrgLevel){
			
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
			param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseConsultation.organisationId, organisationId));
			param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
			param.add(new BasicNameValuePair("headerType", headerType));
			param.add(new BasicNameValuePair("headerTypeValue",headerTypeValue));
			param.add(new BasicNameValuePair("footerType", footerType));
			param.add(new BasicNameValuePair("footerTypeValue",footerTypeValue));
			param.add(new BasicNameValuePair("docBannerFlag", docBannerFlag));
			param.add(new BasicNameValuePair("patBannerFlag", patBannerFlag));
			param.add(new BasicNameValuePair("tataBrandingFlag", tataBrandingFlag));
			param.add(new BasicNameValuePair("disclaimerFlag", disclaimerFlag));
			param.add(new BasicNameValuePair("docSignatureFlag", docSignatureFlag));
			param.add(new BasicNameValuePair("kmcFlag",kmcFlag));
			param.add(new BasicNameValuePair("paperType", paperType));
			param.add(new BasicNameValuePair("operation",operation));
			param.add(new BasicNameValuePair("headerFooterDesignerId", headerFooterDesignerId));
			param.add(new BasicNameValuePair("templateId",templateId));
			param.add(new BasicNameValuePair("isOrgLevel", isOrgLevel));
			return param;
		}

    
    
    
    
    
}