package com.clinic.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class ParamGenerator {

	public static List<NameValuePair> param = null;
	public static List<NameValuePair> getSignIn_Param(String username,String password) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("patientUsername", username));
		param.add(new BasicNameValuePair("patientPassword", password));
	
		return param;

	}
	
	public static List<NameValuePair> getEMRSignIn_Param(String username,String password) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, username));
		param.add(new BasicNameValuePair(JsonParseLogin.password, password));
	
		return param;

	}
	
	
	public static List<NameValuePair> getEMRlogout_Param(String username,String token) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, username));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
	
		return param;

	}
	
	public static JSONObject getJSONBookregistration( String  addressLine1,
			String addressLine2, String age, String alternate_number,String birthDayWish,String bloodGroup,String casualty,String city,String clinic_id,String country,String countrycode_alternateNo,String countrycode_mobileNo,String employment,String gender,String government_id_number,String government_id_type,String homeMonitoring,String lastName,
			String martial_status,String mlc,String mlc_description, String mlc_number,String mobileNo,ArrayList patientAllergy,String patientDOB,String patientName, String patientPhoto,String religion,String state,String title,String zipCode  ) {

		JSONObject updatePatient = new JSONObject();

		//JSONArray patientDetailsArray = new JSONArray();
		JSONObject patientDetailsArray  = new JSONObject();
		patientDetailsArray .put(JsonParseAppointment.addressLine1, addressLine1);
		patientDetailsArray .put(JsonParseAppointment.addressLine2, addressLine2);
		patientDetailsArray .put(JsonParseAppointment.age, age);
		patientDetailsArray .put(JsonParseAppointment.alternate_number, alternate_number);
		patientDetailsArray .put(JsonParseAppointment.birthDayWish, birthDayWish);
		patientDetailsArray .put(JsonParseAppointment.bloodGroup, bloodGroup);
		patientDetailsArray .put(JsonParseAppointment.casualty, casualty);
		patientDetailsArray .put(JsonParseAppointment.city, city);
		patientDetailsArray .put(JsonParseAppointment.clinic_id, clinic_id);
		patientDetailsArray .put(JsonParseAppointment.birthDayWish, birthDayWish);
		patientDetailsArray .put(JsonParseAppointment.clinic_id, clinic_id);
		patientDetailsArray .put(JsonParseAppointment.country, country);
		patientDetailsArray .put(JsonParseAppointment.countrycode_alternateNo, countrycode_alternateNo);
		patientDetailsArray .put(JsonParseAppointment.countrycode_mobileNo, countrycode_mobileNo);
	//	patientDetailsArray .put(JsonParseAppointment.emailID, emailID);
		patientDetailsArray .put(JsonParseAppointment.employment, employment);
		patientDetailsArray .put(JsonParseAppointment.gender, gender);
		patientDetailsArray .put(JsonParseAppointment.government_id_number, government_id_number);
		patientDetailsArray .put(JsonParseAppointment.government_id_type, government_id_type);
		patientDetailsArray .put(JsonParseAppointment.homeMonitoring, homeMonitoring);
		patientDetailsArray .put(JsonParseAppointment.lastName, lastName);
		patientDetailsArray .put(JsonParseAppointment.martial_status, martial_status);
		patientDetailsArray .put(JsonParseAppointment.mlc, mlc);
		patientDetailsArray .put(JsonParseAppointment.mlc_description, mlc_description);
		patientDetailsArray .put(JsonParseAppointment.mlc_number, mlc_number);
		patientDetailsArray .put(JsonParseAppointment.mobileNo, mobileNo);
		patientDetailsArray .put(JsonParseAppointment.patientAllergy, patientAllergy);
		patientDetailsArray .put(JsonParseAppointment.patientDOB, patientDOB);
		patientDetailsArray .put(JsonParseAppointment.patientName, patientName);
		patientDetailsArray .put(JsonParseAppointment.patientPhoto, patientPhoto);
		patientDetailsArray .put(JsonParseAppointment.religion, religion);
		patientDetailsArray .put(JsonParseAppointment.state, state);
		patientDetailsArray .put(JsonParseAppointment.title, title);
		patientDetailsArray .put(JsonParseAppointment.zipCode, zipCode);
		
		//accountDetailsArray.put(patientDetailsArray );

		updatePatient.put(JsonParseAppointment.patient, patientDetailsArray );
		System.out.println("req:"+updatePatient.toString());
		return updatePatient;

	}
	
	
	
	public static List<NameValuePair> getBookregistration_Param(String bookedSlotId,String date,String dateSlotId,String 
			doctorId,String doctorName,String startTime,String endTime,String isDuplicateCheckRequired,String isPatientMappingRequired,
			String locationId,String locationName,String medicalHistoryFlag,String organisationId,String organizationName,String reasonForVisit,String siteId,String role,String token,String userId,String userLoginName ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.bookedSlotId, bookedSlotId));
		param.add(new BasicNameValuePair(JsonParseAppointment.date, date));
		param.add(new BasicNameValuePair(JsonParseAppointment.dateSlotId, dateSlotId));
		param.add(new BasicNameValuePair(JsonParseAppointment.doctorId, doctorId));
		param.add(new BasicNameValuePair(JsonParseAppointment.doctorName, doctorName));
		param.add(new BasicNameValuePair(JsonParseAppointment.startTime, startTime));
		param.add(new BasicNameValuePair(JsonParseAppointment.endTime, endTime));
		param.add(new BasicNameValuePair(JsonParseAppointment.isDuplicateCheckRequired, isDuplicateCheckRequired));
		param.add(new BasicNameValuePair(JsonParseAppointment.isPatientMappingRequired, isPatientMappingRequired));
		param.add(new BasicNameValuePair(JsonParseAppointment.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.locationName, locationName));
		param.add(new BasicNameValuePair(JsonParseAppointment.medicalHistoryFlag, medicalHistoryFlag));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organizationName, organizationName));
		param.add(new BasicNameValuePair(JsonParseAppointment.reasonForVisit, reasonForVisit));
		param.add(new BasicNameValuePair(JsonParseAppointment.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		
	
		return param;

	}
	
	public static List<NameValuePair> getEMRHomeDoctor_Param(String token,String userLoginName,String userId,String organisationId,String clinicUserId,String appointmentDate   ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
	param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
	param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
	
	param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
	param.add(new BasicNameValuePair(JsonParseAppointment.clinicUserId, clinicUserId));
	param.add(new BasicNameValuePair(JsonParseAppointment.appointmentDate, appointmentDate));
	
		return param;

	}
	
	
	public static List<NameValuePair> getHomeDoctorList_Param(String token,String userLoginName,String userId,String organisationId  ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> getHomeDoctorSlotDatesList_Param(String token,String userLoginName,String userId,String organisationId,String clinicUserId  ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.clinicUserId, clinicUserId));
		return param;

	}

	
	
	public static List<NameValuePair> getReasonMaster_Param(String token,String userLoginName,String userId,String organisationId,String reasonType  ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
	param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
	param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
	
	param.add(new BasicNameValuePair(JsonParseAppointment.organizationId, organisationId));
	param.add(new BasicNameValuePair(JsonParseAppointment.reasonType, reasonType));
	
	
		return param;

	}
	
	public static List<NameValuePair> getPatienSearch_Param(String token,String username,String orgId,String userID,String StartIndex,String interval,String hmFlag  ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, username));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, orgId));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userID));
		param.add(new BasicNameValuePair(JsonParseAppointment.startIndex, StartIndex));
		param.add(new BasicNameValuePair(JsonParseAppointment.interval, interval));
	
		param.add(new BasicNameValuePair(JsonParseAppointment.hmFlag, hmFlag));
		
		
		return param;

	}
	
	public static JSONObject getJSONPatientSearch( String  mobileNo, String uhid, String patientName, String dob,String gender) {

		JSONObject updatePatient = new JSONObject();

		//JSONArray patientDetailsArray = new JSONArray();
		JSONObject patientDetailsArray  = new JSONObject();
		patientDetailsArray .put(JsonParseAppointment.mobileNo, mobileNo);
		patientDetailsArray .put(JsonParseAppointment.uhid, uhid);
		patientDetailsArray .put(JsonParseAppointment.patientName, patientName);
		patientDetailsArray .put(JsonParseAppointment.dob, dob);
		patientDetailsArray .put(JsonParseAppointment.gender, gender);
		//accountDetailsArray.put(patientDetailsArray );

		updatePatient.put(JsonParseAppointment.patient, patientDetailsArray );
		System.out.println("req:"+updatePatient.toString());
		return updatePatient;

	}
	
/*	public static JSONObject JSON_createBill(String  serviceName, String serviceId, String tariffAmount) {
		JSONArray servicesJSON = new JSONArray();
		
		JSONObject serviceDetailsJSON  = new JSONObject();
		JSONObject patientDetailsArray  = new JSONObject();
		serviceDetailsJSON.put("serviceName", serviceName);
		serviceDetailsJSON.put("serviceId", serviceId);
		serviceDetailsJSON.put("tariffAmount", tariffAmount);

     
		servicesJSON.put(serviceDetailsJSON );
		serviceDetailsJSON.put("service", patientDetailsArray );
		serviceDetailsJSON.put("services", patientDetailsArray );
		
		System.out.println("req:"+ servicesJSON.toString());
		return serviceDetailsJSON;
	}
*/
	
	
	
	
		
	
	public static List<NameValuePair> getStateMasterList(String token,String userLoginName,String userId,String organisationId,String countryId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.countryId, countryId));
		return param;

	}
	
	
	public static List<NameValuePair> getCityMasterList(String token,String userLoginName,String userId,String organisationId,String stateId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.stateId, stateId));
		return param;

	}
	
	public static List<NameValuePair> bookAppointment_Param(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		return param;

	}
	
	
	public static List<NameValuePair> rescheduleAppointment_Param(String token,String userLoginName,String userId,String organisationId,String siteId,String locationId,String patientId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseAppointment.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.patientId, patientId));
		return param;

	}
	
	public static JSONObject getJSONRescheduleBookAppointment(String dateSlotId, String slotId, String appointmentDate,String startTime,String endTime,String clinicUserId,String reasonOfVisit,String locationName,String organizationName,String doctorName ) {

		JSONObject updatePatient = new JSONObject();

		//JSONArray patientDetailsArray = new JSONArray();
		JSONObject patientDetailsArray  = new JSONObject();
	
		patientDetailsArray .put(JsonParseAppointment.dateSlotId, dateSlotId);
		patientDetailsArray .put(JsonParseAppointment.slotId, slotId);
		patientDetailsArray .put(JsonParseAppointment.appointmentDate, appointmentDate);
		patientDetailsArray .put(JsonParseAppointment.startTime, startTime);
		patientDetailsArray .put(JsonParseAppointment.endTime, endTime);
	
		patientDetailsArray .put(JsonParseAppointment.clinicUserId, clinicUserId);
		patientDetailsArray .put(JsonParseAppointment.reasonOfVisit, reasonOfVisit);
		patientDetailsArray .put(JsonParseAppointment.organizationName, organizationName);
		patientDetailsArray .put(JsonParseAppointment.locationName, locationName);
		patientDetailsArray .put(JsonParseAppointment.doctorName, doctorName);
		
		//accountDetailsArray.put(patientDetailsArray );

		updatePatient.put(JsonParseAppointment.bookAppointment, patientDetailsArray );
		System.out.println("req:"+updatePatient.toString());
		return updatePatient;
}
	
	
	
	public static JSONObject getJSONReschBooAppmtCancelAppointment(String appointmentId, String startTime, String endTime,String reasonforCancel ) {

		JSONObject updatePatient = new JSONObject();

		//JSONArray patientDetailsArray1 = new JSONArray();
		JSONObject patientDetailsArray  = new JSONObject();
	
		patientDetailsArray .put(JsonParseAppointment.appointmentId, appointmentId);
		patientDetailsArray .put(JsonParseAppointment.startTime, startTime);
		patientDetailsArray .put(JsonParseAppointment.endTime, endTime);
		patientDetailsArray .put(JsonParseAppointment.reasonforCancel, reasonforCancel);
		
		
		//accountDetailsArray.put(patientDetailsArray );

		updatePatient.put(JsonParseAppointment.cancelAppointment, patientDetailsArray );
		System.out.println("req:"+updatePatient.toString());
		return updatePatient;
}
	
	
	
	
	
	
	
	public static JSONObject getJSONBookAppointment( String  patientId, String dateSlotId, String slotId, String appointmentDate,String startTime,String endTime,String locationId,String siteId,String clinicUserId,String reasonOfVisit,String locationName,String organizationName,String doctorName ) {

		JSONObject updatePatient = new JSONObject();

		//JSONArray patientDetailsArray = new JSONArray();
		JSONObject patientDetailsArray  = new JSONObject();
		patientDetailsArray .put(JsonParseAppointment.patientId, patientId);
		patientDetailsArray .put(JsonParseAppointment.dateSlotId, dateSlotId);
		patientDetailsArray .put(JsonParseAppointment.slotId, slotId);
		patientDetailsArray .put(JsonParseAppointment.appointmentDate, appointmentDate);
		patientDetailsArray .put(JsonParseAppointment.startTime, startTime);
		patientDetailsArray .put(JsonParseAppointment.endTime, endTime);
		patientDetailsArray .put(JsonParseAppointment.locationId, locationId);
		patientDetailsArray .put(JsonParseAppointment.siteId, siteId);
		patientDetailsArray .put(JsonParseAppointment.clinicUserId, clinicUserId);
		patientDetailsArray .put(JsonParseAppointment.reasonOfVisit, reasonOfVisit);
		patientDetailsArray .put(JsonParseAppointment.organizationName, organizationName);
		patientDetailsArray .put(JsonParseAppointment.locationName, locationName);
		patientDetailsArray .put(JsonParseAppointment.doctorName, doctorName);
		
		//accountDetailsArray.put(patientDetailsArray );

		updatePatient.put(JsonParseAppointment.appointment, patientDetailsArray );
		System.out.println("req:"+updatePatient.toString());
		return updatePatient;

	}
	
	
	//UpdateUserProfileDetails
		public static List<NameValuePair> updateUserProfile_Param(String token,String userLoginName,String userId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		
		
			return param;

		}
		
				
		//addUserPublication
		public static List<NameValuePair> addUserPublication_Param(String token,String userLoginName,String userId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		
		
			return param;

		}
		
		
		//listUserPublication
		public static List<NameValuePair> listUserPublication_Param(String token,String userLoginName,String userId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		
		
			return param;

		}

		//updateUserPublication
		public static List<NameValuePair> updateUserPublication_Param(String token,String userLoginName,String userId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
			param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		
			return param;

		}
			
				
					
				
		//patientPreviousVisits
		
		
		public static List<NameValuePair> patientPreviousVisits_Param(String token,String userLoginName,String userId,String organisationId, String patientId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
			param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
			param.add(new BasicNameValuePair(JsonParseAppointment.patientId, patientId));
		
			return param;
		}
		
		
		
		
		
	//Clinic Admin		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		public static List<NameValuePair>getMedicalHistory_Param(String token,String userLoginName,String userId,String organisationId,String patientId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		
			param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
			param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
			param.add(new BasicNameValuePair(JsonParseAppointment.patientId, patientId));

		
			return param;
		}
		
		
		
		
			
		
		public static List<NameValuePair> Param_cancelAppointment(String token,String userLoginName,String userId,String organisationId) {
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
			param.add(new BasicNameValuePair(JsonParseAppointment.organizationId, organisationId));
			return param;
		}
		
		public static JSONObject JSON_cancelAppointment(String siteId, String locationId, String patientId, String appointmentId,String appointmentDate,String startTime,String endTime,String clinicUserId,String roleId,String reasonforCancel,String patientName){
			JSONObject updatePatient = new JSONObject();	 
			JSONObject patientDetailsArray  = new JSONObject();
		
			patientDetailsArray .put(JsonParseAppointment.siteId, siteId);
			patientDetailsArray .put(JsonParseAppointment.locationId, locationId);
			patientDetailsArray .put(JsonParseAppointment.appointmentDate, appointmentDate);
			patientDetailsArray .put(JsonParseAppointment.startTime, startTime);
			patientDetailsArray .put(JsonParseAppointment.endTime, endTime);
			patientDetailsArray .put(JsonParseAppointment.reasonforCancel, reasonforCancel);	
			patientDetailsArray .put(JsonParseAppointment.doctorId, clinicUserId);
			patientDetailsArray .put(JsonParseAppointment.roleId, roleId);
			patientDetailsArray .put(JsonParseAppointment.patientName, patientName);
			patientDetailsArray .put(JsonParseAppointment.patientId, patientId);
			patientDetailsArray .put(JsonParseAppointment.appointmentId, appointmentId);
			updatePatient.put(JsonParseAppointment.appointment, patientDetailsArray );
			System.out.println("req:"+updatePatient.toString());
			return updatePatient;
	     }

		public static List<NameValuePair> Param_blockUnblockAppointmentSlot(String token,String userLoginName,String organisationId) {
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
			return param;
		}
		
		public static JSONObject JSON_blockUnblockAppointmentSlot(String userId,String slotId,String fromDate, String fromTime, String toDate,String toTime,String clinicUserId,String reason,String siteId,String locationId) {		
			JSONObject updatePatient = new JSONObject();	 
			JSONObject patientDetailsArray  = new JSONObject();	 
			param = new ArrayList<NameValuePair>();	
			patientDetailsArray .put(JsonParseAppointment.slotId, slotId);
			patientDetailsArray .put(JsonParseAppointment.userId, userId);
			patientDetailsArray .put(JsonParseAppointment.fromDate, fromDate);
			patientDetailsArray .put(JsonParseAppointment.fromTime, fromTime);
			patientDetailsArray .put(JsonParseAppointment.toDate, toDate);
			patientDetailsArray .put(JsonParseAppointment.toTime, toTime);
			patientDetailsArray .put(JsonParseAppointment.clinicUserId, clinicUserId);
			patientDetailsArray .put(JsonParseAppointment.reason, reason);
			patientDetailsArray .put(JsonParseAppointment.locationId, locationId);
			patientDetailsArray .put(JsonParseAppointment.siteId, siteId);
			updatePatient.put(JsonParseAppointment.blockAppointmentSlot,patientDetailsArray);
			System.out.println("req:"+updatePatient.toString());
			return updatePatient;
	     }
		

		
		public static List<NameValuePair> Param_blockAppointmentCalender(String token,String userLoginName,String userId,String organisationId,String fromDate, String fromTime, String toDate,String toTime,String clinicUserId,String reason,String siteId,String locationId) {
			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
			param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
			param.add(new BasicNameValuePair(JsonParseAppointment.fromDate, fromDate));
			param.add(new BasicNameValuePair(JsonParseAppointment.fromTime, fromTime));
			param.add(new BasicNameValuePair(JsonParseAppointment.toDate, toDate));
			param.add(new BasicNameValuePair(JsonParseAppointment.toTime, toTime));
			param.add(new BasicNameValuePair(JsonParseAppointment.clinicUserId, clinicUserId));
			param.add(new BasicNameValuePair(JsonParseAppointment.reason, reason));
			param.add(new BasicNameValuePair(JsonParseAppointment.locationId, locationId));
			param.add(new BasicNameValuePair(JsonParseAppointment.siteId, siteId));
			return param;
		}
		public static List<NameValuePair> Param_rescheduleAppointment(String token,String userLoginName,String userId,String organisationId,String siteId,String locationId,String patientId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseAppointment.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.patientId, patientId));

		

			return param;

		}
		
		public static JSONObject JSON_rescheduleAppointment(String dateSlotId, String slotId, String appointmentDate,String startTime,String endTime,String clinicUserId,String reasonOfVisit,String locationName,String organizationName,String doctorName,String appointmentId, String startTime2, String endTime2,String reasonforCancel ) {

			JSONObject updatePatient = new JSONObject();

			//JSONArray patientDetailsArray = new JSONArray();
			JSONObject patientDetailsArray  = new JSONObject();
		
			patientDetailsArray .put(JsonParseAppointment.dateSlotId, dateSlotId);
			patientDetailsArray .put(JsonParseAppointment.slotId, slotId);
			patientDetailsArray .put(JsonParseAppointment.appointmentDate, appointmentDate);
			patientDetailsArray .put(JsonParseAppointment.startTime, startTime);
			patientDetailsArray .put(JsonParseAppointment.endTime, endTime);
		
			patientDetailsArray .put(JsonParseAppointment.clinicUserId, clinicUserId);
			patientDetailsArray .put(JsonParseAppointment.reasonOfVisit, reasonOfVisit);
			patientDetailsArray .put(JsonParseAppointment.organizationName, organizationName);
			patientDetailsArray .put(JsonParseAppointment.locationName, locationName);
			patientDetailsArray .put(JsonParseAppointment.doctorName, doctorName);
			
			//accountDetailsArray.put(patientDetailsArray );
	        
			updatePatient.put(JsonParseAppointment.bookAppointment, patientDetailsArray );
			
			JSONObject patientDetailsArray2  = new JSONObject();
			
			patientDetailsArray2 .put(JsonParseAppointment.appointmentId, appointmentId);
			patientDetailsArray2 .put(JsonParseAppointment.startTime, startTime2);
			patientDetailsArray2 .put(JsonParseAppointment.endTime, endTime2);
			patientDetailsArray2 .put(JsonParseAppointment.reasonforCancel, reasonforCancel);
			
			updatePatient.put(JsonParseAppointment.cancelAppointment, patientDetailsArray2 );
			System.out.println("req:"+updatePatient.toString());
			return updatePatient;
	     }
		
		//patientRegistration
		public static List<NameValuePair> patientRegistration_Param(String token,String userId,String userLoginName,String organisationId,String doctorId,String startTime,String endTime,String date,String dateSlotId,String bookedSlotId,
				String reasonForVisit,String locationId,String siteId,String locationName,String organizationName,String doctorName,String isDuplicateCheckRequired,String isPatientMappingRequired){
		param = new ArrayList<NameValuePair>();

		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.doctorId, doctorId));
		param.add(new BasicNameValuePair(JsonParseAppointment.startTime, startTime));
		param.add(new BasicNameValuePair(JsonParseAppointment.endTime, endTime));
		param.add(new BasicNameValuePair(JsonParseAppointment.date, date));
		param.add(new BasicNameValuePair(JsonParseAppointment.dateSlotId, dateSlotId));
		param.add(new BasicNameValuePair(JsonParseAppointment.bookedSlotId, bookedSlotId));
		param.add(new BasicNameValuePair(JsonParseAppointment.reasonForVisit, reasonForVisit));
		param.add(new BasicNameValuePair(JsonParseAppointment.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseAppointment.locationName, locationName));
		param.add(new BasicNameValuePair(JsonParseAppointment.organizationName, organizationName));
		param.add(new BasicNameValuePair(JsonParseAppointment.doctorName, doctorName));
		param.add(new BasicNameValuePair(JsonParseAppointment.isDuplicateCheckRequired, isDuplicateCheckRequired));
		param.add(new BasicNameValuePair(JsonParseAppointment.isPatientMappingRequired, isPatientMappingRequired));

		return param;
		}

		public static JSONObject getJSONPatientRegistration(String patientId,String title,String patientName,String lastName,String gender,String bloodGroup,String patientDOB,String age,String cityId,String mobileNo,String emailId,String addressLine1,String addressLine2,String country,
				String state,String city,String zipCode,String patientPhoto,String birthDayWish,String homeMonitoring,String alternate_number,String casualty,String religion,String employment,String mlc,String mlc_number,String mlc_description,String government_id_type,String government_id_number,
				String martial_status,String clinic_id,String countrycode_mobileNo,String countrycode_alternateNo,String patientAllergyId,String allergyId,String allergyCimsCode,String allergyName,String operation,String medicalHistoryFlag,String immunization,String bcg,String dpt,String tetnus,String immOtherDesc,
				String familyMedicalHistory,String patientMedicalHistory,String pneumonia,String hepatitis_a,String hepatitis_b,String typhoid,String influenza,String polio,String socialHabitsSmoking,String socialHabitsDrinking,String socialHabitsOthers,String patientMedicalConditionsDiabetes,String patientMedicalConditionsHypertension,
				String patientMedicalConditionsAsthma,String patientMedicalConditionsHeartDisease,String patientMedicalConditionsOthers,String familyMedicalConditionsDiabetes,String familyMedicalConditionsHypertension,
			    String familyMedicalConditionsAsthma,String familyMedicalConditionsHeartDisease,String familyMedicalConditionsOthers,String notes,String medicine,String comments,String operation1) {
			
			JSONObject patient = new JSONObject();
			
			JSONObject patientRegistrationArray = new JSONObject();
			patientRegistrationArray .put(JsonParseAppointment.patientId, patientId);
			patientRegistrationArray .put(JsonParseAppointment.title, title);
			patientRegistrationArray .put(JsonParseAppointment.patientName,patientName);
			patientRegistrationArray .put(JsonParseAppointment.lastName,lastName);
			patientRegistrationArray .put(JsonParseAppointment.gender, gender);
			patientRegistrationArray .put(JsonParseAppointment.bloodGroup, bloodGroup);
			patientRegistrationArray .put(JsonParseAppointment.patientDOB, patientDOB);
			patientRegistrationArray .put(JsonParseAppointment.age, age);
			patientRegistrationArray .put(JsonParseAppointment.cityId, cityId);
			patientRegistrationArray .put(JsonParseAppointment.mobileNo, mobileNo);
			patientRegistrationArray .put(JsonParseAppointment.emailId, emailId);
			patientRegistrationArray .put(JsonParseAppointment.addressLine1, addressLine1);
			patientRegistrationArray .put(JsonParseAppointment.addressLine2, addressLine2);
			patientRegistrationArray .put(JsonParseAppointment.country, country);
			patientRegistrationArray .put(JsonParseAppointment.state, state);
			patientRegistrationArray .put(JsonParseAppointment.city, city);
			patientRegistrationArray .put(JsonParseAppointment.zipCode, zipCode);
			patientRegistrationArray .put(JsonParseAppointment.patientPhoto, patientPhoto);
			patientRegistrationArray .put(JsonParseAppointment.birthDayWish, birthDayWish);
			patientRegistrationArray .put(JsonParseAppointment.homeMonitoring, homeMonitoring);
			patientRegistrationArray .put(JsonParseAppointment.alternate_number, alternate_number);
			patientRegistrationArray .put(JsonParseAppointment.casualty, casualty);
			patientRegistrationArray .put(JsonParseAppointment.religion, religion);
			patientRegistrationArray .put(JsonParseAppointment.employment, employment);
			patientRegistrationArray .put(JsonParseAppointment.mlc, mlc);
			patientRegistrationArray .put(JsonParseAppointment.mlc_number, mlc_number);
			patientRegistrationArray .put(JsonParseAppointment.mlc_description, mlc_description);
			patientRegistrationArray .put(JsonParseAppointment.government_id_type, government_id_type);
			patientRegistrationArray .put(JsonParseAppointment.government_id_number, government_id_number);
			patientRegistrationArray .put(JsonParseAppointment.martial_status, martial_status);
			patientRegistrationArray .put(JsonParseAppointment.clinic_id, clinic_id);
			patientRegistrationArray .put(JsonParseAppointment.countrycode_mobileNo, countrycode_mobileNo);
			patientRegistrationArray .put(JsonParseAppointment.countrycode_alternateNo, countrycode_alternateNo);
			
			JSONArray allergyArray = new JSONArray();
			JSONObject allergy = new JSONObject();
			allergy .put(JsonParseAppointment.patientAllergyId, patientAllergyId);
			allergy .put(JsonParseAppointment.allergyId, allergyId);
			allergy .put(JsonParseAppointment.allergyCimsCode, allergyCimsCode);
			allergy .put(JsonParseAppointment.allergyName, allergyName);
			allergy .put(JsonParseAppointment.operation, operation);
			allergyArray.put(allergy);
			patientRegistrationArray .put(JsonParseAppointment.patientAllergy, allergyArray);
			
			patient .put(JsonParseAppointment.medicalHistoryFlag, medicalHistoryFlag);
			
			JSONObject medicalHistoryArray = new JSONObject();
			medicalHistoryArray .put(JsonParseAppointment.immunization, immunization);
			medicalHistoryArray .put(JsonParseAppointment.bcg, bcg);
			medicalHistoryArray .put(JsonParseAppointment.dpt, dpt);
			medicalHistoryArray .put(JsonParseAppointment.tetnus, tetnus);
			medicalHistoryArray .put(JsonParseAppointment.immOtherDesc, immOtherDesc);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalHistory, familyMedicalHistory);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalHistory, patientMedicalHistory);
			medicalHistoryArray .put(JsonParseAppointment.pneumonia, pneumonia);
			medicalHistoryArray .put(JsonParseAppointment.hepatitis_a, hepatitis_a);
			medicalHistoryArray .put(JsonParseAppointment.hepatitis_b, hepatitis_b);
			medicalHistoryArray .put(JsonParseAppointment.typhoid, typhoid);
			medicalHistoryArray .put(JsonParseAppointment.influenza, influenza);
			medicalHistoryArray .put(JsonParseAppointment.polio, polio);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsSmoking, socialHabitsSmoking);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsDrinking, socialHabitsDrinking);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsOthers, socialHabitsOthers);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsDiabetes, patientMedicalConditionsDiabetes);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsHypertension, patientMedicalConditionsHypertension);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsAsthma, patientMedicalConditionsAsthma);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsHeartDisease, patientMedicalConditionsHeartDisease);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsOthers, patientMedicalConditionsOthers);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsDiabetes, familyMedicalConditionsDiabetes);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsAsthma, familyMedicalConditionsAsthma);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsHeartDisease, familyMedicalConditionsHeartDisease);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsHypertension, familyMedicalConditionsHypertension);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsOthers, familyMedicalConditionsOthers);
			medicalHistoryArray .put(JsonParseAppointment.notes, notes);
			
			JSONArray medication = new JSONArray();
			JSONObject medic = new JSONObject();
			medic .put(JsonParseAppointment.medicine, medicine);
			medic .put(JsonParseAppointment.comments, comments);
			medic .put(JsonParseAppointment.operation, operation1);
			medication.put(medic);
			medicalHistoryArray .put(JsonParseAppointment.currentMedication, medication);
			
			patient .put(JsonParseAppointment.medicalHistory, medicalHistoryArray);
			patient .put(JsonParseAppointment.patient, patientRegistrationArray);

			return patient;
		}

		//patientRegistrationAlternateFlow
		public static List<NameValuePair> patientRegistrationAlternateFlow_Param(String token,String userId,String userLoginName,String organisationId,String siteId,
				  String locationId,String isDuplicateCheckRequired,String isPatientMappingRequired,String isVirtualClinicRequest){
		param = new ArrayList<NameValuePair>();

		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseAppointment.isDuplicateCheckRequired, isDuplicateCheckRequired));
		param.add(new BasicNameValuePair(JsonParseAppointment.isPatientMappingRequired, isPatientMappingRequired));
		param.add(new BasicNameValuePair(JsonParseAppointment.isVirtualClinicRequest, isVirtualClinicRequest));
		return param;
		}

		public static JSONObject getJSONPatientRegistrationAlternateFlow(String patientId,String title,String patientName,String lastName,String gender,String bloodGroup,String patientDOB,String age,String cityId,String mobileNo,String emailId,String addressLine1,String addressLine2,String country,
				String state,String city,String zipCode,String patientPhoto,String birthDayWish,String homeMonitoring,String medicalHistoryFlag,String immunization,String bcg,String dpt,String tetnus,String immOtherDesc,String familyMedicalHistory,String patientMedicalHistory,String pneumonia,
				String hepatitis_a,String hepatitis_b,String typhoid,String influenza,String polio,String socialHabitsSmoking,String socialHabitsDrinking,String socialHabitsOthers,String patientMedicalConditionsDiabetes,String patientMedicalConditionsHypertension,
				String patientMedicalConditionsAsthma,String patientMedicalConditionsHeartDisease,String patientMedicalConditionsOthers,String familyMedicalConditionsDiabetes,String familyMedicalConditionsHypertension,
			    String familyMedicalConditionsAsthma,String familyMedicalConditionsHeartDisease,String familyMedicalConditionsOthers,String notes,String medicine,String comments,String operation1,String patientAllergyId,
			    String allergyId,String allergyCimsCode,String allergyName,String operation,String alternate_number,String casualty,String religion,String employment,String mlc,String mlc_number,String mlc_description,String government_id_type,
			    String government_id_number,String martial_status,String clinic_id,String countrycode_mobileNo,String countrycode_alternateNo,String patient_password,String  patientLoginName) {
			
			JSONObject patient = new JSONObject();
			
			JSONObject patientRegistrationArray = new JSONObject();
			patientRegistrationArray .put(JsonParseAppointment.patientId, patientId);
			patientRegistrationArray .put(JsonParseAppointment.title, title);
			patientRegistrationArray .put(JsonParseAppointment.patientName,patientName);
			patientRegistrationArray .put(JsonParseAppointment.lastName,lastName);
			patientRegistrationArray .put(JsonParseAppointment.gender, gender);
			patientRegistrationArray .put(JsonParseAppointment.bloodGroup, bloodGroup);
			patientRegistrationArray .put(JsonParseAppointment.patientDOB, patientDOB);
			patientRegistrationArray .put(JsonParseAppointment.age, age);
			patientRegistrationArray .put(JsonParseAppointment.cityId, cityId);
			patientRegistrationArray .put(JsonParseAppointment.mobileNo, mobileNo);
			patientRegistrationArray .put(JsonParseAppointment.emailId, emailId);
			patientRegistrationArray .put(JsonParseAppointment.addressLine1, addressLine1);
			patientRegistrationArray .put(JsonParseAppointment.addressLine2, addressLine2);
			patientRegistrationArray .put(JsonParseAppointment.country, country);
			patientRegistrationArray .put(JsonParseAppointment.state, state);
			patientRegistrationArray .put(JsonParseAppointment.city, city);
			patientRegistrationArray .put(JsonParseAppointment.zipCode, zipCode);
			patientRegistrationArray .put(JsonParseAppointment.patientPhoto, patientPhoto);
			patientRegistrationArray .put(JsonParseAppointment.birthDayWish, birthDayWish);
			patientRegistrationArray .put(JsonParseAppointment.homeMonitoring, homeMonitoring);
			
			
			JSONArray allergyArray = new JSONArray();
			JSONObject allergy = new JSONObject();
			allergy .put(JsonParseAppointment.patientAllergyId, patientAllergyId);
			allergy .put(JsonParseAppointment.allergyId, allergyId);
			allergy .put(JsonParseAppointment.allergyCimsCode, allergyCimsCode);
			allergy .put(JsonParseAppointment.allergyName, allergyName);
			allergy .put(JsonParseAppointment.operation, operation);
			allergyArray.put(allergy);
			patientRegistrationArray .put(JsonParseAppointment.patientAllergy, allergyArray);
			
			patientRegistrationArray .put(JsonParseAppointment.medicalHistoryFlag, medicalHistoryFlag);
			
			JSONObject medicalHistoryArray = new JSONObject();
			medicalHistoryArray .put(JsonParseAppointment.immunization, immunization);
			medicalHistoryArray .put(JsonParseAppointment.bcg, bcg);
			medicalHistoryArray .put(JsonParseAppointment.dpt, dpt);
			medicalHistoryArray .put(JsonParseAppointment.tetnus, tetnus);
			medicalHistoryArray .put(JsonParseAppointment.immOtherDesc, immOtherDesc);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalHistory, familyMedicalHistory);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalHistory, patientMedicalHistory);
			medicalHistoryArray .put(JsonParseAppointment.pneumonia, pneumonia);
			medicalHistoryArray .put(JsonParseAppointment.hepatitis_a, hepatitis_a);
			medicalHistoryArray .put(JsonParseAppointment.hepatitis_b, hepatitis_b);
			medicalHistoryArray .put(JsonParseAppointment.typhoid, typhoid);
			medicalHistoryArray .put(JsonParseAppointment.influenza, influenza);
			medicalHistoryArray .put(JsonParseAppointment.polio, polio);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsSmoking, socialHabitsSmoking);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsDrinking, socialHabitsDrinking);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsOthers, socialHabitsOthers);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsDiabetes, patientMedicalConditionsDiabetes);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsHypertension, patientMedicalConditionsHypertension);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsAsthma, patientMedicalConditionsAsthma);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsHeartDisease, patientMedicalConditionsHeartDisease);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsOthers, patientMedicalConditionsOthers);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsDiabetes, familyMedicalConditionsDiabetes);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsAsthma, familyMedicalConditionsAsthma);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsHeartDisease, familyMedicalConditionsHeartDisease);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsHypertension, familyMedicalConditionsHypertension);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsOthers, familyMedicalConditionsOthers);
			medicalHistoryArray .put(JsonParseAppointment.notes, notes);
			
			JSONArray medication = new JSONArray();
			JSONObject medic = new JSONObject();
			medic .put(JsonParseAppointment.medicine, medicine);
			medic .put(JsonParseAppointment.comments, comments);
			medic .put(JsonParseAppointment.operation, operation1);
			medication.put(medic);
			medicalHistoryArray .put(JsonParseAppointment.currentMedication, medication);
			
			patientRegistrationArray .put(JsonParseAppointment.medicalHistory, medicalHistoryArray);
			
			
			patientRegistrationArray .put(JsonParseAppointment.alternate_number, alternate_number);
			patientRegistrationArray .put(JsonParseAppointment.casualty, casualty);
			patientRegistrationArray .put(JsonParseAppointment.religion, religion);
			patientRegistrationArray .put(JsonParseAppointment.employment, employment);
			patientRegistrationArray .put(JsonParseAppointment.mlc, mlc);
			patientRegistrationArray .put(JsonParseAppointment.mlc_number, mlc_number);
			patientRegistrationArray .put(JsonParseAppointment.mlc_description, mlc_description);
			patientRegistrationArray .put(JsonParseAppointment.government_id_type, government_id_type);
			patientRegistrationArray .put(JsonParseAppointment.government_id_number, government_id_number);
			patientRegistrationArray .put(JsonParseAppointment.martial_status, martial_status);
			patientRegistrationArray .put(JsonParseAppointment.clinic_id, clinic_id);
			patientRegistrationArray .put(JsonParseAppointment.countrycode_mobileNo, countrycode_mobileNo);
			patientRegistrationArray .put(JsonParseAppointment.countrycode_alternateNo, countrycode_alternateNo);
			patientRegistrationArray .put(JsonParseAppointment.patientLoginName, patientLoginName );
			patientRegistrationArray .put(JsonParseAppointment.patient_password,patient_password );
			
			patient .put(JsonParseAppointment.patient, patientRegistrationArray);
			return patient;
		}



		//patientRegistrationAlternateFlowVC
		public static List<NameValuePair> patientRegistrationAlternateFlowVC_Param(String token,String userId,String userLoginName,String organisationId,String siteId,
				  String locationId,String isDuplicateCheckRequired,String isPatientMappingRequired,String isVirtualClinicRequest){
		param = new ArrayList<NameValuePair>();

		param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
		param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
		param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseAppointment.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseAppointment.isDuplicateCheckRequired, isDuplicateCheckRequired));
		param.add(new BasicNameValuePair(JsonParseAppointment.isPatientMappingRequired, isPatientMappingRequired));
		param.add(new BasicNameValuePair(JsonParseAppointment.isVirtualClinicRequest, isVirtualClinicRequest));
		return param;
		}

		public static JSONObject getJSONPatientRegistrationAlternateFlowVC(String patientId,String title,String patientName,String lastName,String gender,String bloodGroup,String patientDOB,String age,String cityId,String mobileNo,String emailId,String addressLine1,String addressLine2,String country,
				String state,String city,String zipCode,String patientPhoto,String birthDayWish,String homeMonitoring,String medicalHistoryFlag,String immunization,String bcg,String dpt,String tetnus,String immOtherDesc,String familyMedicalHistory,String patientMedicalHistory,String pneumonia,
				String hepatitis_a,String hepatitis_b,String typhoid,String influenza,String polio,String socialHabitsSmoking,String socialHabitsDrinking,String socialHabitsOthers,String patientMedicalConditionsDiabetes,String patientMedicalConditionsHypertension,
				String patientMedicalConditionsAsthma,String patientMedicalConditionsHeartDisease,String patientMedicalConditionsOthers,String familyMedicalConditionsDiabetes,String familyMedicalConditionsHypertension,
			    String familyMedicalConditionsAsthma,String familyMedicalConditionsHeartDisease,String familyMedicalConditionsOthers,String notes,String medicine,String comments,String operation1,String patientAllergyId,
			    String allergyId,String allergyCimsCode,String allergyName,String operation,String alternate_number,String casualty,String religion,String employment,String mlc,String mlc_number,String mlc_description,String government_id_type,
			    String government_id_number,String martial_status,String clinic_id,String countrycode_mobileNo,String countrycode_alternateNo,String patient_password,String  patientLoginName) {
			
			JSONObject patient = new JSONObject();
			
			JSONObject patientRegistrationArray = new JSONObject();
			patientRegistrationArray .put(JsonParseAppointment.patientId, patientId);
			patientRegistrationArray .put(JsonParseAppointment.title, title);
			patientRegistrationArray .put(JsonParseAppointment.patientName,patientName);
			patientRegistrationArray .put(JsonParseAppointment.lastName,lastName);
			patientRegistrationArray .put(JsonParseAppointment.gender, gender);
			patientRegistrationArray .put(JsonParseAppointment.bloodGroup, bloodGroup);
			patientRegistrationArray .put(JsonParseAppointment.patientDOB, patientDOB);
			patientRegistrationArray .put(JsonParseAppointment.age, age);
			patientRegistrationArray .put(JsonParseAppointment.cityId, cityId);
			patientRegistrationArray .put(JsonParseAppointment.mobileNo, mobileNo);
			patientRegistrationArray .put(JsonParseAppointment.emailId, emailId);
			patientRegistrationArray .put(JsonParseAppointment.addressLine1, addressLine1);
			patientRegistrationArray .put(JsonParseAppointment.addressLine2, addressLine2);
			patientRegistrationArray .put(JsonParseAppointment.country, country);
			patientRegistrationArray .put(JsonParseAppointment.state, state);
			patientRegistrationArray .put(JsonParseAppointment.city, city);
			patientRegistrationArray .put(JsonParseAppointment.zipCode, zipCode);
			patientRegistrationArray .put(JsonParseAppointment.patientPhoto, patientPhoto);
			patientRegistrationArray .put(JsonParseAppointment.birthDayWish, birthDayWish);
			patientRegistrationArray .put(JsonParseAppointment.homeMonitoring, homeMonitoring);
			
			
			JSONArray allergyArray = new JSONArray();
			JSONObject allergy = new JSONObject();
			allergy .put(JsonParseAppointment.patientAllergyId, patientAllergyId);
			allergy .put(JsonParseAppointment.allergyId, allergyId);
			allergy .put(JsonParseAppointment.allergyCimsCode, allergyCimsCode);
			allergy .put(JsonParseAppointment.allergyName, allergyName);
			allergy .put(JsonParseAppointment.operation, operation);
			allergyArray.put(allergy);
			patientRegistrationArray .put(JsonParseAppointment.patientAllergy, allergyArray);
			
			patientRegistrationArray .put(JsonParseAppointment.medicalHistoryFlag, medicalHistoryFlag);
			
			JSONObject medicalHistoryArray = new JSONObject();
			medicalHistoryArray .put(JsonParseAppointment.immunization, immunization);
			medicalHistoryArray .put(JsonParseAppointment.bcg, bcg);
			medicalHistoryArray .put(JsonParseAppointment.dpt, dpt);
			medicalHistoryArray .put(JsonParseAppointment.tetnus, tetnus);
			medicalHistoryArray .put(JsonParseAppointment.immOtherDesc, immOtherDesc);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalHistory, familyMedicalHistory);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalHistory, patientMedicalHistory);
			medicalHistoryArray .put(JsonParseAppointment.pneumonia, pneumonia);
			medicalHistoryArray .put(JsonParseAppointment.hepatitis_a, hepatitis_a);
			medicalHistoryArray .put(JsonParseAppointment.hepatitis_b, hepatitis_b);
			medicalHistoryArray .put(JsonParseAppointment.typhoid, typhoid);
			medicalHistoryArray .put(JsonParseAppointment.influenza, influenza);
			medicalHistoryArray .put(JsonParseAppointment.polio, polio);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsSmoking, socialHabitsSmoking);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsDrinking, socialHabitsDrinking);
			medicalHistoryArray .put(JsonParseAppointment.socialHabitsOthers, socialHabitsOthers);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsDiabetes, patientMedicalConditionsDiabetes);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsHypertension, patientMedicalConditionsHypertension);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsAsthma, patientMedicalConditionsAsthma);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsHeartDisease, patientMedicalConditionsHeartDisease);
			medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsOthers, patientMedicalConditionsOthers);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsDiabetes, familyMedicalConditionsDiabetes);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsAsthma, familyMedicalConditionsAsthma);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsHeartDisease, familyMedicalConditionsHeartDisease);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsHypertension, familyMedicalConditionsHypertension);
			medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsOthers, familyMedicalConditionsOthers);
			medicalHistoryArray .put(JsonParseAppointment.notes, notes);
			
			JSONArray medication = new JSONArray();
			JSONObject medic = new JSONObject();
			medic .put(JsonParseAppointment.medicine, medicine);
			medic .put(JsonParseAppointment.comments, comments);
			medic .put(JsonParseAppointment.operation, operation1);
			medication.put(medic);
			medicalHistoryArray .put(JsonParseAppointment.currentMedication, medication);
			
			patientRegistrationArray .put(JsonParseAppointment.medicalHistory, medicalHistoryArray);
			
			
			patientRegistrationArray .put(JsonParseAppointment.alternate_number, alternate_number);
			patientRegistrationArray .put(JsonParseAppointment.casualty, casualty);
			patientRegistrationArray .put(JsonParseAppointment.religion, religion);
			patientRegistrationArray .put(JsonParseAppointment.employment, employment);
			patientRegistrationArray .put(JsonParseAppointment.mlc, mlc);
			patientRegistrationArray .put(JsonParseAppointment.mlc_number, mlc_number);
			patientRegistrationArray .put(JsonParseAppointment.mlc_description, mlc_description);
			patientRegistrationArray .put(JsonParseAppointment.government_id_type, government_id_type);
			patientRegistrationArray .put(JsonParseAppointment.government_id_number, government_id_number);
			patientRegistrationArray .put(JsonParseAppointment.martial_status, martial_status);
			patientRegistrationArray .put(JsonParseAppointment.clinic_id, clinic_id);
			patientRegistrationArray .put(JsonParseAppointment.countrycode_mobileNo, countrycode_mobileNo);
			patientRegistrationArray .put(JsonParseAppointment.countrycode_alternateNo, countrycode_alternateNo);
			patientRegistrationArray .put(JsonParseAppointment.patientLoginName,patientLoginName );
			patientRegistrationArray .put(JsonParseAppointment.patient_password,patient_password );
			
			patient .put(JsonParseAppointment.patient, patientRegistrationArray);
			return patient;
		}
		
		
		//updatePatientDetails
		public static List<NameValuePair> updatePatientDetails_Param(String token,String userId,String userLoginName,String organizationId,String siteId,String locationId){
			param = new ArrayList<NameValuePair>();

			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseAppointment.organizationId, organizationId));
			param.add(new BasicNameValuePair(JsonParseAppointment.locationId, locationId));
			param.add(new BasicNameValuePair(JsonParseAppointment.siteId, siteId));
			return param;
			}

		public static JSONObject getJSONUpdatePatientDetails(String patientId,String patientUHID,String title,String patientName,String lastName,String gender,String bloodGroup,String patientDOB,String mobileNo,String emailId,String addressLine1,String addressLine2,String country,
					String state,String city,String zipCode,String patientPhoto,String birthDayWish,String homeMonitoring,String medicalHistoryFlag,String medicalHistoryId,String immunization,String bcg,String dpt,String tetnus,String immOtherDesc,String familyMedicalHistory,String patientVisitId, String patientMedicalHistory,String pneumonia,
					String hepatitis_a,String hepatitis_b,String typhoid,String influenza,String polio,String socialHabitsSmoking,String socialHabitsDrinking,String socialHabitsOthers,String patientMedicalConditionsDiabetes,String patientMedicalConditionsHypertension,
					String patientMedicalConditionsAsthma,String patientMedicalConditionsHeartDisease,String patientMedicalConditionsOthers,String familyMedicalConditionsDiabetes,String familyMedicalConditionsHypertension,
				    String familyMedicalConditionsAsthma,String familyMedicalConditionsHeartDisease,String familyMedicalConditionsOthers,String notes,String medicine,String comments,String operation1,String patientAllergyId,
				    String allergyId,String allergyCimsCode,String allergyName,String operation,String alternate_number,String casualty,String religion,String employment,String mlc,String mlc_number,String mlc_description,String government_id_type,
				    String government_id_number,String martial_status,String clinic_id,String countrycode_mobileNo,String countrycode_alternateNo) {
				
				JSONObject patient = new JSONObject();
				
				JSONObject patientRegistrationArray = new JSONObject();
				patientRegistrationArray .put(JsonParseAppointment.patientId, patientId);
				patientRegistrationArray .put(JsonParseAppointment.patientUHID, patientUHID);
				patientRegistrationArray .put(JsonParseAppointment.title, title);
				patientRegistrationArray .put(JsonParseAppointment.patientName,patientName);
				patientRegistrationArray .put(JsonParseAppointment.lastName,lastName);
				patientRegistrationArray .put(JsonParseAppointment.gender, gender);
				patientRegistrationArray .put(JsonParseAppointment.bloodGroup, bloodGroup);
				patientRegistrationArray .put(JsonParseAppointment.patientDOB, patientDOB);
				patientRegistrationArray .put(JsonParseAppointment.mobileNo, mobileNo);
				patientRegistrationArray .put(JsonParseAppointment.emailId, emailId);
				patientRegistrationArray .put(JsonParseAppointment.addressLine1, addressLine1);
				patientRegistrationArray .put(JsonParseAppointment.addressLine2, addressLine2);
				patientRegistrationArray .put(JsonParseAppointment.country, country);
				patientRegistrationArray .put(JsonParseAppointment.state, state);
				patientRegistrationArray .put(JsonParseAppointment.city, city);
				patientRegistrationArray .put(JsonParseAppointment.zipCode, zipCode);
				patientRegistrationArray .put(JsonParseAppointment.patientPhoto, patientPhoto);
				patientRegistrationArray .put(JsonParseAppointment.birthDayWish, birthDayWish);
				patientRegistrationArray .put(JsonParseAppointment.homeMonitoring, homeMonitoring);
				
				
				JSONArray allergyArray = new JSONArray();
				JSONObject allergy = new JSONObject();
				allergy .put(JsonParseAppointment.patientAllergyId, patientAllergyId);
				allergy .put(JsonParseAppointment.allergyId, allergyId);
				allergy .put(JsonParseAppointment.allergyCimsCode, allergyCimsCode);
				allergy .put(JsonParseAppointment.allergyName, allergyName);
				allergy .put(JsonParseAppointment.operation, operation);
				allergyArray.put(allergy);
				patientRegistrationArray .put(JsonParseAppointment.patientAllergy, allergyArray);
				
				patientRegistrationArray .put(JsonParseAppointment.medicalHistoryFlag, medicalHistoryFlag);
				
				JSONObject medicalHistoryArray = new JSONObject();
				medicalHistoryArray .put(JsonParseAppointment.medicalHistoryId, medicalHistoryId);
				medicalHistoryArray .put(JsonParseAppointment.immunization, immunization);
				medicalHistoryArray .put(JsonParseAppointment.bcg, bcg);
				medicalHistoryArray .put(JsonParseAppointment.dpt, dpt);
				medicalHistoryArray .put(JsonParseAppointment.tetnus, tetnus);
				medicalHistoryArray .put(JsonParseAppointment.immOtherDesc, immOtherDesc);
				medicalHistoryArray .put(JsonParseAppointment.familyMedicalHistory, familyMedicalHistory);
				medicalHistoryArray .put(JsonParseAppointment.patientVisitId, patientVisitId);
				medicalHistoryArray .put(JsonParseAppointment.patientMedicalHistory, patientMedicalHistory);
				medicalHistoryArray .put(JsonParseAppointment.pneumonia, pneumonia);
				medicalHistoryArray .put(JsonParseAppointment.hepatitis_a, hepatitis_a);
				medicalHistoryArray .put(JsonParseAppointment.hepatitis_b, hepatitis_b);
				medicalHistoryArray .put(JsonParseAppointment.typhoid, typhoid);
				medicalHistoryArray .put(JsonParseAppointment.influenza, influenza); 
				medicalHistoryArray .put(JsonParseAppointment.polio, polio);
				medicalHistoryArray .put(JsonParseAppointment.socialHabitsSmoking, socialHabitsSmoking);
				medicalHistoryArray .put(JsonParseAppointment.socialHabitsDrinking, socialHabitsDrinking);
				medicalHistoryArray .put(JsonParseAppointment.socialHabitsOthers, socialHabitsOthers);
				medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsDiabetes, patientMedicalConditionsDiabetes);
				medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsHypertension, patientMedicalConditionsHypertension);
				medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsAsthma, patientMedicalConditionsAsthma);
				medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsHeartDisease, patientMedicalConditionsHeartDisease);
				medicalHistoryArray .put(JsonParseAppointment.patientMedicalConditionsOthers, patientMedicalConditionsOthers);
				medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsDiabetes, familyMedicalConditionsDiabetes);
				medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsAsthma, familyMedicalConditionsAsthma);
				medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsHeartDisease, familyMedicalConditionsHeartDisease);
				medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsHypertension, familyMedicalConditionsHypertension);
				medicalHistoryArray .put(JsonParseAppointment.familyMedicalConditionsOthers, familyMedicalConditionsOthers);
				medicalHistoryArray .put(JsonParseAppointment.notes, notes);
				
				JSONArray medication = new JSONArray();
				JSONObject medic = new JSONObject();
				medic .put(JsonParseAppointment.medicine, medicine);
				medic .put(JsonParseAppointment.comments, comments);
				medic .put(JsonParseAppointment.operation, operation1);
				medication.put(medic);
				medicalHistoryArray .put(JsonParseAppointment.currentMedication, medication);
				
				patientRegistrationArray .put(JsonParseAppointment.medicalHistory, medicalHistoryArray);
				
				
				patientRegistrationArray .put(JsonParseAppointment.alternate_number, alternate_number);
				patientRegistrationArray .put(JsonParseAppointment.casualty, casualty);
				patientRegistrationArray .put(JsonParseAppointment.religion, religion);
				patientRegistrationArray .put(JsonParseAppointment.employment, employment);
				patientRegistrationArray .put(JsonParseAppointment.mlc, mlc);
				patientRegistrationArray .put(JsonParseAppointment.mlc_number, mlc_number);
				patientRegistrationArray .put(JsonParseAppointment.mlc_description, mlc_description);
				patientRegistrationArray .put(JsonParseAppointment.government_id_type, government_id_type);
				patientRegistrationArray .put(JsonParseAppointment.government_id_number, government_id_number);
				patientRegistrationArray .put(JsonParseAppointment.martial_status, martial_status);
				patientRegistrationArray .put(JsonParseAppointment.clinic_id, clinic_id);
				patientRegistrationArray .put(JsonParseAppointment.countrycode_mobileNo, countrycode_mobileNo);
				patientRegistrationArray .put(JsonParseAppointment.countrycode_alternateNo, countrycode_alternateNo);
				
				patient .put(JsonParseAppointment.patient, patientRegistrationArray);
				return patient;
			}
		
		
		public static List<NameValuePair> getUpdateQualification(String token,String userLoginName,String userId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseAppointment.userId, userId));
			param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, organisationId));
			return param;

		}
		
		public static JSONObject getJSONUpdateQualification(String instituteName,String fromDate,String toDate, String board, String university,String course,String degree,String specialization) {		
			JSONObject updateQualification = new JSONObject();	 
			JSONObject updateQualificationArray  = new JSONObject();	 
			updateQualificationArray .put("instituteName",instituteName);
			updateQualificationArray .put("fromDate", fromDate);
			updateQualificationArray .put("toDate", toDate);
			updateQualificationArray .put("board", board);
			updateQualificationArray .put("university", university);
			updateQualificationArray .put("course", course);
			updateQualificationArray .put("degree", degree);
			updateQualificationArray .put("specialization", specialization);
			updateQualification.put("qualification",updateQualificationArray);
			System.out.println("req:"+updateQualification.toString());
			return updateQualification;
		}
		
		public static List<NameValuePair> fetchOfflineDBversionInfo_Param(String token,String userLoginName,String organization_id) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
			param.add(new BasicNameValuePair("organizationId", organization_id));
			return param;

		}
		
		public static List<NameValuePair> SyncWebDatawithTabOrg_Param(String token,String userLoginName,String organization_id,String lastsync,String globalLastSync) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
			param.add(new BasicNameValuePair("organizationId", organization_id));
			param.add(new BasicNameValuePair("lastsync", lastsync));
			param.add(new BasicNameValuePair("globalLastSync", globalLastSync));
			return param;

		}
		
		
		public static List<NameValuePair> saveOfflineData_Param(String userLoginName,String token,String organization_id) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
			param.add(new BasicNameValuePair("organizationId", organization_id));
			return param;
		}
		
		public static JSONObject getJSONcreateService( String patient_uhid, String patient_name, String isactive, String created_user_id, String created_timestamp, String modified_user_id, String modified_timestamp, String organization_id, String last_name, String patient_org_serial_no, String custom_clinic_id, String patient_id, String title, String gender, String blood_group, String patient_dob, String martial_status, String age, String mobile_no, String email_id, String address_line1, String address_line2, String zip_code, String city, String state, String country, String patient_photo, String birthday_wish, String home_monitoring, String city_id, String patient_associated_clinic_id, String org_ds_detail_id, String patient_password, String alternate_number, String casualty, String religion, String employment, String mlc, String mlc_number, String mlc_description, String government_id_type, String government_id_number, String countrycode_mobileno, String countrycode_alternateno, String visit_date, String visit_time, String prn_summary_no, String prn_vital, String prn_symptoms, String prn_exam, String prn_diagnosis, String prn_prescription, String prn_generic_adv, String prn_follow_up, String prn_lab_test, String checkedin_st, String pre_consulting_st, String pre_consulted_st, String consulting_st, String checked_out_st, String status, String organization_id1, String created_user_id1, String created_timestamp1, String site_id, String location_id, String patient_id1, String appointment_id, String e_pharmacy_id, String prn_refferal, String created_user_id2, String created_timestamp2, String modified_user_id1, String clinic_user_id, String organization_id2, String onset_date, String onset_visit_id, String status1, String close_date, String close_visit_id, String patient_id2, String terminology_code_id, String allergy_type, String allergy_code, String allergy_name, String created_timestamp3, String patient_allergy_name, String onset_date1, String onset_visit_id1, String close_date1, String close_visit_id1, String created_timestamp4, String allergy_id, String patient_id3, String symptom_name, String created_timestamp5, String patient_visit_id, String duration, String other_details, String symptom_id, String vital_date, String vital_time, String parameter, String parameter_value, String modified_user_id2, String patient_visit_id1, String parameter_comment, String note_value, String clinic_user_id1, String prescription_id, String prescription_date, String prescription_time, String patient_id4, String generic_drug_code, String drug_code, String drug_name, String dosage, String freq_mor, String freq_an, String freq_night, String route, String duration_no, String duration_unit, String start_date, String sos, String af, String bf, String wf, String empty_stomach, String before_sleeping, String prescription_hdr_id, String additional_instruction, String medicine_type, String frequency, String dosage_advice, String duration1, String drug_type, String status2, String service_order_hdr_date, String service_order_hdr_time, String note, String service_id, String service_order_hdr_id, String service_name,String stage,String dr_specific_diagnoses_id) {
			
			JSONObject completeRecord = new JSONObject();
			JSONArray completeRecordArray = new JSONArray();
			JSONObject completeRecordObject  = new JSONObject();
			
			JSONObject  patient = new JSONObject();
			
			patient.put("patient_uhid", patient_uhid);
			patient.put("patient_name", patient_name);
			patient.put("isactive", isactive);
			patient.put("created_user_id", created_user_id);
			patient.put("created_timestamp", created_timestamp);
			patient.put("modified_user_id", modified_user_id);
			patient.put("modified_timestamp", modified_timestamp);
			patient.put("organization_id", organization_id1);
			patient.put("last_name", last_name);
			patient.put("patient_org_serial_no", patient_org_serial_no);
			patient.put("custom_clinic_id", custom_clinic_id);
			
			
			JSONObject c_clinic_assoc_patient  = new JSONObject();
			
			
			c_clinic_assoc_patient.put("patient_id", patient_id);
			c_clinic_assoc_patient.put("patient_uhid", patient_uhid);
			c_clinic_assoc_patient.put("title", title);
			c_clinic_assoc_patient.put("patient_name", patient_name);
			c_clinic_assoc_patient.put("gender", gender);
			c_clinic_assoc_patient.put("blood_group", blood_group);
			c_clinic_assoc_patient.put("patient_dob", patient_dob);
			c_clinic_assoc_patient.put("martial_status", martial_status);
			c_clinic_assoc_patient.put("age", age);
			c_clinic_assoc_patient.put("mobile_no", mobile_no);
			c_clinic_assoc_patient.put("email_id", email_id);
			c_clinic_assoc_patient.put("address_line1", address_line1);
			c_clinic_assoc_patient.put("address_line2", address_line2);
			c_clinic_assoc_patient.put("zip_code", zip_code);
			c_clinic_assoc_patient.put("city", city);
			c_clinic_assoc_patient.put("state", state);
			c_clinic_assoc_patient.put("country", country);
			c_clinic_assoc_patient.put("patient_photo", patient_photo);
			c_clinic_assoc_patient.put("birthday_wish", birthday_wish);
			c_clinic_assoc_patient.put("home_monitoring", home_monitoring);
			c_clinic_assoc_patient.put("isactive", isactive);
			c_clinic_assoc_patient.put("created_user_id", created_user_id);
			c_clinic_assoc_patient.put("created_timestamp", created_timestamp);
			c_clinic_assoc_patient.put("modified_user_id", modified_user_id);
			c_clinic_assoc_patient.put("modified_timestamp", modified_timestamp);
			c_clinic_assoc_patient.put("city_id", city_id);
			c_clinic_assoc_patient.put("patient_associated_clinic_id", patient_associated_clinic_id);
			c_clinic_assoc_patient.put("org_ds_detail_id", org_ds_detail_id);
			c_clinic_assoc_patient.put("last_name", last_name);
			c_clinic_assoc_patient.put("patient_password", patient_password);
			c_clinic_assoc_patient.put("alternate_number", alternate_number);
			c_clinic_assoc_patient.put("casualty", casualty);
			c_clinic_assoc_patient.put("religion", religion);
			c_clinic_assoc_patient.put("employment", employment);
			c_clinic_assoc_patient.put("mlc", mlc);
			c_clinic_assoc_patient.put("mlc_number", mlc_number);
			c_clinic_assoc_patient.put("mlc_description", mlc_description);
			c_clinic_assoc_patient.put("government_id_type", government_id_type);
			c_clinic_assoc_patient.put("government_id_number", government_id_number);
			c_clinic_assoc_patient.put("countrycode_mobileno", countrycode_mobileno);
			c_clinic_assoc_patient.put("countrycode_alternateno", countrycode_alternateno);
			
			patient.put("c_clinic_assoc_patient", c_clinic_assoc_patient);
			
			
			JSONArray patient_visitArray = new JSONArray();
			JSONObject patient_visitObject  = new JSONObject();
			
			
			patient_visitObject.put("visit_date", visit_date);
			patient_visitObject.put("visit_time", visit_time);
			patient_visitObject.put("prn_summary_no", prn_summary_no);
			patient_visitObject.put("prn_vital", prn_vital);
			patient_visitObject.put("prn_symptoms", prn_symptoms);
			patient_visitObject.put("prn_exam", prn_exam);
			patient_visitObject.put("prn_diagnosis", prn_diagnosis);
			patient_visitObject.put("prn_prescription", prn_prescription);
			patient_visitObject.put("prn_generic_adv", prn_generic_adv);
			patient_visitObject.put("prn_follow_up", prn_follow_up);
			patient_visitObject.put("prn_lab_test", prn_lab_test);
			patient_visitObject.put("checkedin_st", checkedin_st);
			patient_visitObject.put("pre_consulting_st", pre_consulting_st);
			patient_visitObject.put("pre_consulted_st", pre_consulted_st);
			patient_visitObject.put("consulting_st", consulting_st);
			patient_visitObject.put("checked_out_st", checked_out_st);
			patient_visitObject.put("isactive", isactive);
			patient_visitObject.put("status", status);
			patient_visitObject.put("organization_id", organization_id1);
			patient_visitObject.put("created_user_id", created_user_id1);
			patient_visitObject.put("created_timestamp", created_timestamp1);
			patient_visitObject.put("modified_user_id", modified_user_id);
			patient_visitObject.put("modified_timestamp", modified_timestamp);
			patient_visitObject.put("site_id", site_id);
			patient_visitObject.put("location_id", location_id);
			patient_visitObject.put("patient_id", patient_id1);
			patient_visitObject.put("appointment_id", appointment_id);
			patient_visitObject.put("e_pharmacy_id", e_pharmacy_id);
			patient_visitObject.put("prn_refferal", prn_refferal);
			
			
			JSONArray patient_diagnosisArray = new JSONArray();
			JSONObject patient_diagnosisObject  = new JSONObject();
			
			patient_diagnosisObject.put("stage", stage);
			patient_diagnosisObject.put("onset_date", onset_date);
			patient_diagnosisObject.put("onset_visit_id", onset_visit_id);
			patient_diagnosisObject.put("status", status1);
			patient_diagnosisObject.put("close_date", close_date);
			patient_diagnosisObject.put("close_visit_id", close_visit_id);
			patient_diagnosisObject.put("isactive", isactive);
			patient_diagnosisObject.put("created_user_id", created_user_id2);
			patient_diagnosisObject.put("created_timestamp", created_timestamp2);
			patient_diagnosisObject.put("modified_user_id", modified_user_id1);
			patient_diagnosisObject.put("modified_timestamp", modified_timestamp);
			patient_diagnosisObject.put("site_id", site_id);
			patient_diagnosisObject.put("location_id", location_id);
			patient_diagnosisObject.put("patient_id", patient_id2);
			patient_diagnosisObject.put("terminology_code_id", terminology_code_id);
			patient_diagnosisObject.put("organization_id", organization_id1);
			patient_diagnosisObject.put("dr_specific_diagnoses_id", dr_specific_diagnoses_id);
			
			patient_diagnosisArray.put(patient_diagnosisObject);
			patient_visitObject.put("patient_diagnosis", patient_diagnosisArray);
			
			
			JSONArray patient_allergyArray = new JSONArray();
			JSONObject patient_allergyObject  = new JSONObject();
			
			JSONObject allergy  = new JSONObject();
			
			allergy.put("allergy_type", allergy_type);
			allergy.put("allergy_code", allergy_code);
			allergy.put("allergy_name", allergy_name);
			allergy.put("isactive", isactive);
			allergy.put("organization_id", organization_id2);
			allergy.put("clinic_user_id", clinic_user_id);
			allergy.put("created_user_id", created_user_id1);
			allergy.put("created_timestamp", created_timestamp3);
			allergy.put("modified_user_id", modified_user_id);
			allergy.put("modified_timestamp", modified_timestamp);
			
			patient_allergyObject.put("allergy", allergy);
			patient_allergyObject.put("patient_allergy_name", patient_allergy_name);
			patient_allergyObject.put("onset_date", onset_date1);
			patient_allergyObject.put("onset_visit_id", onset_visit_id1);
			patient_allergyObject.put("close_date", close_date1);
			patient_allergyObject.put("close_visit_id", close_visit_id1);
			patient_allergyObject.put("isactive", isactive);
			patient_allergyObject.put("created_user_id", created_user_id1);
			patient_allergyObject.put("created_timestamp", created_timestamp4);
			patient_allergyObject.put("modified_user_id", modified_user_id);
			patient_allergyObject.put("modified_timestamp", modified_timestamp);
			patient_allergyObject.put("allergy_id", allergy_id);
			patient_allergyObject.put("patient_id", patient_id3);
			patient_allergyObject.put("organization_id", organization_id1);
						
			patient_allergyArray.put(patient_allergyObject);
			patient_visitObject.put("patient_allergy", patient_allergyArray);
			
			
			JSONArray patient_symptomArray = new JSONArray();
			JSONObject patient_symptomObject1  = new JSONObject();
			
			patient_symptomObject1.put("patient_visit_id", patient_visit_id);
			patient_symptomObject1.put("symptom_name", symptom_name);
			patient_symptomObject1.put("duration", duration);
			patient_symptomObject1.put("other_details", other_details);
			patient_symptomObject1.put("isactive", isactive);
			patient_symptomObject1.put("created_user_id", created_user_id1);
			patient_symptomObject1.put("created_timestamp", created_timestamp5);
			patient_symptomObject1.put("modified_user_id", modified_user_id);
			patient_symptomObject1.put("modified_timestamp", modified_timestamp);
			patient_symptomObject1.put("location_id", location_id);
			patient_symptomObject1.put("patient_id", patient_id3);
			patient_symptomObject1.put("organization_id", organization_id1);
			patient_symptomObject1.put("symptom_id", symptom_id);
			
			patient_symptomArray.put(patient_symptomObject1);
			
			JSONObject patient_symptomObject2  = new JSONObject();
			JSONObject symptom  = new JSONObject();
			
			symptom.put("symptom_name", symptom_name);
			symptom.put("isactive", isactive);
			symptom.put("organization_id", organization_id2);
			symptom.put("created_user_id", created_user_id1);
			symptom.put("created_timestamp", created_timestamp5);
			symptom.put("modified_user_id", modified_user_id);
			symptom.put("modified_timestamp", modified_timestamp);
			symptom.put("clinic_user_id", clinic_user_id);
			
			patient_symptomObject2.put("symptom", symptom);
			patient_symptomObject2.put("patient_visit_id", patient_visit_id);
			patient_symptomObject2.put("symptom_name", symptom_name);
			patient_symptomObject2.put("duration", duration);
			patient_symptomObject2.put("other_details", other_details);
			patient_symptomObject2.put("isactive", isactive);
			patient_symptomObject2.put("created_user_id", created_user_id1);
			patient_symptomObject2.put("created_timestamp", created_timestamp5);
			patient_symptomObject2.put("modified_user_id", modified_user_id);
			patient_symptomObject2.put("modified_timestamp", modified_timestamp);
			patient_symptomObject2.put("location_id", location_id);
			patient_symptomObject2.put("patient_id", patient_id3);
			patient_symptomObject2.put("organization_id", organization_id1);
			patient_symptomObject2.put("symptom_id", symptom_id);
			
			patient_symptomArray.put(patient_symptomObject2);
			patient_visitObject.put("patient_symptom", patient_symptomArray);
			
			JSONArray patient_vitalArray = new JSONArray();
			JSONObject patient_vitalObject1  = new JSONObject();
			
			
			patient_vitalObject1.put("vital_date", vital_date);
			patient_vitalObject1.put("vital_time", vital_time);
			patient_vitalObject1.put("parameter", parameter);
			patient_vitalObject1.put("parameter_value", parameter_value);
			patient_vitalObject1.put("isactive", isactive);
			patient_vitalObject1.put("created_user_id", created_user_id1);
			patient_vitalObject1.put("created_timestamp", created_timestamp1);
			patient_vitalObject1.put("modified_user_id", modified_user_id2);
			patient_vitalObject1.put("modified_timestamp", modified_timestamp);
			patient_vitalObject1.put("site_id", site_id);
			patient_vitalObject1.put("location_id", location_id);
			patient_vitalObject1.put("patient_visit_id", patient_visit_id1);
			patient_vitalObject1.put("patient_id", patient_id1);
			patient_vitalObject1.put("organization_id", organization_id1);
			patient_vitalObject1.put("parameter_comment", parameter_comment);
			
			patient_vitalArray.put(patient_vitalObject1);
			
			/*JSONObject patient_vitalObject2  = new JSONObject();
			
			
			patient_vitalObject2.put("vital_date", vital_date);
			patient_vitalObject2.put("vital_time", vital_time);
			patient_vitalObject2.put("parameter", parameter);
			patient_vitalObject2.put("parameter_value", parameter_value);
			patient_vitalObject2.put("isactive", isactive);
			patient_vitalObject2.put("created_user_id", created_user_id1);
			patient_vitalObject2.put("created_timestamp", created_timestamp1);
			patient_vitalObject2.put("modified_user_id", modified_user_id2);
			patient_vitalObject2.put("modified_timestamp", modified_timestamp);
			patient_vitalObject2.put("site_id", site_id);
			patient_vitalObject2.put("location_id", location_id);
			patient_vitalObject2.put("patient_visit_id", patient_visit_id1);
			patient_vitalObject2.put("patient_id", patient_id1);
			patient_vitalObject2.put("organization_id", organization_id1);
			patient_vitalObject2.put("parameter_comment", parameter_comment);
			
			patient_vitalArray.put(patient_vitalObject2);*/
			
			patient_visitObject.put("patient_vital", patient_vitalArray);
			
			JSONArray noteArray = new JSONArray();
			JSONObject noteObject  = new JSONObject();
			
			noteObject.put("note_value", note_value);
			noteObject.put("isactive", isactive);
			noteObject.put("organization_id", organization_id1);
			noteObject.put("created_user_id", created_user_id1);
			noteObject.put("created_timestamp", created_timestamp1);
			noteObject.put("modified_user_id", modified_user_id);
			noteObject.put("modified_timestamp", modified_timestamp);
			noteObject.put("site_id", site_id);
			noteObject.put("location_id", location_id);
			noteObject.put("patient_id", patient_id1);
			noteObject.put("patient_visit_id", patient_visit_id1);
			noteObject.put("clinic_user_id", clinic_user_id1);
			
			noteArray.put(noteObject);			
			patient_visitObject.put("note", noteArray);
			
			JSONObject prescription_hdr  = new JSONObject();
			
			prescription_hdr.put("prescription_id", prescription_id);
			prescription_hdr.put("prescription_date", prescription_date);
			prescription_hdr.put("prescription_time", prescription_time);
			prescription_hdr.put("isactive", isactive);
			prescription_hdr.put("organization_id", organization_id1);
			prescription_hdr.put("created_user_id", created_user_id1);
			prescription_hdr.put("created_timestamp", created_timestamp1);
			prescription_hdr.put("modified_user_id", modified_user_id2);
			prescription_hdr.put("modified_timestamp", modified_timestamp);
			prescription_hdr.put("site_id", site_id);
			prescription_hdr.put("location_id", location_id);
			prescription_hdr.put("patient_id", patient_id4);
			prescription_hdr.put("patient_visit_id", patient_visit_id1);
			prescription_hdr.put("clinic_user_id", clinic_user_id1);
			
			JSONArray prescription_dtlArray  = new JSONArray();
			JSONObject prescription_dtlObject1  = new JSONObject();
			
			prescription_dtlObject1.put("generic_drug_code", generic_drug_code);
			prescription_dtlObject1.put("drug_code", drug_code);
			prescription_dtlObject1.put("drug_name", drug_name);
			prescription_dtlObject1.put("dosage", dosage);
			prescription_dtlObject1.put("freq_mor", freq_mor);
			prescription_dtlObject1.put("freq_an", freq_an);
			prescription_dtlObject1.put("freq_night", freq_night);
			prescription_dtlObject1.put("route", route);
			prescription_dtlObject1.put("duration_no", duration_no);
			prescription_dtlObject1.put("duration_unit", duration_unit);
			prescription_dtlObject1.put("start_date", start_date);
			prescription_dtlObject1.put("sos", sos);
			prescription_dtlObject1.put("af", af);
			prescription_dtlObject1.put("bf", bf);
			prescription_dtlObject1.put("wf", wf);
			prescription_dtlObject1.put("empty_stomach", empty_stomach);
			prescription_dtlObject1.put("before_sleeping", before_sleeping);
			prescription_dtlObject1.put("isactive", isactive);
			prescription_dtlObject1.put("organization_id", organization_id1);
			prescription_dtlObject1.put("site_id", site_id);
			prescription_dtlObject1.put("location_id", location_id);
			prescription_dtlObject1.put("created_user_id", created_user_id1);
			prescription_dtlObject1.put("created_timestamp", created_timestamp1);
			prescription_dtlObject1.put("modified_user_id", modified_user_id2);
			prescription_dtlObject1.put("modified_timestamp", modified_timestamp);
			prescription_dtlObject1.put("prescription_hdr_id", prescription_hdr_id);
			prescription_dtlObject1.put("additional_instruction", additional_instruction);
			prescription_dtlObject1.put("medicine_type", medicine_type);
			prescription_dtlObject1.put("frequency", frequency);
			prescription_dtlObject1.put("dosage_advice", dosage_advice);
			prescription_dtlObject1.put("duration", duration1);
			prescription_dtlObject1.put("drug_type", drug_type);
			prescription_dtlObject1.put("status", status2);
			
			prescription_dtlArray.put(prescription_dtlObject1);
			
			
			JSONObject prescription_dtlObject2  = new JSONObject();
			prescription_dtlObject2.put("generic_drug_code", generic_drug_code);
			prescription_dtlObject2.put("drug_code", drug_code);
			prescription_dtlObject2.put("drug_name", drug_name);
			prescription_dtlObject2.put("dosage", dosage);
			prescription_dtlObject2.put("freq_mor", freq_mor);
			prescription_dtlObject2.put("freq_an", freq_an);
			prescription_dtlObject2.put("freq_night", freq_night);
			prescription_dtlObject2.put("route", route);
			prescription_dtlObject2.put("duration_no", duration_no);
			prescription_dtlObject2.put("duration_unit", duration_unit);
			prescription_dtlObject2.put("start_date", start_date);
			prescription_dtlObject2.put("sos", sos);
			prescription_dtlObject2.put("af", af);
			prescription_dtlObject2.put("bf", bf);
			prescription_dtlObject2.put("wf", wf);
			prescription_dtlObject2.put("empty_stomach", empty_stomach);
			prescription_dtlObject2.put("before_sleeping", before_sleeping);
			prescription_dtlObject2.put("isactive", isactive);
			prescription_dtlObject2.put("organization_id", organization_id1);
			prescription_dtlObject2.put("site_id", site_id);
			prescription_dtlObject2.put("location_id", location_id);
			prescription_dtlObject2.put("created_user_id", created_user_id1);
			prescription_dtlObject2.put("created_timestamp", created_timestamp1);
			prescription_dtlObject2.put("modified_user_id", modified_user_id2);
			prescription_dtlObject2.put("modified_timestamp", modified_timestamp);
			prescription_dtlObject2.put("prescription_hdr_id", prescription_hdr_id);
			prescription_dtlObject2.put("additional_instruction", additional_instruction);
			prescription_dtlObject2.put("medicine_type", medicine_type);
			prescription_dtlObject2.put("frequency", frequency);
			prescription_dtlObject2.put("dosage_advice", dosage_advice);
			prescription_dtlObject2.put("duration", duration1);
			prescription_dtlObject2.put("drug_type", drug_type);
			prescription_dtlObject2.put("status", status2);
			
			
			prescription_dtlArray.put(prescription_dtlObject2);
			
			prescription_hdr.put("prescription_dtl", prescription_dtlArray);
			patient_visitObject.put("prescription_hdr", prescription_hdr);
			
			
			JSONObject service_order_hdr  = new JSONObject();
			
			service_order_hdr.put("service_order_hdr_date", service_order_hdr_date);
			service_order_hdr.put("service_order_hdr_time", service_order_hdr_time);
			service_order_hdr.put("isactive", isactive);
			service_order_hdr.put("created_user_id", created_user_id1);
			service_order_hdr.put("created_timestamp", created_timestamp1);
			service_order_hdr.put("modified_user_id", modified_user_id);
			service_order_hdr.put("modified_timestamp", modified_timestamp);
			service_order_hdr.put("site_id", site_id);
			service_order_hdr.put("location_id", location_id);
			service_order_hdr.put("patient_id", patient_id1);
			service_order_hdr.put("patient_visit_id", patient_visit_id1);
			service_order_hdr.put("organization_id", organization_id1);
			service_order_hdr.put("clinic_user_id", clinic_user_id1);
						
			JSONArray service_order_dtlArray  = new JSONArray();
			JSONObject service_order_dtlObject  = new JSONObject();
						
			service_order_dtlObject.put("note", note);
			service_order_dtlObject.put("isactive", isactive);
			service_order_dtlObject.put("created_user_id", created_user_id1);
			service_order_dtlObject.put("created_timestamp", created_timestamp1);
			service_order_dtlObject.put("modified_user_id", modified_user_id);
			service_order_dtlObject.put("modified_timestamp", modified_timestamp);
			service_order_dtlObject.put("site_id", site_id);
			service_order_dtlObject.put("location_id", location_id);
			service_order_dtlObject.put("patient_id", patient_id1);
			service_order_dtlObject.put("organization_id", organization_id1);
			service_order_dtlObject.put("service_id", service_id);
			service_order_dtlObject.put("service_order_hdr_id", service_order_hdr_id);
			service_order_dtlObject.put("service_name", service_name);
			
			service_order_dtlArray.put(service_order_dtlObject);
			service_order_hdr.put("service_order_dtl", service_order_dtlArray);
			
			patient_visitObject.put("service_order_hdr", service_order_hdr);
			
			patient_visitArray.put(patient_visitObject);
			
			patient.put("patient_visit", patient_visitArray);
			
			completeRecordObject.put("patient", patient);
			completeRecordArray.put(completeRecordObject);
			
			completeRecord.put("completeRecord", completeRecordArray);
			
			System.out.println("req:"+completeRecord.toString());
			return completeRecord;

		}
		
		public static List<NameValuePair> getPatienSearchAdvance_Param(String token,String username,String orgId,String userID,String StartIndex,String interval) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseAppointment.token, token));
			param.add(new BasicNameValuePair(JsonParseAppointment.userLoginName, username));
			param.add(new BasicNameValuePair(JsonParseAppointment.organisationId, orgId));
			param.add(new BasicNameValuePair(JsonParseAppointment.userId, userID));
			param.add(new BasicNameValuePair(JsonParseAppointment.startIndex, StartIndex));
			param.add(new BasicNameValuePair(JsonParseAppointment.interval, interval));
			return param;

		}
		
		public static JSONObject getJSONPatientSearchAdvance( String  mobileNo, String uhid, String patientName, String dob,String gender,String visit_date_to,String visit_date_from,String age,String addressLine1,String customClinicId) {

			JSONObject updatePatient = new JSONObject();

			//JSONArray patientDetailsArray = new JSONArray();
			JSONObject patientDetailsArray  = new JSONObject();
			patientDetailsArray .put(JsonParseAppointment.mobileNo, mobileNo);
			patientDetailsArray .put(JsonParseAppointment.uhid, uhid);
			patientDetailsArray .put(JsonParseAppointment.patientName, patientName);
			patientDetailsArray .put(JsonParseAppointment.dob, dob);
			patientDetailsArray .put(JsonParseAppointment.gender, gender);
			patientDetailsArray.put("visit_date_to",visit_date_to);
			patientDetailsArray.put("visit_date_from",visit_date_from);
			patientDetailsArray.put("age",age);
			patientDetailsArray.put("addressLine1",addressLine1);
			patientDetailsArray.put("customClinicId", customClinicId);
			
			//accountDetailsArray.put(patientDetailsArray );

			updatePatient.put("patient", patientDetailsArray );
			System.out.println("req:"+updatePatient.toString());
			return updatePatient;

		}
		public static List<NameValuePair> patientPreviousVisitsDetail_Param(String interval,String startIndex,String patientId,String userId,String userLoginName,String token, String organizationId){

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseConsultation.userId, userId));
			param.add(new BasicNameValuePair(JsonParseConsultation.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseConsultation.token, token));
			param.add(new BasicNameValuePair(JsonParseConsultation.organisationId, organizationId));
			param.add(new BasicNameValuePair("interval", interval));
			param.add(new BasicNameValuePair("startIndex",startIndex));
			param.add(new BasicNameValuePair(JsonParseConsultation.patientId,patientId));
			return param;
		}
		
		public static List<NameValuePair> patientPreviousVisitDate_Param(String username,String token,String userId,String clinicUserId,String orgId,String patientId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, username));
			param.add(new BasicNameValuePair(JsonParseLogin.token, token));
			param.add(new BasicNameValuePair(JsonParseLogin.userId,userId));
			param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId,clinicUserId));
			param.add(new BasicNameValuePair(JsonParseLogin.organisationId,orgId));
			param.add(new BasicNameValuePair("patientId",patientId));
			return param;

		}
		
		//patientQuickSearch
		public static List<NameValuePair> patientQuickSearch_Param(String token,String userLoginName,String organisationId,String userId,String startIndex,String interval) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseLogin.token, token));
			param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseLogin.organisationId,organisationId));
			param.add(new BasicNameValuePair(JsonParseLogin.userId,userId));
			param.add(new BasicNameValuePair("startIndex",startIndex));
			param.add(new BasicNameValuePair("interval",interval));
			return param;

		}
		
		public static JSONObject getJSONPatientQuickSearch(String uhid,String patientName,String mobileNo, String customClinicId) {		
			JSONObject patientQuickSearch = new JSONObject();	 
			JSONObject patientQuickSearchArray  = new JSONObject();	 
			patientQuickSearchArray .put("uhid",uhid);
			patientQuickSearchArray .put("patientName", patientName);
			patientQuickSearchArray .put("mobileNo", mobileNo);
			patientQuickSearchArray .put("customClinicId", customClinicId);
			
			patientQuickSearch.put("patient",patientQuickSearchArray);
			System.out.println("req:"+patientQuickSearch.toString());
			return patientQuickSearch;
		}

	

}
