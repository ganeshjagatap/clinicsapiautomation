package com.clinic.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class JsonParseConsultation {
	public static String userLoginName=null;
	public static String userId=null;

	public static String siteId=null;
	public static String token=null;
	public static String user=null;
	public static String role=null;
	public static String locationId=null;
	public static String patientId=null;
	public static String visitId=null;
	public static String currentStatus=null;
	public static String persistAllergies=null;
	public static String persistVitals=null;
	public static String persistSymptoms=null;
	public static String patientAllergy=null;
	public static String patientVital=null;
	public static String patientSymptom=null;
	//patientAllergy
	public static String patientAllergyId = null;
	public static String allergyId = null;
	public static String allergyCimsCode = null;
	public static String allergyName = null;
	public static String operation = null;
	//patientVital
	public static String patientVitalId = null;
	public static String parameter = null;
	public static String parameterValue = null;
	public static String parameterComment = null;
	//patientSymptom
	public static String patientSymptomId = null;
	public static String symptomId = null;
	public static String symptomName = null;
	public static String duration = null;
	public static String otherDetails = null;

	//public static Object patientDiagnosis = null;
	//patientDiagnosis
	public static String patientDiagnosisId = null;
	public static String diagnosisId = null;
	public static String drSpecDiagnosisId = null;
	public static String diagnosisName = null;
	public static String stage = null;
	public static String onsetDate = null;
	public static String status = null;
	public static String closeDate = null;
	public static String onsetVisitId = null;
	public static String immunization = null;
	public static String bcg = null;
	public static String dpt = null;
	public static String tetanus = null;
	public static String immunizationDesc = null;
	public static String familyMedicalHistory = null;
	public static String patientMedicalHistory = null;
	public static String orgId = null;
	public static String appointmentId = null;
	public static String patientVisitId = null;
	public static String patientMobileNo = null;
	public static String patientEmailId = null;
	public static String doctorTitle = null;
	public static String doctorName = null;
	public static String organizationName = null;
	public static String printVitals = null;
	public static String printSymptoms = null;
	public static String printExamination =null;
	public static String printDiagnosis = null;
	public static String printPrescription = null;
	public static String printGenericAdvice = null;
	public static String printFollowUp = null;
	public static String printTests = null;
	public static String refferal = null;
	public static String searchString = null;
	public static String templateId	= null;
	public static String doctorSpecialty = null;
	public static String templateSectionId = null;
	public static String specialtyExam = null;
	public static String templateSectionStatus = null;
	public static String templateItemId = null;
	public static String itemValue = null;
	public static String testOrScanDtlId = null;
	public static String reportId = null;
	public static String reportPath = null;
	public static String currentVisit = null;
	public static String prescriptionHdrId = null;
	public static String genericAdvice = null;
	public static String genericAdviceId = null;
	public static String prescriptionId = null;
	public static String genericDrugCode = null;
	public static String drugCode = null;
	public static String drugName = null;
	public static String frequency = null;
	public static String freqSpecInstruction = null;
	public static String startDate = null;
	public static String dosageAdvice = null;
	public static String drugType = null;
	public static String prescriptionDetailId = null;
	public static String templatePrescriptionId = null;
	public static String templateName = null;
	public static String toUserId = null;
	public static String commentRemark = null;
	public static String followUpAfter = null;
	public static String followUpUnit = null;
	public static String sendSms = null;
	public static String sendEmail = null;
	public static String scope = null;
	public static String serviceOrderHdrId = null;
	public static String serviceOrderDtlId = null;
	public static String serviceId = null;
	public static String serviceName = null;
	public static String note = null;
	public static String serviceType = null;
	public static String deviceId = null;
	public static String deviceName = null;
	public static String prescriptionTemplateName = null;
	public static String route = null;
	public static String frequencyMor = null;
	public static String frequencyAn = null;
	public static String frequencyNight = null;
	public static String dosage = null;
	public static String durationNo = null;
	public static String durationUnit = null;
	public static String sos = null;
	public static String af = null;
	public static String bf = null;
	public static String wf = null;
	public static String emptyStomach = null;
	public static String beforeSleeping = null;
	public static String medicineType = null;
	public static String uhid = null;
	public static String pharmacyId = null;
	public static String patientContactNumber = null;
	public static String digitalSignaturePath = null;
	public static String digitalSignaturePassword = null;
	public static String cartridgeName = null;
	public static String byPassValidationCheckForJob = null;	
	public static String labName = null;
	public static String testDate = null;
	public static String patientInstrumentalId = null;
	public static String parametervalue = null;
	public static String parameterunit = null;
	public static String parameterreference = null;
	public static String parametercomment = null;
	public static String createdUserId = null;
	public static String barcode = null;
	public static String provisionalPdf = null;
	public static String fileType = null;
	public static String doctorId = null;
	public static String testOrscanTemplateName = null;
	public static String notes = null;
	public static String vitalParameterName = null;
	public static String limit = null;
	public static String dateSlotId = null;
	public static String slotId = null;
	public static String appointmentDate = null;
	public static String startTime = null;
	public static String endTime = null;
	public static String clinicUserId = null;
	public static String reasonOfVisit = null;
	public static String locationName = null;
	public static String isDuplicateCheckRequired = null;
	public static String isPatientMappingRequired = null;
	public static String title = null;
	public static String patientName = null;
	public static String lastName = null;
	public static String gender = null;
	public static String bloodGroup = null;
	public static String patientDOB = null;
	public static String age = null;
	public static String cityId = null;
	public static String mobileNo = null;
	public static String emailId = null;
	public static String addressLine1 = null;
	public static String addressLine2 = null;
	public static String country = null;
	public static String state = null;
	public static String city = null;
	public static String zipCode = null;
	public static String patientPhoto = null;
	public static String birthDayWish = null;
	public static String homeMonitoring = null;
	public static String alternate_number = null;
	public static String casualty = null;
	public static String religion = null;
	public static String employment = null;
	public static String mlc = null;
	public static String mlc_number = null;
	public static String mlc_description = null;
	public static String government_id_type = null;
	public static String government_id_number = null;
	public static String martial_status = null;
	public static String clinic_id = null;
	public static String countrycode_mobileNo = null;
	public static String countrycode_alternateNo = null;
	public static String pneumonia = null;
	public static String hepatitis_a = null;
	public static String hepatitis_b = null;
	public static String typhoid = null;
	public static String influenza = null;
	public static String polio = null;
	public static String socialHabitsSmoking = null;
	public static String socialHabitsDrinking = null;
	public static String socialHabitsOthers = null;
	public static String patientMedicalConditionsDiabetes = null;
	public static String patientMedicalConditionsHypertension = null;
	public static String patientMedicalConditionsAsthma = null;
	public static String patientMedicalConditionsHeartDisease = null;
	public static String patientMedicalConditionsOthers = null;
	public static String familyMedicalConditionsDiabetes = null;
	public static String familyMedicalConditionsHypertension = null;
	public static String familyMedicalConditionsAsthma = null;
	public static String familyMedicalConditionsHeartDisease = null;
	public static String familyMedicalConditionsOthers = null;
	public static String medicine =  null;
	public static String comments = null;
	public static String patientDiagnosis = null;
	public static String printMedicalHistory = null;
	public static String prescription = null;
	public static String serviceOrderDtl = null;
	public static String deviceInfo = null;
	public static String printReferral = null;
	public static String organisationId= null;
	public static String medicalHistoryFlag = null;
	public static String tetnus = null;
	public static String immOtherDesc = null;
	public static String currentMedication = null;
	public static String medicalHistory = null;
	public static String patient = null;
	public static String currentSatus = null;
	public static String prescribingDrug = null;
	public static String prescribedDrugs = null;
	public static String patientAllergies = null;
	public static String patientInstrumental = null;
	public static String patientTestList = null;
	public static String patientInstrumentaldatalist = null;
	public static String reasonForVisit = null;
	public static String date = null;
	public static String appointment = null;
	public static String bookedSlotId = null;
	public static String testOrscan = null;
	public static String organizationId=null;
	
	
	
	//api
	
	
	
	
	@Test
	public void jsonParseConsultation(){
		String jsonData = "";
		
		BufferedReader br = null;
		try {
			String line;
			br = new BufferedReader(new FileReader(".//json//consultation.json"));
			while ((line = br.readLine()) != null) {
				jsonData += line + "\n";
				}
			}
		catch (IOException e) {
			e.printStackTrace();
			}
		finally {
			try {
				if (br != null)
					br.close();
				}
			catch (IOException ex) {
				ex.printStackTrace();
				}
			}
		JSONObject obj = new JSONObject(jsonData);
		JSONObject pathsObj=	obj.getJSONObject("paths");
		
		Iterator<?> keys = pathsObj.keys();
		
		while(keys.hasNext()){
			 String t=keys.next().toString();
			 //System.out.println("t"+t);
			 //System.out.println(t.length());
			 //S.add(t);
			 String t1=t.substring(52);
			 //System.out.println("t1"+t1);
			 //S1.add(t1);
			 switch (t1) {
			case "persistVitalsInfoHandler":
				Constants.persistVitalsInfoHandler = t;
				break;
			case "persistPatientDiagnosisInfoHandler"	:
				Constants.persistPatientDiagnosisInfoHandler = t;
				break;
			case "fetchVitalDtlsHandler":
				Constants.fetchVitalDtlsHandler = t;
				break;
			case "fetchPatientMedicalHistoryHandler":
				Constants.fetchPatientMedicalHistoryHandler = t;
				break;
			case "persistPatientMedicalHistoryHandler":
				Constants.persistPatientMedicalHistoryHandler = t;
				break;
			case "createOrUpdatePatientVisitHandler":
				Constants.createOrUpdatePatientVisitHandler = t;
				break;
			case "searchAllergyHandler":
				Constants.searchAllergyHandler = t;
				break;
			case "searchMedicineHandler":
				Constants.searchMedicineHandler = t;
				break;
			case "searchSymptomsHandler":
				Constants.searchSymptomsHandler = t;
				break;
			case "searchExaminationTemplateHandler":
				Constants.searchExaminationTemplateHandler = t;
				break;
			case "fetchExaminationTemplateHandler":
				Constants.fetchExaminationTemplateHandler = t;
				break;
			case "fetchExaminationDtlsHandler":
				Constants.fetchExaminationDtlsHandler = t;
				break;
			case "fetchExaminationTemplateItemsHandler":
				Constants.fetchExaminationTemplateItemsHandler = t;
				break;
			case "persistExaminationInfoHandler":
				Constants.persistExaminationInfoHandler = t;
				break;
			case "searchDiagnosisHandler":
				Constants.searchDiagnosisHandler = t;
				break;
			case "searchFrequencyHandler":
				Constants.searchFrequencyHandler = t;
				break;
			case "fetchPatientReportsHandler":
				Constants.fetchPatientReportsHandler = t;
				break;
			case "deletePatientTestOrScanReportHandler":
				Constants.deletePatientTestOrScanReportHandler = t;
				break;
			case "uploadPatientTestOrScanReportHandler":
				Constants.uploadPatientTestOrScanReportHandler = t;
				break;
			case "generateSummaryHandler":
				Constants.generateSummaryHandler = t;
				break;
			case "persistDiagnosisAndPrescriptionHandler":
				Constants.persistDiagnosisAndPrescriptionHandler = t;
				break;
			case "persistPrescription":
				Constants.persistPrescription = t;
				break;
			case "fetchDiagnosisAndPrescriptionHandler":
				Constants.fetchDiagnosisAndPrescriptionHandler = t;
				break;
			case "fetchDiagnosisHandler":
				Constants.fetchDiagnosisHandler = t;
				break;
			case "fetchPatientPrescriptionDtlsHandler":
				Constants.fetchPatientPrescriptionDtlsHandler = t;
				break;
			case "fetchPrescriptionHandler":
				Constants.fetchPrescriptionHandler = t;
				break;
			case "loadPrescriptionTemplateHandler":
				Constants.loadPrescriptionTemplateHandler = t;
				break;
			case "fetchReferralInfoHandler":
				Constants.fetchReferralInfoHandler = t;
				break;
			case "persistReferralInfoHandler":
				Constants.persistReferralInfoHandler = t;
				break;
			case "fetchFollowupVisitInfoHandler":
				Constants.fetchFollowupVisitInfoHandler = t;
				break;
			case "persistFollowupVisitInfoHandler":
				Constants.persistFollowupVisitInfoHandler = t;
				break;
			case "fetchPatientTestsOrScansInfoHandler":
				Constants.fetchPatientTestsOrScansInfoHandler = t;
				break;
			case "persistDoctorSpecifiedTestOrScanInfoHandler":
				Constants.persistDoctorSpecifiedTestOrScanInfoHandler = t;
				break;
			case "searchTestOrScanHandler":
				Constants.searchTestOrScanHandler = t;
				break;
			case "persistHomeMonitoringInfoHandler":
				Constants.persistHomeMonitoringInfoHandler = t;
				break;
			case "fetchHomeMonitoringInfoHandler":
				Constants.fetchHomeMonitoringInfoHandler = t;
				break;
			case "savePrescriptionAsTemplateHandler":
				Constants.savePrescriptionAsTemplateHandler = t;
				break;
			case "persistPrescriptionHandler":
				Constants.persistPrescriptionHandler = t;
				break;
			case "generateSummaryPDFHandler":
				Constants.generateSummaryPDFHandler = t;
				break;
			case "generateModuleWiseSummaryPDFHandler":
				Constants.generateModuleWiseSummaryPDFHandler = t;
				break;
			case "generateConsultingPrescriptionPdfHandler":
				Constants.generateConsultingPrescriptionPdfHandler = t;
				break;
			case "generatePrescriptionPDFHandler":
				Constants.generatePrescriptionPDFHandler = t;
				break;
			case "fetchPharmacyListHandler":
				Constants.fetchPharmacyListHandler = t;
				break;
			case "checkCimsInteractionsHandler":
				Constants.checkCimsInteractionsHandler = t;
				break;
			case "updatePrescriptionAsTemplateHandler":
				Constants.updatePrescriptionAsTemplateHandler = t;
				break;
			case "deletePrescriptionAsTemplateHandler":
				Constants.deletePrescriptionAsTemplateHandler = t;
				break;
			case "persistPatientInstrumentalInfoHandler":
				Constants.persistPatientInstrumentalInfoHandler = t;
				break;
			case "fetchPatientInstrumentalInfoHandler":
				Constants.fetchPatientInstrumentalInfoHandler = t;
				break;
			case "persistPatientProvisionalInfoHandler":
				Constants.persistPatientProvisionalInfoHandler = t;
				break;
			case "savePatientTestListHandler":
				Constants.savePatientTestListHandler = t;
				break;
			case "persistPatientInstrumentalListInfoHandler":
				Constants.persistPatientInstrumentalListInfoHandler = t;
				break;
			case "fetchOrLoadAllTemplatesForUser":
				Constants.fetchOrLoadAllTemplatesForUser = t;
				break;
			case "savePrescriptionTemplate":
				Constants.savePrescriptionTemplate = t;
				break;
			case "updatePrescriptionTemplate":
				Constants.updatePrescriptionTemplate = t;
				break;
			case "fetchPatientPrescription":
				Constants.fetchPatientPrescription = t;
				break;
			case "printConsultationSheet":
				Constants.printConsultationSheet = t;
				break;
			case "persistTestOrScanTemplateMaster":
				Constants.persistTestOrScanTemplateMaster = t;
				break;
			case "updateTestOrScanTemplate":
				Constants.updateTestOrScanTemplate = t;
				break;
			case "fetchOrLoadTestOrScanTemplatesForUser":
				Constants.fetchOrLoadTestOrScanTemplatesForUser = t;
				break;
			case "fetchVitalsForPreviousVisits":
				Constants.fetchVitalsForPreviousVisits = t;
				break;
			case "openConsultationDetails":
				Constants.openConsultationDetails = t;
				break;
			case "bookConsultation":
				Constants.bookConsultation = t;
				break;
			case "registrationBookConsultation":
				Constants.registrationBookConsultation = t;
				break;
			default:
				break;
			}
			 
		
		JSONObject definitionsObj=	obj.getJSONObject("definitions");
		
		//---------------------------------persistVitalsInfoHandler---------------------------------------//
		
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject vitalInfoObj=	definitionsObj.getJSONObject("persistVitalsInfoHandler");
		JSONArray	vitalInfoArr=	vitalInfoObj.getJSONArray("required");
		userLoginName=vitalInfoArr.getString(0);
		userId=vitalInfoArr.getString(1);
		token=vitalInfoArr.getString(2);
		role=vitalInfoArr.getString(3);
		organizationId=vitalInfoArr.getString(4);
		siteId=vitalInfoArr.getString(5);
		locationId=vitalInfoArr.getString(6);
		patientId=vitalInfoArr.getString(7);
		visitId=vitalInfoArr.getString(8);
		currentStatus=vitalInfoArr.getString(9);
		persistAllergies=vitalInfoArr.getString(10);
		persistVitals=vitalInfoArr.getString(11);
		persistSymptoms=vitalInfoArr.getString(12);
		patientAllergy=vitalInfoArr.getString(13);
		patientVital=vitalInfoArr.getString(14);
		patientSymptom=vitalInfoArr.getString(15);
		
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//--------------------------------- ---------------------------------------//
		
		/*System.out.println("File Content: \n" + jsonData);
		JSONObject patientAllergyObj=	definitionsObj.getJSONObject(" ");
		JSONArray	patientAllergyArr=	patientAllergyObj.getJSONArray("required");
		patientAllergyId = patientAllergyArr.getString(0);
		allergyId = patientAllergyArr.getString(1);
		allergyCimsCode = patientAllergyArr.getString(2);
		allergyName = patientAllergyArr.getString(3);
		operation = patientAllergyArr.getString(4);
		*/
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------patientVital---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject patientVitalObj=	definitionsObj.getJSONObject("patientVital");
		JSONArray	patientVitalArr=	patientVitalObj.getJSONArray("required");
		patientVitalId=patientVitalArr.getString(0);
		parameter=patientVitalArr.getString(1);
		parameterValue=patientVitalArr.getString(2);
		parameterComment=patientVitalArr.getString(3);
		operation=patientVitalArr.getString(4);
		
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------patientSymptom---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject patientSymptomObj=	definitionsObj.getJSONObject("patientSymptom");
		JSONArray	patientSymptomArr=	patientSymptomObj.getJSONArray("required");
		patientSymptomId=patientSymptomArr.getString(0);
		symptomId=patientSymptomArr.getString(1);
		symptomName=patientSymptomArr.getString(2);
		duration=patientSymptomArr.getString(3);
		otherDetails=patientSymptomArr.getString(4);
	
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------persistPatientDiagnosisInfoHandler---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject persistPatientDiagnosisInfoHandlerObj=	definitionsObj.getJSONObject("persistPatientDiagnosisInfoHandler");
		JSONArray	persistPatientDiagnosisInfoHandlerArr=	persistPatientDiagnosisInfoHandlerObj.getJSONArray("required");
		patientDiagnosis=persistPatientDiagnosisInfoHandlerArr.getString(8);
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------patientDiagnosis---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject patientDiagnosisObj=	definitionsObj.getJSONObject("patientDiagnosis");
		JSONArray	patientDiagnosisArr=	patientDiagnosisObj.getJSONArray("required");
		patientDiagnosisId=patientDiagnosisArr.getString(0);
		diagnosisId=patientDiagnosisArr.getString(1);
		drSpecDiagnosisId=patientDiagnosisArr.getString(2);
		diagnosisName=patientDiagnosisArr.getString(3);
		stage=patientDiagnosisArr.getString(4);
		onsetDate=patientDiagnosisArr.getString(5);
		status=patientDiagnosisArr.getString(6);
		closeDate=patientDiagnosisArr.getString(7);
		onsetVisitId=patientDiagnosisArr.getString(9);
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------fetchVitalDtlsHandler---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject fetchVitalDtlsHandlerObj=	definitionsObj.getJSONObject("fetchVitalDtlsHandler");
		JSONArray	fetchVitalDtlsHandlerArr=	fetchVitalDtlsHandlerObj.getJSONArray("required");

		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------fetchPatientMedicalHistoryHandler---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject fetchPatientMedicalHistoryHandlerObj=	definitionsObj.getJSONObject("fetchPatientMedicalHistoryHandler");
		JSONArray	fetchPatientMedicalHistoryHandlerArr=	fetchPatientMedicalHistoryHandlerObj.getJSONArray("required");
		

		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------persistPatientMedicalHistoryHandler---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject persistPatientMedicalHistoryHandlerObj=	definitionsObj.getJSONObject("persistPatientMedicalHistoryHandler");
		JSONArray	persistPatientMedicalHistoryHandlerArr=	persistPatientMedicalHistoryHandlerObj.getJSONArray("required");
		immunization = persistPatientMedicalHistoryHandlerArr.getString(3);
		bcg = persistPatientMedicalHistoryHandlerArr.getString(4);
		dpt = persistPatientMedicalHistoryHandlerArr.getString(5);
		tetanus = persistPatientMedicalHistoryHandlerArr.getString(6);
		immunizationDesc = persistPatientMedicalHistoryHandlerArr.getString(7);
		familyMedicalHistory = persistPatientMedicalHistoryHandlerArr.getString(8);
		patientMedicalHistory = persistPatientMedicalHistoryHandlerArr.getString(9);
		orgId = persistPatientMedicalHistoryHandlerArr.getString(10);
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------createOrUpdatePatientVisitHandler---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject createOrUpdatePatientVisitHandlerObj=	definitionsObj.getJSONObject("createOrUpdatePatientVisitHandler");
		JSONArray	createOrUpdatePatientVisitHandlerArr=	createOrUpdatePatientVisitHandlerObj.getJSONArray("required");
		appointmentId = createOrUpdatePatientVisitHandlerArr.getString(7);
		patientVisitId = createOrUpdatePatientVisitHandlerArr.getString(10);
		patientMobileNo = createOrUpdatePatientVisitHandlerArr.getString(12);
		patientEmailId = createOrUpdatePatientVisitHandlerArr.getString(13);
		doctorTitle = createOrUpdatePatientVisitHandlerArr.getString(14);
		doctorName = createOrUpdatePatientVisitHandlerArr.getString(15);
		organizationName = createOrUpdatePatientVisitHandlerArr.getString(16);
		printVitals = createOrUpdatePatientVisitHandlerArr.getString(17);
		printSymptoms = createOrUpdatePatientVisitHandlerArr.getString(18);
		printExamination = createOrUpdatePatientVisitHandlerArr.getString(19);
		printDiagnosis = createOrUpdatePatientVisitHandlerArr.getString(20);
		printPrescription = createOrUpdatePatientVisitHandlerArr.getString(21);
		printGenericAdvice = createOrUpdatePatientVisitHandlerArr.getString(22);
		printFollowUp = createOrUpdatePatientVisitHandlerArr.getString(23);
		printTests = createOrUpdatePatientVisitHandlerArr.getString(24);
		refferal = createOrUpdatePatientVisitHandlerArr.getString(25);

		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------searchAllergyHandler---------------------------------------//
		
				//System.out.println("File Content: \n" + jsonData);
				JSONObject searchAllergyHandlerObj=	definitionsObj.getJSONObject("searchAllergyHandler");
				JSONArray	searchAllergyHandlerArr=	searchAllergyHandlerObj.getJSONArray("required");
				searchString = searchAllergyHandlerArr.getString(5);
				
	  /////////////////////////////////////////////////////////////////////////////////
				
	  //---------------------------------searchMedicineHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject searchMedicineHandlerObj=	definitionsObj.getJSONObject("searchMedicineHandler");
				JSONArray	searchMedicineHandlerArr=	searchMedicineHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
		
	 //---------------------------------searchSymptomsHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject searchSymptomsHandlerObj=	definitionsObj.getJSONObject("searchSymptomsHandler");
				JSONArray	searchSymptomsHandlerArr=	searchSymptomsHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
		
	 //---------------------------------searchExaminationTemplateHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject searchExaminationTemplateHandlerObj =	definitionsObj.getJSONObject("searchExaminationTemplateHandler");
				JSONArray	searchExaminationTemplateHandlerArr =searchExaminationTemplateHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchExaminationTemplateHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchExaminationTemplateHandlerObj =	definitionsObj.getJSONObject("fetchExaminationTemplateHandler");
				JSONArray	fetchExaminationTemplateHandlerArr =fetchExaminationTemplateHandlerObj.getJSONArray("required");
				
				templateId = fetchExaminationTemplateHandlerArr.getString(3);
				
	  /////////////////////////////////////////////////////////////////////////////////
		
	 //---------------------------------fetchExaminationDtlsHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchExaminationDtlsHandlerObj =	definitionsObj.getJSONObject("fetchExaminationDtlsHandler");
				JSONArray	fetchExaminationDtlsHandlerArr =fetchExaminationDtlsHandlerObj.getJSONArray("required");
				doctorSpecialty = fetchExaminationDtlsHandlerArr.getString(6);
				
				
	  /////////////////////////////////////////////////////////////////////////////////
		
	 //---------------------------------fetchExaminationTemplateItemsHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchExaminationTemplateItemsHandlerObj =	definitionsObj.getJSONObject("fetchExaminationTemplateItemsHandler");
				JSONArray	fetchExaminationTemplateItemsHandlerArr =  fetchExaminationTemplateItemsHandlerObj.getJSONArray("required");
				templateSectionId = fetchExaminationTemplateItemsHandlerArr.getString(4);

				
	  /////////////////////////////////////////////////////////////////////////////////
		
	 //---------------------------------persistExaminationInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistExaminationInfoHandlerObj =	definitionsObj.getJSONObject("persistExaminationInfoHandler");
				JSONArray	persistExaminationInfoHandlerArr =  persistExaminationInfoHandlerObj.getJSONArray("required");
				specialtyExam = persistExaminationInfoHandlerArr.getString(9);
				
	  /////////////////////////////////////////////////////////////////////////////////
				
	 //---------------------------------specialtyExam---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject specialtyExamObj =	definitionsObj.getJSONObject("specialtyExam");
				JSONArray	specialtyExamArr = specialtyExamObj.getJSONArray("required");
				templateSectionStatus = specialtyExamArr.getString(1);
				templateItemId = specialtyExamArr.getString(2);
				itemValue = specialtyExamArr.getString(3);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------searchDiagnosisHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject searchDiagnosisHandlerObj =	definitionsObj.getJSONObject("searchDiagnosisHandler");
				JSONArray	searchDiagnosisHandlerArr = searchDiagnosisHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------searchFrequencyHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject searchFrequencyHandlerObj =	definitionsObj.getJSONObject("searchFrequencyHandler");
				JSONArray	searchFrequencyHandlerArr = searchFrequencyHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchPatientReportsHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchPatientReportsHandlerObj =	definitionsObj.getJSONObject("fetchPatientReportsHandler");
				JSONArray	fetchPatientReportsHandlerArr = fetchPatientReportsHandlerObj.getJSONArray("required");
				testOrScanDtlId = fetchPatientReportsHandlerArr.getString(7);
				
	  /////////////////////////////////////////////////////////////////////////////////
				
	 //---------------------------------deletePatientTestOrScanReportHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject deletePatientTestOrScanReportHandlerObj =	definitionsObj.getJSONObject("deletePatientTestOrScanReportHandler");
				JSONArray	deletePatientTestOrScanReportHandlerArr = deletePatientTestOrScanReportHandlerObj.getJSONArray("required");
				reportId = deletePatientTestOrScanReportHandlerArr.getString(8);
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------uploadPatientTestOrScanReportHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject uploadPatientTestOrScanReportHandlerObj =	definitionsObj.getJSONObject("uploadPatientTestOrScanReportHandler");
				JSONArray	uploadPatientTestOrScanReportHandlerArr = uploadPatientTestOrScanReportHandlerObj.getJSONArray("required");
				reportPath = uploadPatientTestOrScanReportHandlerArr.getString(8);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------generateSummaryHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject generateSummaryHandlerObj =	definitionsObj.getJSONObject("generateSummaryHandler");
				JSONArray	generateSummaryHandlerArr = generateSummaryHandlerObj.getJSONArray("required");
				currentVisit = generateSummaryHandlerArr.getString(8);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------persistDiagnosisAndPrescriptionHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistDiagnosisAndPrescriptionHandlerObj =	definitionsObj.getJSONObject("persistDiagnosisAndPrescriptionHandler");
				JSONArray	persistDiagnosisAndPrescriptionHandlerArr = persistDiagnosisAndPrescriptionHandlerObj.getJSONArray("required");
				prescriptionHdrId = persistDiagnosisAndPrescriptionHandlerArr.getString(10);
				prescription = persistDiagnosisAndPrescriptionHandlerArr.getString(11);
				genericAdvice = persistDiagnosisAndPrescriptionHandlerArr.getString(12);
				genericAdviceId= persistDiagnosisAndPrescriptionHandlerArr.getString(13);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------prescription---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject prescriptionObj =	definitionsObj.getJSONObject("prescription");
				JSONArray	prescriptionArr = prescriptionObj.getJSONArray("required");
				prescriptionId = prescriptionArr.getString(0);
				genericDrugCode = prescriptionArr.getString(1);
				drugCode = prescriptionArr.getString(2);
				drugName = prescriptionArr.getString(3);
				frequency = prescriptionArr.getString(4);
				startDate = prescriptionArr.getString(6);
				freqSpecInstruction = prescriptionArr.getString(7);
				dosageAdvice  = prescriptionArr.getString(9);
				drugType = prescriptionArr.getString(10);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------persistPrescription---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistPrescriptionObj =	definitionsObj.getJSONObject("persistPrescription");
				JSONArray	persistPrescriptionArr = persistPrescriptionObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchDiagnosisAndPrescriptionHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchDiagnosisAndPrescriptionHandlerObj =	definitionsObj.getJSONObject("fetchDiagnosisAndPrescriptionHandler");
				JSONArray	fetchDiagnosisAndPrescriptionHandlerArr = fetchDiagnosisAndPrescriptionHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchPatientPrescriptionDtlsHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchPatientPrescriptionDtlsHandlerObj =	definitionsObj.getJSONObject("fetchPatientPrescriptionDtlsHandler");
				JSONArray	fetchPatientPrescriptionDtlsHandlerArr = fetchPatientPrescriptionDtlsHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
     //---------------------------------fetchPrescriptionHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchPrescriptionHandlerObj =	definitionsObj.getJSONObject("fetchPrescriptionHandler");
				JSONArray	fetchPrescriptionHandlerArr = fetchPrescriptionHandlerObj.getJSONArray("required");
				prescriptionDetailId = fetchPrescriptionHandlerArr.getString(4);
				templatePrescriptionId = fetchPrescriptionHandlerArr.getString(5);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------loadPrescriptionTemplateHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject loadPrescriptionTemplateHandlerObj =	definitionsObj.getJSONObject("loadPrescriptionTemplateHandler");
				JSONArray	loadPrescriptionTemplateHandlerArr = loadPrescriptionTemplateHandlerObj.getJSONArray("required");
				templateName = loadPrescriptionTemplateHandlerArr.getString(3);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchReferralInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchReferralInfoHandlerObj =	definitionsObj.getJSONObject("fetchReferralInfoHandler");
				JSONArray	fetchReferralInfoHandlerArr = fetchReferralInfoHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	//---------------------------------persistReferralInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistReferralInfoHandlerObj =	definitionsObj.getJSONObject("persistReferralInfoHandler");
				JSONArray	persistReferralInfoHandlerArr = persistReferralInfoHandlerObj.getJSONArray("required");
				toUserId = persistReferralInfoHandlerArr.getString(3);
				commentRemark= persistReferralInfoHandlerArr.getString(9);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchFollowupVisitInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchFollowupVisitInfoHandlerObj =	definitionsObj.getJSONObject("fetchFollowupVisitInfoHandler");
				JSONArray	fetchFollowupVisitInfoHandlerArr = fetchFollowupVisitInfoHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------persistFollowupVisitInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistFollowupVisitInfoHandlerObj =	definitionsObj.getJSONObject("persistFollowupVisitInfoHandler");
				JSONArray	persistFollowupVisitInfoHandlerArr = persistFollowupVisitInfoHandlerObj.getJSONArray("required");
				followUpAfter = persistFollowupVisitInfoHandlerArr.getString(3);
				followUpUnit = persistFollowupVisitInfoHandlerArr.getString(4);
				sendSms = persistFollowupVisitInfoHandlerArr.getString(5);
				sendEmail = persistFollowupVisitInfoHandlerArr.getString(6);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchPatientTestsOrScansInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchPatientTestsOrScansInfoHandlerObj =	definitionsObj.getJSONObject("fetchPatientTestsOrScansInfoHandler");
				JSONArray	fetchPatientTestsOrScansInfoHandlerArr = fetchPatientTestsOrScansInfoHandlerObj.getJSONArray("required");
				scope = fetchPatientTestsOrScansInfoHandlerArr.getString(8);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------persistDoctorSpecifiedTestOrScanInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistDoctorSpecifiedTestOrScanInfoHandlerObj =	definitionsObj.getJSONObject("persistDoctorSpecifiedTestOrScanInfoHandler");
				JSONArray	persistDoctorSpecifiedTestOrScanInfoHandlerArr = persistDoctorSpecifiedTestOrScanInfoHandlerObj.getJSONArray("required");
				serviceOrderHdrId = persistDoctorSpecifiedTestOrScanInfoHandlerArr.getString(8);
				serviceOrderDtl = persistDoctorSpecifiedTestOrScanInfoHandlerArr.getString(9);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------serviceOrderDtl---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject serviceOrderDtlObj =	definitionsObj.getJSONObject("serviceOrderDtl");
				JSONArray	serviceOrderDtlArr = serviceOrderDtlObj.getJSONArray("required");
				serviceOrderDtlId = serviceOrderDtlArr.getString(0);
				serviceId = serviceOrderDtlArr.getString(1);
				serviceName = serviceOrderDtlArr.getString(2);
				note = serviceOrderDtlArr.getString(3);
				
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------searchTestOrScanHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject searchTestOrScanHandlerObj =	definitionsObj.getJSONObject("searchTestOrScanHandler");
				JSONArray	searchTestOrScanHandlerArr = searchTestOrScanHandlerObj.getJSONArray("required");
				
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------persistHomeMonitoringInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistHomeMonitoringInfoHandlerObj =	definitionsObj.getJSONObject("persistHomeMonitoringInfoHandler");
				JSONArray	persistHomeMonitoringInfoHandlerArr = persistHomeMonitoringInfoHandlerObj.getJSONArray("required");
				deviceInfo = persistHomeMonitoringInfoHandlerArr.getString(8);
				serviceType = persistHomeMonitoringInfoHandlerArr.getString(9);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------deviceInfo---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject deviceInfoObj =	definitionsObj.getJSONObject("deviceInfo");
				JSONArray	deviceInfoArr = deviceInfoObj.getJSONArray("required");
				deviceId = deviceInfoArr.getString(0);
				deviceName = deviceInfoArr.getString(1);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
 //---------------------------------fetchHomeMonitoringInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchHomeMonitoringInfoHandlerObj =	definitionsObj.getJSONObject("fetchHomeMonitoringInfoHandler");
				JSONArray	fetchHomeMonitoringInfoHandlerArr = fetchHomeMonitoringInfoHandlerObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------savePrescriptionAsTemplateHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject savePrescriptionAsTemplateHandlerObj =	definitionsObj.getJSONObject("savePrescriptionAsTemplateHandler");
				JSONArray	savePrescriptionAsTemplateHandlerArr = savePrescriptionAsTemplateHandlerObj.getJSONArray("required");
				prescriptionTemplateName = savePrescriptionAsTemplateHandlerArr.getString(4);
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------persistPrescriptionHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistPrescriptionHandlerObj =	definitionsObj.getJSONObject("persistPrescriptionHandler");
				JSONArray	persistPrescriptionHandlerArr = persistPrescriptionHandlerObj.getJSONArray("required");
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------prescriptionPersist---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject prescriptionPersistObj =	definitionsObj.getJSONObject("prescriptionPersist");
				JSONArray	prescriptionPersistArr = prescriptionPersistObj.getJSONArray("required");
				route = prescriptionPersistArr.getString(4);
				frequencyMor = prescriptionPersistArr.getString(5);
				frequencyAn = prescriptionPersistArr.getString(6);
				frequencyNight = prescriptionPersistArr.getString(7);
				dosage = prescriptionPersistArr.getString(8);
				durationNo = prescriptionPersistArr.getString(9);
				durationUnit = prescriptionPersistArr.getString(10);
				sos = prescriptionPersistArr.getString(12);
				af = prescriptionPersistArr.getString(13);
				bf = prescriptionPersistArr.getString(14);
				wf = prescriptionPersistArr.getString(15);
				emptyStomach = prescriptionPersistArr.getString(16);
				beforeSleeping = prescriptionPersistArr.getString(17);
				medicineType = prescriptionPersistArr.getString(19);
											
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------generateSummaryPDFHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject generateSummaryPDFHandlerObj =	definitionsObj.getJSONObject("generateSummaryPDFHandler");
				JSONArray	generateSummaryPDFHandlerArr = generateSummaryPDFHandlerObj.getJSONArray("required");
				uhid = generateSummaryPDFHandlerArr.getString(7);
				pharmacyId = generateSummaryPDFHandlerArr.getString(10);
				patientContactNumber = generateSummaryPDFHandlerArr.getString(12);
				digitalSignaturePath = generateSummaryPDFHandlerArr.getString(13);
				digitalSignaturePassword = generateSummaryPDFHandlerArr.getString(14);
									
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------generateModuleWiseSummaryPDFHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject generateModuleWiseSummaryPDFHandlerObj =	definitionsObj.getJSONObject("generateModuleWiseSummaryPDFHandler");
				JSONArray	generateModuleWiseSummaryPDFHandlerArr = generateModuleWiseSummaryPDFHandlerObj.getJSONArray("required");
				printReferral = generateModuleWiseSummaryPDFHandlerArr.getString(18);
				printMedicalHistory = generateModuleWiseSummaryPDFHandlerArr.getString(19);
											
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------generateConsultingPrescriptionPdfHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject generateConsultingPrescriptionPdfHandlerObj =	definitionsObj.getJSONObject("generateConsultingPrescriptionPdfHandler");
				JSONArray	generateConsultingPrescriptionPdfHandlerArr = generateConsultingPrescriptionPdfHandlerObj.getJSONArray("required");
				
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------generatePrescriptionPDFHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject generatePrescriptionPDFHandlerObj =	definitionsObj.getJSONObject("generatePrescriptionPDFHandler");
				JSONArray	generatePrescriptionPDFHandlerArr = generatePrescriptionPDFHandlerObj.getJSONArray("required");
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchPharmacyListHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchPharmacyListHandlerObj =	definitionsObj.getJSONObject("fetchPharmacyListHandler");
				JSONArray	fetchPharmacyListHandlerArr = fetchPharmacyListHandlerObj.getJSONArray("required");
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------checkCimsInteractionsHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject checkCimsInteractionsHandlerObj =	definitionsObj.getJSONObject("checkCimsInteractionsHandler");
				JSONArray	checkCimsInteractionsHandlerArr = checkCimsInteractionsHandlerObj.getJSONArray("required");
				
				prescribingDrug = checkCimsInteractionsHandlerArr.getString(4);
				prescribedDrugs = checkCimsInteractionsHandlerArr.getString(5);
				patientAllergies = checkCimsInteractionsHandlerArr.getString(6);
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------prescribingDrug---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject prescribingDrugObj =	definitionsObj.getJSONObject("prescribingDrug");
				JSONArray	prescribingDrugArr = prescribingDrugObj.getJSONArray("required");
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------prescribedDrugs---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject prescribedDrugsObj =	definitionsObj.getJSONObject("prescribedDrugs");
				JSONArray	prescribedDrugsArr = prescribedDrugsObj.getJSONArray("required");
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------patientAllergies---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject patientAllergiesObj =	definitionsObj.getJSONObject("patientAllergies");
				JSONArray	patientAllergiesArr = patientAllergiesObj.getJSONArray("required");
				allergyCimsCode = patientAllergiesArr.getString(0);
				allergyName = patientAllergiesArr.getString(1);
							
	  /////////////////////////////////////////////////////////////////////////////////

	 //---------------------------------updatePrescriptionAsTemplateHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject updatePrescriptionAsTemplateHandlerObj =	definitionsObj.getJSONObject("updatePrescriptionAsTemplateHandler");
				JSONArray	updatePrescriptionAsTemplateHandlerArr = updatePrescriptionAsTemplateHandlerObj.getJSONArray("required");
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------updatePrescription---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject updatePrescriptionObj =	definitionsObj.getJSONObject("updatePrescription");
				JSONArray	updatePrescriptionArr = updatePrescriptionObj.getJSONArray("required");
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------deletePrescriptionAsTemplateHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject deletePrescriptionAsTemplateHandlerObj =	definitionsObj.getJSONObject("deletePrescriptionAsTemplateHandler");
				JSONArray	deletePrescriptionAsTemplateHandlerArr = deletePrescriptionAsTemplateHandlerObj.getJSONArray("required");
				
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------persistPatientInstrumentalInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistPatientInstrumentalInfoHandlerObj =	definitionsObj.getJSONObject("persistPatientInstrumentalInfoHandler");
				JSONArray	persistPatientInstrumentalInfoHandlerArr = persistPatientInstrumentalInfoHandlerObj.getJSONArray("required");
				cartridgeName = persistPatientInstrumentalInfoHandlerArr.getString(5);
				byPassValidationCheckForJob = persistPatientInstrumentalInfoHandlerArr.getString(6);
				labName = persistPatientInstrumentalInfoHandlerArr.getString(7);
				testDate = persistPatientInstrumentalInfoHandlerArr.getString(8);
				patientInstrumental = persistPatientInstrumentalInfoHandlerArr.getString(9);
							
	  /////////////////////////////////////////////////////////////////////////////////

	 //---------------------------------patientInstrumental---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject patientInstrumentalObj =	definitionsObj.getJSONObject("patientInstrumental");
				JSONArray	patientInstrumentalArr = patientInstrumentalObj.getJSONArray("required");
				patientInstrumentalId = patientInstrumentalArr.getString(0);
				parametervalue = patientInstrumentalArr.getString(2);
				parameterunit = patientInstrumentalArr.getString(3);
				parameterreference = patientInstrumentalArr.getString(4);
				parametercomment= patientInstrumentalArr.getString(5);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchPatientInstrumentalInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchPatientInstrumentalInfoHandlerObj =	definitionsObj.getJSONObject("fetchPatientInstrumentalInfoHandler");
				JSONArray	fetchPatientInstrumentalInfoHandlerArr = fetchPatientInstrumentalInfoHandlerObj.getJSONArray("required");
				createdUserId = fetchPatientInstrumentalInfoHandlerArr.getString(0);
				 
				
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------persistPatientProvisionalInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistPatientProvisionalInfoHandlerObj =	definitionsObj.getJSONObject("persistPatientProvisionalInfoHandler");
				JSONArray	persistPatientProvisionalInfoHandlerArr = persistPatientProvisionalInfoHandlerObj.getJSONArray("required");
				barcode = persistPatientProvisionalInfoHandlerArr.getString(0);
				provisionalPdf = persistPatientProvisionalInfoHandlerArr.getString(1);
				fileType = persistPatientProvisionalInfoHandlerArr.getString(2);
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------savePatientTestListHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject savePatientTestListHandlerObj =	definitionsObj.getJSONObject("savePatientTestListHandler");
				JSONArray	savePatientTestListHandlerArr = savePatientTestListHandlerObj.getJSONArray("required");
				patientTestList = savePatientTestListHandlerArr.getString(8);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------patientTestList---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject patientTestListObj =	definitionsObj.getJSONObject("patientTestList");
				JSONArray	patientTestListArr = patientTestListObj.getJSONArray("required");
							
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------persistPatientInstrumentalListInfoHandler---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistPatientInstrumentalListInfoHandlerObj =	definitionsObj.getJSONObject("persistPatientInstrumentalListInfoHandler");
				JSONArray	persistPatientInstrumentalListInfoHandlerArr = persistPatientInstrumentalListInfoHandlerObj.getJSONArray("required");
				patientInstrumentaldatalist = persistPatientInstrumentalListInfoHandlerArr.getString(1);
				
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------patientInstrumentaldatalist---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject patientInstrumentaldatalistObj =	definitionsObj.getJSONObject("patientInstrumentaldatalist");
				JSONArray	patientInstrumentaldatalistArr = patientInstrumentaldatalistObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------patientInstrumentaldata---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject patientInstrumentaldataObj =	definitionsObj.getJSONObject("patientInstrumentaldata");
				JSONArray	patientInstrumentaldataArr = patientInstrumentaldataObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchOrLoadAllTemplatesForUser---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchOrLoadAllTemplatesForUserObj =	definitionsObj.getJSONObject("fetchOrLoadAllTemplatesForUser");
				JSONArray	fetchOrLoadAllTemplatesForUserArr = fetchOrLoadAllTemplatesForUserObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------savePrescriptionTemplate---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject savePrescriptionTemplateObj =	definitionsObj.getJSONObject("savePrescriptionTemplate");
				JSONArray	savePrescriptionTemplateArr = savePrescriptionTemplateObj.getJSONArray("required");
				
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------fetchPatientPrescription---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchPatientPrescriptionObj =	definitionsObj.getJSONObject("fetchPatientPrescription");
				JSONArray	fetchPatientPrescriptionArr = fetchPatientPrescriptionObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------printConsultationSheet---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject printConsultationSheetObj =	definitionsObj.getJSONObject("printConsultationSheet");
				JSONArray	printConsultationSheetArr = printConsultationSheetObj.getJSONArray("required");
				doctorId = printConsultationSheetArr.getString(6);
				currentSatus = printConsultationSheetArr.getString(9);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
     //---------------------------------persistTestOrScanTemplateMaster---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject persistTestOrScanTemplateMasterObj =	definitionsObj.getJSONObject("persistTestOrScanTemplateMaster");
				JSONArray	persistTestOrScanTemplateMasterArr = persistTestOrScanTemplateMasterObj.getJSONArray("required");
				testOrscanTemplateName = persistTestOrScanTemplateMasterArr.getString(4);
				testOrscan = persistTestOrScanTemplateMasterArr.getString(5);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	 //---------------------------------testOrscan---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject testOrscanObj =	definitionsObj.getJSONObject("testOrscan");
				JSONArray	testOrscanArr = testOrscanObj.getJSONArray("required");
				notes = testOrscanArr.getString(2);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------fetchOrLoadTestOrScanTemplatesForUser---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchOrLoadTestOrScanTemplatesForUserObj =	definitionsObj.getJSONObject("fetchOrLoadTestOrScanTemplatesForUser");
				JSONArray	fetchOrLoadTestOrScanTemplatesForUserArr = fetchOrLoadTestOrScanTemplatesForUserObj.getJSONArray("required");
								
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------fetchVitalsForPreviousVisits---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject fetchVitalsForPreviousVisitsObj =	definitionsObj.getJSONObject("fetchVitalsForPreviousVisits");
				JSONArray	fetchVitalsForPreviousVisitsArr = fetchVitalsForPreviousVisitsObj.getJSONArray("required");
				vitalParameterName = fetchVitalsForPreviousVisitsArr.getString(5);
				limit = fetchVitalsForPreviousVisitsArr.getString(6);
				
	  /////////////////////////////////////////////////////////////////////////////////
	
	  //---------------------------------openConsultationDetails---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject openConsultationDetailsObj =	definitionsObj.getJSONObject("openConsultationDetails");
				JSONArray	openConsultationDetailsArr = openConsultationDetailsObj.getJSONArray("required");
				
	  /////////////////////////////////////////////////////////////////////////////////

	  //---------------------------------bookConsultation---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject bookConsultationObj =	definitionsObj.getJSONObject("bookConsultation");
				JSONArray	bookConsultationArr =bookConsultationObj.getJSONArray("required");
				organisationId = bookConsultationArr.getString(3);
				appointment = bookConsultationArr.getString(4);
				
	  /////////////////////////////////////////////////////////////////////////////////

     //---------------------------------appointment---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject appointmentObj =	definitionsObj.getJSONObject("appointment");
				JSONArray	appointmentArr =appointmentObj.getJSONArray("required");
				dateSlotId = appointmentArr.getString(1);
				slotId = appointmentArr.getString(2);
				appointmentDate = appointmentArr.getString(3);
				startTime = appointmentArr.getString(4);
				endTime = appointmentArr.getString(5);
				clinicUserId = appointmentArr.getString(8);
				reasonOfVisit = appointmentArr.getString(9);
				locationName = appointmentArr.getString(10);
				
	  /////////////////////////////////////////////////////////////////////////////////

	 //---------------------------------registrationBookConsultation---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject registrationBookConsultationObj =	definitionsObj.getJSONObject("registrationBookConsultation");
				JSONArray	registrationBookConsultationArr =registrationBookConsultationObj.getJSONArray("required");
				date = registrationBookConsultationArr.getString(7);
				bookedSlotId = registrationBookConsultationArr.getString(9);
				reasonForVisit = registrationBookConsultationArr.getString(10);
				patient = registrationBookConsultationArr.getString(16);
				medicalHistoryFlag = registrationBookConsultationArr.getString(17);
				medicalHistory = registrationBookConsultationArr.getString(18);
				isDuplicateCheckRequired = registrationBookConsultationArr.getString(19);
				isPatientMappingRequired = registrationBookConsultationArr.getString(20);
				
	  /////////////////////////////////////////////////////////////////////////////////

	  //---------------------------------patient---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject patientObj =	definitionsObj.getJSONObject("patient");
				JSONArray	patientArr =patientObj.getJSONArray("required");
				title = patientArr.getString(1);
				patientName = patientArr.getString(2);
				lastName = patientArr.getString(3);
				gender = patientArr.getString(4);
				bloodGroup = patientArr.getString(5);
				patientDOB = patientArr.getString(6);
				age = patientArr.getString(7);
				cityId = patientArr.getString(8);
				mobileNo = patientArr.getString(9);
				emailId = patientArr.getString(10);
				addressLine1 = patientArr.getString(11);
				addressLine2 = patientArr.getString(12);
				country = patientArr.getString(13);
				state = patientArr.getString(14);
				city = patientArr.getString(15);
				zipCode = patientArr.getString(16);
				patientPhoto = patientArr.getString(17);
				birthDayWish = patientArr.getString(18);
				homeMonitoring = patientArr.getString(19);
				alternate_number = patientArr.getString(20);
				casualty = patientArr.getString(21);
				religion = patientArr.getString(22);
				employment = patientArr.getString(23);
				mlc = patientArr.getString(24);
				mlc_number = patientArr.getString(25);
				mlc_description = patientArr.getString(26);
				government_id_type = patientArr.getString(27);
				government_id_number = patientArr.getString(28);
				martial_status = patientArr.getString(29);
				clinic_id = patientArr.getString(30);
				countrycode_mobileNo = patientArr.getString(31);
				countrycode_alternateNo = patientArr.getString(32);
				
	  /////////////////////////////////////////////////////////////////////////////////

	  //---------------------------------patientAllergy---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject patientAllergyObj =	definitionsObj.getJSONObject("patientAllergy");
				JSONArray	patientAllergyArr =patientAllergyObj.getJSONArray("required");
				patientAllergyId = patientAllergyArr.getString(0);
				allergyId = patientAllergyArr.getString(1);
				
	  /////////////////////////////////////////////////////////////////////////////////
				
//---------------------------------medicalHistory---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject medicalHistoryObj =	definitionsObj.getJSONObject("medicalHistory");
				JSONArray	medicalHistoryArr =medicalHistoryObj.getJSONArray("required");
				tetnus = medicalHistoryArr.getString(3);
				immOtherDesc = medicalHistoryArr.getString(4);
				pneumonia = medicalHistoryArr.getString(7);
				hepatitis_a = medicalHistoryArr.getString(8);
				hepatitis_b = medicalHistoryArr.getString(9);
				typhoid = medicalHistoryArr.getString(10);
				influenza = medicalHistoryArr.getString(11);
				polio = medicalHistoryArr.getString(12);
				socialHabitsSmoking = medicalHistoryArr.getString(13);
				socialHabitsDrinking = medicalHistoryArr.getString(14);
				socialHabitsOthers = medicalHistoryArr.getString(15);
				patientMedicalConditionsDiabetes = medicalHistoryArr.getString(16);
				patientMedicalConditionsHypertension = medicalHistoryArr.getString(17);
				patientMedicalConditionsAsthma = medicalHistoryArr.getString(18);
				patientMedicalConditionsHeartDisease = medicalHistoryArr.getString(19);
				patientMedicalConditionsOthers = medicalHistoryArr.getString(20);
				familyMedicalConditionsDiabetes = medicalHistoryArr.getString(21);
				familyMedicalConditionsHypertension = medicalHistoryArr.getString(22);
				familyMedicalConditionsAsthma = medicalHistoryArr.getString(23);
				familyMedicalConditionsHeartDisease = medicalHistoryArr.getString(24);
				familyMedicalConditionsOthers = medicalHistoryArr.getString(25);
				currentMedication = medicalHistoryArr.getString(27);
				
	  /////////////////////////////////////////////////////////////////////////////////
			
	 //---------------------------------currentMedication---------------------------------------//
				
				//System.out.println("File Content: \n" + jsonData);
				JSONObject currentMedicationObj =	definitionsObj.getJSONObject("currentMedication");
				JSONArray	currentMedicationArr =currentMedicationObj.getJSONArray("required");
				medicine = currentMedicationArr.getString(0);
				comments = currentMedicationArr.getString(1);

				
	  /////////////////////////////////////////////////////////////////////////////////
			
	}
	
}
}
