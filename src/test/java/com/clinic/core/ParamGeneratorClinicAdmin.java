package com.clinic.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class ParamGeneratorClinicAdmin {

	public static List<NameValuePair> param = null;
		
	public static List<NameValuePair> changePassword_Param(String userLoginName,String password,String oldPassword,String token) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.password, password));
		param.add(new BasicNameValuePair(JsonParseLogin.oldPassword, oldPassword));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		return param;
	}
	
	public static List<NameValuePair> forgotPassword_Param(String userLoginName) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		return param;

	}
	
	public static List<NameValuePair> resetPassword_Param(String userLoginName,String password) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.password, password));
		return param;

	}
	
	public static List<NameValuePair> getUserBasicDetails_Param(String token,String clinicUserId,String userLoginName,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		return param;

	}
	
	public static List<NameValuePair> autoSuggestionUsers_Param(String searchText,String token,String userLoginName,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.searchText, searchText));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> retrieveUsers_Param(String startRow,String interval,String searchText,String token,String clinicUserId,String userLoginName,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.searchText, searchText));
		param.add(new BasicNameValuePair(JsonParseLogin.startRow, startRow));
		param.add(new BasicNameValuePair(JsonParseLogin.interval, interval));
		return param;

	}
	
	public static List<NameValuePair> userStatusUpdate_Param(String adminUserLoginName,String adminUserId,String userId,String token,String organisationId,String isActive,String roleType) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.adminUserLoginName, adminUserLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.adminUserId, adminUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.userId, userId));
		param.add(new BasicNameValuePair(JsonParseLogin.isActive, isActive));
		param.add(new BasicNameValuePair(JsonParseLogin.roleType, roleType));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> getUserDetails_Param(String adminUserLoginName,String clinicUserId,String token,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.adminUserLoginName, adminUserLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> userAssocToClinic_Param(String userLoginName,String clinicUserId,String token,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> getUserConsultationTabDetails_Param(String adminUserLoginName,String clinicUserId,String token,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.adminUserLoginName, adminUserLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> acceptTermCondition_Param(String userLoginName,String clinicUserId,String token,String acceptTermAndCondition) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.acceptTermAndCondition, acceptTermAndCondition));
		return param;

	}
	
	public static List<NameValuePair> otpVerification_Param(String otpCode,String clinicUserId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.otpCode, otpCode));
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> userSignUpValidate_Param(String clinicUserId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> createUserAuthentication_Param(String userLoginName,String clinicUserId,String organisationId,String password) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseLogin.password, password));
		return param;

	}
	
	public static List<NameValuePair> userMaster_Param(String userLoginName,String token,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		return param;

	}
	
	public static List<NameValuePair> updateUser_Param(String token,String adminUserLoginName,String adminUserId,String siteId,String locationId,String clinicUserId,String organisationId,String userName,String roleType){
		param = new ArrayList<NameValuePair>();

		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.adminUserLoginName, adminUserLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.adminUserId, adminUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.siteId, siteId));
		param.add(new BasicNameValuePair(JsonParseLogin.locationId, locationId));
		param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		param.add(new BasicNameValuePair(JsonParseLogin.userName, userName));
		param.add(new BasicNameValuePair(JsonParseLogin.roleType, roleType));
		return param;
		}

		public static JSONObject getJSONUpdateUser(List<String>  roleId) {
			
			JSONObject updateUser = new JSONObject();
			updateUser.put(JsonParseLogin.roleId, roleId);
			return updateUser;
		}

		//createUser
		public static List<NameValuePair> createUser_Param(String token,String userLoginName,String adminUserId,String siteId){
		param = new ArrayList<NameValuePair>();

		param.add(new BasicNameValuePair(JsonParseLogin.token, token));
		param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		param.add(new BasicNameValuePair(JsonParseLogin.adminUserId, adminUserId));
		param.add(new BasicNameValuePair(JsonParseLogin.siteId, siteId));
		return param;
		}

		public static JSONObject getJSONCreateUser(String userId,String userName,String title,String gender,String bloodGroup,String dob,String mobileNumber,String speciality,String email,String address,String medicalRegNo,String signature,String userPhoto,String roleType,
				String isActive,String organisationId,String organisationName,String isVirtualClinic,List<String>  roleId,List<String> locationId,String isDuplicateCheckRequired,String isUserMappingRequired) {
			
			JSONObject user = new JSONObject();
			
			JSONObject createUserArray = new JSONObject();
			createUserArray .put(JsonParseLogin.userId, userId);
			createUserArray .put(JsonParseLogin.userName, userName);
			createUserArray .put(JsonParseLogin.title, title);
			createUserArray .put(JsonParseLogin.gender, gender);
			createUserArray .put(JsonParseLogin.bloodGroup, bloodGroup);
			createUserArray .put(JsonParseLogin.dob, dob);
			createUserArray .put(JsonParseLogin.mobileNumber, mobileNumber);
			createUserArray .put(JsonParseLogin.speciality, speciality);
			createUserArray .put(JsonParseLogin.email, email);
			createUserArray .put(JsonParseLogin.address, address);
			createUserArray .put(JsonParseLogin.medicalRegNo, medicalRegNo);
			createUserArray .put(JsonParseLogin.signature, signature);
			createUserArray .put(JsonParseLogin.userPhoto, userPhoto);
			createUserArray .put(JsonParseLogin.roleType, roleType);
			createUserArray .put(JsonParseLogin.isActive, isActive);
			createUserArray .put(JsonParseLogin.organisationId, organisationId);
			createUserArray .put(JsonParseLogin.organisationName, organisationName);
			createUserArray .put(JsonParseLogin.isVirtualClinic, isVirtualClinic);
			
			user.put(JsonParseLogin.user, createUserArray );
			user.put(JsonParseLogin.roleId, roleId);
			user.put(JsonParseLogin.locationId, locationId);
			user.put(JsonParseLogin.isDuplicateCheckRequired, isDuplicateCheckRequired);
			user.put("isUserMappingRequired", isUserMappingRequired);
			return user;
		}
		
		public static List<NameValuePair> updateUserConsultationTabDetails_Param(String token,String adminUserLoginName,String clinicUserId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseLogin.token, token));
			param.add(new BasicNameValuePair(JsonParseLogin.adminUserLoginName, adminUserLoginName));
			param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
			param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
			return param;
		}

		public static JSONObject getJSONupdateUserConsultationTabDetails(String consultationTabDetails) {

			JSONObject updatePatientObj = new JSONObject();
			JSONArray updatePatientArr = new JSONArray();		
			updatePatientArr .put(consultationTabDetails);
			updatePatientObj .put(JsonParseLogin.consultationTabDetails, updatePatientArr);
			System.out.println("patientTestList:"+updatePatientObj.toString());
			return updatePatientObj;
		}
		
		public static List<NameValuePair> fetchAppUpdateHandler_param(String appType) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("appType", appType));
			return param;
		}
		
		public static List<NameValuePair> autoSuggestionParameters_Param(String token,String userLoginName,String searchText,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
			param.add(new BasicNameValuePair("searchText", searchText));
			param.add(new BasicNameValuePair("organisationId", organisationId));

		
			return param;
		}	
		
		public static List<NameValuePair> updateSystemParam_Param(String token,String userLoginName,String adminUserId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
			param.add(new BasicNameValuePair("adminUserId", adminUserId));
			
			param.add(new BasicNameValuePair("organisationId", organisationId));
			return param;
		}	
		
		public static JSONObject getJSONupdateSystemParam( String paramId, String  sysParamNameId, String sysParamName, String paramValue, String level,String levelValueId,String levelValue,String siteId) {

			JSONObject addUserExperience = new JSONObject();

			JSONObject AddUserExperienceArray  = new JSONObject();
			AddUserExperienceArray.put("paramId", paramId);
			AddUserExperienceArray .put("sysParamNameId", sysParamNameId);
			AddUserExperienceArray .put("sysParamName",sysParamName);
			AddUserExperienceArray .put("paramValue", paramValue);
			AddUserExperienceArray .put("level", level);
			AddUserExperienceArray .put("levelValueId", levelValueId);
			AddUserExperienceArray .put("levelValue", levelValue);
			AddUserExperienceArray .put("siteId", siteId);
			
			addUserExperience.put("systemParam", AddUserExperienceArray );
			System.out.println("req:"+addUserExperience.toString());
			return addUserExperience;

		}
		
		
		
		public static List<NameValuePair> retrieveSystemParams_Param(String token,String userLoginName,String startRow,String interval,String searchText,String sysParamId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
			param.add(new BasicNameValuePair("startRow", startRow));
			param.add(new BasicNameValuePair("interval", interval));
			param.add(new BasicNameValuePair("searchText", searchText));
			param.add(new BasicNameValuePair("sysParamId", sysParamId));
			param.add(new BasicNameValuePair("organisationId", organisationId));
			return param;
		}
		
		public static List<NameValuePair> getParametersMaster_Param(String token,String userLoginName,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("organisationId", organisationId));
			return param;
		}
		
		public static List<NameValuePair> getOperationalRoleList_Param(String token,String userLoginName,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("organisationId", organisationId));
			return param;
		}
		
		public static List<NameValuePair> GetSpecialty_Param(String token,String userLoginName,String organisationId,String isClinicAdminUser) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("organisationId", organisationId));
			param.add(new BasicNameValuePair("isClinicAdminUser", isClinicAdminUser));
			return param;
		}
		
		public static List<NameValuePair> searchClinicServiceGlobal_Param(String token,String userLoginName,String organisationId,String serviceCategory,String serviceClassificationId,String serviceName) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("organisationId", organisationId));
			param.add(new BasicNameValuePair("serviceCategory", serviceCategory));
			param.add(new BasicNameValuePair("serviceClassificationId", serviceClassificationId));
			param.add(new BasicNameValuePair("serviceName", serviceName));
			return param;
		}
		
		public static List<NameValuePair> searchUserGlobal_Param(String token,String userLoginName,String organisationId,String userType,String userName,String roleId,String phoneNumber,String speciality,String isClinicAdminUser) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("organisationId", organisationId));
			param.add(new BasicNameValuePair("userType", userType));
			param.add(new BasicNameValuePair("userName", userName));
			
			param.add(new BasicNameValuePair("roleId", roleId));
			param.add(new BasicNameValuePair("phoneNumber", phoneNumber));
			param.add(new BasicNameValuePair("isClinicAdminUser",isClinicAdminUser));
			return param;
		}
		
		public static List<NameValuePair>autoSuggestionSpeciality_Param(String token,String userLoginName,String organisationId,String searchText) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("organisationId", organisationId));
			param.add(new BasicNameValuePair("searchText", searchText));
			return param;
		}
		
		public static List<NameValuePair>serviceCategory_Param(String token,String userLoginName,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseServiceMaster.token, token));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.organisationId, organisationId));
		
			return param;
		}
		
		public static List<NameValuePair>serviceClassification_Param(String token,String userLoginName,String organisationId,String category) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseServiceMaster.token, token));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.organisationId, organisationId));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.category, category));
		
			return param;
		}
		
		public static List<NameValuePair>searchClinicService_Param(String token,String userLoginName,String organisationId,String searchText) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseServiceMaster.token, token));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.organisationId, organisationId));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.searchText, searchText));
		
			return param;
		}
		
		public static List<NameValuePair>createService_Param(String token,String userLoginName,String adminUserId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseServiceMaster.token, token));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.adminUserId, adminUserId));
			return param;
		}
		
		public static JSONObject getJSONcreateService( String serviceName , String  serviceCategory, String serviceType, String chargable, String resultApplicable,String classificationId,String organisationId,String status,String tariff,String siteId,String locationId) {

			JSONObject addUserExperience = new JSONObject();

			JSONObject AddUserExperienceArray  = new JSONObject();
			AddUserExperienceArray.put(JsonParseServiceMaster.serviceName, serviceName );
			AddUserExperienceArray .put(JsonParseServiceMaster.serviceCategory, serviceCategory);
			AddUserExperienceArray .put(JsonParseServiceMaster.serviceType, serviceType);
			AddUserExperienceArray .put(JsonParseServiceMaster.chargable, chargable);
			AddUserExperienceArray .put(JsonParseServiceMaster.resultApplicable, resultApplicable);
			AddUserExperienceArray .put(JsonParseServiceMaster.classificationId, classificationId);
			AddUserExperienceArray .put(JsonParseServiceMaster.organisationId, organisationId);
			AddUserExperienceArray .put(JsonParseServiceMaster.status, status);
			AddUserExperienceArray .put(JsonParseServiceMaster.tariff, tariff);
			AddUserExperienceArray .put(JsonParseServiceMaster.siteId, siteId);
			AddUserExperienceArray .put(JsonParseServiceMaster.locationId, locationId);
			
			addUserExperience.put(JsonParseServiceMaster.service, AddUserExperienceArray );
			System.out.println("req:"+addUserExperience.toString());
			return addUserExperience;

		}
		
		public static List<NameValuePair>retrieveClinicServices_Param(String token,String userLoginName,String serviceId,String startRow,String interval,String searchText,String organisationId,String locationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseServiceMaster.token, token));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.serviceId, serviceId));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.startRow, startRow));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.organisationId, organisationId));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.interval, interval));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.locationId, locationId));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.searchText, searchText));
		
			return param;
		}
		
		public static List<NameValuePair>updateService_Param(String token,String userLoginName,String adminUserId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseServiceMaster.token, token));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.adminUserId, adminUserId));
			return param;
		}
		
		
		public static JSONObject getJSONupdateService( String serviceId,String serviceName , String serviceType, String chargable, String resultApplicable,String organisationId,String status,String tariff,String tariffId) {

			JSONObject addUserExperience = new JSONObject();

			JSONObject AddUserExperienceArray  = new JSONObject();
			AddUserExperienceArray.put(JsonParseServiceMaster.serviceId, serviceId );
			AddUserExperienceArray.put(JsonParseServiceMaster.serviceName, serviceName );
			
			AddUserExperienceArray .put(JsonParseServiceMaster.serviceType, serviceType);
			AddUserExperienceArray .put(JsonParseServiceMaster.chargable, chargable);
			AddUserExperienceArray .put(JsonParseServiceMaster.resultApplicable, resultApplicable);
		
			AddUserExperienceArray .put(JsonParseServiceMaster.organisationId, organisationId);
			AddUserExperienceArray .put(JsonParseServiceMaster.status, status);
			AddUserExperienceArray .put(JsonParseServiceMaster.tariff, tariff);
			AddUserExperienceArray .put(JsonParseServiceMaster.tariffId, tariffId);
		
			
			addUserExperience.put(JsonParseServiceMaster.service, AddUserExperienceArray );
			System.out.println("req:"+addUserExperience.toString());
			return addUserExperience;

		}
				
		public static List<NameValuePair>serviceMaster_Param(String token,String userLoginName,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseServiceMaster.token, token));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.userLoginName, userLoginName));
			param.add(new BasicNameValuePair(JsonParseServiceMaster.organisationId, organisationId));
			return param;
		}
		
		public static List<NameValuePair>locationMaster_Param(String token,String userLoginName,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("organisationId", organisationId));
			

		
			return param;
		}
		
		public static List<NameValuePair>createTariff_Param(String token,String userLoginName,String adminUserId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("adminUserId", adminUserId));
			

		
			return param;
		}
		
		public static JSONObject getJSONcreateTariff( int tariffAmount ,int serviceId, String serviceName, String siteId, String locationId,String organisationId) {

			JSONObject addUserExperience = new JSONObject();

			JSONObject AddUserExperienceArray  = new JSONObject();
			AddUserExperienceArray.put("tariffAmount", tariffAmount );
			AddUserExperienceArray .put("serviceId", serviceId);
			AddUserExperienceArray .put("serviceName", serviceName);
			AddUserExperienceArray .put("siteId", siteId);
			AddUserExperienceArray .put("locationId", locationId);
			AddUserExperienceArray .put("organisationId", organisationId);

			
			addUserExperience.put("tariff", AddUserExperienceArray );
			System.out.println("req:"+addUserExperience.toString());
			return addUserExperience;

		}
		
		
		
		
		
		public static List<NameValuePair>UpdateTariff_Param(String token,String userLoginName,String adminUserId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("adminUserId", adminUserId));
			param.add(new BasicNameValuePair("organisationId", organisationId));

		
			return param;
		}
		
		public static JSONObject getJSONUpdateTariff( int tariffAmount ,String tariffId, String serviceName ) {

			JSONObject addUserExperience = new JSONObject();

			JSONObject AddUserExperienceArray  = new JSONObject();
			AddUserExperienceArray.put("tariffAmount", tariffAmount );
			AddUserExperienceArray .put("tariffId", tariffId);
			AddUserExperienceArray .put("serviceName", serviceName);
;

			
			addUserExperience.put("tariff", AddUserExperienceArray );
			System.out.println("req:"+addUserExperience.toString());
			return addUserExperience;

		}
		
		public static List<NameValuePair>retrieveTariffs_Param(String token,String userLoginName,String locationId,String serviceName,String startRow,String interval, String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("userLoginName", userLoginName));
		
			
			param.add(new BasicNameValuePair("locationId", locationId));
			param.add(new BasicNameValuePair("serviceName", serviceName));
			param.add(new BasicNameValuePair("startRow", startRow));
			param.add(new BasicNameValuePair("interval", interval));
			
			param.add(new BasicNameValuePair("organisationId", organisationId));

		
			return param;
		}
		
		public static List<NameValuePair>verifyUserLoginName_Param(String userLoginName) {

			param = new ArrayList<NameValuePair>();
		
			param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
		

		
			return param;
		}
		
		public static List<NameValuePair> resendSms_Param(String mobileNumber,String clinicUserId,String organisationId) {

			param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(JsonParseLogin.mobileNumber, mobileNumber));

			param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
			param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
		
			return param;

		}
		
		//fetchMedicalCertificateTemplate
				public static List<NameValuePair> fetchMedicalCertificateTemplate_Param(String organisationId,String token,String clinicUserId,String userLoginName) {
					param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
					param.add(new BasicNameValuePair(JsonParseLogin.token,token));
					param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
					param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
					return param;
				}

				//saveMedicalCertificateTemplate
				public static List<NameValuePair> saveMedicalCertificateTemplate_Param(String organisationId,String token,String clinicUserId,String userLoginName,String templateName,String templateData,
						String certificateId,String templateId) {
					param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair(JsonParseLogin.organisationId, organisationId));
					param.add(new BasicNameValuePair(JsonParseLogin.token,token));
					param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
					param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
					param.add(new BasicNameValuePair("templateName", templateName));
					param.add(new BasicNameValuePair("templateData", templateData));
					param.add(new BasicNameValuePair("certificateId", certificateId));
					param.add(new BasicNameValuePair("templateId", templateId));
					return param;
				}
				
				//saveMedicalCertificate
				public static List<NameValuePair> saveMedicalCertificate_Param(String userLoginName,String token,String userId,String orgId,String certificateId,String templateId,
						String certificateData,String clinicUserId,String patientUhid) {
					param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
					param.add(new BasicNameValuePair(JsonParseLogin.token,token));
					param.add(new BasicNameValuePair("userId",userId));
					param.add(new BasicNameValuePair("orgId", orgId));
					param.add(new BasicNameValuePair("certificateId", certificateId));
					param.add(new BasicNameValuePair("templateId", templateId));
					param.add(new BasicNameValuePair("certificateData", certificateData));
					param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
					param.add(new BasicNameValuePair("patientUhid", patientUhid));
					return param;
				}
				
				//searchMedicalCertificates
				public static List<NameValuePair> searchMedicalCertificates_Param(String userLoginName,String token,String userId,String orgId,String startIndex,String interval) {
					param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
					param.add(new BasicNameValuePair(JsonParseLogin.token,token));
					param.add(new BasicNameValuePair("userId",userId));
					param.add(new BasicNameValuePair("orgId", orgId));
					param.add(new BasicNameValuePair("startIndex", startIndex));
					param.add(new BasicNameValuePair(JsonParseLogin.interval,interval));
					return param;
				}
				
				public static JSONObject getJSONSearchMedicalCertificates(String patientName ,String mobileNumber, String patientUhid,String fromDate,String toDate,JSONArray certificateIds ) {

					JSONObject searchMedical = new JSONObject();
					JSONObject SearchMedicalArray  = new JSONObject();
					SearchMedicalArray.put("patientName", patientName );
					SearchMedicalArray .put("mobileNumber", mobileNumber);
					SearchMedicalArray .put("patientUhid", patientUhid);
					SearchMedicalArray .put("fromDate",fromDate);
					SearchMedicalArray .put("toDate",toDate);
					SearchMedicalArray .put("certificateIds", certificateIds);

					searchMedical.put("searchDetails", SearchMedicalArray );
					System.out.println("req:"+searchMedical.toString());
					return searchMedical;

				}
				
				public static List<NameValuePair> getCommunicationParam_Param(String userLoginName,String token,String orgId) {
					param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
					param.add(new BasicNameValuePair(JsonParseLogin.token,token));
					param.add(new BasicNameValuePair(JsonParseLogin.organisationId, orgId));
					return param;
				}
				
				public static List<NameValuePair> updateCommunicationParam_Param(String userLoginName,String token,String clinicUserId,String orgId,String receiptUser,String workflow,String smsMessage,String communicationType,String mailSubject,String mailBody,String isActive) {
					param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair(JsonParseLogin.userLoginName, userLoginName));
					param.add(new BasicNameValuePair(JsonParseLogin.token,token));
					param.add(new BasicNameValuePair(JsonParseLogin.clinicUserId, clinicUserId));
					param.add(new BasicNameValuePair(JsonParseAppointment.organizationId, orgId));
					param.add(new BasicNameValuePair("receiptUser", receiptUser));
					param.add(new BasicNameValuePair("workflow", workflow));
					param.add(new BasicNameValuePair("smsMessage", smsMessage));
					param.add(new BasicNameValuePair("communicationType", communicationType));
					param.add(new BasicNameValuePair("mailSubject", mailSubject));
					param.add(new BasicNameValuePair("mailBody", mailBody));
					param.add(new BasicNameValuePair("isActive", isActive));
					return param;
				}

		
}