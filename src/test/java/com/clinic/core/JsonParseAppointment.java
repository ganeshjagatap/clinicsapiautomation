package com.clinic.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class JsonParseAppointment {
	
	public static String host=null;
	
	
	public static String token=null;
	public static String userLoginName=null;
	public static String userId=null;
	public static String organisationId=null;
	public static String organizationId=null;
	public static String clinicUserId=null;
	public static String appointmentDate=null;
	public static String allApointmentsCount=null;
	public static String checkedInCount=null;
	public static String doneCount=null;
	public static String scheduledCount=null;
	public static String siteId=null;
	public static String locationId=null;
	public static String bookedslots=null;
	public static String dateSlotId=null;
	public static String startTime=null;
	public static String endTime=null;
	public static String slotId=null;
	public static String slotInterval=null;
	public static String normalBookSize=null;
	public static String overBookSize=null;
	public static String bookedSlotStatus=null;
	public static String patientList=null;
	public static String countryId=null;
	public static String stateId=null;
	public static String startIndex=null;
	public static String interval=null;
	public static String patient=null;
	public static String hmFlag=null;
	public static String uhid=null;
	public static String patientName=null;
	public static String gender=null;
	public static String dob=null;
	public static String mobileNo=null;
	public static String visit_date_from=null;
	public static String visit_date_to=null;
	public static String appointment=null;
	public static String patientId=null;
	public static String reasonOfVisit=null;
	public static String locationName=null;
	public static String organizationName=null;
	public static String doctorName=null;
	public static String reasonType=null;
	public static String appointmentId=null;
	public static String doctorId=null;
	public static String roleId=null;
	public static String reasonforCancel=null;
	public static String cancelAppointment=null;
	public static String bookAppointment=null;
	public static String blockAppointmentSlot=null;
	public static String fromDate=null;
	public static String toDate=null;
	public static String fromTime=null;
	public static String toTime=null;
	public static String reason=null;
	public static String isDuplicateCheckRequired=null;
	public static String isPatientMappingRequired=null;
	public static String isVirtualClinicRequest=null;
	public static String title=null;
	public static String bloodGroup=null;
	public static String patientDOB=null;
	public static String age=null;
	public static String emailId=null;
	public static String addressLine1=null;
	public static String addressLine2=null;
	public static String country=null;
	public static String state=null;
	public static String city=null;
	public static String zipCode=null;
	public static String patientPhoto=null;
	public static String birthDayWish=null;
	public static String homeMonitoring=null;
	public static String medicalHistoryFlag=null;
	public static String medicalHistory=null;
	public static String patientAllergy=null;
	public static String lastName=null;
	public static String alternate_number=null;
	public static String casualty=null;
	public static String religion=null;
	public static String employment=null;
	public static String mlc=null;
	public static String mlc_number=null;
	public static String mlc_description=null;
	public static String government_id_type=null;
	public static String government_id_number=null;
	public static String martial_status=null;
	public static String clinic_id=null;
	public static String countrycode_mobileNo=null;
	public static String countrycode_alternateNo=null;
	public static String patient_password=null;
	public static String patientLoginName=null;
	public static String immunization=null;
	public static String bcg=null;
	public static String dpt=null;
	public static String tetnus=null;
	public static String immOtherDesc=null;
	public static String familyMedicalHistory=null;
	public static String patientMedicalHistory=null;
	public static String pneumonia=null;
	public static String hepatitis_a=null;
	public static String hepatitis_b=null;
	public static String typhoid=null;
	public static String influenza=null;
	public static String polio=null;
	public static String socialHabitsSmoking=null;
	public static String socialHabitsDrinking=null;
	public static String socialHabitsOthers=null;
	public static String patientMedicalConditionsDiabetes=null;
	public static String patientMedicalConditionsHypertension=null;
	public static String patientMedicalConditionsAsthma=null;
	public static String patientMedicalConditionsHeartDisease=null;
	public static String patientMedicalConditionsOthers=null;
	public static String familyMedicalConditionsDiabetes=null;
	public static String familyMedicalConditionsHypertension=null;
	public static String familyMedicalConditionsAsthma=null;
	public static String familyMedicalConditionsHeartDisease=null;
	public static String familyMedicalConditionsOthers=null;
	public static String notes=null;
	public static String currentMedication=null;
	public static String patientAllergyId=null;
	public static String allergyId=null;
	public static String allergyCimsCode=null;
	public static String allergyName=null;
	public static String operation=null;
	public static String medicine=null;
	public static String comments=null;
	public static String date=null;
	public static String bookedSlotId=null;
	public static String reasonForVisit=null;
	public static String patientUHID=null;
	public static String cityId=null;
	public static String medicalHistoryId=null;
	public static String patientVisitId=null;
	
	
	
	@Test
	public void jsonParseAppointment(){
		String jsonData = "";
		
		BufferedReader br = null;
		try {
			String line;
			br = new BufferedReader(new FileReader(".//json//appointment.json"));
			while ((line = br.readLine()) != null) {
				jsonData += line + "\n";
				}
			}
		catch (IOException e) {
			e.printStackTrace();
			}
		finally {
			try {
				if (br != null)
					br.close();
				}
			catch (IOException ex) {
				ex.printStackTrace();
				}
			}
		JSONObject obj = new JSONObject(jsonData);
		
		host=obj.getString("host");
		System.out.println(host);
		JSONObject pathsObj=	obj.getJSONObject("paths");
		
		Iterator<?> keys = pathsObj.keys();
		
		while(keys.hasNext()){
			 String t=keys.next().toString();
			 //S.add(t);
			 String t1=t.substring(55);
			 //System.out.println(t);
			 //S1.add(t1);
			 if(t1.equalsIgnoreCase("appointmentHomeDoctor"))
				 Constants.homeDoctor=t;
			 if(t1.equalsIgnoreCase("countryList"))
				 Constants.countryList=t;
			 if(t1.equalsIgnoreCase("stateMasterList"))
				 Constants.stateMasterList=t;
			 if(t1.equalsIgnoreCase("cityMasterList"))
				 Constants.cityMasterList=t;
			 if(t1.equalsIgnoreCase("patientSearch"))
				 Constants.patientSearch=t;
			 if(t1.equalsIgnoreCase("bookAppointment"))
				 Constants.bookAppointment=t;
			 if(t1.equalsIgnoreCase("appointmentHomeDoctorList"))
				 Constants.appointmentHomeDoctorList=t;
			 if(t1.equalsIgnoreCase("appointmentHomeDoctorSlotDatesList"))
				 Constants.appointmentHomeDoctorSlotDatesList=t;
			 if(t1.equalsIgnoreCase("reasonMaster"))
				 Constants.appointmentReasonMaster=t;
			 if(t1.equalsIgnoreCase("cancelAppointmentByParamedic"))
				 Constants.cancelAppointment=t;
			 if(t1.equalsIgnoreCase("rescheduleAppointment"))
				 Constants.rescheduleAppointment=t;
			 if(t1.equalsIgnoreCase("blockAppointmentSlot"))
				 Constants.blockAppointmentSlot=t;
			 if(t1.equalsIgnoreCase("unblockAppointmentSlot"))
				 Constants.unblockAppointmentSlot=t;
			 if(t1.equalsIgnoreCase("blockAppointmentCalender"))
				 Constants.blockAppointmentCalender=t;
			 if(t1.equalsIgnoreCase("patientRegistrationAlternateFlow"))
				 Constants.patientRegistrationAlternateFlow=t;
			 if(t1.equalsIgnoreCase("patientRegistrationAlternateFlowVC"))
				 Constants.patientRegistrationAlternateFlowVC=t;
			 if(t1.equalsIgnoreCase("patientPreviousVisits"))
				 Constants.patientPreviousVisits=t;
			 if(t1.equalsIgnoreCase("patientRegistration"))
				 Constants.patientRegistration=t;
			 if(t1.equalsIgnoreCase("updatePatientDetails"))
				 Constants.updatePatientDetails=t;
			 if(t1.equalsIgnoreCase("getMedicalHistory"))
				 Constants.getMedicalHistory=t;
			 if(t1.equalsIgnoreCase("patientSearchGetAllPatient"))
				 Constants.patientSearchGetAllPatient=t;
			 
		 }
		
		
		JSONObject definitionsObj=	obj.getJSONObject("definitions");
		
		
		
		//---------------------------------appointmentHomeDoctor---------------------------------------//
		
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject appointmentHomeDoctorObj=	definitionsObj.getJSONObject("appointmentHomeDoctor");
		JSONArray	appointmentHomeDoctorArr=	appointmentHomeDoctorObj.getJSONArray("required");
		token=appointmentHomeDoctorArr.getString(0);
		userLoginName=appointmentHomeDoctorArr.getString(1);
		userId=appointmentHomeDoctorArr.getString(2);
		organisationId=appointmentHomeDoctorArr.getString(3);
		clinicUserId=appointmentHomeDoctorArr.getString(4);
		appointmentDate=appointmentHomeDoctorArr.getString(5);
		//Assert.assertNotNull(patienID, "PatientID is null");
		
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------ApiResponse---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject ApiResponseObj=	definitionsObj.getJSONObject("ApiResponse");
		JSONArray	ApiResponseArr=	ApiResponseObj.getJSONArray("required");
		allApointmentsCount=ApiResponseArr.getString(0);
		checkedInCount=ApiResponseArr.getString(1);
		doneCount=ApiResponseArr.getString(2);
		scheduledCount=ApiResponseArr.getString(3);
		siteId=ApiResponseArr.getString(4);
		locationId=ApiResponseArr.getString(5);
		bookedslots=ApiResponseArr.getString(7);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------Slot---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject SlotObj=	definitionsObj.getJSONObject("Slot");
		JSONArray	SlotArr=	SlotObj.getJSONArray("required");
		dateSlotId=SlotArr.getString(0);
		startTime=SlotArr.getString(1);
		endTime=SlotArr.getString(2);
		slotId=SlotArr.getString(3);
		slotInterval=SlotArr.getString(4);
		normalBookSize=SlotArr.getString(5);
		overBookSize=SlotArr.getString(6);
		bookedSlotStatus=SlotArr.getString(7);
		patientList=SlotArr.getString(8);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------stateMasterList---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject stateMasterListObj=	definitionsObj.getJSONObject("stateMasterList");
		JSONArray	stateMasterListArr=	stateMasterListObj.getJSONArray("required");
		countryId=stateMasterListArr.getString(4);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------cityMasterList---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject cityMasterListObj=	definitionsObj.getJSONObject("cityMasterList");
		JSONArray	cityMasterListArr=	cityMasterListObj.getJSONArray("required");
		stateId=cityMasterListArr.getString(4);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------patientSearch---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject patientSearchObj=	definitionsObj.getJSONObject("patientSearch");
		JSONArray	patientSearchArr=	patientSearchObj.getJSONArray("required");
		startIndex=patientSearchArr.getString(4);
		interval=patientSearchArr.getString(5);
		patient=patientSearchArr.getString(6);
		hmFlag=patientSearchArr.getString(7);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------Patients---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject PatientsObj=	definitionsObj.getJSONObject("Patients");
		JSONArray	PatientsArr=	PatientsObj.getJSONArray("required");
		uhid=PatientsArr.getString(0);
		patientName=PatientsArr.getString(1);
		gender=PatientsArr.getString(2);
		dob=PatientsArr.getString(3);
		mobileNo=PatientsArr.getString(4);
		visit_date_from=PatientsArr.getString(5);
		visit_date_to=PatientsArr.getString(6);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------bookAppointment---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject bookAppointmentObj=	definitionsObj.getJSONObject("bookAppointment");
		JSONArray	bookAppointmentArr=	bookAppointmentObj.getJSONArray("required");
		appointment=bookAppointmentArr.getString(4);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------Appointment---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject AppointmentObj=	definitionsObj.getJSONObject("Appointment");
		JSONArray AppointmentArr=	AppointmentObj.getJSONArray("required");
		patientId=AppointmentArr.getString(0);
		reasonOfVisit=AppointmentArr.getString(9);
		locationName=AppointmentArr.getString(10);
		organizationName=AppointmentArr.getString(11);
		doctorName=AppointmentArr.getString(12);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------reasonMaster---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject reasonMasterObj=	definitionsObj.getJSONObject("reasonMaster");
		JSONArray	reasonMasterArr=	reasonMasterObj.getJSONArray("required");
		reasonType=reasonMasterArr.getString(4);
		organizationId=reasonMasterArr.getString(2);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------Appointments---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject AppointmentsObj=	definitionsObj.getJSONObject("Appointments");
		JSONArray	AppointmentsArr=	AppointmentsObj.getJSONArray("required");
		appointmentId=AppointmentsArr.getString(3);
		doctorId=AppointmentsArr.getString(7);
		roleId=AppointmentsArr.getString(8);
		reasonforCancel=AppointmentsArr.getString(9);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------rescheduleAppointment---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject rescheduleAppointmentObj=	definitionsObj.getJSONObject("rescheduleAppointment");
		JSONArray	rescheduleAppointmentArr=	rescheduleAppointmentObj.getJSONArray("required");
		cancelAppointment=rescheduleAppointmentArr.getString(7);
		bookAppointment=rescheduleAppointmentArr.getString(8);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------blockAppointmentSlot---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject blockAppointmentSlotObj=	definitionsObj.getJSONObject("blockAppointmentSlot");
		JSONArray	blockAppointmentSlotArr=	blockAppointmentSlotObj.getJSONArray("required");
		blockAppointmentSlot=blockAppointmentSlotArr.getString(3);
		//Assert.assertNotNull(patienID, "PatientID is null");
		
		//---------------------------------BlockAppointmentSlot---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject BlockAppointmentSlotObj=	definitionsObj.getJSONObject("BlockAppointmentSlot");
		JSONArray	BlockAppointmentSlotArr=	BlockAppointmentSlotObj.getJSONArray("required");
		fromDate=BlockAppointmentSlotArr.getString(1);
		toDate=BlockAppointmentSlotArr.getString(2);
		fromTime=BlockAppointmentSlotArr.getString(3);
		toTime=BlockAppointmentSlotArr.getString(4);
		reason=BlockAppointmentSlotArr.getString(6);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------patientRegistrationAlternateFlow---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject patientRegistrationAlternateFlowObj=	definitionsObj.getJSONObject("patientRegistrationAlternateFlow");
		JSONArray	patientRegistrationAlternateFlowArr=	patientRegistrationAlternateFlowObj.getJSONArray("required");
		isDuplicateCheckRequired=patientRegistrationAlternateFlowArr.getString(6);
		isPatientMappingRequired=patientRegistrationAlternateFlowArr.getString(7);
		isVirtualClinicRequest=patientRegistrationAlternateFlowArr.getString(9);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------PatientRegistration---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject PatientRegistrationObj=	definitionsObj.getJSONObject("PatientRegistration");
		JSONArray	PatientRegistrationArr=	PatientRegistrationObj.getJSONArray("required");
		title=PatientRegistrationArr.getString(1);
		bloodGroup=PatientRegistrationArr.getString(4);
		patientDOB=PatientRegistrationArr.getString(5);
		age=PatientRegistrationArr.getString(6);
		emailId=PatientRegistrationArr.getString(8);
		addressLine1=PatientRegistrationArr.getString(9);
		addressLine2=PatientRegistrationArr.getString(10);
		country=PatientRegistrationArr.getString(11);
		state=PatientRegistrationArr.getString(12);
		city=PatientRegistrationArr.getString(13);
		zipCode=PatientRegistrationArr.getString(14);
		patientPhoto=PatientRegistrationArr.getString(15);
		birthDayWish=PatientRegistrationArr.getString(16);
		homeMonitoring=PatientRegistrationArr.getString(17);
		medicalHistoryFlag=PatientRegistrationArr.getString(18);
		medicalHistory=PatientRegistrationArr.getString(19);
		patientAllergy=PatientRegistrationArr.getString(20);
		lastName=PatientRegistrationArr.getString(21);
		alternate_number=PatientRegistrationArr.getString(22);
		casualty=PatientRegistrationArr.getString(23);
		religion=PatientRegistrationArr.getString(24);
		employment=PatientRegistrationArr.getString(25);
		mlc=PatientRegistrationArr.getString(26);
		mlc_number=PatientRegistrationArr.getString(27);
		mlc_description=PatientRegistrationArr.getString(28);
		government_id_type=PatientRegistrationArr.getString(29);
		government_id_number=PatientRegistrationArr.getString(30);
		martial_status=PatientRegistrationArr.getString(31);
		clinic_id=PatientRegistrationArr.getString(32);
		countrycode_mobileNo=PatientRegistrationArr.getString(33);
		countrycode_alternateNo=PatientRegistrationArr.getString(34);
		patient_password=PatientRegistrationArr.getString(35);
		patientLoginName=PatientRegistrationArr.getString(36);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//--------------------------------medicalHistory-----------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject medicalHistoryObj=	definitionsObj.getJSONObject("medicalHistory");
		JSONArray	medicalHistoryArr=	medicalHistoryObj.getJSONArray("required");
		immunization=medicalHistoryArr.getString(0);
		bcg=medicalHistoryArr.getString(1);
		dpt=medicalHistoryArr.getString(2);
		tetnus=medicalHistoryArr.getString(3);
		immOtherDesc=medicalHistoryArr.getString(4);
		familyMedicalHistory=medicalHistoryArr.getString(5);
		patientMedicalHistory=medicalHistoryArr.getString(6);
		pneumonia=medicalHistoryArr.getString(7);
		hepatitis_a=medicalHistoryArr.getString(8);
		hepatitis_b=medicalHistoryArr.getString(9);
		typhoid=medicalHistoryArr.getString(10);
		influenza=medicalHistoryArr.getString(11);
		polio=medicalHistoryArr.getString(12);
		socialHabitsSmoking=medicalHistoryArr.getString(13);
		socialHabitsDrinking=medicalHistoryArr.getString(14);
		socialHabitsOthers=medicalHistoryArr.getString(15);
		patientMedicalConditionsDiabetes=medicalHistoryArr.getString(16);
		patientMedicalConditionsHypertension=medicalHistoryArr.getString(17);
		patientMedicalConditionsAsthma=medicalHistoryArr.getString(18);
		patientMedicalConditionsHeartDisease=medicalHistoryArr.getString(19);
		patientMedicalConditionsOthers=medicalHistoryArr.getString(20);
		familyMedicalConditionsDiabetes=medicalHistoryArr.getString(21);
		familyMedicalConditionsHypertension=medicalHistoryArr.getString(22);
		familyMedicalConditionsAsthma=medicalHistoryArr.getString(23);
		familyMedicalConditionsHeartDisease=medicalHistoryArr.getString(24);
		familyMedicalConditionsOthers=medicalHistoryArr.getString(25);
		notes=medicalHistoryArr.getString(26);
		currentMedication=medicalHistoryArr.getString(27);
		medicalHistoryId=medicalHistoryArr.getString(28);
		patientVisitId=medicalHistoryArr.getString(29);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------patientAllergy---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject patientAllergyObj=	definitionsObj.getJSONObject("patientAllergy");
		JSONArray	patientAllergyArr=	patientAllergyObj.getJSONArray("required");
		patientAllergyId=patientAllergyArr.getString(0);
		allergyId=patientAllergyArr.getString(1);
		allergyCimsCode=patientAllergyArr.getString(2);
		allergyName=patientAllergyArr.getString(3);
		operation=patientAllergyArr.getString(4);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------currentMedication---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject currentMedicationObj=	definitionsObj.getJSONObject("currentMedication");
		JSONArray	currentMedicationArr=	currentMedicationObj.getJSONArray("required");
		medicine=currentMedicationArr.getString(0);
		comments=currentMedicationArr.getString(1);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------patientRegistration---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject patientRegistrationObj=	definitionsObj.getJSONObject("patientRegistration");
		JSONArray	patientRegistrationArr=	patientRegistrationObj.getJSONArray("required");
		date=patientRegistrationArr.getString(7);
		bookedSlotId=patientRegistrationArr.getString(9);
		reasonForVisit=patientRegistrationArr.getString(10);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		
		//---------------------------------updatePatient---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject updatePatientObj=	definitionsObj.getJSONObject("updatePatient");
		JSONArray	updatePatientArr=	updatePatientObj.getJSONArray("required");
		patientUHID=updatePatientArr.getString(1);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		

		//---------------------------------patient---------------------------------------//
		
		//System.out.println("File Content: \n" + jsonData);
		JSONObject patientObj=	definitionsObj.getJSONObject("patient");
		JSONArray	patientArr=	patientObj.getJSONArray("required");
		cityId=patientArr.getString(8);
		//Assert.assertNotNull(patienID, "PatientID is null");
				
		
		/////////////////////////////////////////////////////////////////////////////////
		

						
		}
	
}