package com.clinic.core;

import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class OEL001_fetchOfflineDBversionInfo extends TestBase {
	
	@Test(priority=0)
	public void fetchOfflineDBversionInfo_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.fetchOfflineDBversionInfo;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGenerator.fetchOfflineDBversionInfo_Param(token, username, orgId);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=responseJson_object.getJSONObject("dbinfo");
		String dbversion=e.getString("dbversion");
		System.out.println("dbversion: "+dbversion);
		Assert.assertNotNull(dbversion, "dbversion is null");	
	}
}