package com.clinic.core;

import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class OEL002_SyncWebDatawithTabOrg extends TestBase {
	
	@Test(priority=0)
	public void SyncWebDatawithTabOrg_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.SyncWebDatawithTabOrg;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		String lastsync="2017-01-02 22:19:31.098";
		String globalLastSync="2017-01-02 22:19:31.098";
		List<NameValuePair> userDetails = ParamGenerator.SyncWebDatawithTabOrg_Param(token, username, orgId, lastsync, globalLastSync);
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, userDetails);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=responseJson_object.getJSONObject("CompleteRecord");
		JSONArray arr = e.getJSONArray("patient");
		JSONObject obj = arr.getJSONObject(0);
		String patient_id=obj.getString("patient_id");
		System.out.println("patient_id: "+patient_id);
		Assert.assertNotNull(patient_id, "patient_id is null");	
	}
}