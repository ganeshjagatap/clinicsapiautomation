package com.clinic.core;
import java.awt.Desktop;
import java.io.File;

import org.testng.annotations.Test;

public class OpenReport {
	@Test(priority=190)
	public void openhtml() throws Exception
	{
		String htmlFilePath =System.getProperty("user.dir") +"/target/surefire-reports/html/index.html"; // path to your new file
		System.out.println("path "+htmlFilePath);
		File htmlFile = new File(htmlFilePath);
		Desktop.getDesktop().browse(htmlFile.toURI());
//		Desktop.getDesktop().open(htmlFile);
	}
}