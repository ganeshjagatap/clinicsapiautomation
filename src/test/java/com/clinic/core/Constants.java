package com.clinic.core;

public class Constants {
	
	//login.json
	public static String login;
	public static String verifyUserLoginName;
	public static String userSignUpValidate;
	public static String otpVerification;
	public static String resendSms;
	public static String changePassword;
	public static String forgotPassword;
	public static String resetPassword;
	public static String createUserAuthentication;
	public static String getUserBasicDetails;
	public static String createUser;
	public static String updateUser;
	public static String logout;
	public static String autoSuggestionUsers;
	public static String retrieveUsers;
	public static String userStatusUpdate;
	public static String getUserDetails;
	public static String userMaster;
	public static String userAssocToClinic;
	public static String getUserConsultationTabDetails;
	public static String updateUserConsultationTabDetails;
	
	
	//appointment.json
	public static String homeDoctor;
	public static String countryList;
	public static String stateMasterList;
	public static String cityMasterList;
	public static String patientSearch;
	public static String bookAppointment;
	public static String appointmentHomeDoctorList;
	public static String appointmentHomeDoctorSlotDatesList;
	public static String appointmentReasonMaster;
	public static String cancelAppointment;
	public static String rescheduleAppointment;
	public static String blockAppointmentSlot;
	public static String unblockAppointmentSlot;
	public static String blockAppointmentCalender;
	public static String patientRegistrationAlternateFlow;
	public static String patientRegistrationAlternateFlowVC;
	public static String patientPreviousVisits;
	public static String patientRegistration;
	public static String updatePatientDetails;
	public static String getMedicalHistory;
	public static String patientSearchGetAllPatient;
	public static String patientSearchAdvance = "/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/patientSearchAdvance";
	public static String patientPreviousVisitsDetail = "/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/patientPreviousVisitsDetail";
	public static String patientPreviousVisitDate = "/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/patientPreviousVisitDate";
	public static String patientQuickSearch = "/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/patientQuickSearch";
	
	
	
	//serviceMaster.json
	public static String serviceCategory;
	public static String serviceClassification;
	public static String createService;
	public static String searchClinicService;
	public static String retrieveClinicServices;
	public static String updateService;
	public static String serviceMaster;
	

	//Appointment.json
	public static String persistHomeMonitoringInfoHandler;
	public static String savePrescriptionAsTemplateHandler;
	public static String fetchPrescriptionHandler;
	
	
	public static String fetchOfflineDBversionInfo="/HPPClinicOfflineRestApp/rest/clinicOffline/fetchOfflineDBversionInfo";
	public static String SyncWebDatawithTabOrg="/HPPClinicOfflineRestApp/rest/clinicOffline/SyncWebDatawithTabOrg";
	public static String saveOfflineData="/HPPClinicOfflineRestApp/rest/clinicOffline/saveOfflineData";
	
	
	public static String BookRegistration="/HPPConsultationRestApp/rest/HPPClinicsConsultation/registrationBookConsultation";
	public static String CREATE_SESSION_PATH="/rest/session/create";
	public static String updateQualifaction="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/addUserQualification";
	public static String getUserProfileDetail="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/getUserProfileDetails";
	public static String listUserQualification="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/listUserQualification";
	public static String getMyTimings="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/getMyTimings";
	public static String listUserLocation="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/listUserLocation";
	
	
	
	
	
	
	public static String appUpdate="/HPPClinicsCARestApp/rest/HPPClinicsAppUpdate/fetchAppUpdateHandler";
	public static String autoSuggestionParameters="/HPPClinicsCARestApp/rest/clinicAdministration/autoSuggestionParameters";
	public static String retrieveSystemParams="/HPPClinicsCARestApp/rest/clinicAdministration/retrieveSystemParams";
	public static String updateSystemParam="/HPPClinicsCARestApp/rest/clinicAdministration/updateSystemParam";
	public static String getParametersMaster="/HPPClinicsCARestApp/rest/clinicAdministration/getParametersMaster";
	public static String getOperationalRoleList="/HPPClinicsCARestApp/rest/clinicAdministration/getOperationalRoleList";
	public static String getSpecialty="/HPPClinicsCARestApp/rest/clinicAdministration/getSpecialty";
	public static String searchClinicServiceGlobal="/HPPClinicsCARestApp/rest/clinicAdministration/searchClinicServiceGlobal";
	public static String searchUserGlobal="/HPPClinicsCARestApp/rest/clinicAdministration/searchUserGlobal";
	public static String autoSuggestionSpeciality="/HPPClinicsCARestApp/rest/clinicAdministration/autoSuggestionSpeciality";
	
	public static String locationMaster="/HPPClinicsCARestApp/rest/clinicAdministration/locationMaster";
	public static String createTariff="/HPPClinicsCARestApp/rest/clinicAdministration/createTariff";
	public static String UpdateTariff="/HPPClinicsCARestApp/rest/clinicAdministration/updateTariff";
	
	public static String retrieveTariffs="/HPPClinicsCARestApp/rest/clinicAdministration/retrieveTariffs";
	
	
	








	
	
	//UserProfile/HomeMonitoring
	public static String updateUserProfile="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/updateUserProfile";
	public static String listUserPublication="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/listUserPublication";
	public static String addUserPublication ="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/addUserPublication";
	public static String updateUserPublication ="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/updateUserPublication";
	public static String deleteUserPublication = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/deleteUserPublication";
	public static String addUserExperience = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/addUserExperience";
	public static String listUserExerience="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/listUserExperince";
	public static String updateUserExperience = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/updateUserExperience";
	public static String deleteUserExperience = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/deleteUserExperience";
	public static String updateUserQualification="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/updateUserQualification";
	public static String deleteUserQualification ="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/deleteUserQualification";
	public static String listDoctorTimings = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/listDoctorTimings";
	public static String getClinicConfigParam ="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/getClinicConfigParams";
	public static String deleteMyTimings = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/deleteMyTimings";
	public static String deleteDaySlotTiming = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/deleteDayslotTimings";
	public static String fetchHmPatientDeviceDataHandler = "/HPPConsultationRestApp/rest/HPPClinicsHomeMonitoring/fetchHmPatientDeviceDataHandler";
	public static String fetchHmNurseNotesHandler = "/HPPConsultationRestApp/rest/HPPClinicsHomeMonitoring/fetchHmNurseNotesHandler";
	public static String fetchHmAlertMessagesHandler = "/HPPConsultationRestApp/rest/HPPClinicsHomeMonitoring/fetchHmAlertMessagesHandler";
	public static String persistHmNurseNotesHandler = "/HPPConsultationRestApp/rest/HPPClinicsHomeMonitoring/persistHmNurseNotesHandler";
	public static String fetchVitalDtlsHandler;
	public static String persistPatientMedicalHistoryHandler;
	public static String searchAllergyHandler;
	public static String searchMedicineHandler;
	public static String searchSymptomsHandler;
	public static String searchExaminationTemplateHandler;
	public static String fetchExaminationTemplateHandler;
	public static String fetchExaminationDtlsHandler;
	public static String fetchExaminationTemplateItemsHandler;
	public static String fetchPatientReportsHandler;
	public static String searchDiagnosisHandler;
	public static String searchTestOrScanHandler;
	public static String fetchHomeMonitoringInfoHandler;
	public static String persistFollowupVisitInfoHandler;
	public static String fetchFollowupVisitInfoHandler;
	public static String persistReferralInfoHandler;
	public static String fetchReferralInfoHandler;
	public static String loadPrescriptionTemplateHandler;
	public static String fetchPatientPrescriptionDtlsHandler;
	public static String fetchDiagnosisAndPrescriptionHandler;
	public static String fetchDiagnosisHandler;
	public static String generateSummaryHandler;
	public static String uploadPatientTestOrScanReportHandler;
	public static String deletePatientTestOrScanReportHandler;
	public static String searchFrequencyHandler;
	public static String fetchPharmacyListHandler;
	public static String deletePrescriptionAsTemplateHandler;
	public static String fetchPatientInstrumentalInfoHandler;
	public static String persistPatientProvisionalInfoHandler;
    public static String fetchOrLoadAllTemplatesForUser;
    public static String fetchPatientPrescription;
    public static String printConsultationSheet;
    public static String fetchOrLoadTestOrScanTemplatesForUser;
    public static String fetchVitalsForPreviousVisits;
	public static String openConsultationDetails;
	public static String fetchPatientDtlsForVCConsultation = "/HPPConsultationRestApp/rest/HPPClinicsConsultation/fetchPatientDtlsForVCConsultation";
	public static String bookConsultation;
	public static String generateModuleWiseSummaryPDFHandler;
	public static String fetchHmPatientUploadedDataHandler = "/HPPConsultationRestApp/rest/HPPClinicsHomeMonitoring/fetchHmPatientUploadedDataHandler";
	public static String generateSummaryPDFHandler;
	public static String fetchPatientTestsOrScansInfoHandler;
	public static String persistPatientDiagnosisInfoHandler;
	
	public static String updatePrescriptionAsTemplateHandler;
	public static String addDoctorTimings="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/addDoctorTimings";
	//Clinic admin
	
	  
		
		public static String siteMaster="/HPPClinicsCARestApp/rest/clinicAdministration/siteMaster";
		public static String acceptTermCondition="/HPPClinicsCARestApp/rest/clinicAdministration/acceptTermCondition";
		
	//Billing
		
		public static String allBills="/HPPClinicsBillingRestApp/rest/clinicBilling/allBills";
		public static String calculateDiscountAndNetAmount="/HPPClinicsBillingRestApp/rest/clinicBilling/calculateDiscountAndNetAmount";
		public static String generateDailyBillingReportForOrgId="/HPPClinicsBillingRestApp/rest/clinicBilling/generateDailyBillingReportForOrgId";
		public static String populateTariff="/HPPClinicsBillingRestApp/rest/clinicBilling/populateTariff";
		public static String cancelBill="/HPPClinicsBillingRestApp/rest/clinicBilling/cancelBill";
		public static String searchService="/HPPClinicsBillingRestApp/rest/clinicBilling/searchService";
		public static String calculateTotal="/HPPClinicsBillingRestApp/rest/clinicBilling/calculateTotal";
		public static String printBill="/HPPClinicsBillingRestApp/rest/clinicBilling/printBill";
		public static String createBill="/HPPClinicsBillingRestApp/rest/clinicBilling/createBill";
		public static String generateBill="/HPPClinicsBillingRestApp/rest/clinicBilling/generateBill";
	
	
	


public static String updateDoctorTimings="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/updateDoctorTimings";
	public static String validateDigitalSignature="/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/validateDigitalSignature";
	 	
	public static String savePatientTestListHandler;
	public static String generatePrescriptionPDFHandler;
	public static String generateConsultingPrescriptionPdfHandler;
	public static String updateTestOrScanTemplate;
	public static String persistTestOrScanTemplateMaster;
	public static String updatePrescriptionTemplate;
	public static String savePrescriptionTemplate;
	public static String checkCimsInteractionsHandler;
	public static String persistPrescriptionHandler;
	public static String persistDoctorSpecifiedTestOrScanInfoHandler;
	public static String persistExaminationInfoHandler;
	public static String createOrUpdatePatientVisitHandler;
	public static String fetchPatientMedicalHistoryHandler;
	public static String persistVitalsInfoHandler;
	public static String persistPatientInstrumentalListInfoHandler;
	public static String persistPatientInstrumentalInfoHandler;
	public static String registrationBookConsultation;
	
	
	public static String persistDiagnosisAndPrescriptionHandler;
	public static String persistPrescription;
	
	public static String persistConsultationSummarySectionPrefHandler = "/HPPConsultationRestApp/rest/HPPClinicsConsultation/persistConsultationSummarySectionPrefHandler";
	public static String fetchConsultationSummarySectionPrefHandler = "/HPPConsultationRestApp/rest/HPPClinicsConsultation/fetchConsultationSummarySectionPrefHandler";
	public static String checkDuplicateUser = "/HPPClinicsCARestApp/rest/clinicAdministration/checkDuplicateUser";
	public static String fetchPatientLabTestsTrendsData = "/HPPConsultationRestApp/rest/HPPClinicsConsultation/fetchPatientLabTestsTrendsData";
	public static String fetchPatientLabTestsMasterRequest = "/HPPConsultationRestApp/rest/HPPClinicsConsultation/fetchPatientLabTestsMasterRequest";
	
	
	public static String fetchConsultationMenuDetail = "/HPPClinicsCARestApp/rest/clinicAdministration/fetchConsultationMenuDetail";
    public static String saveConsultationMenuDetail = "/HPPClinicsCARestApp/rest/clinicAdministration/saveConsultationMenuDetail";
	
    
    public static String saveMedicalCertificateTemplate = "/HPPClinicsCARestApp/rest/clinicAdministration/saveMedicalCertificateTemplate";
    public static String fetchMedicalCertificateTemplate = "/HPPClinicsCARestApp/rest/clinicAdministration/fetchMedicalCertificateTemplate";
    public static String saveMedicalCertificate = "/HPPClinicsCARestApp/rest/clinicAdministration/saveMedicalCertificate";
	public static String searchMedicalCertificates = "/HPPClinicsCARestApp/rest/clinicAdministration/searchMedicalCertificates";
	
	public static String searchManualLabTestHandler= "/HPPConsultationRestApp/rest/HPPClinicsConsultation/searchManualLabTestHandler";
	public static String getCommunicationParam= "/HPPClinicsCARestApp/rest/clinicAdministration/getCommunicationParam";
	public static String updateCommunicationParam= "/HPPClinicsCARestApp/rest/clinicAdministration/updateCommunicationParam";
	
	public static String fetchVitalParameterView= "/HPPConsultationRestApp/rest/HPPClinicsConsultation/fetchVitalParameterView";
	public static String updateVitalParameterView= "/HPPConsultationRestApp/rest/HPPClinicsConsultation/updateVitalParameterView";
	
	public static String fetchGlobalPrintSelection= "/HPPConsultationRestApp/rest/HPPClinicsConsultation/fetchGlobalPrintSelection";
	public static String updateGlobalPrintSelection= "/HPPConsultationRestApp/rest/HPPClinicsConsultation/updateGlobalPrintSelection";
	public static String fetchTestParametersHandler= "/HPPConsultationRestApp/rest/HPPClinicsConsultation/fetchTestParametersHandler";
	
	public static String fetchConsultationPdfHeaderFooterDesigner = "/HPPClinicsCARestApp/rest/clinicAdministration/fetchConsultationPdfHeaderFooterDesigner";
	public static String saveConsultationPdfHeaderFooterDesigner = "/HPPClinicsCARestApp/rest/clinicAdministration/saveConsultationPdfHeaderFooterDesigner";
	
	public static String persistMedicalKit = "/HPPConsultationRestApp/rest/HPPClinicsConsultation/2.2/persistMedicalKit";
	public static String fetchMedicalKit = "/HPPConsultationRestApp/rest/HPPClinicsConsultation/2.2/fetchMedicalKit";
	public static String fetchMedicalDetailKit = "/HPPConsultationRestApp/rest/HPPClinicsConsultation/2.2/fetchMedicalDetailKit";
	
	public static String setUserGmailRefreshToken = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/2.2/setUserGmailRefreshToken";
	public static String fetchUserGmailRefreshToken = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/2.2/fetchUserGmailRefreshToken";
	public static String updateUserGmailRefreshToken = "/HPPClinicsUserProfileRestApp/rest/clinicUserProfile/2.2/updateUserGmailRefreshToken";
	
	
	
}
