package com.clinic.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class Billing_ParamGenerator {
	public static List<NameValuePair> param = null;
	// From Abhinaya - For Billing - Start
	
	public static List<NameValuePair> Param_AllBills(String token,String userLoginName,String userId,String organisationId,String billDate,String startRow,String interval) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
	    param.add(new BasicNameValuePair("userId", userId));	
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
    	param.add(new BasicNameValuePair("billDate", billDate));
    	param.add(new BasicNameValuePair("startRow", startRow));
    	param.add(new BasicNameValuePair("interval", interval));
    	return param;

	}
	
	public static List<NameValuePair> Param_calculateDiscountAndNetAmount(String token,String userLoginName,String organisationId,String total,String discount) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
	    //param.add(new BasicNameValuePair("userId", userId));	
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
    	param.add(new BasicNameValuePair("total", total));
    	param.add(new BasicNameValuePair("discount", discount));
    	return param;

	}
	
	public static List<NameValuePair> Param_generateDailyBillingReportForOrgId(String token,String userLoginName,String organisationId,String billDate) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
	    param.add(new BasicNameValuePair("organisationId", organisationId));	
    	param.add(new BasicNameValuePair("billDate", billDate));
    	return param;

	}	
	
	public static List<NameValuePair> Param_populateTariff(String token,String userLoginName,String organisationId,String locationId,String serviceId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
    	param.add(new BasicNameValuePair("locationId", locationId));
    	param.add(new BasicNameValuePair("serviceId", serviceId));
    	return param;

	}
	
	public static List<NameValuePair> Param_cancelBill(String token,String userLoginName,String userId,String organisationId,String siteId,String locationId,String billNumber,String billHeaderId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
	    param.add(new BasicNameValuePair("userId", userId));	
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
     	param.add(new BasicNameValuePair("siteId", siteId));	
     	param.add(new BasicNameValuePair("locationId", locationId));	
     	param.add(new BasicNameValuePair("billNumber", billNumber));
    	param.add(new BasicNameValuePair("billHeaderId", billHeaderId));
    	return param;

	}
	
	public static List<NameValuePair> Param_searchService(String token,String userLoginName,String organisationId,String searchText) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));	
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
    	param.add(new BasicNameValuePair("searchText", searchText));
    	return param;
	}
	
	public static List<NameValuePair> Param_printBill(String token,String userLoginName,String userId,String organisationId,String siteId,String locationId,String billNumber,String billHeaderId,String patientName,String uhid,String age,String gender) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
	    param.add(new BasicNameValuePair("userId", userId));	
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
     	param.add(new BasicNameValuePair("siteId", siteId));	
     	param.add(new BasicNameValuePair("locationId", locationId));	
     	param.add(new BasicNameValuePair("billNumber", billNumber));
    	param.add(new BasicNameValuePair("billHeaderId", billHeaderId));
     	param.add(new BasicNameValuePair("patientName", patientName));
    	param.add(new BasicNameValuePair("uhid", uhid));
    	param.add(new BasicNameValuePair("age", age));
    	param.add(new BasicNameValuePair("gender", gender));
    	return param;

	}
	// Billing - param + JSON


	public static List<NameValuePair> Param_createBill(String token,String userLoginName,String userId,String organisationId,String siteId,String locationId,String total,String applicableDiscount,String patientName,String patientId,String uhid,String age,String gender,String discountAmount,String modeOfPayment,String netPayable,String doctorId,String doctorName) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
	    param.add(new BasicNameValuePair("userId", userId));	
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
     	param.add(new BasicNameValuePair("siteId", siteId));	
     	param.add(new BasicNameValuePair("locationId", locationId));	
     	param.add(new BasicNameValuePair("total", total));
    	param.add(new BasicNameValuePair("applicableDiscount", applicableDiscount));
     	param.add(new BasicNameValuePair("patientName", patientName));
     	param.add(new BasicNameValuePair("patientId", patientId));
    	param.add(new BasicNameValuePair("uhid", uhid));
    	param.add(new BasicNameValuePair("age", age));
    	param.add(new BasicNameValuePair("gender", gender));
    	param.add(new BasicNameValuePair("discountAmount", discountAmount));
    	param.add(new BasicNameValuePair("modeOfPayment", modeOfPayment));
    	param.add(new BasicNameValuePair("netPayable", netPayable));
    	param.add(new BasicNameValuePair("doctorId", doctorId));
    	param.add(new BasicNameValuePair("doctorName", doctorName));
    	return param;

	}


	public static JSONObject JSON_createBill(String  serviceName, String serviceId, String tariffAmount) {
		JSONObject servicesJSON = new JSONObject();
		JSONObject serviceJSON = new JSONObject();
        JSONArray  serviceJARR  = new JSONArray();
		JSONObject serviceDetailsJSON  = new JSONObject();

		serviceDetailsJSON.put("serviceName", serviceName);
		serviceDetailsJSON.put("serviceId", serviceId);
		serviceDetailsJSON.put("tariffAmount", tariffAmount);

        serviceJARR.put(serviceDetailsJSON);
        
		serviceJSON.put("service", serviceJARR);
		servicesJSON.put("services", serviceJSON);
		System.out.println("req:"+ servicesJSON.toString());
		return servicesJSON;
	}
	

	public static List<NameValuePair> Param_generateBill(String token,String userLoginName,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
    	return param;

	}
	
	public static JSONObject JSON_generateBill( String  mobileNo, String uhid, String patientName,String patientId) {

		JSONObject patient = new JSONObject();

		//JSONArray patientDetailsArray = new JSONArray();
		JSONObject patientDetails  = new JSONObject();
		patientDetails .put("contactNumber", mobileNo);
		patientDetails .put("uhid", uhid);
		patientDetails .put("patientName", patientName);
		patientDetails .put("patientId", patientId);
		//accountDetailsArray.put(patientDetailsArray );

		patient.put("patient", patientDetails );
		System.out.println("req:" + patient.toString());
		return patient;
	}
	
	


	public static List<NameValuePair> Param_calculateTotal(String token,String userLoginName,String userId,String organisationId) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
	    param.add(new BasicNameValuePair("userLoginName", userLoginName));
	    param.add(new BasicNameValuePair("userId", userId));	
     	param.add(new BasicNameValuePair("organisationId", organisationId));	
     	
     	return param;

	}
	
	public static JSONObject JSON_calculateTotal( String serviceName,String tariffAmount,String serviceName2,String tariffAmount2) {

		JSONObject servicesJSON = new JSONObject();
		JSONObject serviceJSON = new JSONObject();
        JSONArray  serviceJARR  = new JSONArray();
		JSONObject serviceDetailsJSON  = new JSONObject();

		serviceDetailsJSON.put("serviceName", serviceName);
		serviceDetailsJSON.put("tariffAmount", tariffAmount);

        serviceJARR.put(serviceDetailsJSON);
		serviceDetailsJSON.put("serviceName", serviceName2);
		serviceDetailsJSON.put("tariffAmount", tariffAmount2);

        serviceJARR.put(serviceDetailsJSON);
        
		serviceJSON.put("service", serviceJARR);
		servicesJSON.put("services", serviceJSON);
		
		return servicesJSON;

	}
	
	//Billing - end
}
