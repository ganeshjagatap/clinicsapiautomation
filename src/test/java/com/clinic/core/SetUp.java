package com.clinic.core;

import java.io.FileInputStream;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SetUp {
	private FileInputStream configFile = null;
	public static Properties testConfigFile = null;
	

	//*************************************************************************************************************************
		//Generic function name :SetUp
		//Description :Setting up the desired capabilities
		//Author: Ganesh Jagatap
		//
		//*************************************************************************************************************************	
	
	public SetUp() {
		try {
			configFile = new FileInputStream(".//config.properties");
			testConfigFile = new Properties();
			testConfigFile.load(configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
