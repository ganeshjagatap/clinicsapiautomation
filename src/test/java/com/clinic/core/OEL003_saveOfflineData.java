package com.clinic.core;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.clinic.core.Constants;
import com.clinic.core.Envirnonment;
import com.clinic.core.ParamGenerator;
import com.clinic.core.RequestUtil;
import com.clinic.core.TestBase;

public class OEL003_saveOfflineData extends TestBase {
	
	@Test(priority=0)
	public void saveOfflineData_SuccessUsecase() throws Exception{	
		mailUrl = Envirnonment.env + Constants.saveOfflineData;
		System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
		List<NameValuePair> userDetails = ParamGenerator.saveOfflineData_Param(username, token, orgId);
		JSONObject param=ParamGenerator.getJSONcreateService("1527", "deiva n", "Y", "1", "2016-09-22 11:13:39", "null", "null", "109", "null", "0", "null", "1527", "Mr", "Male", "null", "2009-9-22", "null", "7", "9999999999", "null", "null", "null","null", "null", "null", "null", "null", "N","N","null","","5527","null","null","null","null","null","null","null","null","null","null","null","null","2016-09-20","17:58:51","0","null","null","null","null","null","null","null","null","null","null","null","null","null","CONSULTATION","109","552","2016-09-20 17:58:51","110","109","1160","0","null","null","551","2016-09-23 19:05:24","551","null","null","2016-09-23","5334","C","2016-09-23","5334","2297","null","DR_SPECIFIC","null","poiiuuy","2016-09-22 15:15:13","poiiuuy","2016-09-22","5138","null","null","2016-09-22 15:16:27","654","1116","ghxngc","2016-09-22 11:31:23","5043","null","null","360","2016-09-20","17:58:51","BMI","23.23","0","5037","Comments","tfv","552","10","2016-09-20","17:58:51","552","","","cjgvgj","tab","1","1","1","Mouth Dissolving","555","days","2016-10-12","N","N","N","N","N","N","1236","instruction","medicine_type","100","","555Days","","","2016-09-20","null","Diag Services","49","4524","null","Provisional","362");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, userDetails, param);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		JSONObject e=responseJson_object.getJSONObject("success");
		//JSONArray arr = e.getJSONArray("patient");
		//JSONObject obj = arr.getJSONObject(0);
		String message=e.getString("message");
		System.out.println("message: "+message);
		Assert.assertNotNull(message, "message is null");	
	}
}